package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;

import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.util.SKU_DSDetails;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import in.valtech.westcon.QuotingPages.QuotingLoginPage;

public class TC05_Quoting_DealId extends BaseTest {

	//QuotingAddProductsPage add = new QuotingAddProductsPage(driver);
	//QuotingQuoteDetailpage am= new QuotingQuoteDetailpage(driver);
	SKU_DSDetails SKU = new SKU_DSDetails();
	String SKUCount = null;
	String vendorRefNo;
	static QuotingLoginPage amLoginPage;
	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */	
	@Test(description = "Step 1: Navigate to Add Product Page by Calling TC01 TC02", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");

	}

	@Test(description = "Step 2: Selecting Vendor from the drop down displayed", priority = 2)
	public void Step02_SelectingVendor() throws Exception {

		QuotingAddProductsPage add = new QuotingAddProductsPage(driver);

		add.VendorRefNo(driver);
		add.VendorRefNoTabAssertion(driver);
		add.VendorText(driver);	

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {
			add.VendorDDL(driver , SKUdataSetList.get(i).getVendorName()); 
		}	

		log.info("Successfully Selected the vendor from the drop down displayed /n");
		Reporter.log("<p>selected vendor from the drop down displayed");
			
	}

	@Test(description = "Step 3: entering DealID and submitting", priority = 3)
	public void Step03_EnterDealIDAndSubmit() throws Exception {

		QuotingAddProductsPage add = new QuotingAddProductsPage(driver);

		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			vendorRefNo = SKUdataSetList.get(i).getVendorRefNo();
		}
		add.VendorRefNoTextBox(driver, vendorRefNo); 
		Thread.sleep(2000);
		add.VendorSubmitButton(driver);		

		log.info("Successfully entered deal id and submitted \n");
		Reporter.log("<p>Successfully entered deal id and submitted");
	}

	@Test(description = "Step 4: Verifying the vendor Ref.No displayed", priority = 4)
	public void Step04_VendorRefnoVerify() throws Exception {

		QuotingQuoteDetailpage am= new QuotingQuoteDetailpage(driver);
		am.VendorFieldVerifyAssertion(driver);
		am.vendorValueVerify(driver);
		log.info("Selected the vendor");
		Reporter.log("<p>Verified the vendor's Ref.No -- "+ driver.getTitle());	
	}

	@Test(description = "Step 5: Click on Requester info tab", priority = 5)
	public void Step05_ClickonRequesterinfotab() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 200);
		amQuoteDetailsPage.clickOnRequesterinfotab();

		log.info("Successfully Click on Requester info tab\n");
		Reporter.log("<p>Successfully Click on Requester info tab");
	}

	@Test(description = "Step 6: Verificaiton of Quote file link", priority = 6)
	public void Step06_VerifyQuoteFileLnk() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.QuoteFileLnkVerify(driver);
		GUIFunctions.pageScrollUP(driver, 200);
		CustomFun.waitForPageLoaded(driver);
		log.info("Quote file link verified\n");
		Reporter.log("<p>Quote file link verified");

	}
}

