package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC78_Quoting_ReplaceOldSBA extends BaseTest {

	String manufacturerListPriceBeforeSBA;
	String purchasePriceBeforeSBA;
	String vendorDiscountBeforeSBA;
	String SKU;
	String sbaRefNumber;
	String manufacturerListPriceAfterSBA;
	String purchasePriceAfterSBA;
	String vendorDiscountAfterSBA;
	String sbaDiscount;
	String[] sbaDiscountvalue;

	String[] skuNumber;
	String[] actualMListBeforeSBA;
	String[] actualVDiscountBeforeSBA;
	String[] actualWestCostBeforeSBA;

	String[] actualMListAfterSBA;
	String[] actualVDiscountAfterSBA;
	String[] actualWestCostAfterSBA;
	
	String[] actualMListAfterRemovingSBA;
	String[] actualVDiscountAfterRemovingSBA;
	String[] actualWestCostAfterRemovingSBA;

	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step1 : Call TC06 which selects the End User and all mandatory fields filled", priority = 1)
	public void Step01_NavigateToTheCreateQuotePageWithAllMandatoryInfo() throws Exception {

		//Execute TC06 to navigate to Create Quote page with End User linked and all mandatory fields filled
		// Create Quote page is displayed with End user selected and all mandatory fields filled

		log.info("Create Quote page is displayed with End user selected and all mandatory fields filled");
		Reporter.log("<p>Create Quote page is displayed with End user selected and all mandatory fields filled-- "
				+ driver.getTitle());			
	}

	/**
	 * Check the pricing condition for Manual / Auto SBA
	 * @throws Exception 
	 */
	@Test(description = "Step7 : Check the pricing condition for Manual / Auto SBA", priority = 7)
	public void Step07_CheckPricingConditions() throws Exception {
		//Checking for Pricing Conditions

		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickPricingTab();

		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL") && SKUdataSetList.get(i).getSbaType().equals("ZSD0")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto");
			}
			
			else if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText()
						, "None");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto","Improper pricing condition");
			}

		}
		log.info("Verified pricing condition\n");
		Reporter.log("<p>Verified pricing condition");
	}

	/**
	 * Save the values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step8 : Save the values like vendor discount, list price and purchase price", priority = 8)
	public void Step08_SaveTableValues() {
		//Save the table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListBeforeSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountBeforeSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostBeforeSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceBeforeSBA = "+actualMListBeforeSBA[1]);
		System.out.println("vendorDiscountBeforeSBA = "+actualVDiscountBeforeSBA[0]);
		System.out.println("purchasePriceBeforeSBA = "+actualWestCostBeforeSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");
	}

	/**
	 * Click on SBA tab
	 */
	@Test(description = "Step9 : Click on SBA tab", priority = 9)
	public void Step09_ClickSBATab() {
		//Click on SBA tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBATab();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaTabSearchLink_XPATH"))).getText().trim(),
				TextProp.getProperty("SBATabData"));

		log.info("Successfully clicked on SBA tab and SBA tab is opened");
		Reporter.log("<p>Successfully clicked on SBA tab and SBA tab is opened");
	}

	/**
	 * Click on SBA search link
	 */
	@Test(description = "Step10 : Click on SBA search link", priority = 10)
	public void Step10_ClickSBASearchLink() {
		//Click on SBA search link
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBASearchLink();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaSearchOverlayHeader_XPATH"))).getText().trim(), 
				TextProp.getProperty("SBASearchOverlayHeader"));

		log.info("Successfully clicked on SBA search link and SBA search overlay is opened");
		Reporter.log("<p>Successfully clicked on SBA search link and SBA search overlay is opened");
	}

	/**
	 * Enter part number and Click on Search button
	 * @throws InterruptedException 
	 */
	@Test(description = "Step11 : Enter part number and Search SBA", priority = 11)
	public void Step11_EnterAndSearchSBA() throws InterruptedException {
		//Enter Part NumbersbaTable_XPATH
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			SKU = SKUdataSetList.get(i).getSKU();
		}
		quoteDetails.enterPartNumber(SKU);
		log.info("Successfully entered part number");
		Reporter.log("<p>Successfully entered part number");

		//Search for SBA
		quoteDetails.clickSearchButton();

		log.info("Successfully clicked on search button and SBA table is displayed");
		Reporter.log("<p>Successfully clicked on search button and SBA table is displayed");
	}

	/**
	 * Select the SBA
	 * @throws InterruptedException 
	 */
	@Test(description = "Step12 : Select SBA", priority = 12)
	public void Step12_SelectSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Select the SBA displayed in the table
		Thread.sleep(5000);
		quoteDetails.selectSBA();
		log.info("Successfully selected SBA in SBA table");
		Reporter.log("<p>Successfully selected SBA in SBA table");
	}

	/**
	 * Apply the selected SBA
	 */
	@Test(description = "Step13 : Apply the selected SBA and verify in SBA tab", priority = 13)
	public void Step13_ClickApplyButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Select the SBA displayed in the table

		quoteDetails.applySBA();
		log.info("Successfully applied the SBA");
		Reporter.log("<p>Successfully applied the SBA");
		CustomFun.waitObjectToLoad(driver, By.xpath(ObjRepoProp
				.getProperty("appliedSBAInTable_XPATH")), 60);

		//Verify the applied SBA in SBA table
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			sbaRefNumber = SKUdataSetList.get(i).getSbaRefNumber();
		}

		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("appliedSBAInTable_XPATH"))).getText(), sbaRefNumber);

		log.info("Successfully verified the applied SBA in SBA table");
		Reporter.log("<p>Successfully verified the applied SBA in SBA table");

		//Verify the applied SBA at header level in SBA tab

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("appliedSBAHeaderLevel_XPATH"))).getText(),
				sbaRefNumber);
		log.info("Successfully verified the applied SBA at header level in SBA tab");
		Reporter.log("<p>Successfully verified the applied SBA at header level in SBA tab");

	}

	/**
	 * Click on applied SBA and verify the overlay displayed
	 * @throws InterruptedException 
	 */
	@Test(description = "Step14 : Click on applied SBA and verify the overlay displayed", priority = 14)
	public void Step14_ClickAppliedSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on applied SBA

		quoteDetails.clickAppliedManualSBA();
		log.info("Successfully clicked on applied SBA");
		Reporter.log("<p>Successfully clicked on applied SBA");

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlay_XPATH"))).getText()
				, TextProp.getProperty("SBADetailsOverlay"));

		log.info("Successfully verified the SBA details ovarlay");
		Reporter.log("<p>Successfully verified the SBA details ovarlay");
	}

	/**
	 * Save the discount
	 */
	@Test(description = "Step15 : Save the SBA discount", priority = 15)
	public void Step15_SaveSBADiscount() {
		//Save SBA Discount
		sbaDiscount = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsoverlayDiscount_XPATH")))
				.getText();

		sbaDiscountvalue = sbaDiscount.split("-");
		//sbaDiscount = sbaDiscount.substring(1,sbaDiscount.length()-1);
		System.out.println("sbaDiscountvalue = "+sbaDiscountvalue[1]);

		log.info("Successfully saved SBA discount");
		Reporter.log("<p>Successfully saved SBA discount");
	}

	/**
	 * Click on Cancel button
	 */
	@Test(description = "Step16 : Click Cancel button", priority = 16)
	public void Step16_ClickCancelButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Cancel button
		quoteDetails.clickCancelButton();

		log.info("Successfully closed the overlay");
		Reporter.log("<p>Successfully closed the overlay");
	}

	/**
	 * Click on Pricing tab and verify updated pricing conditions
	 * @throws InterruptedException 
	 */
	@Test(description = "Step17 : Click on Pricing tab and verify updated pricing conditions", priority = 17)
	public void Step17_ClickPricingTabAndVerifyPricingConditions() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");

		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingConditionColumn_XPATH"))));

		//Verify updated pricing conditions
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Manual");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto + Manual");
			}
		}
		log.info("Create Quote page with pricing condition as "+driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText());
		Reporter.log("<p>Create Quote page with pricing condition as -- "
				+ driver.findElement(By.xpath(ObjRepoProp
						.getProperty("pricingConditionColumn_XPATH"))).getText());
	}

	/**
	 * Save the updated values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step18 : Save the updated values like vendor discount, list price and purchase price", priority = 18)
	public void Step18_SaveTableValues() {
		//Save the updated table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceBeforeSBA = "+actualMListAfterSBA[1]);
		System.out.println("vendorDiscountBeforeSBA = "+actualVDiscountAfterSBA[0]);
		System.out.println("purchasePriceBeforeSBA = "+actualWestCostAfterSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");

	}

	/**
	 * Compare vendor discount, list price and purchase price values before and after applying SBA
	 */
	@Test(description = "Step19 : Compare vendor discount, list price and purchase price values before and after applying SBA", priority = 19)
	public void Step19_CompareTableValues() {	

		//Compare the values before and after applying SBA
		Assert.assertEquals(Float.parseFloat(actualMListBeforeSBA[1].replace(",", "")), Float.parseFloat(actualMListAfterSBA[1].replace(",", "")));
		Assert.assertEquals(Float.parseFloat(actualVDiscountAfterSBA[0]), Float.parseFloat(actualVDiscountBeforeSBA[0]) + Float.parseFloat(sbaDiscountvalue[1]));
		Assert.assertEquals(Float.parseFloat(actualWestCostAfterSBA[1].replace(",", "")), Float.parseFloat(actualMListBeforeSBA[1].replace(",", ""))-((Float.parseFloat(actualMListBeforeSBA[1].replace(",", "")) * Float.parseFloat(actualVDiscountAfterSBA[0]))/100));

		log.info("Successfully compared the table values before and after applying SBA");
		Reporter.log("<p>Successfully compared the table values before and after applying SBA");
	}

	@Test(description = "Step20 : Click on SBA tab", priority = 20)
	public void Step20_ClickSBATab() {
		//Click on SBA tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBATab();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaTabSearchLink_XPATH"))).getText().trim(),
				TextProp.getProperty("SBATabData"));

		log.info("Successfully clicked on SBA tab and SBA tab is opened");
		Reporter.log("<p>Successfully clicked on SBA tab and SBA tab is opened");
	}
	
	/**
	 * Click on applied SBA and verify the overlay displayed
	 * @throws InterruptedException 
	 */
	@Test(description = "Step21 : Click on applied SBA and verify the overlay displayed", priority = 21)
	public void Step21_ClickAppliedSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on applied SBA

		quoteDetails.clickAppliedManualSBA();
		log.info("Successfully clicked on applied SBA");
		Reporter.log("<p>Successfully clicked on applied SBA");

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlay_XPATH"))).getText()
				, TextProp.getProperty("SBADetailsOverlay"));

		log.info("Successfully verified the SBA details ovarlay");
		Reporter.log("<p>Successfully verified the SBA details ovarlay");
	}
	
	@Test(description = "Step22 : Click on Remove button", priority = 22)
	public void Step22_ClickRemoveButton() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		
		//Click on Remove Button
		quoteDetails.removeSBA();
		
		log.info("Successfully clicked on Remove button");
		Reporter.log("<p>Successfully clicked on Remove button");
	}
	
	/**
	 * Click on Pricing tab and verify updated pricing conditions
	 * @throws InterruptedException 
	 */
	@Test(description = "Step23 : Click on Pricing tab and verify updated pricing conditions", priority = 23)
	public void Step23_ClickPricingTabAndVerifyPricingConditions() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");

		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingConditionColumnNone_XPATH"))));

		//Verify updated pricing conditions
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL") && SKUdataSetList.get(i).getSbaType().equals("ZSD0")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto");
			}
			
			else if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText()
						, "None");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto","Improper pricing condition");
			}

		}
		log.info("Create Quote page with pricing condition as "+driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText());
		Reporter.log("<p>Create Quote page with pricing condition as -- "
				+ driver.findElement(By.xpath(ObjRepoProp
						.getProperty("pricingConditionColumnNone_XPATH"))).getText());
	}

	/**
	 * Save the updated values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step24 : Save the updated values like vendor discount, list price and purchase price", priority = 24)
	public void Step24_SaveTableValuesAfterRemovingSBA() {
		//Save the updated table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterRemovingSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountAfterRemovingSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostAfterRemovingSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceAfterRemovingSBA = "+actualMListAfterRemovingSBA[1]);
		System.out.println("vendorDiscountAfterRemovingSBA = "+actualVDiscountAfterRemovingSBA[0]);
		System.out.println("purchasePriceAfterRemovingSBA = "+actualWestCostAfterRemovingSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");

	}
	
	/**
	 * Compare vendor discount, list price and purchase price values before and after removing SBA
	 */
	@Test(description = "Step25 : Compare vendor discount, list price and purchase price values before and after applying SBA", priority = 25)
	public void Step25_CompareTableValues() {	

		//Compare the values after removing the applied SBA
		Assert.assertEquals(Float.parseFloat(actualMListBeforeSBA[1].replace(",", "")), Float.parseFloat(actualMListAfterRemovingSBA[1].replace(",", "")));
		Assert.assertEquals(Float.parseFloat(actualVDiscountBeforeSBA[0]), Float.parseFloat(actualVDiscountAfterRemovingSBA[0]));
		Assert.assertEquals(Float.parseFloat(actualWestCostBeforeSBA[1].replace(",", "")), Float.parseFloat(actualWestCostAfterRemovingSBA[1].replace(",", "")));

		log.info("Successfully compared the table values before and after removing SBA");
		Reporter.log("<p>Successfully compared the table values before and after removing SBA");
	}
	
	@Test(description = "Step26 : Click on SBA tab", priority = 26)
	public void Step26_ClickSBATab() {
		//Click on SBA tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBATab();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaTabSearchLink_XPATH"))).getText().trim(),
				TextProp.getProperty("SBATabData"));

		log.info("Successfully clicked on SBA tab and SBA tab is opened");
		Reporter.log("<p>Successfully clicked on SBA tab and SBA tab is opened");
	}
	
	@Test(description = "Step27 : Enter valid SBA ID", priority = 27)
	public void Step27_EnterSBAID() {
		//Enter valid SBA ID
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.enterSBAID(TextProp.getProperty("SBAID"));
		
		log.info("Successfully entered SBA ID");
		Reporter.log("<p>Successfully entered SBA ID");
	}
	
	@Test(description = "Step28 : Click on Apply button", priority = 28)
	public void Step28_clickSBAApplyButton() {
		//Click on Apply button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBAApplyButton();
		
		log.info("Successfully Clicked on Apply Button");
		Reporter.log("<p>Successfully Clicked on Apply Button");
	}
	
	/**
	 * Click on applied SBA and verify the overlay displayed
	 * @throws InterruptedException 
	 */
	@Test(description = "Step29 : Click on applied SBA and verify the overlay displayed", priority = 29)
	public void Step29_ClickAppliedSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on applied SBA

		quoteDetails.clickAppliedManualSBA();
		log.info("Successfully clicked on applied SBA");
		Reporter.log("<p>Successfully clicked on applied SBA");

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlay_XPATH"))).getText()
				, TextProp.getProperty("SBADetailsOverlay"));

		log.info("Successfully verified the SBA details ovarlay");
		Reporter.log("<p>Successfully verified the SBA details ovarlay");
	}

	/**
	 * Save the discount
	 */
	@Test(description = "Step30 : Save the SBA discount", priority = 30)
	public void Step30_SaveSBADiscount() {
		//Save SBA Discount
		sbaDiscount = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsoverlayDiscount_XPATH")))
				.getText();

		sbaDiscountvalue = sbaDiscount.split("-");
		//sbaDiscount = sbaDiscount.substring(1,sbaDiscount.length()-1);
		System.out.println("sbaDiscountvalue = "+sbaDiscountvalue[1]);

		log.info("Successfully saved SBA discount");
		Reporter.log("<p>Successfully saved SBA discount");
	}

	/**
	 * Click on Cancel button
	 */
	@Test(description = "Step31 : Click Cancel button", priority = 31)
	public void Step31_ClickCancelButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Cancel button
		quoteDetails.clickCancelButton();

		log.info("Successfully closed the overlay");
		Reporter.log("<p>Successfully closed the overlay");
	}

	/**
	 * Click on Pricing tab and verify updated pricing conditions
	 * @throws InterruptedException 
	 */
	@Test(description = "Step32 : Click on Pricing tab and verify updated pricing conditions", priority = 32)
	public void Step32_ClickPricingTabAndVerifyPricingConditions() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");

		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingConditionColumn_XPATH"))));

		//Verify updated pricing conditions
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Manual");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto + Manual");
			}
		}
		log.info("Create Quote page with pricing condition as "+driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText());
		Reporter.log("<p>Create Quote page with pricing condition as -- "
				+ driver.findElement(By.xpath(ObjRepoProp
						.getProperty("pricingConditionColumn_XPATH"))).getText());
	}

	/**
	 * Save the updated values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step33 : Save the updated values like vendor discount, list price and purchase price", priority = 33)
	public void Step33_SaveTableValues() {
		//Save the updated table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceBeforeSBA = "+actualMListAfterSBA[1]);
		System.out.println("vendorDiscountBeforeSBA = "+actualVDiscountAfterSBA[0]);
		System.out.println("purchasePriceBeforeSBA = "+actualWestCostAfterSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");

	}

	/**
	 * Compare vendor discount, list price and purchase price values before and after applying SBA
	 */
	@Test(description = "Step34 : Compare vendor discount, list price and purchase price values before and after applying SBA", priority = 34)
	public void Step34_CompareTableValues() {	

		//Compare the values before and after applying SBA
		Assert.assertEquals(Float.parseFloat(actualMListBeforeSBA[1].replace(",", "")), Float.parseFloat(actualMListAfterSBA[1].replace(",", "")));
		Assert.assertEquals(Float.parseFloat(actualVDiscountAfterSBA[0]), Float.parseFloat(actualVDiscountBeforeSBA[0]) + Float.parseFloat(sbaDiscountvalue[1]));
		Assert.assertEquals(Float.parseFloat(actualWestCostAfterSBA[1].replace(",", "")), Float.parseFloat(actualMListBeforeSBA[1].replace(",", ""))-((Float.parseFloat(actualMListBeforeSBA[1].replace(",", "")) * Float.parseFloat(actualVDiscountAfterSBA[0]))/100));

		log.info("Successfully compared the table values before and after applying SBA");
		Reporter.log("<p>Successfully compared the table values before and after applying SBA");
	}
	
}
