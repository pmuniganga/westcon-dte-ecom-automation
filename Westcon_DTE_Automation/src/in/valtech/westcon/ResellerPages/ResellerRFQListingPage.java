package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

public class ResellerRFQListingPage {

	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller self service page Constructor
	public ResellerRFQListingPage(WebDriver driver) throws InterruptedException {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right Reseller self service Page.

		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerRFQListingPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not selfservice page");
		}
	}

	By resellerRFQListingPageSearchInput_XPATH=By.xpath(ObjRepoProp.getProperty("resellerRFQListingPageSearchInput_XPATH"));
	By resellerRFQListingTablePlusIcon_XPATH=By.xpath(ObjRepoProp.getProperty("resellerRFQListingTablePlusIcon_XPATH"));

	/** This function will search for the RFQ saved 
	 * 
	 */

	public ResellerRFQListingPage searchForTheRFQSavedAndHitEnter(String RFQ) throws Exception
	{
		CustomFun.waitForPageLoaded(driver);
		GUIFunctions.typeTxtboxValue(driver, resellerRFQListingPageSearchInput_XPATH, RFQ);

		driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerRFQListingPageSearchInput_XPATH"))).sendKeys(Keys.ENTER);

		CustomFun.waitForPageLoaded(driver);
		Thread.sleep(5000);
		return new ResellerRFQListingPage(driver);
	}

	public ResellerRFQListingPage searchForTheRFQinAM() throws Exception

	{
		BaseTest.SearchRFQID=driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerRFQListingPageRFQIDInSearchTable_XPATH"))).getText();

		CustomFun.waitForPageLoaded(driver);
		Thread.sleep(2000);
		return new ResellerRFQListingPage(driver);
	}

	/***
	 * This function will click on '+' in RFQ listing table 
	 */

	public ResellerRFQListingPage clickOnPlusIcon() throws Exception
	{


		GUIFunctions.clickElement(driver, resellerRFQListingTablePlusIcon_XPATH, " + icon click");

		return new ResellerRFQListingPage(driver);
	}

}
