package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;

import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC87_Quoting_EditServicePeriod extends BaseTest {

	String SKU;

	String[] skuNumber;
	String[] actualMList;
	String[] actualVDiscount;
	String[] actualWestCost;
	String servicePeriod;
	String salesUnit;
	float mListPricePerUnit;
	float mListAfterServicePeriodUpdation;
	String[] actualMListAfterServicePeriodUpdation;
	String[] actualWestCostAfterServicePeriodUpdation;
	
	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}
	
	/**
	 * Save the values like vendor discount, list price, purchase price, Service Period and Sales Unit
	 */
	@Test(description = "Step2 : Save the values like vendor discount, list price, purchase price, Service Period and Sales Unit", priority = 2)
	public void Step02_SaveTableValues() {
		//Save the table values like vendor discount, list price, purchase price, Service Period and Sales Unit

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMList = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscount = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCost = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
				salesUnit = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='durationUnit']")).getText();
				servicePeriod = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='durationUnit']/preceding-sibling::td[1]")).getText();
			}
		}

		System.out.println("manufacturerListPrice = "+actualMList[1]);
		System.out.println("vendorDiscount = "+actualVDiscount[0]);
		System.out.println("purchasePrice = "+actualWestCost[1]);
		System.out.println("servicePeriod = "+servicePeriod);
		System.out.println("salesUnit = "+salesUnit);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");
	}

	/**
	 * Change Service Period
	 */
	@Test(description = "Step3 : Change Service Period", priority = 3)
	public void Step03_ChangeServicePeriod() throws Exception {
		//Change Service Period
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterServicePeriod(TextProp.getProperty("alteredServicePeriod"));
		
		log.info("Successfully altered Service Period");
		Reporter.log("<p>Successfully altered Service Period");
	}
	
	@Test(description = "Step4 : Verify Manufacturer List Price updation and accordingly purchase price calculation", priority = 4)
	public void Step04_VerifyPrices() {
		//Verify Manufacturer List Price updation
		
		By manufacturerListPrice = By.xpath(ObjRepoProp.getProperty("manufacturerListPrice_XPATH"));
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(manufacturerListPrice));
				
		mListPricePerUnit = Float.parseFloat(actualMList[1].replace(",", "")) / Float.parseFloat(servicePeriod);
		
		mListAfterServicePeriodUpdation = mListPricePerUnit * Float.parseFloat(TextProp.getProperty("alteredServicePeriod"));
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("manufacturerListPrice_XPATH"))));
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingTab_XPATH"))));
		
		
		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterServicePeriodUpdation = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[contains(@class,'htNumeric')][1]")).getText().split(" ");
				actualWestCostAfterServicePeriodUpdation = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[contains(@class,'htNumeric')][3]")).getText().split(" ");
				
				Assert.assertEquals(Double.parseDouble(actualMListAfterServicePeriodUpdation[1].replace(",", "")), (double)Math.round(mListAfterServicePeriodUpdation));
				Assert.assertEquals(Double.parseDouble(actualWestCostAfterServicePeriodUpdation[1].replace(",", "")), (double)Math.round(Float.parseFloat(actualMListAfterServicePeriodUpdation[1].replace(",","")) * (1 - (Float.parseFloat(actualVDiscount[0])/100))));
			}
		}
		
		log.info("Successfully verified List Price updation and purchase price calculation");
		Reporter.log("<p>Successfully verified List Price updation and purchase price calculation");
	}
}

