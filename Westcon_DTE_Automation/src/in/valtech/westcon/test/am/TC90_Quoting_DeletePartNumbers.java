package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;

import java.util.List;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;

import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC90_Quoting_DeletePartNumbers extends BaseTest {

	String SKU;

	String[] skuNumber;
	String[] skuQty;
	int count;
	int countAfterDeletion;
	


	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	@Test(description = "Step 2 : Count the number of products available in the grid", priority = 2)
	public void Step02_CountNumberOfProducts() {
		//Count the number of products added to the grid
		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");
		}
		
		count = skuNumber.length;
		System.out.println("Count = "+count);
		
		log.info("Successfully counted the number of products added to the grid\n");
		Reporter.log("<p>Successfully counted the number of products added to the grid");
	}
	
	@Test(description = "Step 3 : Delete (n/2) part number from the grid", priority = 3)
	public void Step03_DeletePartNumbers() throws Exception {
		//Delete the part numbers
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.selectSomeCheckbox(count/2);
		
		By pricingGridSpinner = By.xpath(ObjRepoProp.getProperty("pricingGridSpinner_XPATH"));
		
		quoteDetails.deletePartNumbers(driver);
		CustomFun.waitObjectToInvisible(driver, pricingGridSpinner, 60);
		
		log.info("Successfully deleted (n/2) products from the grid\n");
		Reporter.log("<p>Successfully deleted (n/2) products from the grid");
	}
	
	@Test(description = "Step 4 : Count the number of part numbers available after deletion" , priority = 4)
	public void Step04_CountPartNumbersAfterDeletion() {
		//Count number of products after deletion
		 List<WebElement> ele = driver.findElements(By.xpath(ObjRepoProp.getProperty("fetchingNumberOfProducts_XPATH")));
		 countAfterDeletion = ele.size();
		 
		 System.out.println("Count after deletion = "+countAfterDeletion);
		
		log.info("Successfully counted number of products in the grid\n");
		Reporter.log("<p>Successfully counted number of products in the grid");
	}
	
	@Test(description = "Step 5 : Verify the number of part numbers displayed properly", priority = 5)
	public void Step05_VerifyNumberOfPartNumbersAfterDeletion() {
		//Verify the number of part numbers displayed properly
	
		if(count%2 == 1) {
			Assert.assertEquals(countAfterDeletion, (count/2)+1);
		}
		else {
			Assert.assertEquals(countAfterDeletion, count/2);
		}
		
		log.info("Successfully Verified the number of part numbers available in the grid after deletion\n");
		Reporter.log("<p>Successfully Verified the number of part numbers available in the grid after deletion");
		
	}
}