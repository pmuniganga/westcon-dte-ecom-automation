package in.valtech.westcon.test.rs;

import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.ResellerPages.ResellerCartPage;
import in.valtech.westcon.ResellerPages.ResellerCheckoutAddressPage;
import in.valtech.westcon.ResellerPages.ResellerOrderConfirmationPage;
import in.valtech.westcon.ResellerPages.ResellerOrderListingPage;
import in.valtech.westcon.ResellerPages.ResellerQuoteCartPage;
import in.valtech.westcon.ResellerPages.ResellerQuoteRequestCategoryPage;
import in.valtech.westcon.ResellerPages.ResellerRequestAQuotePage;
import in.valtech.westcon.ResellerPages.ResellerReviewAndSubmitPage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;
import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC60_Reseller_CheckoutProcess extends BaseTest{

	static  ResellerSelfServicePage rsRequestSelfServicePage;
	static  ResellerQuoteRequestCategoryPage rsQuoteRequestCategoryPage;
	static ResellerQuoteCartPage rssellerQuoteCartPage;
	static ResellerCheckoutAddressPage rsCheckoutAddressPage;
	static ResellerOrderConfirmationPage rsOrderConfirmationPage;
	static ResellerOrderListingPage rsOrderListingPage;
	static ResellerReviewAndSubmitPage rsReviewAndSubmitPage;

	String SKUCount = null;
	static String PONum = null;
	static String carrier=null;
	static String OrderNum=null;

	static  ResellerRequestAQuotePage rsRequestAQuotePage;
	static ResellerCartPage rssellerCartPage;

	@Test(description = "Step 1: Call TC51", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-51
		log.info("Reseller Dashboard page is displayed \n");
		Reporter.log("<p>Reseller Dashboard page is displayed ");
	}

	@Test(description = "Step 2: Mouse hover on Product catalog from Menu and Click on Quick order entry link", priority = 2)
	public void Step02_QuickOrderEntry() throws Exception {

		rsRequestSelfServicePage = new ResellerSelfServicePage(driver);
		CustomFun.waitForPageLoaded(driver);
		rsRequestSelfServicePage.mouseHoverOnProductCatalogNavigateQuickEntryPage();

		log.info("Quick entry page is displayed \n");
		Reporter.log("<p>Quick entry page is displayed ");
	}

	@Test(description = "Step 3: Enter partnumbers to Quick entry box and click on Add to cart", priority = 3)
	public void Step03_EnterPartNumAndAddToCart() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());

			rsQuoteRequestCategoryPage.EnterValueTextbox(driver, SKUdataSetList.get(i)
					.getSKU()); 

			log.info("Quick entry page is displayed \n");
			Reporter.log("<p>Quick entry page is displayed ");
		}
	}

	@Test(description = "Step 4: Click view cart from the overlay", priority = 4)
	public void Step04_ClickViewCartButton() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		rsQuoteRequestCategoryPage.QuickEntry_ClickViewCartButton(driver, "Click on view cart in overlay");

		log.info("Shopping cart page should be displayed \n");
		Reporter.log("<p>Shopping cart page should be displayed ");
	}

	@Test(description = "Step 5: Click on Checkout button", priority = 5)
	public void Step05_ClickCheckOutButton() throws Exception {

		rssellerCartPage = new ResellerCartPage(driver);
		Thread.sleep(5000);
		rssellerCartPage.clickCheckoutButton();

		log.info("Billing and shipping page should be displayed \n");
		Reporter.log("<p>Billing and shipping page should be displayed ");
	}

	@Test(description = "Step 6: Select new address radio button", priority = 6)
	public void Step06_SelectNewAddressRadioButtonVerifyFieldsEnable() throws Exception {

		rsCheckoutAddressPage = new ResellerCheckoutAddressPage(driver);

		rsCheckoutAddressPage.clickEnterNewAddressVerifyFields();

		log.info("Fields should be enabled \n");
		Reporter.log("<p>Fields should be enabled ");
	}

	@Test(description = "Step 7: Fill Attn , Company Name, country , address , postal code , phone number ", priority = 7)
	public void Step07_EnterDataInFields() throws Exception {

		rsCheckoutAddressPage = new ResellerCheckoutAddressPage(driver);

		System.out.println("Attn=" +userInfoDSDetails.getAttn());
		System.out.println("Company=" +userInfoDSDetails.getCompanyName());
		System.out.println("Country=" +userInfoDSDetails.getCountry());
		System.out.println("Address1=" +userInfoDSDetails.getAdddress1());
		System.out.println("PostalCode=" +userInfoDSDetails.getPostalCode());
		System.out.println("phNum=" +userInfoDSDetails.getPhoneNumber());

		rsCheckoutAddressPage.enterDataInFields(userInfoDSDetails.getAttn(),
				userInfoDSDetails.getCompanyName(),userInfoDSDetails.getCountry(),
				userInfoDSDetails.getAdddress1(),userInfoDSDetails.getPostalCode(),
				userInfoDSDetails.getPhoneNumber());

		log.info("All fields entered should be shown \n");
		Reporter.log("<p>All fields entered should be shown ");
	}

	@Test(description = "Step 8: Enter PO number ", priority = 8)
	public void Step08_EnterDataInFields() throws Exception {

		rsCheckoutAddressPage = new ResellerCheckoutAddressPage(driver);

		System.out.println("PoNum=" +userInfoDSDetails.getPoNumber());

		PONum = userInfoDSDetails.getPoNumber();	

		rsCheckoutAddressPage.enterPONum(PONum);


		log.info("PO number should be entered \n");
		Reporter.log("<p>PO number should be entered ");
	}

	@Test(description = "Step 9:Select shipping method , enter carrier account number, shipdate , select request blind shipment, select request ship complete,"
			+ "  enter shipping instructions and Click Next button", priority = 9)
	public void Step09_SelectShippingMethodOtherDetailsNdSave() throws Exception {

		rsCheckoutAddressPage = new ResellerCheckoutAddressPage(driver);

		if(BaseTest.locale.equalsIgnoreCase("US")){
			//Select shipping method
			rsCheckoutAddressPage.SelectShippingMethod(TextProp.getProperty("shippingMethodvalue"));
		}else{
			//Select shipping method
			rsCheckoutAddressPage.SelectShippingMethod(TextProp.getProperty("shippingMethodvalue_CA"));
		}
		log.info("Successfully selected the shipping method");
		Reporter.log("<p>--Successfully selected the shipping method");	

		//enter carrier account number
		carrier=userInfoDSDetails.getCarrierNumber();
		rsCheckoutAddressPage.EnterCarrierNumber(carrier);

		//select request blind shipment
		rsCheckoutAddressPage.clickOnBlindshipmentCheckbox();

		//select request ship complete

		//enter shipping instructions 
		rsCheckoutAddressPage.enterShippingInstructions(TextProp.getProperty("shippingInstructions"));

		//click on save shipping instruction
		rsCheckoutAddressPage.clickOnSaveShippingInstructionCheckBox();

		//select ship date 
		rsCheckoutAddressPage.enterEarliestRequiredShipDate(CustomFun.getCurrentDate());

		//Click Save button
		rsCheckoutAddressPage.clickOnSaveButton();

		log.info("Review and submit page should be displayed ");
		Reporter.log("<p>--Review and submit page should be displayed");	
	}

	@Test(description = "Step 10: Accept terms and conditions checkbox & click submit ", priority = 10)
	public void Step10_AcceptTermsAndClickSubmit() throws Exception {

		rsReviewAndSubmitPage = new ResellerReviewAndSubmitPage(driver);

		rsReviewAndSubmitPage.clickOnTermsAndSubmit();


		log.info("PO number should be entered \n");
		Reporter.log("<p>PO number should be entered ");
	}

	@Test(description = "Step 11: Retrive Order number and save ", priority = 11)
	public void Step11_SaveOrderNum() throws Exception {

		rsOrderConfirmationPage = new ResellerOrderConfirmationPage(driver);

		OrderNum=rsOrderConfirmationPage.saveOrderNum();


		log.info("Order number should be saved in parameter \n");
		Reporter.log("<p>Order number should be saved in parameter ");
	}

	@Test(description = "Step 12: Click on My Orders button from confirmation page ", priority = 12)
	public void Step12_ClickMyOrderBtn() throws Exception {

		rsOrderConfirmationPage = new ResellerOrderConfirmationPage(driver);

		rsOrderConfirmationPage.clickMYOrderButton();


		log.info("Order listing page should be displayed \n");
		Reporter.log("<p>Order listing page should be displayed ");
	}

	@Test(description = "Step 13: Enter Order number in search order field and click Enter key ", priority = 13)
	public void Step13_SearchOrderNum() throws Exception {


		rsOrderListingPage = new ResellerOrderListingPage(driver);

		rsOrderListingPage.searchCreatedOrder(OrderNum);


		log.info("Order number should be available in the search result table \n");
		Reporter.log("<p>Order number should be available in the search result table ");
	}

}

