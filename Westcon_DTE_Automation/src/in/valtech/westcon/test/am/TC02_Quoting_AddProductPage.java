package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;
import in.valtech.westcon.QuotingPages.QuotingSelectResellerPage;

public class TC02_Quoting_AddProductPage extends BaseTest {

	static QuotingQuotesPage amQuotelistingPage;
	static QuotingSelectResellerPage amResellerlistingpage;
	static  QuotingAddProductsPage amAddProductsPage;

	/**
	 * Login to Quoting website by calling TC01
	 * 
	 */

	@Test(description = "Step 1: Login to Quoting website by calling TC01", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
	}

	@Test(description = "Step 2: Click on Create new Quote", priority = 2)
	public void Step02_clickCreateNewQuoteButton() throws Exception {

		amQuotelistingPage = new QuotingQuotesPage(driver);

		amQuotelistingPage.clickCreateNewQuoteButton();

		log.info("Reseller listing page is displayed\n");
		Reporter.log("<p>Reseller listing page is displayed");
	}

	@Test(description = "Step 3: Enter Reseller and Search", priority = 3)
	public void Step03_enteResellerandSearch() throws Exception {
		System.out.println("user=" +userInfoDSDetails.getReseller());

		amResellerlistingpage= new QuotingSelectResellerPage(driver);

		amResellerlistingpage.enterResellerAndSearch(userInfoDSDetails.getReseller());



		log.info("Respective reseller is displayed in the listing page\n");
		Reporter.log("<p>Respective reseller is displayed in the listing page");
	}

	@Test(description = "Step 4: Select Reseller", priority = 4)
	public void Step04_selectReseller() throws Exception {

		// Select First searched Reseller in list
		amResellerlistingpage.selectFirstReseller();

		//Verifying selected tab
		amAddProductsPage = new QuotingAddProductsPage(driver);
		amAddProductsPage.verifyCopyPasteBOMTabOne();


		log.info("Successfully displayed Add product page and default Copy & Paste BOM is selected \n");
		Reporter.log("<p>Successfully displayed Add product page and default Copy & Paste BOM is selected ");
	}


}
