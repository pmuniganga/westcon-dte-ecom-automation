package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC105_Quoting_EnterQuoteNotes extends BaseTest {

	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08 -- ", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");		

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}	

	@Test(description = "Step 2 : Click on Pricing Tab", priority = 2)
	public void Step02_ClickOnPricingTab() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();
		
		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");
	}

	@Test(description = "Step 3 : Enter Quote Notes in the text area available", priority = 3)
	public void Step03_EnterQuoteNotes() {
		//Enter the Quote Notes
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterQuoteNotes(TextProp.getProperty("NotesTextInternal"));

		log.info("Successfully entered quote notes\n");
		Reporter.log("<p>Successfully entered quote notes");
	}

	@Test(description = "Step 4 : Select Westcon Only Radio button", priority = 4)
	public void Step04_SelectWestconOnlyRadioButton() {
		//Select Westcon Internal Only Radio Button
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.selectQuoteNotesWestconInternalOnly();

		log.info("Successfully Selected Westcon Internal Only Radio button\n");
		Reporter.log("<p>Successfully Selected Westcon Internal Only Radio button");
	}
	
	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Select Submit quote from the dropdown and click on GO button", priority = 5)
	public void Step05_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Click On No, do not validate button", priority = 6)
	public void Step06_ClickOnDoNotValidateButton1()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}
	
	@Test(description = "Step 7 : Verify the Saved Notes", priority = 7)
	public void Step07_VerifyNotesText() {
		//Verify Saved Notes
		By AddQuoteNotesTextarea = By.xpath(ObjRepoProp.getProperty("QuoteNotes_XPATH"));
		Assert.assertEquals(driver.findElement(AddQuoteNotesTextarea).getText(), TextProp.getProperty("NotesTextInternal"));

		log.info("Successfully verified the saved text\n");
		Reporter.log("<p>Successfully verified the saved text");
	}
}