package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuoteToOrderPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC110_Quoting_VerifyPOMandatory extends BaseTest {

	String expQuoteId=null;
	String totalQuotePrice=null;
	String expDate;
	String closeDate;
	String expEndUSer;


	/**
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}
	
	/**
	 * Save Quote details before submitting quote
	 */
	@Test(description = "Step 2: Select Submit quote from the dropdown and click on GO button", priority = 2)
	public void Step02_RetriveQuoteDetails_BeforeSubmit()	throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		
		amQuoteDetailsPage.getValues();
		expQuoteId=quoteID;
		totalQuotePrice = driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().trim();
		expDate = driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim();		
		closeDate =	driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim();
		expEndUSer=userInfoDSDetails.getEndUserAccountNumber();
		
		log.info("Successfully retrived Quote details before submitting Quote\n");
		Reporter.log("<p>Successfully retrived Quote details before submitting Quote");

	}
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 3: Select Submit quote from the dropdown and click on GO button", priority = 3)
	public void Step03_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Click On No, do not validate button", priority = 4)
	public void Step04_ClickOnDoNotValidateButton()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
	
		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");				
		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message");

	}
	

	/**
	 * Select Convert quote to Order from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Select Convert quote to Order from the dropdown and click on GO button", priority = 5)
	public void Step05_SelectConvertQuoteFromDropdown()
			throws Exception {
		//GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectConvertQuoteOption();

		log.info("Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
	}	
	
	/**
	 * Verify PO Number mandatory error message
	 */

	@Test(description = "Step 6:Enter PO number ", priority = 6)
	public void Step06_VerifyPoNumberError() throws Exception 
	{
		QuoteToOrderPage amConvertToOrder = new QuoteToOrderPage(driver);
		
		By poNumberMandatoryError = By.xpath(ObjRepoProp.getProperty("poNumberMandatoryError_XPATH"));
		amConvertToOrder.ClickOnSubmitError();
		
		Assert.assertEquals(driver.findElement(poNumberMandatoryError).getText(), TextProp.getProperty("poMandatoryErrorMsg"));
		
		log.info("Successfully verified PO number error message\n");
		Reporter.log("<p>Successfully verified PO number error message");

	}
}
