package in.valtech.util;

public class UserInfoDSDetails {

	private String dataVersion;
	private String locale;
	private String description;
	private String amUserName;
	private String amPassword;
	private String amUrl;
	private String rsUrl;	
	private String rsUserName;
	private String rsPassword;
	
	private String attn;
	private String companyName;
	private String adddress1;
	private String adddress2;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private String phoneNumber;
	private String shippingMtd;
	private String poNumber;
	private String shippingInstruction;
	private String requestBlind;
	private String requestShipComplete;
	private String carrierNumber;
	private String earliestShipDate;
	private String endUser;
	private String accountManager;
	private String reseller;
	private String quoteCloseDate;
	private String probability;
	private String endUserAccountNumber;
	private String resellerName;
	private String salesOrg;
	
	
	/**
	 * @return dataVersion
	 */
	public String getDataVersion() {
		return dataVersion;
	}
	
	/**
	 * @param dataVersion
	 */
	public void setDataVersion(String dataVersion) {
		this.dataVersion = dataVersion;
	}
	
	/**
	 * @return locale
	 */
	public String getLocale() {
		return locale;
	}
	
	/**
	 * @param locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return amUrl
	 */
	public String getAmUrl() {
		return amUrl;
	}
	
	/**
	 * @param amUrl
	 */
	public void setAmUrl(String amUrl) {
		this.amUrl = amUrl;
	}	
	
	/**
	 * @return userName
	 */
	public String getAmUserName() {
		return amUserName;
	}
	
	/**
	 * @param userName
	 */
	public void setAmUserName(String amUserName) {
		this.amUserName = amUserName;
	}
	
	/**
	 * @return password
	 */
	public String getAmPassword() {
		return amPassword;
	}
	
	/**
	 * @param password
	 */
	public void setAmPassword(String amPassword) {
		this.amPassword = amPassword;
	}
		
	/**
	 * @return rsUrl
	 */
	public String getRsUrl() {
		return rsUrl;
	}
	
	/**
	 * @param rsUrl
	 */
	public void setRsUrl(String rsUrl) {
		this.rsUrl = rsUrl;
	}	
	
	/**
	 * @return rsUserName
	 */
	public String getRsUserName() {
		return rsUserName;
	}
	
	/**
	 * @param rsUserName
	 */
	public void setRsUserName(String rsUserName) {
		this.rsUserName = rsUserName;
	}
	
	/**
	 * @return rsPassword
	 */
	public String getRsPassword() {
		return rsPassword;
	}
	
	/**
	 * @param rsPassword
	 */
	public void setRsPassword(String rsPassword) {
		this.rsPassword = rsPassword;
	}	
	
	/** 
	 * @return address1
	 */
	public String getAdddress1() {
		return adddress1;
	}
	
	/**
	 * @param adddress1
	 */
	public void setAdddress1(String adddress1) {
		this.adddress1 = adddress1;
	}
	
	/**
	 * @return address2
	 */
	public String getAdddress2() {
		return adddress2;
	}
	
	/**
	 * @param adddress2
	 */
	public void setAdddress2(String adddress2) {
		this.adddress2 = adddress2;
	}
	
	/**
	 * @return city
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * @return state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getShippingMtd() {
		return shippingMtd;
	}
	public void setShippingMtd(String shippingMtd) {
		this.shippingMtd = shippingMtd;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getShippingInstruction() {
		return shippingInstruction;
	}
	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}
	public String getRequestBlind() {
		return requestBlind;
	}
	public void setRequestBlind(String requestBlind) {
		this.requestBlind = requestBlind;
	}
	public String getRequestShipComplete() {
		return requestShipComplete;
	}
	public void setRequestShipComplete(String requestShipComplete) {
		this.requestShipComplete = requestShipComplete;
	}
	public String getCarrierNumber() {
		return carrierNumber;
	}
	public void setCarrierNumber(String carrierNumber) {
		this.carrierNumber = carrierNumber;
	}
	public String getEarliestShipDate() {
		return earliestShipDate;
	}
	public void setEarliestShipDate(String earliestShipDate) {
		this.earliestShipDate = earliestShipDate;
	}
	public String getEndUser() {
		return endUser;
	}
	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}
	public String getAccountManager() {
		return accountManager;
	}
	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}
	public String getReseller() {
		return reseller;
	}
	public void setReseller(String reseller) {
		this.reseller = reseller;
	}
	public String getQuoteCloseDate() {
		return quoteCloseDate;
	}
	public void setQuoteCloseDate(String quoteCloseDate) {
		this.quoteCloseDate = quoteCloseDate;
	}
	public String getProbability() {
		return probability;
	}
	public void setProbability(String probability) {
		this.probability = probability;
	}

	public String getAttn() {
		return attn;
	}

	public void setAttn(String attn) {
		this.attn = attn;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEndUserAccountNumber() {
		return endUserAccountNumber;
	}

	public void setEndUserAccountNumber(String endUserAccountNumber) {
		this.endUserAccountNumber = endUserAccountNumber;
	}
	
	public String getResellerName() {
		return resellerName;
	}

	public void setResellerName(String resellerName) {
		this.resellerName = resellerName;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
}
