package in.valtech.westcon.test.rs;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.testng.Assert;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.ResellerPages.ResellerEndUserandShippingPage;
import in.valtech.westcon.ResellerPages.ResellerRFQListingPage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;

public class TC65_Reseller_CreateRFQDealId_SubmitRFQ extends BaseTest{

	static ResellerRFQListingPage resellerRFQListingPage;
	static ResellerSelfServicePage resellerSelfServicePage;

	/**
	 * Call TC57
	 */
	@Test(description = "Step 1: Call TC51 ,TC52 ,TC55,TC56,TC57", priority = 1)
	public void Step01_NavigateToRFQShippingPage() throws Exception {

		// Execute TC51
		log.info("TC51 Successfully Executed \n");
		Reporter.log("<p>TC51 Successfully Eexecuted ");
		// Execute TC52
		log.info("TC52 Successfully Executed \n");
		Reporter.log("<p>TC52 Successfully Eexecuted ");
		// Execute TC55
		log.info("TC55 Successfully Executed \n");
		Reporter.log("<p>TC55 Successfully Eexecuted ");
		// Execute TC56
		log.info("TC56 Successfully Executed \n");
		Reporter.log("<p>TC56 Successfully Eexecuted ");
		// Execute TC57
		log.info("TC57 Successfully Executed \n");
		Reporter.log("<p>TC57 Successfully Eexecuted ");

		log.info(" Successfully navigated to RFQ shipping page \n");
		Reporter.log("<p>Successfully navigated to RFQ shipping pag ");

	}

	/**
	 * Select shipping method and click on Submit
	 */

	@Test(description = "Step 2: Select shipping method and click on Submit", priority = 2)
	public void Step02_SelectShippingMethodAndSubmit() throws Exception {

		ResellerEndUserandShippingPage resellerEnduserAndShippingPage = new ResellerEndUserandShippingPage(driver);

		//select shipping method
		//resellerEnduserAndShippingPage.selectShippingMehtod(TextProp.getProperty("shippingMethod"));

		resellerEnduserAndShippingPage.selectShippingMehtod(userInfoDSDetails.getShippingMtd());

		log.info(" Successfully selected shipping method \n");
		Reporter.log("<p>Successfully selected shipping method ");

		//Click on submit button
		resellerEnduserAndShippingPage.clickOnRFQSubmitButton();

		log.info(" Successfully clicked on submit button \n");
		Reporter.log("<p>Successfully clicked on submit button ");

		//IE Browser 
		Thread.sleep(5000);
				
		//Verify the confirmation overlay
		Assert.assertTrue(CustomFun.isElementVisible(By.xpath(ObjRepoProp.getProperty("reselleRFQSubmitConfirmationOverlay_XPATH")), driver),
				"Overlay not displayed");

		log.info(" Successfully verified the confirmation overlay \n");
		Reporter.log("<p>Successfully verified the confirmation overlay ");

	}
	/**
	 * Retrive RFQ number and save
	 */
	@Test(description = "Step 3: Retrive RFQ number and save", priority = 3)
	public void Step03_RetriveRFQNumberAndSave() throws Exception {

		ResellerEndUserandShippingPage resellerEnduserAndShippingPage = new ResellerEndUserandShippingPage(driver);

		resellerEnduserAndShippingPage.getRFQId();

		log.info(" Successfully saved the retrived ID \n");
		Reporter.log("<p>Successfully saved the retrived ID ");
	}

	/**
	 * Click on OK in overlay
	 */
	@Test(description = "Step 4: Click on OK in overlay", priority = 4)
	public void Step04_ClickOKInOverlay() throws Exception {

		ResellerEndUserandShippingPage resellerEnduserAndShippingPage = new ResellerEndUserandShippingPage(driver);
		//click on ok button in the overlay
		resellerSelfServicePage=resellerEnduserAndShippingPage.clickOnOKButtonInSubmitConfirmationOverlay();

		log.info(" Successfully clicked on ok button and navigated to self service page\n");
		Reporter.log("<p>Successfully clicked on ok button and navigated to self service page");

	}

	/**
	 * Mouse hover on Quotes from Menu and Click on View Unproccessed Quote requests
	 */

	@Test(description = "Step 5: Mouse hover on Quotes from Menu and Click on View Unproccessed Quote requests", priority = 5)
	public void Step05_ClickOnUnprocessedQuotes() throws Exception {

		ResellerSelfServicePage resellerSelfServicePage= new ResellerSelfServicePage(driver);

		//click on unprocessed quotes link
		resellerRFQListingPage=resellerSelfServicePage.mouseHoverOnQuotesAndClickOnUnProcessedQuotesLink();

		log.info(" Successfully navigated to resellerRFQListingPage\n");
		Reporter.log("<p>Successfully navigated to resellerRFQListingPage ");

	}

	/***
	 * Enter RFQ ID (saved on submission) in search input field and click enter 
	 */

	@Test(description = "Step 6:Enter RFQ ID (saved on submission) in search input field and click enter", priority = 6)
	public void Step06_SearchForTheSubmittedRFQ() throws Exception {

		//Enter RFQ in search 

		resellerRFQListingPage.searchForTheRFQSavedAndHitEnter(RFQID);
		resellerRFQListingPage.searchForTheRFQinAM();
		//Verify the RFQ in the search result

		Assert.assertEquals(SearchRFQID,RFQID);

		log.info(" Successfully verified the RFQ in the search result\n");
		Reporter.log("<p>Successfully verified the RFQ in the search result ");

	}
}
