package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC94_Quoting_SelectResellerAsEndUser extends BaseTest{
	//static QuotingQuoteDetailpage amQuotePage;
		String resellerName;

		@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
		public void Step01_Call() throws Exception {

			// Execute TC-01
			log.info("FTC-01 Successfully Executed \n");
			Reporter.log("<p>TC-01 Successfully Executed ");
			// Execute TC-02
			log.info("TC-02 Successfully Executed \n");
			Reporter.log("<p>TC-02 Successfully Executed ");
			// Execute TC-03
			log.info("TC-03 Successfully Executed \n");
			Reporter.log("<p>TC-03 Successfully Executed ");
		}

		@Test(description = "Step 2: Selecting end user Search Link", priority = 2)
		public void Step02_ClickOnEndUserSearchLink() throws Exception {
			// Selecting end user from end user modal 

			QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
			GUIFunctions.pageScrollDown(driver, 100);
			amQuotePage.AMClickEndUserSearchLink();
			
			log.info("Successfully end user search modal is displayed");
			Reporter.log("<p>Successfully end user search modal is displayed");
		}

		@Test(description = "Step 3: Enter Reseller Username & Click on Search button", priority = 3)
		public void Step03_EnterUN_Search() throws Exception {
			// Search list should be displayed	
			System.out.println("Seleted Reseller Account for End user****" +userInfoDSDetails.getReseller());
			QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
			resellerName = userInfoDSDetails.getResellerName();	
			
            CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("endUserModalNameTxtBox_XPATH")), driver);
            System.out.println("resellerName>>"+resellerName);
			amQuotePage.AMendUserModal_enterEndUserName(resellerName);
			amQuotePage.endUserModalResellerSearch_enterEndUserAcc();
			amQuotePage.AMendUserModal_ClickonSearch();
			amQuotePage.AMVerifyEndUserModalHeader();

			log.info("Successfully reseller search list is displayed");
			Reporter.log("<p>Successfully reseller search list is displayed");
		}


		@Test(description = "Step 4: Select Reseller as End user from the list shown", priority = 4)
		public void Step04_SelectResellerAsEndUser() throws Exception {
			// Create Quote page should be displayed with End user selected 
			QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
			amQuotePage.endUserModalResellerSearch_ClickonRadio();
			amQuotePage.endUserModalResellerSearch_ClickonSelectUser();
			
			log.info("Successfully Reseller as end user is selected from modal");
			Reporter.log("<p>Successfully Reseller as end user is selected from modal");

		}
		
		@Test(description = "Step 5: Verify Reseller account selected", priority = 5)
		public void Step05_VerifySelectedResellerForEndUser() throws Exception {
			
			// Verifying End User Retrieved	
		    String endUserDetails =driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserAccountDetails_XPATH"))).getText().trim();
			System.out.println("End USer Account retrieved from SAP: "+endUserDetails);
			Assert.assertEquals(endUserDetails,userInfoDSDetails.getReseller() ,"Proper End User not retrived");
			
			log.info("Successfully Reseller is selected from modal");
			Reporter.log("<p>Successfully Reseller is selected from modal");

		}
		

}
