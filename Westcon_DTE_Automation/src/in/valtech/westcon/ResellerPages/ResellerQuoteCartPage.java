package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import org.openqa.selenium.JavascriptExecutor;


public class ResellerQuoteCartPage {
	
	private final WebDriver driver;

	GUIFunctions gui = new GUIFunctions();
	//For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	//RS quote cart page Constructor
	public ResellerQuoteCartPage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Check that we're on the right RS quote cart Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerQuoteCartPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not reseller quote cart page");
		}
	}

	static By nextButton=By.xpath(ObjRepoProp.getProperty("nextButton_XPATH"));
	By checkoutButtonQuoteCartPage_XPATH=By.xpath(ObjRepoProp.getProperty("checkoutButtonQuoteCartPage_XPATH"));

	/**
	 * This function is to click on next button from quote cart page
	 * 
	 * @throws Exception
	 */
	public ResellerEndUserandShippingPage clickOnNextButton() throws Exception
	{
		//to click on next button from quote cart page
		GUIFunctions.pageScrollDown(driver, 300);
		GUIFunctions.clickElement(driver, nextButton, "next Button clicked");
		System.out.println("Clicked on next button");
		return new ResellerEndUserandShippingPage(driver);
	}
	
	public ResellerCheckoutAddressPage clickCheckoutButton() throws Exception
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		//Wait for checkout button to display after navigating to Quote cart page
		CustomFun.waitObjectToLoad(driver, checkoutButtonQuoteCartPage_XPATH, 60);
		//click on view Checkout button
		GUIFunctions.clickElement(driver, checkoutButtonQuoteCartPage_XPATH, "Click on Chcekout Button");
		return new ResellerCheckoutAddressPage(driver);
	}



}
