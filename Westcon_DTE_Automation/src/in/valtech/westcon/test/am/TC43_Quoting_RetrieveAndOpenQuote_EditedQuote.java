package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC43_Quoting_RetrieveAndOpenQuote_EditedQuote extends BaseTest{

    String endUser;
    String closeDate;
    String expDate;
    String requesterEmail;
    String requesterName;
	String quoteId_Before;
	String rev_Before;
    String totalQuotePrice;
    
    
	/**
	 * User to Retrieve the submitted quote in quoting by executing TC-09
	 * 
	 */
	@Test(description = "Step 1: Submitted Quote detail page should displayed with all details", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");
		// Execute TC-09
		log.info("TC-09 Successfully Executed \n");
		Reporter.log("<p>TC-09 Successfully Executed ");

		log.info("Quote details page is displayed with Requester Info, End user details, VRF fields  selected");
		Reporter.log("<p>Quote details page is displayed with Requester Info, End user details, VRF fields  selected");	
	}
	
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Retrive QuoteId, Rev, TotalPrice before submitting modified quote", priority = 2)
	public void Step02_RetriveQuoteId_Rev_totalPrice_BeforeSubmit()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		
		totalQuotePrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().trim();
        System.out.println("total price before modifing:"+totalQuotePrice);
        
		amQuoteDetailsPage.getValues();		
		quoteId_Before=quoteID;
		rev_Before=revesionID;
		
		log.info("Successfully Retrive QuoteId, Rev, TotalPrice before submitting modified quote\n");
		Reporter.log("<p>Successfully Retrive QuoteId, Rev, TotalPrice before submitting modified quote");

	} 
	
	/**
	 * 
	 * Change Quote Close Date and Quote Expiration Date
	 *  @throws Exception
	 */
	@Test(description = "Step 3: Change Quote Expiration & Quote Close Date", priority =3)
	public void Step03_Changing_Quoteexpdate_Quoteclosedate()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
			
		amQuoteDetailsPage.change_Quoteexpdate_Quoteclosedate(
				        CustomFun.addDays(new Date(), (Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim())+6), locale),
						CustomFun.addDays(new Date(), (Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim())+4), locale));
			
		

			expDate = driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim();
				
				closeDate =	driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim();
	

		log.info("Successfully Changed Quote Close Date\n");
		Reporter.log("<p>Successfully Changed Quote Close Date\n");

	}
	
	/**
	 * Change End User
	 *  @throws Exception
	 */
	@Test(description = "Step 4: Change End User", priority =4)
	public void Step04_ChangingEndUser()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
	    GUIFunctions.pageScrollUP(driver, 150);
		amQuoteDetailsPage.AMClickEndUserSearchLink();
		
		System.out.println("End user Name*****" +TextProp.getProperty("EndUserName"));	
		
		// Search list should be displayed	
		driver.findElement(By.xpath(ObjRepoProp.getProperty("endUserModalNameTxtBox_XPATH"))).clear();
		amQuoteDetailsPage.AMendUserModal_enterEndUserName(TextProp.getProperty("EndUserName"));		
		amQuoteDetailsPage.AMendUserModal_ClickonSearch();
		amQuoteDetailsPage.AMVerifyEndUserModalHeader();		
		amQuoteDetailsPage.AMendUserModal_ClickonFirstRadio();
		amQuoteDetailsPage.AMendUserModal_ClickonSelectUser_FirstRadio();
        Thread.sleep(5000);
        
        endUser =driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserAccountDetails_XPATH"))).getText().trim();

		
		log.info("Successfully Changed End User\n");
		Reporter.log("<p>Successfully Changed End User\n");

	}
	

	/**
	 * Edit Requester Info
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Edit Requester Info", priority =5)
	public void Step05_EditingRequestorInfo() throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.clickOnRequesterinfotab();
		
		amQuoteDetailsPage.editRequesterinfo(TextProp.getProperty("RequesterName_V2"),TextProp.getProperty("RequesterEmail_V2"));
			
	requesterName=driver.findElement(By.id(ObjRepoProp.getProperty("requesterName_ID"))).getAttribute("value").trim();
	requesterEmail=driver.findElement(By.xpath(ObjRepoProp.getProperty("requesterEmail_XPATH"))).getAttribute("value").trim();
			
		log.info("Successfully Editted Requester Name and Email\n");
		Reporter.log("<p>Successfully Editted Requester Name and Email\n");

	}
	

	
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority = 6)
	public void Step06_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}


	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Click On No, do not validate button", priority = 7)
	public void Step07_ClickOnDoNotValidateButton()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
	
		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");
		
		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message");

	}
    
    /**
	 * Get Quote ID and Total Quote Price after Submitting the Quote
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 8: Get Quote ID and Total Quote Price", priority = 8)
	public void Step08_GetQuoteIDAndTotalPrice() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.getValues();
		
		log.info("Successfully fetched Quote ID and Quote total price\n");
		Reporter.log("<p>Successfully fetched Quote ID and Quote total price");

	}
	
	@Test(description = "Step 9: Click on Quotes tab", priority = 9)
	public void Step09_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);
		
		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");
	
	}
	
	@Test(description = "Step 10 : Search for the submitted quote", priority = 10)
	public void Step10_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);
		
		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}   
    
    /**
	 * Verify Quote details with modified field values, after submitting
	 * 
	 * @throws Exception
	 */
	
	@Test(description = "Step 11: Verify Quote details with modified field values", priority = 11)
	public void Step11_VerifyQuoteDetails_withModifiedValues() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		
		//Verify Quote ID & Rev
		amQuoteDetailsPage.getValues();
		Assert.assertTrue(quoteId_Before.equals(quoteID), "Quote Id verification fails");
		Assert.assertFalse(rev_Before.equals(revesionID), "Quote Revision verification fails");
		
		//Verify modified End User 
		String actualEndUserAcc=driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserAccountDetails_XPATH"))).getText().trim();
		System.out.println("End User******"+actualEndUserAcc);
		Assert.assertTrue(actualEndUserAcc.contains(endUser), "End User Account mismatch");
		
		//Verify Quote Expiration Date
		System.out.println("Quote Expiration Date******"+driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim());
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim(),
				             expDate,"Quote Expiration Date mismatch");
		
		//Verify Quote Close Date
		System.out.println("Quote Close Date******"+driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim());
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim(),
				             closeDate,"Quote Close Date mismatch");
	    		   
		//Verify Total price	
        GUIFunctions.pageScrollDown(driver, 800);
		System.out.println("Total Price******"+
				driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().trim());
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().trim(),totalQuotePrice,
	        	"Quote Price mismatch");
		
       //Verify Requester Info
		amQuoteDetailsPage.clickOnRequesterinfotab();
		
        System.out.println("Requester Name******"+
        		driver.findElement(By.id(ObjRepoProp.getProperty("requesterName_ID"))).getAttribute("value").trim());
        Assert.assertEquals(driver.findElement(By.id(ObjRepoProp.getProperty("requesterName_ID"))).getAttribute("value").trim(),
        		          requesterName,"Requester name mismatch");
        
        System.out.println("Requester Email Id******"+
        		driver.findElement(By.xpath(ObjRepoProp.getProperty("requesterEmail_XPATH"))).getAttribute("value").trim());
        Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("requesterEmail_XPATH"))).getAttribute("value").trim(),
		    	          requesterEmail,"Requester name mismatch");
        
		log.info("Successfully verified the Quote details with modified field values\n");
		Reporter.log("<p>Successfully verified the Quote details with modified field values");
	
	}
    
	
}
