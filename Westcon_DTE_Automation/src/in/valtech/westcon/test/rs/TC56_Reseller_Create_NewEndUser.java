package in.valtech.westcon.test.rs;

import in.valtech.westcon.ResellerPages.ResellerEndUserandShippingPage;
import in.valtech.westcon.ResellerPages.ResellerQuoteCartPage;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;

public class TC56_Reseller_Create_NewEndUser extends BaseTest
{
	ResellerQuoteCartPage rsQuoteCartPage;
	static 	String enduser=null;

	/**
	 * User to land on create quote page by executing TC51 TC52 and TC53
	 * 
	 */
	@Test(description = "Step 1: Calling TC51 TC52 TC53", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-51 - Access
		log.info("TC-51 Successfully Executed \n");
		Reporter.log("<p>TC-51 Successfully Eexecuted ");
		// Execute TC-52 - Login
		log.info("TC-52 Successfully Executed \n");
		Reporter.log("<p>TC-52 Successfully Eexecuted ");
		// Execute TC-53 - Enter Part number and submit
		log.info("FTC-53 Successfully Executed \n");
		Reporter.log("<p>TC-53 Successfully Eexecuted ");
	}

	/**
	 * To click on next button from create quote page
	 * 
	 */
	@Test(description = "Step 2: To Click on Next button", priority = 2)
	public void Step02_ClickOnNextButton() throws Exception 
	{
		ResellerQuoteCartPage rsQuoteCartPage=new ResellerQuoteCartPage(driver);
		//To click on next button
		CustomFun.waitForPageLoaded(driver);
		Thread.sleep(5000);
		rsQuoteCartPage.clickOnNextButton();
		log.info("TC-56 successfully clicked on next button and RFQ shipping page is displayed\n");
		Reporter.log("<p>TC-56 successfully clicked on next button and RFQ shipping page is displayed ");
	}

	/**
	 * To click on create new radio button for end user
	 * 
	 * /
	 *  */
	@Test(description = "Step 3: To Click on create new end user radio button", priority = 3)
	public void Step03_ClickCreateNewEnduserRadioButton() throws Exception 
	{
		//to select create new end user radio button
		ResellerEndUserandShippingPage rsEndUseandShippingPage=new ResellerEndUserandShippingPage(driver);
		rsEndUseandShippingPage.selectcreateNewEnduserRadioButton();
		log.info("TC-56 successfully selected create new end user radio button \n");
		Reporter.log("<p>TC-56 successfully selected create new end user radio button ");
	}

	/**
	 * To input new end user
	 */
	@Test(description = "Step 4: To enter new end user in to text box", priority = 4)

	public void Step04_enterNewEndUserName() throws Exception 
	{
		//To enter new end user
		ResellerEndUserandShippingPage rsEndUseandShippingPage=new ResellerEndUserandShippingPage(driver);
		rsEndUseandShippingPage.enterNewEndUser(userInfoDSDetails.getEndUser()) ;
		log.info("TC-56 successfully entered enduser name \n");
		Reporter.log("<p>TC-56 successfully entered enduser name ");
	}
}
