package in.valtech.westcon.ResellerPages;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;


public class ResellerCartPage {
	private final WebDriver driver;

	GUIFunctions gui = new GUIFunctions();
	//For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	//RS quote cart page Constructor
	public ResellerCartPage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Check that we're on the right RS quote cart Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerCartPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not reseller cart page");
		}
	}

	By checkoutButtonQuoteCartPage_XPATH=By.xpath(ObjRepoProp.getProperty("checkoutButtonQuoteCartPage_XPATH"));
	static By productRowCount_XPATH = By.xpath(ObjRepoProp.getProperty("productCount_XPATH"));
	static By clearCartButton_XPATH =By.xpath(ObjRepoProp.getProperty("clearCartButton_XPATH"));
	static By clearCartYesButton_XPATH= By.xpath(ObjRepoProp.getProperty("clearCartYesButton_XPATH"));
	static By enterPartNoTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("enterPartNoTextBox_XPATH"));
	static By addToCartButton_XPATH = By.xpath(ObjRepoProp.getProperty("addToCartButton_XPATH"));
    static By addToCartContinueButton_XPATH = By.xpath(ObjRepoProp.getProperty("addToCartContinueButton_XPATH"));
	public ResellerCheckoutAddressPage clickCheckoutButton() throws Exception
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		//Wait for checkout button to display after navigating to Quote cart page
		CustomFun.waitObjectToLoad(driver, checkoutButtonQuoteCartPage_XPATH, 60);
		//click on view Checkout button
		GUIFunctions.clickElement(driver, checkoutButtonQuoteCartPage_XPATH, "Click on Chcekout Button");
		return new ResellerCheckoutAddressPage(driver);
	}
	public ResellerCartPage clickClearCartButton() throws Exception
	{
		Thread.sleep(2000);
		CustomFun.waitObjectToLoad(driver, productRowCount_XPATH, 60);
		GUIFunctions.clickElement(driver, clearCartButton_XPATH, "Clear cart button");
		return new ResellerCartPage(driver);
	}
	public ResellerCartPage clickYesButton() throws Exception
	{
		Thread.sleep(2000);
		CustomFun.waitObjectToLoad(driver, clearCartYesButton_XPATH, 60);
		GUIFunctions.clickElement(driver, clearCartYesButton_XPATH, "Clear cart yes button");
		return new ResellerCartPage(driver);
	}
	public ResellerCartPage verify_Number_Of_Products_In_The_Cart() throws Exception
	{
		Thread.sleep(2000);
		CustomFun.waitObjectToLoad(driver, productRowCount_XPATH, 60);
		int rowCount=driver.findElements(productRowCount_XPATH).size();
		System.out.println("-------------------------------->"+rowCount);
		Assert.assertEquals(rowCount,1,"product Count mismatch");
		
		return new ResellerCartPage(driver);
	}
	public ResellerCartPage EnterValueTextbox(WebDriver driver, String data) throws Exception
	{

		GUIFunctions.typeTxtboxValue(driver, enterPartNoTextBox_XPATH, data);
		System.out.println("Entered the PartNumbers");
		GUIFunctions.clickElement(driver, addToCartButton_XPATH, "Click on add cart Button");
		return new ResellerCartPage(driver);
	}
	public ResellerCartPage clickOnContinueButton() throws Exception
	{
		Thread.sleep(15000);
		CustomFun.waitObjectToLoad(driver, addToCartContinueButton_XPATH, 60);
		GUIFunctions.clickElement(driver, addToCartContinueButton_XPATH, "Click on continue Button");
		return new ResellerCartPage(driver);
	}
}
