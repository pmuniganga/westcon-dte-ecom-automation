package in.valtech.westcon.test.rs;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.westcon.ResellerPages.ResellerCartPage;
import in.valtech.westcon.ResellerPages.ResellerQuoteCartPage;
import in.valtech.westcon.ResellerPages.ResellerQuoteRequestCategoryPage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;

public class TC61_Reseller_ShoppingCart  extends BaseTest {
	
	static  ResellerSelfServicePage rsRequestSelfServicePage;
	static  ResellerQuoteRequestCategoryPage rsQuoteRequestCategoryPage;
	static  ResellerQuoteCartPage resellerQuoteCartPage;
	static  ResellerCartPage resellerCartPage;
	
	String SKUCount = null;
	
	@Test(description = "Step 1: Call TC51", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("Reseller Dashboard page is displayed \n");
		Reporter.log("<p>Reseller Dashboard page is displayed ");
	}

	@Test(description = "Step 2: Mouse hover on Product catalog from Menu and Click on Quick order entry link", priority = 2)
	public void Step02_QuickOrderEntry() throws Exception {

		rsRequestSelfServicePage = new ResellerSelfServicePage(driver);

		rsRequestSelfServicePage.mouseHoverOnProductCatalogNavigateQuickEntryPage();

		log.info("Quick entry page is displayed \n");
		Reporter.log("<p>Quick entry page is displayed ");
	}
	@Test(description = "Step 3: save product qty from shopping cart", priority = 3)
	public void Step03_SaveProductQty() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		rsQuoteRequestCategoryPage.saveProductQty();

		log.info("Quick entry page is displayed \n");
		Reporter.log("<p>Quick entry page is displayed ");
	}
	@Test(description = "Step 4: Enter partnumbers to Quick entry box and click on Add to cart", priority = 4)
	public void Step04_EnterPartNumAndAddToCart() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		int SKUCountInt = SKUdataSetList.size();

		SKUCount = Integer.toString(SKUCountInt);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());


			rsQuoteRequestCategoryPage.EnterValueTextbox(driver, SKUdataSetList.get(i)
					.getSKU()); 
			//           rsRequestAQuotePage.CopyPasteBOM(driver , SKUdataSetList.get(i).getSKU()); 

			log.info("Quick entry page is displayed \n");
			Reporter.log("<p>Quick entry page is displayed ");
		}
	}
	
	@Test(description = "Step 5: Click continue from the overlay", priority = 5)
	public void Step05_ClickContinueButton() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		rsQuoteRequestCategoryPage.clickContinueButton(driver, "Click on view cart in overlay");

		log.info("Quick entry page should be displayed\n");
		Reporter.log("<p>Quick entry page should be displayed ");
	}
	
	@Test(description = "Step 6: Verify the cart total from shopping cart icon", priority = 6)
	public void Step06_VerifyTheCartTotalFromShoppingCartIcon() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		rsQuoteRequestCategoryPage.VerifyTheCartTotalFromShoppingCartIcon();

		log.info("Cart total should be matched with number of products added \n");
		Reporter.log("<p>Cart total should be matched with number of products added ");
	}
	@Test(description = "Step 7: Click on cart icon", priority = 7)
	public void Step07_Click_On_Cart_Icon() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		rsQuoteRequestCategoryPage.click_On_Cart_Icon();

		log.info("Overlay should be dsiplayed \n");
		Reporter.log("<p>Overlay should be dsiplayed ");
	}
	
	@Test(description = "Step 8: Click on view cart button from overlay", priority = 8)
	public void Step08_Click_On_View_Cart_Button_From_Overlay() throws Exception {

		rsQuoteRequestCategoryPage = new ResellerQuoteRequestCategoryPage(driver);

		rsQuoteRequestCategoryPage.click_On_View_Cart_Button_From_Overlay();

		log.info("Shopping cart page should be displayed \n");
		Reporter.log("<p>Shopping cart page should be displayed ");
	}
	
	@Test(description = "Step 9:Click on Clear cart", priority = 9)
	public void Step09_ClickClearCartButton() throws Exception {

		resellerCartPage = new ResellerCartPage(driver);
        
		resellerCartPage.clickClearCartButton();

		log.info("Clear Cart confirmation overlay is displayed \n");
		Reporter.log("<p>Clear Cart confirmation overlay is displayed");
	}
	@Test(description = "Step 10:Click on yes button", priority = 10)
	public void Step10_ClickYesButton() throws Exception {

		resellerCartPage = new ResellerCartPage(driver);
        
		resellerCartPage.clickYesButton();

		log.info("Clear Cart confirmation overlay is displayed \n");
		Reporter.log("<p>Clear Cart confirmation overlay is displayed");
	}
	@Test(description = "Step 11: Enter partnumbers to Quick entry box and click on Add to cart", priority = 11)
	public void Step11_EnterPartNumAndAddToCart() throws Exception {

		resellerCartPage = new ResellerCartPage(driver);

		int SKUCountInt = SKUdataSetList.size();

		SKUCount = Integer.toString(SKUCountInt);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());


			resellerCartPage.EnterValueTextbox(driver, SKUdataSetList.get(i)
					.getSKU()); 
			//           rsRequestAQuotePage.CopyPasteBOM(driver , SKUdataSetList.get(i).getSKU()); 

			log.info("Quick entry page is displayed \n");
			Reporter.log("<p>Quick entry page is displayed ");
		}
	}
	@Test(description = "Step 12: Click continue from the overlay", priority = 12)
	public void Step12_ClickOnContinueButton() throws Exception {

		resellerCartPage = new ResellerCartPage(driver);
        
		resellerCartPage.clickOnContinueButton();

		log.info("Quick entry page should be displayed\n");
		Reporter.log("<p>Quick entry page should be displayed ");
	}
	@Test(description = "Step 13: Verify the number of products in the cart", priority = 13)
	public void Step13_Verify_The_Number_Of_Products_In_The_Cart() throws Exception {

		resellerCartPage = new ResellerCartPage(driver);
        
		resellerCartPage.verify_Number_Of_Products_In_The_Cart();

		log.info("Products count should be matched according to the added partnumbers\n");
		Reporter.log("<p>Products count should be matched according to the added partnumbers ");
	}
	
}
