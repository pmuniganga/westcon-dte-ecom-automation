package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;
import in.valtech.westcon.ResellerPages.ResellerLoginPage;
import org.testng.Assert; 


public class TC21_Quoting_ConvertQuoteOrderByReseller_WithVRF extends BaseTest
{
	static ResellerLoginPage rsLoginPage;
	static String username = null;
	static String password = null;
	String PoNumber;
	String dir;
	/**
	 * User to Submit quote in quoting by executing TC-11
	 * 
	 */
	@Test(description = "Step 1: Call testcase to enter mandatory data", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		
		
		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");	
	}

	/**
	 * Click on VRF tab
	 * @throws Exception 
	 */
	@Test(description = "Step2 : Click on VRF tab", priority = 2)
	public void Step02_ClickVRFTab() throws Exception {
		//Click on VRF tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//GUIFunctions.pageScrollUP(driver, 10);
		quoteDetails.clickVRFTab();

		log.info("Successfully Clicked on VRF tab");
		Reporter.log("<p>Successfully Clicked on VRF tab");
	}

	/**
	 * Enter VRF Auth Number
	 */
	@Test(description = "Step3 : Enter VRF Auth Number", priority = 3)
	public void Step03_EnterVRFAuthNumber() {
		//Enter VRF Auth Number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.EnterVRFAuthNumber();

		log.info("Successfully Entered VRF Auth Number");
		Reporter.log("<p>Successfully Entered VRF Auth Number");
	}

	/**
	 * Click on Apply Button
	 */
	@Test(description = "Step4 : Click on Apply Button", priority = 4)
	public void Step04_ClickApplyButton() {
		//Click on Apply Button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.ClickApplybutton();

		log.info("Successfully Clicked on Apply Button");
		Reporter.log("<p>Successfully Clicked on Apply Button");
	}

	/**
	 * Enter VRF Reseller PO number and EndUser PO number
	 * @throws InterruptedException 
	 */
	@Test(description = "Step5 : Enter VRF field values", priority = 5)
	public void Step05_EnterVRFFieldValues() throws InterruptedException {
		//Enter VRF Reseller PO number and EndUser PO number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.EnterSingleVRFFieldValues();

		log.info("Successfully Entered VRF field values");
		Reporter.log("<p>Successfully Entered VRF field values");
	}

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority =6)
	public void Step06_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Click On No, do not validate button", priority = 7)
	public void Step07_ClickOnDoNotValidateButton1()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}

	/**
	 * Get Quote ID and Total Quote Price after Submitting the Quote
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 8: Get Quote ID and Total Quote Price", priority = 8)
	public void Step08_GetQuoteIDAndTotalPrice() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.getValues();
		
		log.info("Successfully fetched Quote ID and Quote total price\n");
		Reporter.log("<p>Successfully fetched Quote ID and Quote total price");

	}
	
	@Test(description = "Step 9: Click on Quotes tab", priority = 9)
	public void Step09_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);
		
		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");
	
	}
	
	@Test(description = "Step 10 : Search for the submitted quote", priority = 10)
	public void Step10_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);
		
		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}
	
	
	@Test(description = "Step 11 : Verify the total price", priority = 11)
	public void Step11_VerifyTotalPrice() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.verifyTotalQuotePrice(driver);
		
		log.info("Successfully verified the Quote total price\n");
		Reporter.log("<p>Successfully verified the Quote total price");
	}

	/**
	 * Navigate to reseller url	
	 *//*

	@Test(description = "Step 9: Open browser,Navigate to the reseeler URL", priority =9)
	public void Step09_NavigateToWestconReseller_URL() throws Exception 
	{
		String resellerUrl = userInfoDSDetails.getRsUrl();
		rsLoginPage = ResellerLoginPage.navigateTo_URL(driver, resellerUrl, driverName);
		log.info("Navigated to the Reseller URL");
		Reporter.log("<p>Navigated to Reseller Login Page URL -- "
				+ driver.getTitle());
	}

	*//**
	 * Enter username , Pwd and click on Login link	
	 *//*
	@Test(description = "Step 10: Enter username , Pwd and click on Login link", priority =10)
	public void Step10_ResellerLogin() throws Exception {

		username = userInfoDSDetails.getRsUserName();
		password = userInfoDSDetails.getRsPassword();

		ResellerLoginPage rsLoginPage = new  ResellerLoginPage(driver);
		//amLoginPage = new AMLogin(driver);
		rsLoginPage.enterLoginCreds(username, password);
		log.info("The Reseller successfully logged-in and SelfSerivice page is displayed\n");
		Reporter.log("<p>The Reseller successfully logged-in and SelfSerivice  page is displayed");

	}
	*//**
	 * Mouse hover on quotes and click on view processed quote request
	 *//*

	@Test(description = "Step 11:Mouse hover on quotes and click on view processed quote request ", priority = 11)
	public void Step11_MousehoverOnQuotesAndClickOnViewProcessedQuote() throws Exception 
	{
		ResellerSelfServicePage rsSelfServicePage = new  ResellerSelfServicePage(driver);
		rsSelfServicePage.clickOnViewProcessedQuoteRequests();
		log.info("Clicked on viewquoteRequestlink\n");
		Reporter.log("<p>TClicked on viewquoteRequestlink");
	}


	*//**
	 * Function to enter quote id in to text box
	 *//*
	@Test(description = "Step 12:Enter QuoteID in to input box ", priority = 12)
	public void Step12_EnterQuoteIdToTextBox() throws Exception 
	{
		System.out.println("quoteID="+quoteID);
		ResellerQuoteListingPage rsquotelistingpage=new ResellerQuoteListingPage(driver);
		CustomFun.waitForPageLoaded(driver);
		rsquotelistingpage.MyQuotes_EnterValueTextbox(quoteID);
		log.info("TC-63 Quote id entered\n");
		Reporter.log("<p>TC-63 Quote id entered");

	}
	*//**
	 * Function to press enter key
	 *//*
	@Test(description = "Step 13:Press enter key ", priority =13)
	public void Step13_HitEnterKey() throws Exception 
	{
		System.out.println("quoteID="+quoteID);
		ResellerQuoteListingPage rsquotelistingpage=new ResellerQuoteListingPage(driver);
		rsquotelistingpage.HitEnterKey();
		log.info("TC-63 Preseed enter key\n");
		Reporter.log("<p>TC-63 Preseed enter key");

	}

	*//**
	 * Function to hit on quoteid
	 * 
	 *//*
	@Test(description = "Step 14:Click on quote id ", priority =14)
	public void Step14_ClickQuoteId() throws Exception 
	{
		ResellerQuoteListingPage rsquotelistingpage=new ResellerQuoteListingPage(driver);
		rsquotelistingpage.ClickonQuoteID();
		log.info("TC-63 Clicked on quoteid\n");
		Reporter.log("<p>TC-63 Clicked on quoteid");


	}

	*//**
	 * Function to select convert to order option from action drop down
	 *//*
	@Test(description = "Step 15:Click on quote id ", priority =15)
	public void Step15_SelectConvertToOrderOption() throws Exception 
	{
		//String dropDown=TextProp.getProperty("convertToOrder");		
		Reseller_QuoteDeatilPage rsQuoteDetailPage=new Reseller_QuoteDeatilPage(driver);
		rsQuoteDetailPage.SelectValueFromActionDropdown("convOrder");
		log.info("TC-63 Convert to order option selected\n");
		Reporter.log("<p>TC-63 Convert to order option selected");

	}
	*//**
	 * Function to click on submit button
	 *//*
	@Test(description = "Step 16:click on submit button ", priority =16)
	public void Step16_SubmitButtonClick() throws Exception 
	{
		Reseller_QuoteDeatilPage rsQuoteDetailPage=new Reseller_QuoteDeatilPage(driver);
		rsQuoteDetailPage.SubmitClick();
		log.info("TC-63 Submit button clicked\n");
		Reporter.log("<p>TC-63 Submit button clicked");

	}
	*//**
	 * Function to enter PO number
	 *//*


	@Test(description = "Step 17:click on submit button ", priority =17)
	public void Step17_EnterPoNumber() throws Exception 
	{
		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		PoNumber=userInfoDSDetails.getPoNumber();
		rsconverttoorder.EnterPoNumber(PoNumber);
		log.info("TC-63 PO number entered\n");
		Reporter.log("<p>TC-63 PO number entered");

	}

	*//**
	 * Browse file and Click on Submit button 
	 *//*
	@Test(description = "Step18: Browse file ", priority = 18)
	public void Step18_BrowseFile() throws Exception {
		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		//Click on Browse button
		rsconverttoorder.ClickOnBrowseButton();
		//upload file 
		Thread.sleep(5000);

		log.info("Successfully uploaded a PO file");
		Reporter.log("<p>--Successfully uploaded a PO file");
	}	


	*//**
	 * To Select terms and condition check box
	 *//*
	@Test(description="steps19: To fetch sales order number",priority = 19)
	public void Step19_checkTermsAndConditionCheckbox()throws Exception
	{

		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		rsconverttoorder.checkTermsAndConditionCheckBox();

		log.info("Successfully checked terms and conditions checkbox\n");
		Reporter.log("<p>--Successfully checked terms and conditions checkbox");
	}

	*//**
	 *Click on submit
	 *//*
	@Test(description="steps20: Click on submit button",priority = 20)
	public void Step20_ClickSubmitButton()throws Exception
	{

		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		rsconverttoorder.ClickOnSubmit();
		log.info("SUbmit button clicked");
		Reporter.log("<p>--SUbmit button clicked");
	}


	*//**
	 * To fetch sales order number
	 *//*
	@Test(description="steps 21: To fetch sales order number",priority = 21)
	public void Step21_salesOrderNumber()throws Exception
	{
		Reseller_QuoteToOrderConfirmationPage rsquotetoorderconfirmation=new Reseller_QuoteToOrderConfirmationPage(driver);
		rsquotetoorderconfirmation.FetchOrderNumber();
		log.info("Sales order number displayed");
		Reporter.log("<p>--Sales order number displayed");
	}

	*//**
	 * mouse hover on order history
	 *//*
	@Test(description="steps 22: Mouse hover order history",priority = 22)

	public void Step22_MouseHoverOnOrderHistory()throws Exception
	{
		Reseller_QuoteToOrderConfirmationPage rsquotetoorderconfirmation=new Reseller_QuoteToOrderConfirmationPage(driver);
		rsquotetoorderconfirmation.mouseHoverOnOrderHistory();
		log.info("Mouse hovered on order history link");
		Reporter.log("<p>--Mouse hovered on order history link");
	}


	*//**
	 * Click on view open and closed order link
	 *//*
	@Test(description="steps 23: Click on open and closed order",priority = 23)

	public void Step24_ClickOnOpenAndClosedOrderink()throws Exception
	{
		Reseller_QuoteToOrderConfirmationPage rsquotetoorderconfirmation=new Reseller_QuoteToOrderConfirmationPage(driver);
		rsquotetoorderconfirmation.clickOnViewOpenAndClosedOrderLink();
		log.info("Clicked on open and closed order link");
		Reporter.log("<p>--Clicked on open and closed order link");
	}

	*//**
	 * To select sales order number from drop down
	 *//*

	@Test(description="steps 25: Select slaes order number from drop down  Enter sales order id and perform search",priority = 25)

	public void Step25_SelectSalesOrderNumberInDropdown()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);

		rsOrderlistingpage.searchCreatedOrder(Reseller_QuoteToOrderConfirmationPage.salesorderNumber);
		log.info("Selected slaes order number from drop down");
		Reporter.log("<p>--Selected slaes order number from drop down");
	}

	*//**
	 * To click on order id
	 *//*
	@Test(description="steps 26: click on order id",priority = 26)
	public void Step26_ClickOnOrderNo()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);

		rsOrderlistingpage.ClickOrderId();
		log.info("Clicked on order no");
		Reporter.log("<p>--Clicked on order no");
	}

	//To compare VRF Authorization no
	@Test(description="steps 27: To compare VRf fileds",priority = 27)
	public void Step27_CompareVrfFields()throws Exception
	{

		Reseller_OrderDetailPage rsOrderDetailPage=new Reseller_OrderDetailPage(driver);
		String authNumber = rsOrderDetailPage.FetchAutorizationNo();
		Assert.assertEquals(authNumber, TextProp.getProperty("VRFAuthNumber"),"Vrf authorization not matching");
		log.info("VRF authorization number matching");
		Reporter.log("<p>--VRF authorization number matching");
		Thread.sleep(10000);

		//GUIFunctions.pageScrollDown(driver, 300);
		Thread.sleep(2000);
		rsOrderDetailPage.ExpandFirstLineItem();
		log.info("First line item expanded..");
		Reporter.log("<p>--First line item expanded");
		Thread.sleep(5000);

		String euPoNumber = rsOrderDetailPage.FetchEndUserPONumber();
		String rsPoNumber = rsOrderDetailPage.FetchResellerPONumber();

		System.out.println("###########"+euPoNumber);		
		System.out.println("###########"+rsPoNumber);		

		Assert.assertEquals(euPoNumber, TextProp.getProperty("vrfEnduserPONumberField"),"End User PO Number not matching");
		log.info("End User PO Number matching");
		Reporter.log("<p>--End User PO Number matching");

		Assert.assertEquals(rsPoNumber, TextProp.getProperty("vrfResellerPONumberField"),"Reseller PO Number not matching");
		log.info("Reseller PO Number matching");
		Reporter.log("<p>--Reseller PO Number matching");

	}*/

}