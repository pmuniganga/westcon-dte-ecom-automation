package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.List;

import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC15_Quoting_Search_AddProducts extends BaseTest {

	//static QuotingQuoteDetailpage amQuoteDetailsPage;
	List<WebElement> partnumbers=null;
	/**
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");			

		log.info("Create Quote page is displayed ");
		Reporter.log("<p>Create Quote page is displayed ");
	}
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Click on search for products icon above the quote table", priority = 2)
	public void Step02_SearchForProducts()throws Exception {

		//Click on Search for Products Icon above quote table
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver,1000);
		amQuoteDetailsPage.ClickOnProductIcon(driver);
		amQuoteDetailsPage.verifyEnterPartNumberTab(driver);

		log.info("Successfully Add product overlay is displayed with EnterPartnumer tab default selected\n");
		Reporter.log("<p>Successfully Add product overlay is displayed with EnterPartnumer tab default selected\n");

	}

	@Test(description = "Step 3: Enter PartNumbers in Add product modal and Click Submit", priority = 3)
	public void Step03_EnterPartNumbersAndClickSubmit() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		Thread.sleep(10000);
		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			System.out.println(SKUdataSetList.get(i).getSKU());
			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());

			amQuoteDetailsPage.enterPartNumber(driver , TextProp
					.getProperty("partNumberValue")); 
			amQuoteDetailsPage.PartNumberTab_ClickOnAdd(driver);
			
		} 	  
		log.info("Successfully Added product through Search AddPRoduct modal and Quote detail page is displayed");
		Reporter.log("<p>Successfully Added product through Search AddPRoduct modal and Quote detail page is displayed");

	}  
	
	
	@Test(description = "Step 4: Verify the product in the quote table", priority = 4)
	public void Step04_VerifyInQuoteTable() throws Exception {

		
		Thread.sleep(6000);
		partnumbers = driver.findElements(By.xpath(ObjRepoProp.getProperty("partNumberTableValue_XPATH")));		
		
		System.out.println(partnumbers.get(0).getText());
			System.out.println(partnumbers.get(1).getText());
		Assert.assertEquals(partnumbers.get(1).getText(), TextProp
				.getProperty("partNumberValue"),
				"Partnumber mismatch");			
	
		Thread.sleep(10000);

		log.info("Successfully Verified the added product in Quote table");
		Reporter.log("<p>Successfully Verified the added product in Quote table");

	}  

	@Test(description = "Step 5: Click on search for products icon above the quote table", priority = 5)
	public void Step05_SearchForProducts()throws Exception {

		//Click on Search for Products Icon above quote table
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);	
		//GUIFunctions.pageScrollUP(driver,100);
		amQuoteDetailsPage.ClickOnProductIcon(driver);
		amQuoteDetailsPage.verifyEnterPartNumberTab(driver);

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	@Test(description = "Step 6: Select browse catalog tab, enter part number and click enter", priority = 6)
	public void Step06_SelectBrowseCatalogTab_enterPartNoandSumbit() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.ClickOnBrowseCatalog(driver);

		amQuoteDetailsPage.browseCatalog_SearchForPartNumber(driver , TextProp
				.getProperty("partNumberValue1"));

		log.info("successfully Selected browse catalog tab and Added partnumber");
		Reporter.log("<p>successfully Selected browse catalog tab and Added partnumber");

	} 	

	@Test(description = "Step 7: Select the checkbox for the part number and click on Add button", priority = 7)
	public void Step07_SelectCheckBoxPartNoandAdd() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.BrowseCatalog_ClickOnProductCheckbox(driver);		
		amQuoteDetailsPage.BrowseCatalog_ClickOnAdd(driver);
		Thread.sleep(5000);

		log.info("successfully submitted the part numbers and Quote detail page is displayed");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");

	} 

	@Test(description = "Step 8: Verify the product in the quote table", priority = 8)
	public void Step08_VerifyInQuoteTable() throws Exception {
		partnumbers = driver.findElements(By.xpath(ObjRepoProp.getProperty("partNumberTableValue_XPATH")));	
		System.out.println("Part Numbers count = "+partnumbers.size());
		Thread.sleep(10000);
		Assert.assertEquals(partnumbers.get(2).getText(), TextProp
				.getProperty("partNumberValue1"),
				"Partnumber mismatch");
		Thread.sleep(10000);
		log.info("successfully submitted the part numbers and Quote detail page is displayed");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");

	} 

	@Test(description = "Step 9: Verify the number of products in the quote table", priority = 9)
	public void Step09_VerifyNoOfProductsInTabe() throws Exception {
		partnumbers = driver.findElements(By.xpath(ObjRepoProp.getProperty("partNumberTableValue_XPATH")));		
		Thread.sleep(2000);
		Assert.assertEquals(partnumbers.size(), Integer.parseInt(TextProp
				.getProperty("partNumbercount")),
				"Partnumber count mismatch");
		Thread.sleep(5000);
		log.info("successfully submitted the part numbers and Quote detail page is displayed");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");

	} 


}
