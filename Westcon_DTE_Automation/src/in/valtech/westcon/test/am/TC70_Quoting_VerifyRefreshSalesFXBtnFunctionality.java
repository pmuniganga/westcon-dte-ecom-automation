/**
 * 
 */
package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * @author Deepak.tripathi
 *
 */
public class TC70_Quoting_VerifyRefreshSalesFXBtnFunctionality extends BaseTest{

	String salesFxRate=null;
	
	@Test(description = "Step 1: User to submit quote  by executing TC11", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		

		log.info("Quote details page is displayed with Part Number added");
		Reporter.log("<p>Quote details page is displayed with Part Number added");	
	}
	
	
	/**
	 * 
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Click on Refresh Sales Fx button", priority =2)
	public void Step02_ClickOnRefreshSalesFxBtn() throws Exception {
		GUIFunctions.pageScrollDown(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);				
	
		amQuoteDetailsPage.ClickOnRefreshSalesFXBtn();
		
		log.info("Successfully \n");
		Reporter.log("<p>Successfully \n");

	}
	
	/**
	 * 
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Verify Copy Quote Overlay", priority =4)
	public void Step04_VerifyCopyQuoteOverlay() throws Exception {
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("RefreshSalesFxOverlayMsg_XPATH"))));
		
		System.out.println("Refresh Sales Fx model message:"+driver.findElement(By.xpath(ObjRepoProp
						.getProperty("RefreshSalesFxOverlayMsg_XPATH"))).getText().trim());

		salesFxRate=driver.findElement(By.xpath(ObjRepoProp.getProperty("RefreshSalesFxOverlayMsg_XPATH"))).getText().replaceAll("[a-zA-Z,?']", "").trim();
		System.out.println("Today's Sales Fx Rate***"+salesFxRate);
		
		Assert.assertTrue(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("RefreshSalesFxOverlayMsg_XPATH"))).getText().trim().
				contains(TextProp.getProperty("RefreshSalesFxOverlayMsg1")),
				"Refresh Sales Fx Overlay Msg not proper");
		
		Assert.assertTrue(GUIFunctions.isElementPresent(By.xpath(ObjRepoProp.getProperty("RefreshSalesFxOverlayYesBtn_XPATH")), driver), "Create Quote Copy Btn not displayed");
		Assert.assertTrue(GUIFunctions.isElementPresent(By.xpath(ObjRepoProp.getProperty("RefreshSalesFxOverlayNoBtn_XPATH")), driver), "Copy Quote Cancel Btn not displayed");
		
		log.info("Successfully Verified Copy Quote Overlay details\n");
		Reporter.log("<p>Successfully Verified Copy Quote Overlay details\n");

	}
	
	/**
	 * Click on 'Create Quote copy' button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Click on 'Yes' button", priority = 5)
	public void Step05_ClickOnYesButton()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnRefreshSalesFxOverlayYesBtn();
		
		log.info("Successfully clicked on 'Yes' button in Refresh Sales FX overlay\n");
		Reporter.log("<p>Successfully clicked on 'Yes' button in Refresh Sales FX overlay\n");

	}
	
	/**
	 * Verify Quote details after copying the Quote
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Verify  Sales FX Rate & Sales FX Date in Project Overview section", priority = 6)
	public void Step06_VerifySalesFXRateAndSalesFXDate() throws Exception
	{
		String expSalesFxDate=CustomFun.addDays(new Date(),0, locale);
		System.out.println("Expected Date::"+expSalesFxDate);
		
		//Verify Quote Sales Rate and Date in Project Overview section
		System.out.println("Project Overview Section Sales Fx Rate****"+driver.findElement(By.xpath(ObjRepoProp
				.getProperty("ProjectOverviewSecSalesFxRate_XPATH"))).getText().trim());
    	System.out.println("Sales Fx Date****"+driver.findElement(By.xpath(ObjRepoProp.
    			getProperty("ProjectOverviewSecSalesFxDate_XPATH"))).getText().trim());

 		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("ProjectOverviewSecSalesFxDate_XPATH"))).getText().trim(),
 				expSalesFxDate,"Sales Fx Date mismatch");		
 		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("ProjectOverviewSecSalesFxRate_XPATH"))).getText().trim(),
 				salesFxRate,"Sales Fx Rate mismatch");
        
		log.info("Successfully verified Sales FX Rate & Sales FX Date in Project Overview section\n");
		Reporter.log("<p>Successfully verified Sales FX Rate & Sales FX Date in Project Overview section");

	}
}
