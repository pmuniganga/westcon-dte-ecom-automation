package in.valtech.westcon.ResellerPages;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Reseller_ConvertToOrderPage {

	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller Convert to order option
	public Reseller_ConvertToOrderPage(WebDriver driver) throws InterruptedException {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right convert to order page.

		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("ConvertToOrderPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not convert to order page");

		}
	}

	//This function is to enter PO number

	By poNoInputbox=By.xpath(ObjRepoProp.getProperty("poNoInputbox_XPATH"));
	public Reseller_ConvertToOrderPage EnterPoNumber(String PoNumber)throws Exception
	{
		//To select PO number
		GUIFunctions.typeTxtboxValue(driver, poNoInputbox, PoNumber);
		return new Reseller_ConvertToOrderPage(driver);
	}

	//This function is to click on browse button
	By browseButton=By.xpath(ObjRepoProp.getProperty("browseButtonConvert_xpath"));
	By closeButton=By.xpath(ObjRepoProp.getProperty("closeButton_XPATH"));
	public Reseller_ConvertToOrderPage ClickOnBrowseButton() throws Exception{
		//Click on upload file

//		String dir = CustomFun.getRootDir();	
//		for (int i = 0; i < SKUdataSetList.size(); i++) {
//			String path = dir+SKUdataSetList.get(i).getVendorFilePath();
//			System.out.println("file path --- "+path);
//			driver.findElement(browseButton).sendKeys(path);
//		}
		
		String path = null;		
		String dir = CustomFun.getRootDir();	
		System.out.println(dir);				
		for (int i = 0; i < SKUdataSetList.size(); i++) {
			path = dir+SKUdataSetList.get(i).getVendorFilePath();
			System.out.println(path);			
		}
		if (BaseTest.driverName.equalsIgnoreCase("FF")
				|| BaseTest.driverName.equalsIgnoreCase("CHROME"))
		{			
			driver.findElement(browseButton).sendKeys(path);
		}else {
			// For IE
			WebElement browseBtn =driver.findElement(browseButton);			
			//browseBtn.sendKeys(path);
			browseBtn.sendKeys("D:\\Westcon_Auto_Workspace\\Westcon DTE Automation\\resources\\Bartercard Group Series Options.xlsx");
			//GUIFunctions.clickElement(driver, browseButton_xpath," Browse button ");	
		}	
		
		GUIFunctions.clickElement(driver, closeButton," Close button ");		
		return new Reseller_ConvertToOrderPage(driver);
	}

	//This function is to check terms and condition checkbox
	By TermsAndConditionCheckBox=By.xpath(ObjRepoProp.getProperty("TermsAndConditionCheckBox_XPATH"));
	public Reseller_ConvertToOrderPage checkTermsAndConditionCheckBox()throws Exception
	{
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(TermsAndConditionCheckBox));
		GUIFunctions.pageScrollDown(driver, 200);
		Thread.sleep(5000);
		GUIFunctions.checkOrUncheckCheckBox(driver, TermsAndConditionCheckBox, "Terms and condition check box", true);
		return new Reseller_ConvertToOrderPage(driver);	
	}

	//This function is to click on submit and fetch order id from order confirmation page
	By submitButtoninOrderconversion=By.xpath(ObjRepoProp.getProperty("submitButtoninOrderconversion_XPATH"));
	public Reseller_QuoteToOrderConfirmationPage ClickOnSubmit()throws Exception
	{
		GUIFunctions.pageScrollDown(driver, 200);
		GUIFunctions.clickElement(driver, submitButtoninOrderconversion, "Submit button clicked");
		CustomFun.waitForPageLoaded(driver);
		Thread.sleep(5000);
		return new Reseller_QuoteToOrderConfirmationPage(driver);	
	}


}
