package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.ResellerPages.ResellerLoginPage;
import in.valtech.westcon.ResellerPages.ResellerOrderListingPage;
import in.valtech.westcon.ResellerPages.ResellerQuoteListingPage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;
import in.valtech.westcon.ResellerPages.Reseller_ConvertToOrderPage;
import in.valtech.westcon.ResellerPages.Reseller_OrderDetailPage;
import in.valtech.westcon.ResellerPages.Reseller_QuoteDeatilPage;
import in.valtech.westcon.ResellerPages.Reseller_QuoteToOrderConfirmationPage;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import org.testng.Assert; 


public class TC23_Quoting_ConvertQuoteOrderByReseller extends BaseTest
{
	static ResellerLoginPage rsLoginPage;
	static String username = null;
	static String password = null;
	String PoNumber;
	String dir;
	/**
	 * User to Submit quote in quoting by executing TC-11
	 * 
	 */

	@Test(description = "Step 1: User to submit quote  by executing TC09", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		
		// Execute TC-08 (without Opportunity)
		log.info("TC-09 Successfully Executed \n");
		Reporter.log("<p>TC-09 Successfully Eexecuted ");	

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");	
	}

	/**
	 * Navigate to reseller url	
	 */

	@Test(description = "Step 2: Open browser,Navigate to the reseeler URL", priority =2)
	public void Step02_NavigateToWestconReseller_URL() throws Exception 
	{
		String resellerUrl = userInfoDSDetails.getRsUrl();
		rsLoginPage = ResellerLoginPage.navigateTo_URL(driver, resellerUrl, driverName);
		log.info("Navigated to the Reseller URL");
		Reporter.log("<p>Navigated to Reseller Login Page URL -- "
				+ driver.getTitle());
	}

	/**
	 * Enter username , Pwd and click on Login link	
	 */
	@Test(description = "Step 3: Enter username , Pwd and click on Login link", priority =3)
	public void Step03_ResellerLogin() throws Exception {

		username = userInfoDSDetails.getRsUserName();
		password = userInfoDSDetails.getRsPassword();

		ResellerLoginPage rsLoginPage = new  ResellerLoginPage(driver);
		//amLoginPage = new AMLogin(driver);
		rsLoginPage.enterLoginCreds(username, password);
		log.info("The Reseller successfully logged-in and SelfSerivice page is displayed\n");
		Reporter.log("<p>The Reseller successfully logged-in and SelfSerivice  page is displayed");

	}
	/**
	 * Mouse hover on quotes and click on view processed quote request
	 */

	@Test(description = "Step 4:Mouse hover on quotes and click on view processed quote request ", priority = 4)
	public void Step04_MousehoverOnQuotesAndClickOnViewProcessedQuote() throws Exception 
	{
		ResellerSelfServicePage rsSelfServicePage = new  ResellerSelfServicePage(driver);
		rsSelfServicePage.clickOnViewProcessedQuoteRequests();
		log.info("Clicked on viewquoteRequestlink\n");
		Reporter.log("<p>TClicked on viewquoteRequestlink");
	}


	/**
	 * Function to enter quote id in to text box
	 */
	@Test(description = "Step 5:Enter QuoteID in to input box ", priority = 5)
	public void Step05_EnterQuoteIdToTextBox() throws Exception 
	{
		System.out.println("quoteID="+quoteID);
		ResellerQuoteListingPage rsquotelistingpage=new ResellerQuoteListingPage(driver);
		CustomFun.waitForPageLoaded(driver);
		rsquotelistingpage.MyQuotes_EnterValueTextbox(quoteID);
		log.info("TC-63 Quote id entered\n");
		Reporter.log("<p>TC-63 Quote id entered");

	}
	/**
	 * Function to press enter key
	 */
	@Test(description = "Step 6:Press enter key ", priority =6)
	public void Step06_HitEnterKey() throws Exception 
	{
		System.out.println("quoteID="+quoteID);
		ResellerQuoteListingPage rsquotelistingpage=new ResellerQuoteListingPage(driver);
		rsquotelistingpage.HitEnterKey();
		Thread.sleep(5000);
		log.info("TC-63 Preseed enter key\n");
		Reporter.log("<p>TC-63 Preseed enter key");
	}

	/**
	 * Function to hit on quoteid
	 * 
	 */
	@Test(description = "Step 7:Click on quote id ", priority =7)
	public void Step07_ClickQuoteId() throws Exception 
	{
		ResellerQuoteListingPage rsquotelistingpage=new ResellerQuoteListingPage(driver);
		rsquotelistingpage.ClickonQuoteID();
		log.info("TC-63 Clicked on quoteid\n");
		Reporter.log("<p>TC-63 Clicked on quoteid");
	}

	/**
	 * Function to select convert to order option from action drop down
	 */
	@Test(description = "Step 8:Click on quote id ", priority =8)
	public void Step08_SelectConvertToOrderOption() throws Exception 
	{
		//String dropDown=TextProp.getProperty("convertToOrder");		
		Reseller_QuoteDeatilPage rsQuoteDetailPage=new Reseller_QuoteDeatilPage(driver);
		rsQuoteDetailPage.SelectValueFromActionDropdown("convOrder");
		log.info("TC-63 Convert to order option selected\n");
		Reporter.log("<p>TC-63 Convert to order option selected");
	}

	/**
	 * Function to click on submit button
	 */
	@Test(description = "Step 9:click on submit button ", priority =9)
	public void Step09_SubmitButtonClick() throws Exception 
	{
		Reseller_QuoteDeatilPage rsQuoteDetailPage=new Reseller_QuoteDeatilPage(driver);
		rsQuoteDetailPage.SubmitClick();
		log.info("TC-63 Submit button clicked\n");
		Reporter.log("<p>TC-63 Submit button clicked");
	}

	/**
	 * Function to enter PO number
	 */
	@Test(description = "Step 10:click on submit button ", priority =10)
	public void Step10_EnterPoNumber() throws Exception 
	{
		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		PoNumber=userInfoDSDetails.getPoNumber();
		rsconverttoorder.EnterPoNumber(PoNumber);
		log.info("TC-63 PO number entered\n");
		Reporter.log("<p>TC-63 PO number entered");
	}

	/**
	 * Browse file and Click on Submit button 
	 */
	@Test(description = "Step11: Browse file ", priority = 11)
	public void Step11_BrowseFile() throws Exception {
		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		//Click on Browse button
		rsconverttoorder.ClickOnBrowseButton();
		//upload file 
		Thread.sleep(5000);

		log.info("Successfully uploaded a PO file");
		Reporter.log("<p>--Successfully uploaded a PO file");
	}	

	/**
	 * To Select terms and condition check box
	 */
	@Test(description="steps12: To fetch sales order number",priority = 12)
	public void Step12_checkTermsAndConditionCheckbox()throws Exception
	{

		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		rsconverttoorder.checkTermsAndConditionCheckBox();

		log.info("Successfully checked terms and conditions checkbox\n");
		Reporter.log("<p>--Successfully checked terms and conditions checkbox");
	}

	/**
	 *Click on submit
	 */
	@Test(description="steps13: Click on submit button",priority = 13)
	public void Step13_ClickSubmitButton()throws Exception
	{

		Reseller_ConvertToOrderPage rsconverttoorder=new Reseller_ConvertToOrderPage(driver);
		rsconverttoorder.ClickOnSubmit();
		log.info("SUbmit button clicked");
		Reporter.log("<p>--SUbmit button clicked");
	}


	/**
	 * To fetch sales order number
	 */
	@Test(description="steps14: To fetch sales order number",priority = 14)
	public void Step14_salesOrderNumber()throws Exception
	{
		Reseller_QuoteToOrderConfirmationPage rsquotetoorderconfirmation=new Reseller_QuoteToOrderConfirmationPage(driver);
		rsquotetoorderconfirmation.FetchOrderNumber();
		log.info("Sales order number displayed");
		Reporter.log("<p>--Sales order number displayed");
	}

	/**
	 * mouse hover on order history
	 */
	@Test(description="steps15: Mouse hover order history",priority = 15)

	public void Step15_MouseHoverOnOrderHistory()throws Exception
	{
		Reseller_QuoteToOrderConfirmationPage rsquotetoorderconfirmation=new Reseller_QuoteToOrderConfirmationPage(driver);
		CustomFun.waitForPageLoaded(driver);
		rsquotetoorderconfirmation.mouseHoverOnOrderHistory();
		log.info("Mouse hovered on order history link");
		Reporter.log("<p>--Mouse hovered on order history link");
	}


	/**
	 * Click on view open and closed order link
	 */
	@Test(description="steps16: Click on open and closed order",priority = 16)

	public void Step16_ClickOnOpenAndClosedOrderink()throws Exception
	{
		Reseller_QuoteToOrderConfirmationPage rsquotetoorderconfirmation=new Reseller_QuoteToOrderConfirmationPage(driver);
		rsquotetoorderconfirmation.clickOnViewOpenAndClosedOrderLink();
		log.info("Clicked on open and closed order link");
		Reporter.log("<p>--Clicked on open and closed order link");
	}

	/**
	 * To select sales order number from drop down
	 */

	@Test(description="steps17: Select slaes order number from drop down  Enter sales order id and perform search",priority = 17)

	public void Step17_SelectSalesOrderNumberInDropdown()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);

		rsOrderlistingpage.searchCreatedOrder(Reseller_QuoteToOrderConfirmationPage.salesorderNumber);
		log.info("Selected slaes order number from drop down");
		Reporter.log("<p>--Selected slaes order number from drop down");
	}

	/**
	 * To click on order id
	 */
	@Test(description="steps18: click on order id",priority = 18)

	public void Step18_ClickOnOrderNo()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);

		rsOrderlistingpage.ClickOrderId();
		log.info("Clicked on order no");
		Reporter.log("<p>--Clicked on order no");
	}

	//To Verify order details
	@Test(description="steps19: Verify Order details",priority = 19)

	public void Step19_VerifyOrderDetails()throws Exception
	{

		Reseller_OrderDetailPage rsOrderDetailPage=new Reseller_OrderDetailPage(driver);

		//Verifying Sales order number
		String salesOrderNoInConfirmation = rsOrderDetailPage.FetchSalesOrderNo();		
		Assert.assertEquals(salesOrderNoInConfirmation, 
				Reseller_QuoteToOrderConfirmationPage.salesorderNumber, "Sales order number not matching");

		log.info("Sales order number matching");
		Reporter.log("<p>--Sales order number matching");
		Thread.sleep(5000);

		//Verifying EndUser Name	
		String endUserName = rsOrderDetailPage.FetchEndUserName();		
		Assert.assertEquals(endUserName,userInfoDSDetails.getEndUser(), "End User name does not match");		
		log.info("End User name matching");
		Reporter.log("<p>--End User name matching");
		Thread.sleep(5000);

		GUIFunctions.pageScrollDown(driver, 300);

		//Verifying ExtendedPrice
		String extendedPrice = rsOrderDetailPage.FetchExtendedPrice();		
		Assert.assertEquals(extendedPrice,QuotingQuoteDetailpage.totalQuotePrice, "Extended Price does not match");	
		log.info("Extended Price matching");
		Reporter.log("<p>--Extended Price matching");
		Thread.sleep(5000);				
	}
}
