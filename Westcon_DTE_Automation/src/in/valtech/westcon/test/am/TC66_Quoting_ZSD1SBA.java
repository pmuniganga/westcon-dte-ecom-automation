package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;
import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC66_Quoting_ZSD1SBA extends BaseTest {

	String manufacturerListPriceBeforeSBA;
	String purchasePriceBeforeSBA;
	String vendorDiscountBeforeSBA;
	String SKU;
	String sbaRefNumber;
	String manufacturerListPriceAfterSBA;
	String purchasePriceAfterSBA;
	String vendorDiscountAfterSBA;
	String AutoSBADiscount;
	String[] AutoSBADiscountvalue;
	String ManualSBADiscount;
	String[] ManualSBADiscountvalue;

	String[] skuNumber;
	String[] actualMListBeforeSBA;
	String[] actualVDiscountBeforeSBA;
	String[] actualWestCostBeforeSBA;

	String[] actualMListAfterSBA;
	String[] actualVDiscountAfterSBA;
	String[] actualWestCostAfterSBA;

	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step1 : Call TC06 which selects the End User and all mandatory fields filled", priority = 1)
	public void Step01_NavigateToTheCreateQuotePageWithAllMandatoryInfo() throws Exception {

		//Execute TC06 to navigate to Create Quote page with End User linked and all mandatory fields filled
		// Create Quote page is displayed with End user selected and all mandatory fields filled

		log.info("Create Quote page is displayed with End user selected and all mandatory fields filled");
		Reporter.log("<p>Create Quote page is displayed with End user selected and all mandatory fields filled-- "
				+ driver.getTitle());			
	}

	/**
	 * Check the pricing condition for Manual / Auto SBA
	 * @throws Exception 
	 */
	@Test(description = "Step6 : Check the pricing condition for Manual / Auto SBA", priority = 6)
	public void Step06_CheckPricingConditions() throws Exception {
		//Checking for Pricing Conditions

		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickPricingTab();

		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());
			System.out.println("SKUdataSetList.get(i).getSbaType() = "+SKUdataSetList.get(i).getSbaType());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL") && SKUdataSetList.get(i).getSbaType().equals("ZSD0")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto");
			}
			
			else if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText()
						, "None");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto","Improper pricing condition");
			}
			
			

		}
		log.info("Verified pricing condition\n");
		Reporter.log("<p>Verified pricing condition");
	}

	/**
	 * Save the values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step7 : Save the values like vendor discount, list price and purchase price", priority = 7)
	public void Step07_SaveTableValues() {
		//Save the table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListBeforeSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountBeforeSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostBeforeSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceBeforeSBA = "+actualMListBeforeSBA[1]);
		System.out.println("vendorDiscountBeforeSBA = "+actualVDiscountBeforeSBA[0]);
		System.out.println("purchasePriceBeforeSBA = "+actualWestCostBeforeSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");
	}

	/**
	 * Click on SBA tab
	 */
	@Test(description = "Step8 : Click on SBA tab", priority = 8)
	public void Step08_ClickSBATab() {
		//Click on SBA tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBATab();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaTabSearchLink_XPATH"))).getText().trim(),
				TextProp.getProperty("SBATabData"));

		log.info("Successfully clicked on SBA tab and SBA tab is opened");
		Reporter.log("<p>Successfully clicked on SBA tab and SBA tab is opened");
	}
	
	/**
	 * Click on Auto SBA link
	 */
	@Test(description = "Step9 : Click on Auto SBA and Save Discount", priority = 9)
	public void Step09_ClickAutoSBALinkAndSaveDiscount() {
		//Click on Auto SBA link
		
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickAppliedAutoSBA();
		
		AutoSBADiscount = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsoverlayDiscount_XPATH")))
				.getText();

		AutoSBADiscountvalue = AutoSBADiscount.split("-");
		//sbaDiscount = sbaDiscount.substring(1,sbaDiscount.length()-1);
		System.out.println("sbaDiscountvalue = "+AutoSBADiscountvalue[1]);

		log.info("Successfully saved SBA discount");
		Reporter.log("<p>Successfully saved SBA discount");	
		
	}
	
	/**
	 * Click on Cancel button
	 */
	@Test(description = "Step10 : Click Cancel button", priority = 10)
	public void Step10_ClickCancelButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Cancel button
		quoteDetails.clickCancelButton();

		log.info("Successfully closed the overlay");
		Reporter.log("<p>Successfully closed the overlay");
	}
	
	
	
	/**
	 * Click on SBA search link
	 */
	@Test(description = "Step11 : Click on SBA search link", priority = 11)
	public void Step11_ClickSBASearchLink() {
		//Click on SBA search link
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBASearchLink();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaSearchOverlayHeader_XPATH"))).getText().trim(), 
				TextProp.getProperty("SBASearchOverlayHeader"));

		log.info("Successfully clicked on SBA search link and SBA search overlay is opened");
		Reporter.log("<p>Successfully clicked on SBA search link and SBA search overlay is opened");
	}

	/**
	 * Enter part number and Click on Search button
	 * @throws InterruptedException 
	 */
	@Test(description = "Step12 : Enter part number and Search SBA", priority = 12)
	public void Step12_EnterAndSearchSBA() throws InterruptedException {
		//Enter Part NumbersbaTable_XPATH
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			SKU = SKUdataSetList.get(i).getSKU();
		}
		quoteDetails.enterPartNumber(SKU);
		log.info("Successfully entered part number");
		Reporter.log("<p>Successfully entered part number");

		//Search for SBA
		quoteDetails.clickSearchButton();

		log.info("Successfully clicked on search button and SBA table is displayed");
		Reporter.log("<p>Successfully clicked on search button and SBA table is displayed");
	}

	/**
	 * Select the SBA
	 * @throws InterruptedException 
	 */
	@Test(description = "Step13 : Select SBA", priority = 13)
	public void Step13_SelectSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Select the SBA displayed in the table
		Thread.sleep(5000);
		quoteDetails.selectSBA();
		log.info("Successfully selected SBA in SBA table");
		Reporter.log("<p>Successfully selected SBA in SBA table");
	}

	/**
	 * Apply the selected SBA
	 */
	@Test(description = "Step14 : Apply the selected SBA and verify in SBA tab", priority = 14)
	public void Step14_ClickApplyButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Select the SBA displayed in the table

		quoteDetails.applySBA();
		log.info("Successfully applied the SBA");
		Reporter.log("<p>Successfully applied the SBA");
		CustomFun.waitObjectToLoad(driver, By.xpath(ObjRepoProp
				.getProperty("appliedSBAInTable_XPATH")), 60);

		//Verify the applied SBA in SBA table
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			sbaRefNumber = SKUdataSetList.get(i).getSbaRefNumber();
		}

		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("appliedSBAInTable_XPATH"))).getText(), sbaRefNumber);

		log.info("Successfully verified the applied SBA in SBA table");
		Reporter.log("<p>Successfully verified the applied SBA in SBA table");

		//Verify the applied SBA at header level in SBA tab

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("appliedSBAHeaderLevel_XPATH"))).getText(),
				sbaRefNumber);
		log.info("Successfully verified the applied SBA at header level in SBA tab");
		Reporter.log("<p>Successfully verified the applied SBA at header level in SBA tab");

	}

	/**
	 * Click on applied SBA and verify the overlay displayed
	 * @throws InterruptedException 
	 */
	@Test(description = "Step15 : Click on applied SBA and verify the overlay displayed", priority = 15)
	public void Step15_ClickAppliedSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on applied SBA

		quoteDetails.clickAppliedManualSBA();
		log.info("Successfully clicked on applied SBA");
		Reporter.log("<p>Successfully clicked on applied SBA");

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlay_XPATH"))).getText()
				, TextProp.getProperty("SBADetailsOverlay"));

		log.info("Successfully verified the SBA details ovarlay");
		Reporter.log("<p>Successfully verified the SBA details ovarlay");
	}

	/**
	 * Save the discount
	 */
	@Test(description = "Step16 : Save the SBA discount", priority = 16)
	public void Step16_SaveSBADiscount() {
		//Save SBA Discount
		ManualSBADiscount = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsoverlayDiscount_XPATH")))
				.getText();

		ManualSBADiscountvalue = ManualSBADiscount.split("-");
		//sbaDiscount = sbaDiscount.substring(1,sbaDiscount.length()-1);
		System.out.println("sbaDiscountvalue = "+ManualSBADiscountvalue[1]);

		log.info("Successfully saved SBA discount");
		Reporter.log("<p>Successfully saved SBA discount");
	}

	/**
	 * Click on Cancel button
	 */
	@Test(description = "Step17 : Click Cancel button", priority = 17)
	public void Step17_ClickCancelButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Cancel button
		quoteDetails.clickCancelButton();

		log.info("Successfully closed the overlay");
		Reporter.log("<p>Successfully closed the overlay");
	}

	/**
	 * Click on Pricing tab and verify updated pricing conditions
	 * @throws InterruptedException 
	 */
	@Test(description = "Step18 : Click on Pricing tab and verify updated pricing conditions", priority = 18)
	public void Step18_ClickPricingTabAndVerifyPricingConditions() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");

		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingConditionColumn_XPATH"))));

		//Verify updated pricing conditions
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Manual");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto + Manual");
			}
		}
		log.info("Create Quote page with pricing condition as "+driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText());
		Reporter.log("<p>Create Quote page with pricing condition as -- "
				+ driver.findElement(By.xpath(ObjRepoProp
						.getProperty("pricingConditionColumn_XPATH"))).getText());
	}

	/**
	 * Save the updated values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step19 : Save the updated values like vendor discount, list price and purchase price", priority = 19)
	public void Step19_SaveTableValues() {
		//Save the updated table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceBeforeSBA = "+actualMListAfterSBA[1]);
		System.out.println("vendorDiscountBeforeSBA = "+actualVDiscountAfterSBA[0]);
		System.out.println("purchasePriceBeforeSBA = "+actualWestCostAfterSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");

	}

	/**
	 * Compare vendor discount, list price and purchase price values before and after applying SBA
	 */
	@Test(description = "Step20 : Compare vendor discount, list price and purchase price values before and after applying SBA", priority = 20)
	public void Step20_CompareTableValues() {	

		//AutoSBADiscountvalue = AutoSBADiscount.split("-");
		//sbaDiscount = sbaDiscount.substring(1,sbaDiscount.length()-1);
		System.out.println("sbaDiscountvalue = "+AutoSBADiscountvalue[1]);
		
		//Compare the values before and after applying SBA
		Assert.assertEquals(Float.parseFloat(actualMListBeforeSBA[1].replace(",", "")), Float.parseFloat(actualMListAfterSBA[1].replace(",", "")));
		Assert.assertEquals(Float.parseFloat(actualVDiscountAfterSBA[0]), ((Float.parseFloat(actualVDiscountBeforeSBA[0]) - Float.parseFloat(AutoSBADiscountvalue[1]))+ Float.parseFloat(ManualSBADiscountvalue[1])));
		Assert.assertEquals(Float.parseFloat(actualWestCostAfterSBA[1].replace(",", "")), Float.parseFloat(actualMListBeforeSBA[1].replace(",", ""))-((Float.parseFloat(actualMListBeforeSBA[1].replace(",", "")) * Float.parseFloat(actualVDiscountAfterSBA[0]))/100));

		log.info("Successfully compared the table values before and after applying SBA");
		Reporter.log("<p>Successfully compared the table values before and after applying SBA");
	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 21: Select Submit quote from the dropdown and click on GO button", priority = 21)
	public void Step21_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 22: Click On No, do not validate button", priority = 22)
	public void Step22_ClickOnDoNotValidateButton1()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}

	/**
	 * Get Quote ID and Total Quote Price after Submitting the Quote
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 23: Get Quote ID and Total Quote Price", priority = 23)
	public void Step23_GetQuoteIDAndTotalPrice() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.getValues();

		log.info("Successfully fetched Quote ID and Quote total price\n");
		Reporter.log("<p>Successfully fetched Quote ID and Quote total price");

	}

	@Test(description = "Step 24: Click on Quotes tab", priority = 24)
	public void Step24_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);

		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");

	}

	@Test(description = "Step 25 : Search for the submitted quote", priority = 25)
	public void Step25_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);

		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}


	@Test(description = "Step 26 : Verify the total price", priority = 26)
	public void Step26_VerifyTotalPrice() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.verifyTotalQuotePrice(driver);

		log.info("Successfully verified the Quote total price\n");
		Reporter.log("<p>Successfully verified the Quote total price");
	}
}
