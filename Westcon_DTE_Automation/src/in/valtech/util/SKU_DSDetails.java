package in.valtech.util;
public class SKU_DSDetails {
	
	
	private String dataVersion;
	private String SKU;
	private String vendorFilePath;
	private String vendorName;
	private String dealID;
	private String description;
	private String sbaFlag;
	private String sbaRefNumber;
	private String VendorRefNo;
	private String quantity;
	private String listPrice;
	private String vendorDiscount;
	private String westconCost;
	private String resellerPrice;
	private String resellerPriceOffList;
	private String globalPoints;
	private String inputType;
	private String sbaType;
	private String updateQty;
	
	
	/**
	 * @return the dataVersion
	 */
	public String getDataVersion() {
		return dataVersion;
	}
	/**
	 * @param dataVersion the dataVersion to set
	 */
	public void setDataVersion(String dataVersion) {
		this.dataVersion = dataVersion;
	}
	/**
	 * @return the sKU
	 */
	public String getSKU() {
		return SKU;
	}
	/**
	 * @param sKU the sKU to set
	 */
	public void setSKU(String sKU) {
		SKU = sKU;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the vendor vendorFilePath
	 */
	public String getVendorFilePath() {
		return vendorFilePath;
	}
	
	/**
	 * @param vendorFilePath
	 */
	public void setVendorFilePath(String vendorFilePath) {
		this.vendorFilePath = vendorFilePath;
	}
	
	/**
	 * @return vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	
	/**
	 * @param vendorName
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	/**
	 * @return dealID
	 */
	public String getDealID() {
		return dealID;
	}
	
	/**
	 * @param dealID
	 */
	public void setDealID(String dealID) {
		this.dealID = dealID;
	}
	
	/**
	 * @return sbaFlag
	 */
	public String getSbaFlag() {
		return sbaFlag;
	}
	
	/**
	 * @param sbaFlag
	 */
	public void setSbaFlag(String sbaFlag) {
		this.sbaFlag = sbaFlag;
	}
	
	/**
	 * @return sbaRefNumber
	 */
	public String getSbaRefNumber() {
		return sbaRefNumber;
	}
	
	/**
	 * @param sbaRefNumber
	 */
	public void setSbaRefNumber(String sbaRefNumber) {
		this.sbaRefNumber = sbaRefNumber;
	}
	
	public String getVendorRefNo(){
		return VendorRefNo;
	}
	/** @return VendorRefNo **/
	
	public void setVendorRefNo(String VendorRefNo){
		this.VendorRefNo = VendorRefNo;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getListPrice() {
		return listPrice;
	}
	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}
	public String getVendorDiscount() {
		return vendorDiscount;
	}
	public void setVendorDiscount(String vendorDiscount) {
		this.vendorDiscount = vendorDiscount;
	}
	public String getWestconCost() {
		return westconCost;
	}
	public void setWestconCost(String westconCost) {
		this.westconCost = westconCost;
	}
	public String getResellerPrice() {
		return resellerPrice;
	}
	public void setResellerPrice(String resellerPrice) {
		this.resellerPrice = resellerPrice;
	}
	public String getResellerPriceOffList() {
		return resellerPriceOffList;
	}
	public void setResellerPriceOffList(String resellerPriceOffList) {
		this.resellerPriceOffList = resellerPriceOffList;
	}
	public String getGlobalPoints() {
		return globalPoints;
	}
	public void setGlobalPoints(String globalPoints) {
		this.globalPoints = globalPoints;
	}
	public String getInputType() {
		return inputType;
	}
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	public String getSbaType() {
		return sbaType;
	}
	public void setSbaType(String sbaType) {
		this.sbaType = sbaType;
	}
	public String getUpdateQty() {
		return updateQty;
	}
	public void setUpdateQty(String updateQty) {
		this.updateQty = updateQty;
	}
	
	
}
