package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC42_Quoting_ErrorCheckforRequesterInfoMandatory extends BaseTest{
	/**
	 * Call TC06 (without Opportunity)
	 * 
	 */	

	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 / Call TC07 (with Opportunity)", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06
		log.info("TC-06 Successfully Executed \n");
        Reporter.log("<p>TC-06 Successfully Executed ");

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}
	/**
	 * Select Quote exp date , Quote close date, Enter probability
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Select Quote exp date , Quote close date, Enter probability", priority = 2)
	public void Step02_Enter_Quoteexpdate_Quoteclosedate_probability()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		
		System.out.println("Date ="+new Date());

		amQuoteDetailsPage.enterQuoteexpdate_Quoteclosedate_probability(CustomFun
				.addDays(new Date(), Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim()), locale),CustomFun
						.addDays(new Date(), Integer.parseInt(userInfoDSDetails
								.getQuoteCloseDate().trim()), locale), userInfoDSDetails.getProbability());


		log.info("Successfully Enter Quote exp date,Quote close date and probability\n");
		Reporter.log("<p>Successfully Enter Quote exp date,Quote close date and probability");

	}
	

	@Test(description = "Step 3: Select Sales office from drop down", priority = 3)
	public void Step03_SelectSalesOffice() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		
		if(CustomFun.isElementVisible(By.xpath(ObjRepoProp.getProperty("SalesOfficeDDL_XPATH")), driver)== true)
		{
			if(locale.equalsIgnoreCase("US")) {
				amQuoteDetailsPage.selectSalesOffice(TextProp.getProperty("SalesOfficeValue"));
			}
			else if(locale.equalsIgnoreCase("CA")){
				amQuoteDetailsPage.selectSalesOffice(TextProp.getProperty("SalesOfficeValue_CA"));
			}}


		log.info("Successfully Selected Sales office\n");
		Reporter.log("<p>Successfully Selected Sales office");

	}	
	
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Select Submit quote from the dropdown and click on GO button", priority = 4)
	public void Step04_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}
	/**
	 * Click on Requester info tab
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Click on Requester info tab", priority = 5)
	public void Step05_ClickonRequesterinfotab() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnRequesterinfotab();

		log.info("Successfully Click on Requester info tab\n");
		Reporter.log("<p>Successfully Click on Requester info tab");

	}
	
	/**
	 * Verify error message for Requester Info mandatory
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Verify error message for Requester Info mandatory", priority = 6)
	public void Step06_VerifyErrorMessage_RequesterInfo()throws Exception {
		
      //Verify Requester Name Error msg
		System.out.println("Requester Name Error Message*****"+driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("RequesterNameErrorMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("RequesterNameErrorMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("CommonErrorMsg"),
				"Error message not proper");
		
		 //Verify Requester Email Error msg
		System.out.println("Requester Email Error Message*****"+driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("RequesterEmailErrorMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("RequesterEmailErrorMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("CommonErrorMsg"),
				"Error message not proper");
		
		log.info("Successfully verified error message for Requester Name and Requester Email mandatory\n");
		Reporter.log("<p>Successfully verified error message for Requester Name and Requester Email mandatory");

	}
}
