package in.valtech.westcon.test.rs;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.westcon.ResellerPages.ResellerEndUserandShippingPage;
import static in.valtech.custom.CustomFun.userInfoDSDetails;

public class TC57_Reseller_CreateNewShippingAddress extends BaseTest 
{

	/**
	 * User to land in end user and shipping page by executing TC-56
	 * 
	 */
	@Test(description = "Step 1: Calling TC51 TC52 TC53 and TC56", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-51 - Access
		log.info("TC-51 Successfully Executed \n");
		Reporter.log("<p>TC-51 Successfully Eexecuted ");
		// Execute TC-52 - Login
		log.info("TC-52 Successfully Executed \n");
		Reporter.log("<p>TC-52 Successfully Eexecuted ");
		// Execute TC-53 - Enter Part number and submit
		log.info("FTC-53 Successfully Executed \n");
		Reporter.log("<p>TC-53 Successfully Eexecuted ");
		// Execute TC-56 - Creating new end user
		log.info("FTC-56 Successfully Executed \n");
		Reporter.log("<p>TC-56 Successfully Eexecuted ");
	}
	
	/**
	 * To click on next button from create quote page
	 * @throws Exception 
	 * 
	 */
	@Test(description = "Step 2: To Click on enter new shipping address radio button,Fill Attn , Company Name, country , address , postal code , phone number ", priority = 2)
	public void Step02_selectEnterNewAddressRadioButton() throws Exception 
	{
		//To select enter new address radio button
		ResellerEndUserandShippingPage rsendusershippingaddress=new ResellerEndUserandShippingPage(driver);

		//Fetching address from excel
		String attn=userInfoDSDetails.getAttn();
		String company=userInfoDSDetails.getCompanyName();
		String country=userInfoDSDetails.getCountry();
		String address=userInfoDSDetails.getAdddress1();
		String postalcode=userInfoDSDetails.getPostalCode();
		String phoneNumber=userInfoDSDetails.getPhoneNumber();

		//To select enter new address radio button and add value in to fields
		rsendusershippingaddress.EnterNewAddressRadioButton(attn,company,country,address,postalcode,phoneNumber);

		log.info("FTC-57 Successfully entered new address \n");
		Reporter.log("<p>TC-57 Successfully entered new address");
	}

}
