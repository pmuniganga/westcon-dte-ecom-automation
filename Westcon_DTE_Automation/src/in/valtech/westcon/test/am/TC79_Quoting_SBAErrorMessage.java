package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC79_Quoting_SBAErrorMessage extends BaseTest {

	String SKU;
	String sbaRefNumber;
	
	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step1 : Call TC06 which selects the End User and all mandatory fields filled", priority = 1)
	public void Step01_NavigateToTheCreateQuotePageWithAllMandatoryInfo() throws Exception {

		//Execute TC06 to navigate to Create Quote page with End User linked and all mandatory fields filled
		// Create Quote page is displayed with End user selected and all mandatory fields filled

		log.info("Create Quote page is displayed with End user selected and all mandatory fields filled");
		Reporter.log("<p>Create Quote page is displayed with End user selected and all mandatory fields filled-- "
				+ driver.getTitle());			
	}

	/**
	 * Click on SBA tab
	 */
	@Test(description = "Step2 : Click on SBA tab", priority = 2)
	public void Step02_ClickSBATab() {
		//Click on SBA tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBATab();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaTabSearchLink_XPATH"))).getText().trim(),
				TextProp.getProperty("SBATabData"));

		log.info("Successfully clicked on SBA tab and SBA tab is opened");
		Reporter.log("<p>Successfully clicked on SBA tab and SBA tab is opened");
	}
	
	@Test(description = "Step3 : Uncheck select all checkbox", priority = 3)
	public void Step03_ClickSelectAllCheckbox() throws Exception {
		//Click on Select All Checkbox
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.deselectCheckbox();
		
		log.info("Successfully clicked on Select All Checkbox");
		Reporter.log("<p>Successfully clicked on Select All Checkbox");
	}
	
	@Test(description = "Step4 : Click on Apply button", priority = 4)
	public void Step04_clickSBAApplyButton() {
		//Click on Apply button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBAApplyButton();
		
		log.info("Successfully Clicked on Apply Button");
		Reporter.log("<p>Successfully Clicked on Apply Button");
	}
	
	@Test(description = "Step 5 : Verify Error Message", priority = 5)
	public void Step05_verifyErrorMessage() {
		//Verify the error message
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaSelectProductErrorMessage_XPATH"))).getText(), 
					TextProp.getProperty("SBASelectProductError"));
		
		log.info("Successfully verified the error message");
		Reporter.log("<p>Successfully verified the error message");
	}
	
	@Test(description = "Step6 : Check select all checkbox", priority = 6)
	public void Step06_ClickSelectAllCheckbox() throws Exception {
		//Click on Select All Checkbox
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.deselectCheckbox();
		
		log.info("Successfully clicked on Select All Checkbox");
		Reporter.log("<p>Successfully clicked on Select All Checkbox");
	}
	
	
	/**
	 * Click on SBA search link
	 */
	@Test(description = "Step 7 : Click on SBA search link", priority = 7)
	public void Step07_ClickSBASearchLink() {
		//Click on SBA search link
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBASearchLink();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaSearchOverlayHeader_XPATH"))).getText().trim(), 
				TextProp.getProperty("SBASearchOverlayHeader"));

		log.info("Successfully clicked on SBA search link and SBA search overlay is opened");
		Reporter.log("<p>Successfully clicked on SBA search link and SBA search overlay is opened");
	}
	
	/**
	 * Click on Search button
	 * @throws InterruptedException 
	 */
	@Test(description = "Step 8 : Click on search button", priority = 8)
	public void Step08_ClickSearchButton() throws InterruptedException {
		//Click on SBA search link
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSearchButton();

		log.info("Successfully clicked on search button");
		Reporter.log("<p>Successfully clicked on search button");
	}
	
	@Test(description = "Step 9: Verify Error Message", priority = 9)
	public void Step09_verifyErrorMessage() {
		//Verify the error message
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaPartNumberRequiredError_XPATH"))).getText(), 
					TextProp.getProperty("SBAPartNumberRequiredError"));
		
		log.info("Successfully verified the error message");
		Reporter.log("<p>Successfully verified the error message");
	}

	/**
	 * Enter part number and Click on Search button
	 * @throws InterruptedException 
	 */
	@Test(description = "Step10 : Enter invalid part number and Search SBA", priority = 10)
	public void Step10_EnterInvalidPartAndSearchSBA() throws InterruptedException {
		//Enter Invalid Part number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		
		quoteDetails.enterPartNumber(TextProp.getProperty("SBAInvalidPartNumber"));
		log.info("Successfully entered part number");
		Reporter.log("<p>Successfully entered part number");

		//Search for SBA
		quoteDetails.clickSearchButton();

		log.info("Successfully clicked on search button");
		Reporter.log("<p>Successfully clicked on search button");
	}
	
	@Test(description = "Step 11: Verify Error Message", priority = 11)
	public void Step11_verifyErrorMessage() {
		//Verify the error message
		By sbaNoRecordsError = By.xpath(ObjRepoProp.getProperty("sbaNoRecordsError_XPATH"));
		CustomFun.waitObjectToLoad(driver, sbaNoRecordsError, 60);
		
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaNoRecordsError_XPATH"))).getText(), 
					TextProp.getProperty("SBANoRecordsFoundError"));
		
		log.info("Successfully verified the error message");
		Reporter.log("<p>Successfully verified the error message");
	}
	
	/**
	 * Enter part number and Click on Search button
	 * @throws InterruptedException 
	 */
	@Test(description = "Step12 : Enter valid part number and Search SBA", priority = 12)
	public void Step12_EnterValidPartAndSearchSBA() throws InterruptedException {
		//Enter Valid Part number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			SKU = SKUdataSetList.get(i).getSKU();
		}
		quoteDetails.enterPartNumber(SKU);
		
		log.info("Successfully entered part number");
		Reporter.log("<p>Successfully entered part number");

		//Search for SBA
		quoteDetails.clickSearchButton();

		log.info("Successfully clicked on search button and SBA table is displayed");
		Reporter.log("<p>Successfully clicked on search button and SBA table is displayed");
	}

	/**
	 * Select the SBA
	 * @throws InterruptedException 
	 */
	@Test(description = "Step13 : Select SBA", priority = 13)
	public void Step13_SelectSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Select the SBA displayed in the table
		Thread.sleep(5000);
		quoteDetails.selectSBA();
		log.info("Successfully selected SBA in SBA table");
		Reporter.log("<p>Successfully selected SBA in SBA table");
	}

	/**
	 * Apply the selected SBA
	 */
	@Test(description = "Step14 : Apply the selected SBA and verify in SBA tab", priority = 14)
	public void Step14_ClickApplyButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Select the SBA displayed in the table

		quoteDetails.applySBA();
		log.info("Successfully applied the SBA");
		Reporter.log("<p>Successfully applied the SBA");
		CustomFun.waitObjectToLoad(driver, By.xpath(ObjRepoProp
				.getProperty("appliedSBAInTable_XPATH")), 60);

		//Verify the applied SBA in SBA table
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			sbaRefNumber = SKUdataSetList.get(i).getSbaRefNumber();
		}
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("appliedSBAInTable_XPATH"))));
		

		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("appliedSBAInTable_XPATH"))).getText(), sbaRefNumber);

		log.info("Successfully verified the applied SBA in SBA table");
		Reporter.log("<p>Successfully verified the applied SBA in SBA table");

		//Verify the applied SBA at header level in SBA tab

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("appliedSBAHeaderLevel_XPATH"))).getText(),
				sbaRefNumber);
		log.info("Successfully verified the applied SBA at header level in SBA tab");
		Reporter.log("<p>Successfully verified the applied SBA at header level in SBA tab");

	}

	@Test(description = "Step15 : Uncheck select all checkbox", priority = 15)
	public void Step15_ClickSelectAllCheckbox() throws Exception {
		//Click on Select All Checkbox
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.deselectCheckbox();
		
		log.info("Successfully clicked on Select All Checkbox");
		Reporter.log("<p>Successfully clicked on Select All Checkbox");
	}
	
	/**
	 * Click on SBA search link
	 */
	@Test(description = "Step 16 : Click on SBA search link", priority = 16)
	public void Step16_ClickSBASearchLink() {
		//Click on SBA search link
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBASearchLink();

		log.info("Successfully clicked on SBA search link");
		Reporter.log("<p>Successfully clicked on SBA search link");
	}
	
	@Test(description = "Step 17 : Verify Error Message", priority = 17)
	public void Step17_verifyErrorMessage() {
		//Verify the error message
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaSelectProductErrorMessage_XPATH"))).getText(), 
					TextProp.getProperty("SBASelectProductError"));
		
		log.info("Successfully verified the error message");
		Reporter.log("<p>Successfully verified the error message");
	}
	
	@Test(description = "Step18 :Check select all checkbox", priority = 18)
	public void Step18_ClickSelectAllCheckbox() throws Exception {
		//Click on Select All Checkbox
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.deselectCheckbox();
		
		log.info("Successfully clicked on Select All Checkbox");
		Reporter.log("<p>Successfully clicked on Select All Checkbox");
	}
	
	@Test(description = "Step19 : Enter invalid SBA ID", priority = 19)
	public void Step19_EnterSBAID() {
		//Enter Invalid SBA ID
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.enterSBAID(TextProp.getProperty("InvalidSBAID"));
		
		log.info("Successfully entered Invalid SBA ID");
		Reporter.log("<p>Successfully Invalid entered SBA ID");
	}
	
	@Test(description = "Step20 : Click on Apply button", priority = 20)
	public void Step20_clickSBAApplyButton() {
		//Click on Apply button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBAApplyButton();
		
		log.info("Successfully Clicked on Apply Button");
		Reporter.log("<p>Successfully Clicked on Apply Button");
	}
	
	@Test(description = "Step 21 : Verify Error Message", priority = 21)
	public void Step21_verifyErrorMessage() {
		//Verify the error message
		By InvalidSBAAError = By.xpath(ObjRepoProp.getProperty("InvalidSBAAError_XPATH"));
		
		CustomFun.waitObjectToLoad(driver, InvalidSBAAError, 60);
		
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("InvalidSBAAError_XPATH"))).getText(), 
					TextProp.getProperty("InvalidSBAError"));
		
		log.info("Successfully verified the error message");
		Reporter.log("<p>Successfully verified the error message");
	}
}
