package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.custom.CustomFun.SKUdataSetList;


public class TC03_Quoting_CopyPasteBOM extends BaseTest {

	static  QuotingAddProductsPage amAddProductsPage;
	static QuotingQuoteDetailpage amQuoteDetailspage;
	String SKUCount = null;
	String inputType;
	String[] skuNumber;
	String[] skuQty;
	String[] skuListPrice;
	String[] skuVendorDiscount;
	String[] skuWestconCost;
	String[] skuResellerPrice;
	String[] skuResellerPriceOffList;
	String[] skuGlobalPoints;
	String skuData;

	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */

	@Test(description = "Step 1: Call TC01 TC02", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
	}
/*
	@Test(description = "Step 2: Enter PartNumbers and Click Submit", priority = 2)
	public void Step02_EnterPartNumbersAndClickSubmit() throws Exception {

		//QuotingAddProductsPage add = new QuotingAddProductsPage(driver);

		amAddProductsPage = new QuotingAddProductsPage(driver);

		int SKUCountInt = SKUdataSetList.size();

		SKUCount = Integer.toString(SKUCountInt);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());

		amAddProductsPage.CopyPasteBOM(driver , SKUdataSetList.get(i).getSKU()); 

		} 	
		amAddProductsPage.CopyPasteBOMSubmit(driver);  

		log.info("successfully submitted the part numbers and Quote detail page is displayed\n");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");

	}  
	*/

	@Test(description = "Step 2: Enter PartNumbers and Click Submit", priority = 2)
	public void Step02_EnterPartNumbersAndClickSubmit() throws Exception {

		QuotingAddProductsPage amAddProductsPage = new QuotingAddProductsPage(driver);
		
		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {
			
			inputType = SKUdataSetList.get(i).getInputType();
			System.out.println("inputType = "+inputType);
			amAddProductsPage.selectInputFromDropdown(driver, inputType);
			
			skuNumber = SKUdataSetList.get(i).getSKU().split("\n");
			skuQty = SKUdataSetList.get(i).getQuantity().split("\n");
			skuListPrice = SKUdataSetList.get(i).getListPrice().split("\n");
			skuVendorDiscount = SKUdataSetList.get(i).getVendorDiscount().split("\n");
			skuWestconCost = SKUdataSetList.get(i).getWestconCost().split("\n");
			skuResellerPrice = SKUdataSetList.get(i).getResellerPrice().split("\n");
			skuResellerPriceOffList = SKUdataSetList.get(i).getResellerPriceOffList().split("\n");
			skuGlobalPoints = SKUdataSetList.get(i).getGlobalPoints().split("\n");
			for(int j = 0 ; j < skuNumber.length ; j++) {

				log.info("SKU*************" + SKUdataSetList.get(i).getSKU());
				log.info("QTY*************" + SKUdataSetList.get(i).getQuantity());
				skuData = skuNumber[j]+" "+skuQty[j]+" "+skuListPrice[j]+" "+skuVendorDiscount[j]+" "+
						skuWestconCost[j]+" "+skuResellerPrice[j]+" "+skuResellerPriceOffList[j]+" "+skuGlobalPoints[j];

				System.out.println("skuNumber["+j+"] = "+skuNumber[j]);
				
				amAddProductsPage.CopyPasteBOM(driver , skuData.replace("NA", ""));
			} 	  
		}

		amAddProductsPage.CopyPasteBOMSubmit(driver);
		
		log.info("successfully submitted the part numbers and Quote detail page is displayed\n");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");


	}       

}
