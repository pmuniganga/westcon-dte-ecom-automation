package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import java.util.Date;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC77_Quoting_VerifySalesOfficeMandatory_EMEARegions extends BaseTest{
	/**
	 * Call TC06 (without Opportunity)
	 * 
	 */	

	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 / Call TC07 (with Opportunity)", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06
		log.info("TC-06 Successfully Executed \n");
        Reporter.log("<p>TC-06 Successfully Eexecuted ");

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}

	/**
	 * Click on Requester info tab
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Click on Requester info tab", priority = 2)
	public void Step02_ClickonRequesterinfotab() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnRequesterinfotab();

		log.info("Successfully Click on Requester info tab\n");
		Reporter.log("<p>Successfully Click on Requester info tab");

	}

	/**
	 * Click Requester name pencil icon and enter requester name
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 3: Click Requester name pencil icon and enter requester name", priority = 3)
	public void Step03_EnterRequesterName() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterName(TextProp.getProperty("RequesterName"));

		log.info("Successfully Click Requester name pencil icon and enter requester name\n");
		Reporter.log("<p>Successfully Click Requester name pencil icon and enter requester name");

	}

	/**
	 * Click Requester email pencil icon and enter requester email
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Enter requester email", priority = 4)
	public void Step04_EnterRequesterEmail() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterEmail(TextProp.getProperty("RequesterEmail"));

		log.info("Successfully entered requester email\n");
		Reporter.log("<p>Successfully entered requester email");

	}

	
	/**
	 * Select Quote exp date , Quote close date, Enter probability
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Select Quote exp date , Quote close date, Enter probability", priority = 5)
	public void Step05_Enter_QuoteExpDate_Quoteclosedate_probability()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		
		System.out.println("Date ="+new Date());

		amQuoteDetailsPage.enterQuoteexpdate_Quoteclosedate_probability(CustomFun
				.addDays(new Date(), Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim()), locale),CustomFun
						.addDays(new Date(), Integer.parseInt(userInfoDSDetails
								.getQuoteCloseDate().trim()), locale), userInfoDSDetails.getProbability());
		
		//Click on delete icon
         amQuoteDetailsPage.clickOnAccountManagerDeleteIcon();	
		
		log.info("Successfully Enter Quote exp date,Quote close date and probability\n");
		Reporter.log("<p>Successfully Enter Quote exp date,Quote close date and probability");

	}
	
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority = 6)
	public void Step06_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}
	
	/**
	 * Verify error message for SalesOffice mandatory
	 *SalesOfficeErrorMsg_XPATH 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Verify error message for Sales Office mandatory", priority = 7)
	public void Step07_VerifyErrorMessage_SalesOffice()throws Exception {

		System.out.println("Error Message*****"+driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("SalesOfficeErrorMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("SalesOfficeErrorMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("CommonErrorMsg"),
				"Error message not proper");		
		
		log.info("Successfully verified error message for Sales Office mandatory\n");
		Reporter.log("<p>Successfully verified error message for Sales Office mandatory");

	}
	
}
