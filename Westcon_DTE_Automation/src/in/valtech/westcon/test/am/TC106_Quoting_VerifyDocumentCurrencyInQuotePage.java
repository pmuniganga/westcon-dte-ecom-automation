package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC106_Quoting_VerifyDocumentCurrencyInQuotePage extends BaseTest {

	String[] skuNumber;
	String[] actualMargin;
	String[] actualResellerPrice;
	String[] actualExResellerPrice;

	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	@Test(description = "Step 2: Verify the Quoting Currency in the quote details page dropdown", priority = 2)
	public void Step02_VerifyQuotingCurrency() {
		//Verify the Quoting Currency in Quote details page dropdown
		WebElement currencyDropdown = driver.findElement(By.xpath(ObjRepoProp.getProperty("currencyDropdown_XPATH")));

		Select sel = new Select(currencyDropdown);
		currencyDropdown = sel.getFirstSelectedOption();

		if(locale.equalsIgnoreCase("CA")) {
			Assert.assertEquals(currencyDropdown.getText(), "CAD");
		}
		else if(locale.equalsIgnoreCase("UK")) {
			Assert.assertEquals(currencyDropdown.getText(), "GBP");
		}

		log.info("Successfully verified the quoting currency\n");
		Reporter.log("<p>Successfully verified the quoting currency");
	}

	@Test(description = "Step 3: Verify the Quoting Currency displayed for Margin and Reseller Price", priority = 3)
	public void Step03_VerifyMarginResellerPriceCurrency() throws Exception {
		//Verify the Quoting currency displayed for Margin and Reseller Price
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickResetColumnOrderLink();

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMargin = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][7]")).getText().split(" ");
				actualResellerPrice = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][8]")).getText().split(" ");
				actualExResellerPrice = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][10]")).getText().split(" ");
			}
		}

		if(locale.equalsIgnoreCase("CA")) {
			Assert.assertEquals(actualMargin[0], "CAD");
			Assert.assertEquals(actualResellerPrice[0], "CAD");
			Assert.assertEquals(actualExResellerPrice[0], "CAD");
		}
		else if(locale.equalsIgnoreCase("UK")) {
			Assert.assertEquals(actualMargin[0], "GBP");
			Assert.assertEquals(actualResellerPrice[0], "GBP");
			Assert.assertEquals(actualExResellerPrice[0], "GBP");
		}

		log.info("Successfully verified the quoting currency for Margin and Reseller Price\n");
		Reporter.log("<p>Successfully verified the quoting currency for Margin and Reseller Price");
	}

	@Test(description = "Step 4: Change the quoting currency", priority = 4)
	public void Step04_ChangeQuotingCurrency() throws Exception {
		//Change the quoting currency
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.selectCurrencyFromDropdown("USD");

		log.info("Successfully changed the quoting currency\n");
		Reporter.log("<p>Successfully changed the quoting currency");
	}

	@Test(description = "Step 5: Confirm the currency change", priority = 5)
	public void Step05_ConfirmCurrencyChange() throws Exception {
		//Confirm the Currency change
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickYesToConfirmCurrencyChange();

		log.info("Confirmed the currency change\n");
		Reporter.log("<p>Confirmed the currency change");
	}

	@Test(description = "Step 6: Confirm the currency change", priority = 6)
	public void Step06_VerifyMarginResellerPriceCurrencyChange() {
		//Verify the currency change
		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMargin = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][7]")).getText().split(" ");
				actualResellerPrice = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][8]")).getText().split(" ");
				actualExResellerPrice = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][10]")).getText().split(" ");
			}
		}
		
		Assert.assertEquals(actualMargin[0], "USD");
		Assert.assertEquals(actualResellerPrice[0], "USD");
		Assert.assertEquals(actualExResellerPrice[0], "USD");

		log.info("Successfully verified the currency change\n");
		Reporter.log("<p>Successfully verified the currency change");
	}
}

