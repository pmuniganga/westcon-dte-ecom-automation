
package in.valtech.westcon.ResellerPages;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.util.SKU_DSDetails;
import in.valtech.westcon.ResellerPages.ResellerQuoteCartPage;

public class ResellerRequestAQuotePage 
{

	private final WebDriver driver;
	SKU_DSDetails sku_details=new SKU_DSDetails();
	GUIFunctions gui = new GUIFunctions();
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// RS request a quote page Constructor
	public ResellerRequestAQuotePage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Check that we're on the right RS request a quote Page.
		if (!(CustomFun.isElementPresent(
				By.xpath(ObjRepoProp.getProperty("resellerRequestAQuotePage_XPATH")),
				driver))) {

			throw new IllegalStateException("This is not reseller request a quote page");
		}
	}
	By pencilIcon = By.xpath(ObjRepoProp.getProperty("editIcon_XPATH"));
	By UploadAFileSubmit=By.xpath(ObjRepoProp.getProperty("uploadFileSubmit_XPATH"));
	By browseButton_xpath=By.xpath(ObjRepoProp.getProperty("browseButton_XPATH"));
	By selectVendor_XPATH=By.xpath(ObjRepoProp.getProperty("selectVenforDropdown_XPATH"));
	static By CopyPasteBOMTextBox=By.xpath(ObjRepoProp.getProperty("CopyPasteBOMTextBox_XPATH"));
	static By CopyPasteBOMSubmit=By.xpath(ObjRepoProp.getProperty("CopyPasteBOMSubmit_XPATH"));
	static By resellerCopyPasteBOMSection=By.xpath(ObjRepoProp.getProperty("resellerCopyPasteBOMSection_XPATH"));
	static By vendorRefNoTab=By.xpath(ObjRepoProp.getProperty("VendorTabVerify_XPATH"));
	static By VendorSelectTable=By.xpath(ObjRepoProp.getProperty("VendorSelectTable_XPATH"));
	static By VendorRefNoTxtBox=By.xpath(ObjRepoProp.getProperty("VendorRefNoTxtBox_XPATH"));
	static By selectVendorDropdownArrow=By.xpath(ObjRepoProp.getProperty("selectVendorDropdownArrow_XPATH"));
	static By VendorTabVerify=By.xpath(ObjRepoProp.getProperty("VendorTabVerify_XPATH"));
	static By VendorSubmitButton=By.xpath(ObjRepoProp.getProperty("VendorSubmitButton_XPATH"));
	By uploadFile_xpath =By.xpath(ObjRepoProp.getProperty("uploadFileButton_XPATH"));


	//This function is to verify copy and paste bom section is selected by default
	public ResellerRequestAQuotePage ToVerifyCopyAndPasteSelecteByDefault() throws Exception
	{
		CustomFun.isTabSelected(driver.findElement(resellerCopyPasteBOMSection),"Copy paste bom selected");
		return new ResellerRequestAQuotePage(driver);			
	}	

	//This function is to enter part number in to copy and paste bom section
	public ResellerRequestAQuotePage CopyPasteBOM(WebDriver driver, String data) {
		GUIFunctions.typeTxtboxValue(driver, CopyPasteBOMTextBox, data);
		System.out.println("Entered the PartNumbers");
		return new ResellerRequestAQuotePage(driver);
	}

	//this function is to click on submit button from copy and paste bom section

	public ResellerQuoteCartPage CopyPasteBOMSubmit(WebDriver driver) throws InterruptedException {
		Thread.sleep(5000);
		GUIFunctions.clickElement(driver,CopyPasteBOMSubmit , "Submit");
		CustomFun.waitForPageLoaded(driver);
		Thread.sleep(5000);
		System.out.println("submitted successfully");
		return new ResellerQuoteCartPage(driver);
	}


	//this function is to click on vendor ref No tab and verify the tab highlighted
	public ResellerRequestAQuotePage ClickVendorRefNoTab(WebDriver driver) {
		GUIFunctions.clickElement(driver,vendorRefNoTab, "Submit");
		CustomFun.waitForPageLoaded(driver);
		System.out.println("Clicked on Vendor ref tab ");
		Boolean flag=driver.findElement(VendorTabVerify).isDisplayed();
		if(flag==true)
			System.out.println("Vendor Ref tab is selected");
		return new ResellerRequestAQuotePage(driver);
	}

	/**
	 * This function is to  select vendor from vendor dropdown
	 * 
	 * @throws Exception
	 */
	By vendorDropdownValue=By.xpath(ObjRepoProp.getProperty("VendorSelectTable_XPATH"));
	public ResellerRequestAQuotePage SelectVendorFromDropDown(String vendor) throws Exception
	{

		GUIFunctions.selectDropDownValue(driver.findElement(vendorDropdownValue), vendor, "value");	
		return new ResellerRequestAQuotePage(driver);
	}

	/**
	 * This function is to  enter deal id
	 * @throws Exception
	 */
	public ResellerRequestAQuotePage EnterDealID( String data)throws Exception
	{
		GUIFunctions.typeTxtboxValue(driver, VendorRefNoTxtBox, data);
		return new ResellerRequestAQuotePage(driver);
	}

	/**
	 * This function is to click on submit button
	 * @throws InterruptedException 
	 * @throws Exception
	 */

	public ResellerQuoteCartPage VendorSubmitButton(WebDriver driver) throws InterruptedException {
		GUIFunctions.clickElement(driver,VendorSubmitButton , "Submit");
		Thread.sleep(5000);
		CustomFun.waitForPageLoaded(driver);

		System.out.println("submitted successfully");
		return new ResellerQuoteCartPage(driver);
	}

	public ResellerRequestAQuotePage ClickOnUploadFile() throws Exception{


		//Click on upload file
		GUIFunctions.clickElement(driver, uploadFile_xpath,
				" uploadFile ");
		return new ResellerRequestAQuotePage(driver);
	}

	public ResellerRequestAQuotePage SelectVendor(String vendorname) throws Exception{
		//Click on upload file
		GUIFunctions.selectDropDownValue(driver.findElement(By.xpath(ObjRepoProp.
				getProperty("selectVenforDropdown_XPATH"))), vendorname, "value");

		return new ResellerRequestAQuotePage(driver);		 
	}

	public ResellerQuoteCartPage ClickOnBrowseButton() throws Exception{
		//Click on upload file
		GUIFunctions.clickElement(driver, browseButton_xpath," Browse button ");		
		return new ResellerQuoteCartPage(driver);
	}

	public ResellerRequestAQuotePage UploadAFileSubmit(WebDriver driver) {
		GUIFunctions.clickElement(driver,UploadAFileSubmit , "Submit");
		//CustomFun.waitForPageLoaded(driver);
		//	CustomFun.waitObjectToLoad(driver,pencilIcon,60);
		System.out.println("submitted successfully");
		return new ResellerRequestAQuotePage(driver);
	}
	public ResellerRequestAQuotePage EnterUploadFilePath() throws Exception{
	
		String path = null;		
		String dir = CustomFun.getRootDir();	
		System.out.println(dir);				
		for (int i = 0; i < SKUdataSetList.size(); i++) {
			path = dir+SKUdataSetList.get(i).getVendorFilePath();
			System.out.println(path);			
		}
		if (BaseTest.driverName.equalsIgnoreCase("FF")
				|| BaseTest.driverName.equalsIgnoreCase("CHROME"))
		{			
			driver.findElement(browseButton_xpath).sendKeys(path);
		}else {
			// For IE
			WebElement browseBtn =driver.findElement(browseButton_xpath);			
			browseBtn.sendKeys(path);
			//element.sendKeys("D:\\Westcon-DTE-Ecom\\Westcon_Auto_Workspace\\Westcon DTE Automation\\resources\\Bartercard Group Series Options.xlsx");
			//GUIFunctions.clickElement(driver, browseButton_xpath," Browse button ");	
		}
		return new ResellerRequestAQuotePage(driver);
	}
}
