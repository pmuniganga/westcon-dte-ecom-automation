package in.valtech.custom.exceptions;

import org.apache.log4j.Logger;
import org.testng.Reporter;

public class NoSuchElementException extends Exception {
public Logger log = Logger.getLogger(this.getClass().getName());
	
	//Exception constructor
	public NoSuchElementException(String message){
		Reporter.log("<p>"+message);
		log.info(message);
	}
}
