package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC85_Quoting_TANProductErrorMessage extends BaseTest {

	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	/**
	 * Change Manufacturer List price and Vendor Discount
	 */
	@Test(description = "Step2 : Change Manufacturer List price", priority = 2)
	public void Step02_ChangePrice() throws Exception {
		//Change Manufacturer List price
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterManufacturerListPrice(TextProp.getProperty("alteredManufacturerListPrice"));
		
		log.info("Successfully altered Manufacturer List price");
		Reporter.log("<p>Successfully altered Manufacturer List price");
	}
	
	@Test(description = "Step03 : Verify TAN Error Message", priority = 3)
	public void Step03_VerifyTANErrorMsg() {
		//Verify TAN Error Message
		
		By pricingGridTANError = By.xpath(ObjRepoProp.getProperty("pricingGridTANError_XPATH"));		
		CustomFun.waitObjectToLoad(driver, pricingGridTANError, 60);
		
		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingGridTANError_XPATH"))));
		
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingGridTANError_XPATH"))).getText(), TextProp.getProperty("pricingGridTANError"));
		
		log.info("Successfully verified TAN Error Message");
		Reporter.log("<p>Successfully verified TAN Error Message");
	}
}

