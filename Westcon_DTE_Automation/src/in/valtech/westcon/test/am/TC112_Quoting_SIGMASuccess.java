package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC112_Quoting_SIGMASuccess extends BaseTest {

	/**
	 * Call TC03
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		
		log.info("Create Quote page is displayed\n");
		Reporter.log("<p>Create Quote page is displayed");
	}
	
	@Test(description = "Step 2 : Select Probability value", priority = 2)
	public void Step02_SelectProbability() throws Exception {
		//Select Probability value from dropdown
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.selectProbability(userInfoDSDetails.getProbability());
		
		log.info("Successfully selected probability value from dropdown\n");
		Reporter.log("<p>Successfully selected probability value from dropdown");
	}
	
	@Test(description = "Step 3: Click on Save button", priority = 3)
	public void Step03_SaveQuote() {
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.saveQuote();
		
		log.info("Successfully saved the Quote\n");
		Reporter.log("<p>Successfully saved the Quote");
	}
	
	@Test(description = "Step 4: Verify Success Msg in the overlay", priority = 4)
	public void Step04_VerifySuccessMsgAndCloseOverlay() {
		By saveQuoteOverlayMsg = By.xpath(ObjRepoProp.getProperty("saveQuoteOverlayMsg_XPATH"));
		CustomFun.waitObjectToLoad(driver, saveQuoteOverlayMsg, 60);
		Assert.assertEquals(driver.findElement(saveQuoteOverlayMsg).getText().replaceAll("\n", " "), TextProp.getProperty("saveQuoteMsg"));
		
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.closeSaveOverlay();
		
		log.info("Successfully verified the message and closed the overlay\n");
		Reporter.log("<p>Successfully verified the message and closed the overlay");
	}
	
	/**
	 * Select Submit to SIGMA from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Select Submit quote from the dropdown and click on GO button", priority = 5)
	public void Step05_SelectDropdownAndSubmitToSIGMA() throws Exception {	
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitSIGMA();

		log.info("Successfully Select Submit to SIGMA from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit to SIGMA from the dropdown and click on GO button");

	}
	
	@Test(description = "Step 6 : Verify SIGMA success message", priority = 6)
	public void Step06_VerifySIGMASuccessMsg() {
		By sigmaSuccessMsg = By.xpath(ObjRepoProp.getProperty("sigmaSuccessMsg_XPATH"));
		CustomFun.waitObjectToLoad(driver, sigmaSuccessMsg, 60);
		Assert.assertEquals(driver.findElement(sigmaSuccessMsg).getText().replaceAll("\n", " "), TextProp.getProperty("SIGMASuccessMsg"));
		
		log.info("Successfully verified SIGMA Success Message\n");
		Reporter.log("<p>Successfully verified SIGMA Success Message");
	}
	
	@Test(description = "Step 7 : Get quote ID and revision", priority = 7)
	public void Step07_GetQuoteID() {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.getValues();
		
		log.info("Successfully retrieved quote ID\n");
		Reporter.log("<p>Successfully retrieved quote ID");
	}
	
	/**
	 * Search created/submitted Quote
	 * 
	 * @throws Exception
	 */
	
	@Test(description = "Step 8: Click on Quotes tab", priority = 8)
	public void Step08_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);
		
		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");
	
	}
	
	@Test(description = "Step 9 : Search for the submitted quote", priority = 9)
	public void Step09_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);
		
		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}
	
	@Test(description = "Step 10: Expand and verify the SIGMA details", priority = 10)
	public void Step10_ExpandAndVerifySIGMADetails() throws Exception
	{
		By sigmaStatus = By.xpath(ObjRepoProp.getProperty("sigmaStatus_XPATH"));
		By sigmaSubmittedDate = By.xpath(ObjRepoProp.getProperty("sigmaSubmittedDate_XPATH"));
		
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.expandQuote();
		
		String todayDate = CustomFun.addDays(new Date(), 0, locale);
		System.out.println("todayDate = "+todayDate);
		
		Assert.assertEquals(driver.findElement(sigmaStatus).getText().trim(), TextProp.getProperty("SIGMAStatus"));
		Assert.assertEquals(driver.findElement(sigmaSubmittedDate).getText().trim(), todayDate);
		
		log.info("Successfully verified SIGMA Details\n");
		Reporter.log("<p>Successfully verified SIGMA Details");
	}
}
