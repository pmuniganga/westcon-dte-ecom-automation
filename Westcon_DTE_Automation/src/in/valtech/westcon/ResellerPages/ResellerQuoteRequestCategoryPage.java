package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import in.valtech.custom.CustomFun;

import in.valtech.uiFunctions.GUIFunctions;


public class ResellerQuoteRequestCategoryPage {
	private final WebDriver driver;

	GUIFunctions gui = new GUIFunctions();
	//For logging
	public Logger log = Logger.getLogger(this.getClass().getName());
	
	static Integer productQtyCount;

	//RS quote cart page Constructor
	public ResellerQuoteRequestCategoryPage(WebDriver driver) {

		this.driver = driver;
		/*		CustomFun.waitForPageLoaded(driver);
		//Check that we're on the right RS quote cart Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerQuoteRequestCategoryPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not reseller quote request category cart page");
		}

		 */

	}

	By enterPartNoTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("enterPartNoTextBox_XPATH"));
	By submitButtonRequestCategoryPage_XPATH=By.xpath(ObjRepoProp.getProperty("submitButtonRequestCategoryPage_XPATH"));
	By viewCartButton_XPATH=By.xpath(ObjRepoProp.getProperty("viewCartButton_XPATH"));
	By messageDialogHeading_XPATH=By.xpath(ObjRepoProp.getProperty("messageDialogHeading_XPATH"));	
	By continueButton_XPATH =By.xpath(ObjRepoProp.getProperty("continueButton_XPATH"));
	By cartTotal_XPATH =By.xpath(ObjRepoProp.getProperty("cartTotal_XPATH"));
	By soppingCartIcon_XPATH =By.xpath(ObjRepoProp.getProperty("soppingCartIcon_XPATH"));
	By viewMiniCartButton_XPATH = By.xpath(ObjRepoProp.getProperty("viewMiniCartButton_XPATH"));

	public ResellerQuoteRequestCategoryPage EnterValueTextbox(WebDriver driver, String data) throws Exception
	{
		CustomFun.waitObjectToLoad(driver, enterPartNoTextBox_XPATH, 30);
		GUIFunctions.typeTxtboxValue(driver, enterPartNoTextBox_XPATH, data);
		System.out.println("Entered the PartNumbers");
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("scroll(0, 125);");
		
		GUIFunctions.clickElement(driver, submitButtonRequestCategoryPage_XPATH, "Click on Submit Button");
		return new ResellerQuoteRequestCategoryPage(driver);
	}

	public ResellerQuoteCartPage ClickViewCartButton(WebDriver driver, String data) throws Exception
	{
		//Wait for dialog to display after submitting the article
		Thread.sleep(10000);
		CustomFun.waitObjectToLoad(driver, viewCartButton_XPATH, 60);
	//	String dialogMessageHeading= driver.findElement(messageDialogHeading_XPATH).getText();
	//	System.out.println(dialogMessageHeading);
		//	Assert.assertEquals(dialogMessageHeading, TextProp.getProperty("msgDialogHeading"));
		//click on view cart button in dialog
		GUIFunctions.clickElement(driver, viewCartButton_XPATH, "Click on View Cart Button");
		return new ResellerQuoteCartPage(driver);
	}
	
	public ResellerCartPage QuickEntry_ClickViewCartButton(WebDriver driver, String data) throws Exception
	{
		//Wait for dialog to display after submitting the article
		Thread.sleep(10000);
		CustomFun.waitObjectToLoad(driver, viewCartButton_XPATH, 60);	
		GUIFunctions.clickElement(driver, viewCartButton_XPATH, "Click on View Cart Button");
		return new ResellerCartPage(driver);
	}
	
	
	
	public ResellerQuoteRequestCategoryPage clickContinueButton(WebDriver driver, String data) throws Exception
	{
		//Wait for dialog to display after submitting the article
		Thread.sleep(15000);
		CustomFun.waitObjectToLoad(driver, continueButton_XPATH, 60);
		GUIFunctions.clickElement(driver, continueButton_XPATH, "Click on continue shopping  Button");
		return new ResellerQuoteRequestCategoryPage(driver);
	}
	
	public ResellerQuoteRequestCategoryPage VerifyTheCartTotalFromShoppingCartIcon() throws Exception
	{
		//Wait for dialog to display after submitting the article
		Thread.sleep(2000);
		String totalCartValue= driver.findElement(cartTotal_XPATH).getText().trim();
		productQtyCount++;
		Assert.assertEquals(totalCartValue.trim(),productQtyCount.toString(),"Cart value mismatch");
		
		return new ResellerQuoteRequestCategoryPage(driver);
	}
	
	public ResellerQuoteRequestCategoryPage click_On_Cart_Icon() throws Exception
	{
		Thread.sleep(2000);
		GUIFunctions.clickElement(driver, soppingCartIcon_XPATH, "Click on shopping cart icon");
		
		return new ResellerQuoteRequestCategoryPage(driver);
	}
	
	public ResellerCartPage click_On_View_Cart_Button_From_Overlay () throws Exception
	{
		Thread.sleep(2000);
		
		GUIFunctions.clickElement(driver, viewMiniCartButton_XPATH, "Click on view cart button");
		
		return new ResellerCartPage(driver);
	}
	
	public ResellerQuoteRequestCategoryPage saveProductQty() throws Exception{
		Thread.sleep(5000);
		String totalCartValue= driver.findElement(cartTotal_XPATH).getText().trim();
		productQtyCount =Integer.parseInt(totalCartValue);
		return new ResellerQuoteRequestCategoryPage(driver);
	}
}
