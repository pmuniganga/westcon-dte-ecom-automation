package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC12_Quoting_Mandatory_CrossSell extends BaseTest {
	
	static  QuotingQuoteDetailpage amQuoteDetailPage;


	@Test(description = "Call TC03 (add F5-BIG-LTM-7200V part number)", priority = 1)
	public void Step01_EnterPartNumberAndClickSubmit() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");

		//Call TC 03
		log.info("successfully submitted the part numbers and Quote detail page is displayed");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");

	}      

	@Test(description = "Verify the Mandatory cross sell ", priority = 2)
	public void Step02_VerifyCrossSellDialog() throws Exception {

		amQuoteDetailPage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailPage.verifyCrossSellDialog();

		log.info("Mandatory cross sell overlay is displayed");
		Reporter.log("<p>Mandatory cross sell overlay is displayed");

	}

	@Test(description = "Verify both mandatory cross sell part numbers in the overlay", priority = 3)
	public void Step03_VerifyCrossSellParts() throws Exception {

		amQuoteDetailPage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailPage.verifyCrossSellParts();

		log.info("Mandatory cross sell products displayed for the part number added is matches with products from SAP (as per property file)");
		Reporter.log("<p>Mandatory cross sell products displayed for the part number added is matches with products from SAP (as per property file)");

	}

	@Test(description = "Enter quantity 1 for both products and click on add to quote", priority = 4)
	public void Step04_enterQtyAndAddingParts() throws Exception {

		amQuoteDetailPage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailPage.enterQtyCrossSellParts();

		log.info("Successfully entered quantity as 1 for both cross sell products and added to quote");
		Reporter.log("<p>Successfully entered quantity as 1 for both cross sell products and added to quote");

	}

	@Test(description = "Verify the products added in quote table", priority = 5)
	public void Step05_verifyProductInQuoteTable() throws Exception {
		amQuoteDetailPage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailPage.verifyProductInQuoteTable();

		log.info("Successfully verified the Cross sell products in the quote table");
		Reporter.log("<p>Successfully verified the Cross sell products in the quote table");

	}

	@Test(description = "Verify associated line for 3 products", priority = 6)
	public void Step06_verifyAssocLineInProdTable() throws Exception {
		amQuoteDetailPage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailPage.assocLinecells();

		log.info("Successfully verified parent association line numbers for cross sell products added");
		Reporter.log("<p>Successfully verified parent association line numbers for cross sell products added");

	}


}
