package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC49_Quoting_EnterEndUserAccount_DirectInput extends BaseTest{
	
	//static QuotingQuoteDetailpage amQuotePage;
	static String euserAccNo = null;
	String endUserDetails;
	
     /**
       * Call TC03
      */
	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
	}

	/**
	 * Enter End User Account Number
     * 
    */
	@Test(description = "Step 2: Enter End User Account No. ", priority = 2)
	public void Step02_EnterEndUserAccNo() throws Exception {
		
		// Direct Entering End User Account No. 
		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		euserAccNo=userInfoDSDetails.getEndUserAccountNumber();
        System.out.println("End User Account Number*****"+euserAccNo);

		GUIFunctions.pageScrollDown(driver, 100);
        amQuotePage.enterEndUserAcc_DirectInput(euserAccNo);
		
		log.info("Successfully entered End User Account No.");
		Reporter.log("<p>Successfully entered End User Account No.");
	}
	
	/**
	 * Click on Retrieve Account button
     * 
    */
	@Test(description = "Step 3: Click on Retrieve Account button", priority = 3)
	public void Step03_ClickOnRetrieveAccount() throws Exception {
		
		//Clicking on Retrieve Account button
		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		amQuotePage.ClickOnEndUsrRetrivAccBtn(driver);

		log.info("Successfully Clicking on Retrive Account button");
		Reporter.log("<p>Successfully Clicking on Retrive Account button");
	}

	/**
	 * Verify End User Account Retrieved
     * 
    */
	@Test(description = "Step 4: Verify End User Retrived", priority = 4)
	public void Step04_VerifyEndUserRetrived() throws Exception {
		
		// Verifying End User Retrieved	
		endUserDetails =driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserAccountDetails_XPATH"))).getText().trim();
		System.out.println("End USer Account retrieved from SAP: "+endUserDetails);
		Assert.assertEquals(endUserDetails,euserAccNo ,"Proper End User not retrived");
		
		log.info("Successfully verified retrieved End User Account Info");
		Reporter.log("<p>Successfully verified retrieved End User Account Info");

	}	

}
