package in.valtech.westcon.test.rs;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.ResellerPages.ResellerRequestAQuotePage;

public class TC54_Reseller_VendorFile extends BaseTest {

	@Test(description = "Step 1: Call TC53", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("Add product page should be displayed and by default Copy & Paste BOM should be selected  \n");
		Reporter.log("<p>Add product page should be displayed and by default Copy & Paste BOM should be selected ");
	}

	@Test(description = "Step 2: Select Vendor", priority = 2)
	public void Step02_Select_Vendor() throws Exception {

		//Execute TC02 to navigate to Add product page with reseller selected
		ResellerRequestAQuotePage rsRequestAQuotePage =new ResellerRequestAQuotePage(driver);
		//Select upload a file
		rsRequestAQuotePage.ClickOnUploadFile();

		Thread.sleep(5000);
		log.info("Successfully clciked on upload a file\n");
		Reporter.log("<p>--Successfully clciked on upload a file ");	
		//Select a vendor from vendor drop down
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getVendorFilePath());
			log.info("SKU*************" + SKUdataSetList.get(i).getVendorName());
			rsRequestAQuotePage.SelectVendor(SKUdataSetList.get(i).getVendorName());
			Thread.sleep(5000);

			log.info("Vendor should be selected from dropdown\n");
			Reporter.log("<p>--Vendor should be selected from dropdown ");	
		}
	}

	@Test(description = "Step 3: Browse file and Click on Submit button ", priority = 3)
	public void Step03_BrowseFileAndSubmit() throws Exception {
		ResellerRequestAQuotePage rsRequestAQuotePage =new ResellerRequestAQuotePage(driver);

		//Click on Browse button
		rsRequestAQuotePage.EnterUploadFilePath();

		/*
		//upload file - Auto It script
		 rsRequestAQuotePage.ClickOnBrowseButton();
		 String dir = CustomFun.getRootDir();
		 dir = CustomFun.getRootDir();
		 for (int i = 0; i < SKUdataSetList.size(); i++) {
			Runtime.getRuntime()
			.exec("D:\\Westcon-DTE-Ecom\\Westcon_Auto_Workspace\\Westcon DTE Automation\\resources\\autoit\\IE_UploadingVendorFile.exe");
		 }*/

		log.info("Successfully uploaded a file\n");
		Reporter.log("<p>--Successfully uploaded a file");

		Thread.sleep(5000);
		rsRequestAQuotePage.UploadAFileSubmit(driver);  		

		log.info("Reseller Quote details page is displayed\n");
		Reporter.log("<p>--Reseller Quote details page is displayed ");	

	}	

}
