package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC104_Quoting_EnterLineNotes extends BaseTest {

	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08 -- ", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");		

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}	

	@Test(description = "Step 2 : Click on Pricing Tab", priority = 2)
	public void Step02_ClickOnPricingTab() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();
		
		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");
	}

	@Test(description = "Step 3 : Click on Add link for the line item", priority = 3)
	public void Step03_ClickOnAddLink() {
		//Click on Add link for the first product
		By AddNotesProduct1 = By.xpath(ObjRepoProp.getProperty("AddNotesProduct1_XPATH"));

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnAddLink(AddNotesProduct1);

		log.info("Successfully clicked on Add link and notes overlay is opened\n");
		Reporter.log("<p>Successfully clicked on Add link and notes overlay is opened");
	}

	@Test(description = "Step 4 : Enter Notes in the text area available", priority = 4)
	public void Step04_EnterNotes() {
		//Enter the Notes
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterNotes(TextProp.getProperty("NotesTextInternal"));

		log.info("Successfully entered notes\n");
		Reporter.log("<p>Successfully entered notes");
	}

	@Test(description = "Step 5 : Select Westcon Only Radio button", priority = 5)
	public void Step05_SelectWestconOnlyRadioButton() {
		//Select Westcon Internal Only Radio Button
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.selectWestconInternalOnly();

		log.info("Successfully Selected Westcon Internal Only Radio button\n");
		Reporter.log("<p>Successfully Selected Westcon Internal Only Radio button");
	}

	@Test(description = "Step 6 : Click on Save button", priority = 6)
	public void Step06_ClickSaveButton() {
		//Click on Save Button
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickSaveButton();

		log.info("Successfully Clicked on Save button\n");
		Reporter.log("<p>Successfully Clicked on Save button");
	}

	@Test(description = "Step 7 : Click on Add link for the line item", priority = 7)
	public void Step07_ClickOnAddLink() {
		//Click on Add link for the first product
		By AddNotesProduct2 = By.xpath(ObjRepoProp.getProperty("AddNotesProduct2_XPATH"));

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnAddLink(AddNotesProduct2);

		log.info("Successfully clicked on Add link and notes overlay is opened\n");
		Reporter.log("<p>Successfully clicked on Add link and notes overlay is opened");
	}

	@Test(description = "Step 8 : Enter Notes in the text area available", priority = 8)
	public void Step08_EnterNotes() {
		//Enter the Notes
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterNotes(TextProp.getProperty("NotesTextExternal"));

		log.info("Successfully entered notes\n");
		Reporter.log("<p>Successfully entered notes");
	}

	@Test(description = "Step 9 : Select Westcon Only Radio button", priority = 9)
	public void Step09_SelectWestconOnlyRadioButton() {
		//Select Westcon Customer Radio Button
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.selectWestconCustomer();

		log.info("Successfully Selected Westcon Customer Radio button\n");
		Reporter.log("<p>Successfully Selected Westcon Customer Radio button");
	}

	@Test(description = "Step 10 : Click on Save button", priority = 10)
	public void Step10_ClickSaveButton() {
		//Click on Save Button
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickSaveButton();

		log.info("Successfully Clicked on Save button\n");
		Reporter.log("<p>Successfully Clicked on Save button");
	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 11: Select Submit quote from the dropdown and click on GO button", priority = 11)
	public void Step11_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 12: Click On No, do not validate button", priority = 12)
	public void Step12_ClickOnDoNotValidateButton1()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}

	@Test(description = "Step 12 : Click on Add link for the line item", priority = 12)
	public void Step12_ClickOnAddLink() {
		//Click on Add link for the first product
		By AddNotesProduct1 = By.xpath(ObjRepoProp.getProperty("AddNotesProduct1_XPATH"));

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnAddLink(AddNotesProduct1);

		log.info("Successfully clicked on Add link and notes overlay is opened\n");
		Reporter.log("<p>Successfully clicked on Add link and notes overlay is opened");
	}

	@Test(description = "Step 13 : Verify the Saved Notes", priority = 13)
	public void Step13_VerifyNotesText() {
		//Verify Saved Notes
		By AddNotesTextarea = By.xpath(ObjRepoProp.getProperty("AddNotesTextarea_XPATH"));
		Assert.assertEquals(driver.findElement(AddNotesTextarea).getText(), TextProp.getProperty("NotesTextInternal"));

		log.info("Successfully verified the saved text\n");
		Reporter.log("<p>Successfully verified the saved text");
	}

	@Test(description = "Step 14 : Click on Save button", priority = 14)
	public void Step14_ClickCloseIcon() {
		//Click on Close Icon
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickCloseIcon();

		log.info("Successfully Clicked on Close Icon\n");
		Reporter.log("<p>Successfully Clicked on Close Icon");
	}

	@Test(description = "Step 15 : Click on Add link for the line item", priority = 15)
	public void Step15_ClickOnAddLink() {
		//Click on Add link for the first product
		By AddNotesProduct2 = By.xpath(ObjRepoProp.getProperty("AddNotesProduct2_XPATH"));

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnAddLink(AddNotesProduct2);

		log.info("Successfully clicked on Add link and notes overlay is opened\n");
		Reporter.log("<p>Successfully clicked on Add link and notes overlay is opened");
	}

	@Test(description = "Step 16 : Verify the Saved Notes", priority = 16)
	public void Step16_VerifyNotesText() {
		//Verify Saved Notes
		By AddNotesTextarea = By.xpath(ObjRepoProp.getProperty("AddNotesTextarea_XPATH"));
		Assert.assertEquals(driver.findElement(AddNotesTextarea).getText(), TextProp.getProperty("NotesTextExternal"));

		log.info("Successfully verified the saved text\n");
		Reporter.log("<p>Successfully verified the saved text");
	}

	@Test(description = "Step 17 : Click on Save button", priority = 17)
	public void Step17_ClickCloseIcon() {
		//Click on Close Icon
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickCloseIcon();

		log.info("Successfully Clicked on Close Icon\n");
		Reporter.log("<p>Successfully Clicked on Close Icon");
	}
}