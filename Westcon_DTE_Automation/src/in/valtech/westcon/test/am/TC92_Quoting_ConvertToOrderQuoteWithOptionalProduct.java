package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import org.openqa.selenium.By;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.common.CommonFun;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;

import in.valtech.westcon.QuotingPages.QuoteToOrderConfirmationPage;
import in.valtech.westcon.QuotingPages.QuoteToOrderPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC92_Quoting_ConvertToOrderQuoteWithOptionalProduct extends BaseTest {
	
	String alternativeOptionalOverlayMsg;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	/**
	 * Select Convert quote to Order from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Select Convert quote to Order from the dropdown and click on GO button", priority = 2)
	public void Step02_SelectConvertQuoteFromDropdown() throws Exception {
		//GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectConvertQuoteOption();
		
		By alternativeOptionalOverlay = By.xpath(ObjRepoProp.getProperty("alternativeOptionalOverlay_XPATH"));
		
		CustomFun.waitObjectToLoad(driver, alternativeOptionalOverlay, 60);
		
		alternativeOptionalOverlayMsg = driver.findElement(By.xpath(ObjRepoProp.getProperty("alternativeOptionalOverlayMsg_XPATH"))).getText().trim();
		
		Assert.assertEquals(alternativeOptionalOverlayMsg, TextProp.getProperty("alternativeOptionalOverlayMessage"));

		log.info("Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
	}
	
	@Test(description = "Step 3: Select Convert quote to Order from the dropdown and click on GO button", priority = 3)
	public void Step03_ClickConfirmButton() throws Exception {
		//GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.clickConfirmButton();
		
		By splitOrderMessage = By.xpath(ObjRepoProp.getProperty("splitOrderMessage_XPATH"));
		
		CustomFun.waitObjectToLoad(driver, splitOrderMessage, 60);

		log.info("Successfully clicked on confirm button\n");
		Reporter.log("<p>Successfully clicked on confirm button\n");
	}
	
	@Test(description = "Step 5: Click on Yes button", priority = 5)
	public void Step05_ClickYesButton() throws Exception {
		//Click on Yes button in Split Order Overlay
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver); 
		amQuoteDetailsPage.clickYesButton(driver);
		
		log.info("Clicked on Yes button successfully\n");
		Reporter.log("<p>Clicked on Yes button successfully");
	}
	
	@Test(description = "Step 6: Click on Submit button", priority = 6)
	public void Step06_SubmitSplitOrder() throws Exception {
		//Click on Submit button from Split Order Overlay
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.submitSplitOrder(driver);
		
		log.info("Submit button clicked successfully\n");
		Reporter.log("<p>Submit button clicked successfully");
	}
	
	@Test(description = "Step 7 : Click on Yes Button", priority = 7)
	public void Step07_ClickEmailYesButton() throws InterruptedException {
		
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.emailYesButton(driver);
		
		log.info("Yes button clicked successfully\n");
		Reporter.log("<p>Yes button clicked successfully");
		
	}
	
	/**
	 * Function to enter PO number
	 */

	@Test(description = "Step 8:Enter PO number ", priority = 8)
	public void Step08_EnterPoNumber() throws Exception 
	{
		QuoteToOrderPage amConvertToOrder = new QuoteToOrderPage(driver);
		
		amConvertToOrder.EnterPoNumber(userInfoDSDetails.getPoNumber());
		log.info("PO number entered\n");
		Reporter.log("<p>PO number entered");

	}
	
	/**
	 *Click on submit
	 */
	@Test(description="step 9: Click on submit button",priority = 9)
	public void Step09_ClickSubmitButton()throws Exception
	{
		QuoteToOrderPage amConvertToOrder=new QuoteToOrderPage(driver);
		amConvertToOrder.ClickOnSubmit();
		log.info("SUbmit button clicked");
		Reporter.log("<p>--Submit button clicked");
	}

	/**
	 * To fetch sales order number
	 */
	String SalesOrdNumber;
	@Test(description="step 10: To fetch sales order number",priority = 10)
	public void Step10_salesOrderNumber()throws Exception
	{
		QuoteToOrderConfirmationPage amOrderConfirmation=new QuoteToOrderConfirmationPage(driver);
		//amOrderConfirmation.FetchOrderNumber();
		amOrderConfirmation.FetchOrderNumber();
		SalesOrdNumber = amOrderConfirmation.salesorderNumber;
		System.out.println("Sales order number : "+ SalesOrdNumber);
		Thread.sleep(5000);
		CommonFun.QuotingLogout(driver, "You have signed out of your account.");
		log.info("Sales order number displayed");
		Reporter.log("<p>--Sales order number displayed");
	}
}