package in.valtech.westcon.test.rs;

import static in.valtech.custom.CustomFun.SKUdataSetList;

import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.westcon.ResellerPages.ResellerRequestAQuotePage;

public class TC53_Reseller_CopyPasteBOM extends BaseTest
{
	//SKU_DSDetails SKU = new SKU_DSDetails();
	String SKUCount = null;

	static  ResellerRequestAQuotePage rsRequestAQuotePage;

	@Test(description = "Step 1: Call TC51 TC52", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-51
		log.info("TC-51 Successfully Executed \n");
		Reporter.log("<p>TC-51 Successfully Eexecuted ");
		// Execute TC-52
		log.info("TC-52 Successfully Executed \n");
		Reporter.log("<p>TC-52 Successfully Eexecuted ");
	}


	@Test(description = "Step 2: Enter PartNumbers and Click Submit", priority = 2)
	public void Step02_EnterPartNumbersAndClickSubmit() throws Exception {
		//Reseller_RequestAQuotePage add = new Reseller_RequestAQuotePage(driver);

		rsRequestAQuotePage = new ResellerRequestAQuotePage(driver);
		int SKUCountInt = SKUdataSetList.size();
		SKUCount = Integer.toString(SKUCountInt);
		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());

			rsRequestAQuotePage.CopyPasteBOM(driver , SKUdataSetList.get(i)
					.getSKU()); 
			rsRequestAQuotePage.CopyPasteBOMSubmit(driver);  

		}       

		log.info("successfully submitted the part numbers and Quote details page is displayed \n");
		Reporter.log("<p>successfully submitted the part numbers and Quote details page is displayed");

	}                  

}
