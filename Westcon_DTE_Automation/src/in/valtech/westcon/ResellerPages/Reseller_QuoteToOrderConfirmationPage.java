package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class Reseller_QuoteToOrderConfirmationPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller Quote To Order Confirmation Page 
	public  Reseller_QuoteToOrderConfirmationPage (WebDriver driver) throws InterruptedException {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right Quote To OrderConfirmationPage  
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerQuoteToOrderConfirmationPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Quote To OrderConfirmationPage  page");

		}
	}
	//Function to fetch sales order number from order confirmation page
	public static String salesorderNumber;
	public Reseller_QuoteToOrderConfirmationPage FetchOrderNumber() throws Exception
	{
		String orderNumber=driver.findElement(By.xpath("//div[@class='main-content']/h2")).getText();
		salesorderNumber = orderNumber.substring(27, orderNumber.length());
		System.out.println("orderNumber1="+salesorderNumber);
		return new Reseller_QuoteToOrderConfirmationPage(driver);
	}
	
	//Function to mouse hover on order history 
	By orderHistoryNavigationLink=By.xpath(ObjRepoProp.getProperty("orderHistoryNavigationLink_XPATH"));
	public Reseller_QuoteToOrderConfirmationPage mouseHoverOnOrderHistory()throws Exception
	{
		GUIFunctions.mouseOverElement(driver, driver.findElement(orderHistoryNavigationLink));
		return new Reseller_QuoteToOrderConfirmationPage(driver);
	}
	
	//Function to click on view open and closed order link
	By viewOpenAndClosedOrderNavigationLink=By.xpath(ObjRepoProp.getProperty("viewOpenAndClosedOrderNavigationLink_XPATH"));
	public ResellerOrderListingPage clickOnViewOpenAndClosedOrderLink()throws Exception
	{
		GUIFunctions.clickElement(driver, viewOpenAndClosedOrderNavigationLink, "view open and closed order link ");
		System.out.println("clicked on view open and closed order link");
		return new ResellerOrderListingPage(driver);
		
	}
}
