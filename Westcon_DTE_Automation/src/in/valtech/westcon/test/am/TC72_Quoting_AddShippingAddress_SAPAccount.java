package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC72_Quoting_AddShippingAddress_SAPAccount extends BaseTest{

	static QuotingQuoteDetailpage amQuoteDetailspage;
	static String Attn = null;
	static String Company = null;
	static String Add1 = null;
	static String Add2 = null;
	static String City = null;
	static String postal = null;
	static String state = null;
	static String Phone = null;
	static String carrier=null;

	/**
	 *
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_NavigateToCreateQuotePage() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");		

		log.info("Successfully navigated to Create quote page");
		Reporter.log("<p>--Successfully navigated to Create quote page ");	


	}

	/**
	 * Click on Shipping tab
	 * 
	 */
	@Test(description="Step 2:Click on Shipping tab", priority=2)
	public void Step2_ClickOnShippingTab() throws Exception{
		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);

		//click on shipping tab
		amQuoteDetailspage.clickOnShippingtab();

		//Verify shipping tab opened 
		Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("ShippingTabOpen_XPATH")), driver),
				"Shipping tab is not open");

		log.info("Successfully verified shipping tab is open");
		Reporter.log("<p>--Successfully verified shipping tab is open");	

	}

	/**
	 * Click on Shipping tab
	 * 
	 */
	@Test(description="Step 3:Click on Edit shipping information button", priority=3)
	public void Step3_ClickOnEditShippingInformationBtn() throws Exception{
		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);

		//Click on Edit shipping information button
		amQuoteDetailspage.clickOnEditShippingInfoButton();

		log.info("Successfully clicked on edit shipping info button");
		Reporter.log("<p>--Successfully clicked on edit shipping info button");	
	}
	
		
	/**
	 * Select 1View Address Book radio button
	 */
	@Test(description = "Step 4:Select Search SAP Ship-to account radio button", priority = 4)
	public void Step04_SelectSearchSAPShiptoAccountRadioButton() throws Exception {

		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);

		Boolean flag = false;
		flag = CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("InputSAPAccountNo_XPATH")),driver);
		if(!flag) {
			//click on new address radio button
			amQuoteDetailspage.clickOnSearchSAPAccountRadioBtn();

			log.info("Successfully clicked on Search SAP Ship-to account radio button");
			Reporter.log("<p>--Successfully clicked on Search SAP Ship-to account radio button");
		}
		else {

			//Verify fields enabled
			Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("InputSAPAccountNo_XPATH")), driver),
					"Fields not enabled");

			log.info("Successfully verified Search SAP Ship-to account fields enabled");
			Reporter.log("<p>--Successfully verified Search SAP Ship-to account fields enabled");	
		}
	}

	/**
	 * Select saved address from dropdown
	 * 
	 */
	@Test(description="Step 5:Enter SAP Ship to Account Number", priority=5)
	public void Step5_SelectSavedAddressfromDropdown() throws Exception{
		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);

		//Enter Ship to account
		String shipToAccount=userInfoDSDetails.getReseller();
		amQuoteDetailspage.EnterSAPShipToAccount(shipToAccount);

		log.info("Successfully Entered SAP Ship to Account Number");
		Reporter.log("<p>--Successfully Entered SAP Ship to Account Number");
	}

	/***
	 * Click on Search button to add an ship to address
	 * 
	 */
	@Test(description = "Step 6:Click on Search button", priority = 6)
	public void Step06_ClickOnSearchButton() throws Exception {

		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailspage.clickOnSAPAccountSearchBtn();

		log.info("Successfully Clicked on Search button");
		Reporter.log("<p>--Successfully Clicked on Search button");	
	}

	/**
	 * Select shipping method , enter carrier account number, shipdate , select request blind shipment,
	 *  select request ship complete, enter shipping instructions and Click Save button
	 */
	@Test(description = "Step 7:Select shipping method , enter carrier account number, shipdate , select request blind shipment, select request ship complete,"
			+ " enter shipping instructions and Click Save button", priority = 7)
	public void Step07_SelectShippingMethodOtherDetailsAndSave() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		//Select shipping method
		if(locale.equalsIgnoreCase("US")) {
			amQuoteDetailsPage.SelectShippingMethod(TextProp.getProperty("shippingMethodvalue"));
		}
		else if(locale.equalsIgnoreCase("CA")){
			amQuoteDetailsPage.SelectShippingMethod(TextProp.getProperty("shippingMethodvalue_CA"));
		}

		log.info("Successfully selected the shipping method");
		Reporter.log("<p>--Successfully selected the shipping method");	


		//enter carrier account number
		carrier=userInfoDSDetails.getCarrierNumber();
		amQuoteDetailsPage.EnterCarrierNumber(carrier);


		//select request blind shipment
		amQuoteDetailsPage.clickOnBlindshipmentCheckbox();

		//select request ship complete

		//enter shipping instructions 
		amQuoteDetailsPage.enterShippingInstructions(TextProp.getProperty("shippingInstructions"));

		//click on save shipping instruction
		amQuoteDetailsPage.clickOnSaveShippingInstructionCheckBox();

		//select ship date 
		amQuoteDetailsPage.enterEarliestRequiredShipDate(CustomFun.addDays(new Date(), Integer.parseInt(userInfoDSDetails
				.getQuoteCloseDate().trim()), locale));

		log.info("Successfully saved the shipping details");
		Reporter.log("<p>--Successfully saved the shipping details");	
	}
	
	/**
	 * Click Save button
	 * 
	 * @throws Exception
	 */
	@Test(description="Step8:Click on Save Button",priority=8)
	public void Step08_ClickOnSavebtn() throws Exception{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		//Click Save button
		amQuoteDetailsPage.clickOnSaveButton();
		
		log.info("Successfully Clicked on Save Button");
		Reporter.log("<p>--Successfully Clicked on Save Button");	
	} 
	
	/**
	 * Verify Shipping method , carrier account number, 
	 * ship date , request blind shipment,  request ship complete, shipping instructions 
	 * 
	 */
	@Test(description = "Step 9:Verify Shipping method , carrier account number, shipdate , request blind shipment,  "
			+"request ship complete, shipping instructions ", priority = 9)
	public void Step09_VerifyShippingDetails() throws Exception {


		//QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("shippingMethodEntered_XPATH"))));
		
		Thread.sleep(4000);
		System.out.println("Shipping method entered****"+driver.findElement(By.xpath(ObjRepoProp.
		getProperty("shippingMethodEntered_XPATH"))).getText().trim());
		//Verify shipping method
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("shippingMethodEntered_XPATH"))).getText().trim(),TextProp.getProperty("shippingMethod"), " The shipping method selected is not as expected");

		log.info("Successfully verified the shipping method");
		Reporter.log("<p>--Successfully verified the shipping method");


		//verify carrier account number
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("carrierAccountNumberEntered_XPATH"))).getText(), carrier , " The carrier account number is not as expected ");

		log.info("Successfully verified carrier account number");
		Reporter.log("<p>--Successfully verified carrier account number");

		//Verify required ship date
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("BlindShipmentEntered_XPATH"))).getText(), "Yes", "The required shipdate is not as expected");

		log.info("Successfully verified the required ship date");
		Reporter.log("<p>--Successfully verified the required ship date");

		/*//Verify ship complete
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("shipCompleteEntered_XPATH"))).getText(), "Yes", " The ship complete is not as expected");*/

	/*	log.info("Successfully verified the ship complete");
		Reporter.log("<p>--Successfully verified the ship complete");*/

		//Verify instructions
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("shippingInstructionEntered_XPATH"))).getText(),TextProp
				.getProperty("shippingInstructions"), " The instructions are not as expected ");

		log.info("Successfully verified instructions");
		Reporter.log("<p>--Successfully verified instructions");


	}
}
