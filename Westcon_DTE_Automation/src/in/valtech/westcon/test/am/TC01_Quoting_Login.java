package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import in.valtech.westcon.QuotingPages.QuotingLoginPage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

public class TC01_Quoting_Login extends BaseTest {
	
	static QuotingLoginPage amLoginPage;
	static QuotingQuotesPage amQuotelistingPage;
	
	static String username = null;
	static String password = null;
	
	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */
	@Test(description = "Step 1: Open browser,Navigate to the AM URL", priority = 1)
	public void Step01_NavigateToWestconAM_URL() throws Exception {
		
		// Navigate/launch the AM URL
		amLoginPage = QuotingLoginPage.navigateTo_URL(driver, baseUrl, driverName);
		
		log.info("Navigated to the AM URL");
		Reporter.log("<p>Navigated to AM Login Page URL -- "
				+ driver.getTitle());			
	}
	
	/**
	 * Enter username , Pwd and click on Login link
	 * 
	 */
	@Test(description = "Step 2: Enter username , Pwd and click on Login link", priority = 2)
	public void Step02_AMLogin() throws Exception {

		System.out.println("user=" +userInfoDSDetails.getAmUserName());
		System.out.println("pwd=" +userInfoDSDetails.getAmPassword());
		username = userInfoDSDetails.getAmUserName();		
		password = userInfoDSDetails.getAmPassword();
		
		//amLoginPage = new AMLogin(driver);
	 
		amLoginPage.enterLoginCreds(username, password);
		
		//amLoginPage= AMLogin.enterLoginCreds(username, password);
		log.info("The AM successfully logged-in and Quote listing page is displayed\n");
		Reporter.log("<p>The AM successfully logged-in and Quote listing page is displayed");
			
	}
	
	@Test(description = "Step 3: Enter username , Pwd and click on Login link", priority = 3)
	 public void Step03_Login() throws Exception {
	  amQuotelistingPage = amLoginPage.clickSubmit();
	  
	  log.info("The AM successfully logged-in and Quote listing page is displayed\n");
	  Reporter.log("<p>The AM successfully logged-in and Quote listing page is displayed");
	 }
	
}

