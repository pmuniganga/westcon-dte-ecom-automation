package in.valtech.westcon.QuotingPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class AMRequestQuotesListing {
	private final WebDriver driver;

	GUIFunctions gui = new GUIFunctions();
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// AM login page Constructor
	public AMRequestQuotesListing(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("AmRequestQuoteslistingPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Quoting Quote Details page");
		}
	}

	By AmRequestedQuotesPageSearch_XPATH=By.xpath(ObjRepoProp.getProperty("AmRequestedQuotesPageSearch_XPATH"));
	By AmRequestedQuotesPageRFQId_XPATH=By.xpath(ObjRepoProp.getProperty("AmRequestedQuotesPageRFQId_XPATH"));

	/** This function will search for the RFQ submitted
	 * 
	 */
	public AMRequestQuotesListing searchForTheRFQSubmittedAndHitEnter(String RFQ) throws Exception
	{				

		GUIFunctions.typeTxtboxValue(driver,AmRequestedQuotesPageSearch_XPATH, RFQ);
		driver.findElement(By.xpath(ObjRepoProp
				.getProperty("AmRequestedQuotesPageSearch_XPATH"))).sendKeys(Keys.ENTER);
		CustomFun.waitForPageLoaded(driver);
		return new AMRequestQuotesListing(driver);
	}

	/***
	 * This function will click on RFQ id
	 */
	public QuotingQuoteDetailpage clickOnRFQId() throws Exception
	{
		CustomFun.waitObjectToLoad(driver, AmRequestedQuotesPageRFQId_XPATH, 30);
		GUIFunctions.clickElement(driver,AmRequestedQuotesPageRFQId_XPATH, "RFQ id");
		
		return new QuotingQuoteDetailpage(driver);
	}


}

