/**
 * 
 */
package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.ArrayList;
import java.util.List;

import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

/**
 * @author Deepak.tripathi
 *
 */
public class TC93_Quoting_VerifyPartNumbersDescription_PricingGrid extends BaseTest{
	//static QuotingQuoteDetailpage amQuotePage;
	
	 List<String> partNumbers= new ArrayList<String>();

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	@Test(description = "Step 2: Verify the part numbers description in pricing grid ", priority = 2)
	public void Step02_VerifyPartNumbersDescriptions() throws Exception {
		
		GUIFunctions.pageScrollDown(driver, 200);
		 List<WebElement> productsDescription = driver.findElements(By.xpath(ObjRepoProp.getProperty("ProductsDescription_XPATH")));   
	      List<WebElement> SKUs = driver.findElements(By.xpath(ObjRepoProp.getProperty("partNumberTableValue_XPATH")));
	      
	      //collect products added to pricing grid
	        for (int i=0;i<SKUs.size();i++){
	        	partNumbers.add(SKUs.get(i).getText().trim());
		     }   
	       	        	        
    for (int i=0;i<SKUs.size();i++){
	     System.out.println("Part Number****"+partNumbers.get(i)); 
	     System.out.println("Part Descriptrion****"+productsDescription.get(i).getText().trim());
	     
    	Assert.assertEquals(productsDescription.get(i).getText().trim(), TextProp.getProperty(partNumbers.get(i)+"_Description"),
		    	"Partnumber description mismatch");   	
		            
			} 
	        
	        
		log.info("Successfully Verified the part numbers description in pricing grid");
		Reporter.log("<p>Successfully Verified the part numbers description in pricing grid");

	}

}	
