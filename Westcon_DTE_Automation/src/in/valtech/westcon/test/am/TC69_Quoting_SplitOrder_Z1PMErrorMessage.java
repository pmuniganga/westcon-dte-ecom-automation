package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;

import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC69_Quoting_SplitOrder_Z1PMErrorMessage extends BaseTest {

	/**
	 * Call TC06
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}
	
	@Test(description = "Step 3: Select Convert to Order option from dropdown and click on GO", priority = 3)
	public void Step03_SelectConvertToOrderOption() throws Exception {
		//Select Convert to Order option and click on GO
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.selectConvertToOrderAndSubmit();
		
		log.info("Convert to Order option selected successfully and clicked on GO button\n");
		Reporter.log("<p>Convert to Order option selected successfully and clicked on GO button");
	}
	
	@Test(description = "Step 4: Verify Split Order Overlay confirmation popup", priority = 4)
	public void Step04_VerifySplitOrderOverlayConfPopup() throws Exception {
		//Split Order Overlay Confirmation Popup
		String splitOrderText = driver.findElement(By.xpath(ObjRepoProp.getProperty("splitOrderMessage_XPATH"))).getText();
		Assert.assertEquals(splitOrderText, TextProp.getProperty("splitOrderMessage"), "Split Order Overlay Text doesn't match");
			
		log.info("Split Overlay Text verified successfully\n");
		Reporter.log("<p>Split Overlay Text verified successfully");
	}
	
	@Test(description = "Step 5: Click on Yes button", priority = 5)
	public void Step05_ClickYesButton() throws Exception {
		//Click on Yes button in Split Order Overlay
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver); 
		amQuoteDetailsPage.clickYesButton(driver);
		
		log.info("Clicked on Yes button successfully\n");
		Reporter.log("<p>Clicked on Yes button successfully");
	}
	
	@Test(description = "Step 6: Enter Qty value greater than and less than the existing quantity", priority = 6)
	public void Step06_EditQuantity() throws Exception {
		//Edit the quantity of part numbers more than and less than the added quantity
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.editQuantity(driver);
		
		driver.findElement(By.xpath("//label[@for='qut-split-order-select-all-checkbox']")).click();
		driver.findElement(By.xpath("//label[@for='qut-split-order-select-all-checkbox']")).click();
		
		log.info("Quantity edited successfully\n");
		Reporter.log("<p>Quantity edited successfully");
	}
	
	@Test(description = "Step 7: Click on Submit button", priority = 7)
	public void Step06_SubmitSplitOrder() throws Exception {
		//Click on Submit button from Split Order Overlay
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.submitSplitOrder(driver);
		
		log.info("Submit button clicked successfully\n");
		Reporter.log("<p>Submit button clicked successfully");
	}
	
	@Test(description = "Step 8: Verify Error Message for Manual SBA", priority = 8)
	public void Step07_VerifyErrorMessage() throws Exception {
		//Verify the error message displayed
		String errorMessage = driver.findElement(By.xpath(ObjRepoProp.getProperty("splitOrderErrorMessage_XPATH"))).getText();
		Assert.assertTrue(errorMessage.trim().contains(TextProp.getProperty("splitOrderErrorMessage")));
		
		log.info("Verified the error message successfully\n");
		Reporter.log("<p>Verified the error message successfully");
	}
}
