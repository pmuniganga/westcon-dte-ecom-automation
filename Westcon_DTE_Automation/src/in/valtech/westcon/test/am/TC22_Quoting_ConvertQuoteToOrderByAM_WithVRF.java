package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.common.CommonFun;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuoteToOrderConfirmationPage;
import in.valtech.westcon.QuotingPages.QuoteToOrderPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;
import in.valtech.westcon.ResellerPages.ResellerLoginPage;
import in.valtech.westcon.ResellerPages.ResellerOrderListingPage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;
import in.valtech.westcon.ResellerPages.Reseller_OrderDetailPage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC22_Quoting_ConvertQuoteToOrderByAM_WithVRF extends BaseTest {

	String PoNumber;
	static String username = null;
	static String password = null;
	/**
	 * User to Submit quote in quoting by executing TC-11
	 * 
	 */

	@Test(description = "Step 1: User to submit quote  by executing TC11", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");	
	}
	/**
	 * Click on VRF tab
	 * @throws Exception 
	 */
	@Test(description = "Step2 : Click on VRF tab", priority = 2)
	public void Step02_ClickVRFTab() throws Exception {
		//Click on VRF tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//GUIFunctions.pageScrollUP(driver, 10);
		quoteDetails.clickVRFTab();

		log.info("Successfully Clicked on VRF tab");
		Reporter.log("<p>Successfully Clicked on VRF tab");
	}

	/**
	 * Enter VRF Auth Number
	 */
	@Test(description = "Step3 : Enter VRF Auth Number", priority = 3)
	public void Step03_EnterVRFAuthNumber() {
		//Enter VRF Auth Number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.EnterVRFAuthNumber();

		log.info("Successfully Entered VRF Auth Number");
		Reporter.log("<p>Successfully Entered VRF Auth Number");
	}

	/**
	 * Click on Apply Button
	 */
	@Test(description = "Step4 : Click on Apply Button", priority = 4)
	public void Step04_ClickApplyButton() {
		//Click on Apply Button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.ClickApplybutton();

		log.info("Successfully Clicked on Apply Button");
		Reporter.log("<p>Successfully Clicked on Apply Button");
	}

	/**
	 * Enter VRF Reseller PO number and EndUser PO number
	 * @throws InterruptedException 
	 */
	@Test(description = "Step5 : Enter VRF field values", priority = 5)
	public void Step05_EnterVRFFieldValues() throws InterruptedException {
		//Enter VRF Reseller PO number and EndUser PO number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.EnterSingleVRFFieldValues();

		log.info("Successfully Entered VRF field values");
		Reporter.log("<p>Successfully Entered VRF field values");
	}

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority =6)
	public void Step06_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Click On No, do not validate button", priority = 7)
	public void Step07_ClickOnDoNotValidateButton1()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}

	/**
	 * Get Quote ID and Total Quote Price after Submitting the Quote
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 8: Get Quote ID and Total Quote Price", priority = 8)
	public void Step08_GetQuoteIDAndTotalPrice() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.getValues();
		
		log.info("Successfully fetched Quote ID and Quote total price\n");
		Reporter.log("<p>Successfully fetched Quote ID and Quote total price");

	}
	
	@Test(description = "Step 9: Click on Quotes tab", priority = 9)
	public void Step09_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);
		
		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");
	
	}
	
	@Test(description = "Step 10 : Search for the submitted quote", priority = 10)
	public void Step10_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);
		
		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}
	
	
	@Test(description = "Step 11 : Verify the total price", priority = 11)
	public void Step11_VerifyTotalPrice() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.verifyTotalQuotePrice(driver);
		
		log.info("Successfully verified the Quote total price\n");
		Reporter.log("<p>Successfully verified the Quote total price");
	}
	
	/**
	 * Select Convert quote to Order from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 12: Select Convert quote to Order from the dropdown and click on GO button", priority = 12)
	public void Step12_SelectConvertQuoteFromDropdown() throws Exception {
		//GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);			
		amQuoteDetailsPage.selectConvertQuoteOption();

		log.info("Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
	}


	@Test(description = "Step 13:Click on Yes in Split order dialog ", priority = 13)
	public void Step13_ActionInSplitOrderDialog() throws Exception 
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		Thread.sleep(5000);
		amQuoteDetailsPage.submitSplitOrderConfirmation();		
		log.info("Split Order dialog accepted successfully\n");
		Reporter.log("<p>Split Order dialog accepted successfully");
	}

	/**
	 * Function to enter PO number
	 */

	@Test(description = "Step 14:click on submit button ", priority = 14)
	public void Step14_EnterPoNumber() throws Exception 
	{
		QuoteToOrderPage amConvertToOrder = new QuoteToOrderPage(driver);
		PoNumber=userInfoDSDetails.getPoNumber();
		amConvertToOrder.EnterPoNumber(PoNumber);
		log.info("PO number entered\n");
		Reporter.log("<p>PO number entered");

	}

	/**
	 * Browse file and Click on Submit button 
	 */
	@Test(description = "Step 15: Browse file ", priority = 15)
	public void Step15_BrowseFile() throws Exception {
		QuoteToOrderPage amConvertToOrder=new QuoteToOrderPage(driver);
		//Click on Browse button
		amConvertToOrder.ClickOnBrowseButton();
		//upload file 
		Thread.sleep(10000);

		log.info("Successfully uploaded a PO file");
		Reporter.log("<p>--Successfully uploaded a PO file");
	}	

	/**
	 *Click on submit
	 */
	@Test(description="step 16: Click on submit button",priority = 16)
	public void Step16_ClickSubmitButton()throws Exception
	{
		QuoteToOrderPage amConvertToOrder=new QuoteToOrderPage(driver);
		amConvertToOrder.ClickOnSubmit();
		log.info("SUbmit button clicked");
		Reporter.log("<p>--SUbmit button clicked");
	}

	/**
	 * To fetch sales order number
	 */
	String SalesOrdNumber;
	@Test(description="step 17: To fetch sales order number",priority = 17)
	public void Step17_salesOrderNumber()throws Exception
	{
		QuoteToOrderConfirmationPage amOrderConfirmation=new QuoteToOrderConfirmationPage(driver);
		//amOrderConfirmation.FetchOrderNumber();
		amOrderConfirmation.FetchOrderNumber();
		SalesOrdNumber = amOrderConfirmation.salesorderNumber;
		System.out.println("Sales order number : "+ SalesOrdNumber);
		Thread.sleep(5000);
		CommonFun.QuotingLogout(driver, "You have signed out of your account.");
		log.info("Sales order number displayed");
		Reporter.log("<p>--Sales order number displayed");
	}

	/**
	 * Access Reseller Portal
	 *//*
	@Test(description = "Step 18: Open browser,Navigate to the reseeler URL", priority = 18)
	public void Step18_NavigateToWestconReseller_URL() throws Exception {

		// Navigate/launch the Reseller URL	
		ResellerLoginPage.navigateTo_URL(driver, userInfoDSDetails.getRsUrl(), driverName);
		log.info("Navigated to the Reseller URL");
		Reporter.log("<p>Navigated to Reseller Login Page URL -- "
				+ driver.getTitle());
	}

	*//**
	 * Enter user name , Password and click on Login link
	 * User will be landed in Self 
	 *//*
	@Test(description = "Step 19: Enter username , Pwd and click on Login link", priority = 19)
	public void Step19_ResellerLogin() throws Exception {

		username = userInfoDSDetails.getRsUserName();		
		password = userInfoDSDetails.getRsPassword();

		ResellerLoginPage rsLoginPage = new  ResellerLoginPage(driver);
		rsLoginPage.enterLoginCreds(username, password);
		log.info("Reseller successfully logged-in and navigated to SelfSerivice page\n");
		Reporter.log("<p>Reseller successfully logged-in and navigated to SelfSerivice page");
	}

	*//**
	 * mouse hover on order history
	 *//*
	@Test(description="step 17: Mouse hover order history",priority = 17)

	public void Step17_MouseHoverOnOrderHistory()throws Exception
	{
		ResellerSelfServicePage rsSelfService=new ResellerSelfServicePage(driver);
		rsSelfService.mouseHoverOnOrderHistory();
		log.info("Mouse hovered on order history link");
		Reporter.log("<p>--Mouse hovered on order history link");
	}

	*//**
	 * To select sales order number from drop down
	 *//*

	@Test(description="step 18: Select slaes order number from drop down  Enter sales order id and perform search",priority = 18)

	public void Step18_SelectSalesOrderNumberInDropdown()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);
		rsOrderlistingpage.searchCreatedOrder(SalesOrdNumber);

		log.info("Selected slaes order number from drop down");
		Reporter.log("<p>--Selected slaes order number from drop down");
	}

	*//**
	 * To click on order id
	 *//*
	@Test(description="step 19: click on order id",priority = 19)
	public void Step19_ClickOnOrderNo()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);

		rsOrderlistingpage.ClickOrderId();
		log.info("Clicked on order no");
		Reporter.log("<p>--Clicked on order no");
	}

	//To compare VRF Authorization no
	@Test(description="step 20: To compare VRf fileds",priority = 20)

	public void Step20_CompareVrfFields()throws Exception
	{

		Reseller_OrderDetailPage rsOrderDetailPage=new Reseller_OrderDetailPage(driver);
		String authNumber = rsOrderDetailPage.FetchAutorizationNo();
		Assert.assertEquals(authNumber, TextProp.getProperty("VRFAuthNumber"),"Vrf authorization not matching");
		log.info("VRF authorization number matching");
		Reporter.log("<p>--VRF authorization number matching");
		Thread.sleep(10000);

		//GUIFunctions.pageScrollDown(driver, 300);
		Thread.sleep(2000);
		rsOrderDetailPage.ExpandFirstLineItem();
		log.info("First line item expanded..");
		Reporter.log("<p>--First line item expanded");
		Thread.sleep(5000);

		String euPoNumber = rsOrderDetailPage.FetchEndUserPONumber();
		String rsPoNumber = rsOrderDetailPage.FetchResellerPONumber();

		System.out.println("End user PO number: "+euPoNumber);		
		System.out.println("Reseller PO number: "+rsPoNumber);		

		Assert.assertEquals(euPoNumber, TextProp.getProperty("vrfEnduserPONumberField"),"End User PO Number not matching");
		log.info("End User PO Number matching");
		Reporter.log("<p>--End User PO Number matching");

		Assert.assertEquals(rsPoNumber, TextProp.getProperty("vrfResellerPONumberField"),"Reseller PO Number not matching");
		log.info("Reseller PO Number matching");
		Reporter.log("<p>--Reseller PO Number matching");

	}*/
}
