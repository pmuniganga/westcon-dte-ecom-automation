package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC76_Quoting_VerifyMarginThresholdOverlay extends BaseTest{
	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");

		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	

		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");		

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}

	/**
	 * Click on Pricing grid
	 * 
	 */
	@Test(description = "Step2 : Click on Pricing grid", priority = 2)
	public void Step02_ClickOnPricingGrid() throws Exception {
		QuotingQuoteDetailpage amQuoteDetails = new QuotingQuoteDetailpage(driver);
		
		amQuoteDetails.clickPricingTab();
		
		log.info("Successfully Clicked on Pricing grid");
		Reporter.log("<p>Successfully Clicked on Pricing grid");
	}

	/**
	 * Change the Margin value to negative value
	 */
	@Test(description = "Step3 : Change the Margin value to negative value", priority = 3)
	public void Step03_ChangeMarginValue() throws Exception {

		//Change the Margin value to negative value
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterMarginToNegativeValue();

		log.info("Successfully altered Margin value to negative value");
		Reporter.log("<p>Successfully altered Margin value to negative value");
	}

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Select Submit quote from the dropdown and click on GO button", priority = 4)
	public void Step04_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");
	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Click On No, do not validate button", priority = 5)
	public void Step05_ClickOnDoNotValidateButton()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();	

		log.info("Successfully clicked on No,do not validate \n");
		Reporter.log("<p>Successfully clicked on No,do not validate ");

	}
	/**
	 * Verify  margin threshold overlay
	 * @throws Exception
	 */
	@Test(description = "Step 6-7: Verify  margin threshold overlay", priority = 6)
	public void Step06_VerifyMarginThresholdOverlay() throws Exception
	{
		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("MarginThresholdOverlayMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("MarginThresholdOverlayMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("MarginThresholdOverlayMsg"),
				"Margin threshold overlay is not proper");


		log.info("Successfully verified margin threshold overlay \n");
		Reporter.log("<p>Successfully verified margin threshold overlay ");
	}
}
