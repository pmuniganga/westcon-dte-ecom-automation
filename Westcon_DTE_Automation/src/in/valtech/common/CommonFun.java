package in.valtech.common;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingLoginPage;
import in.valtech.westcon.ResellerPages.ResellerLoginPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class CommonFun {

	public static Logger log = Logger.getLogger(CustomFun.class.getName());

	/**
	 * Method Name: QuotingLogout Description:Method for all page logout
	 * 
	 * @param by
	 *            :Element locator
	 * @param driver
	 *            :WebDriver
	 * @return login page
	 */
	static By logoutLink = By.xpath(ObjRepoProp.getProperty("LogoutLink_XPATH"));
	static By logoutOkButton = By.xpath(ObjRepoProp.getProperty("LogoutOkButton_XPATH"));
	static By logoutSuccessMsg=By.xpath(ObjRepoProp.getProperty("LogoutSuccessMsg_XPATH"));

	public static QuotingLoginPage QuotingLogout(WebDriver driver, String logoutMsg) throws Exception {

		// Click on Logout link in any Quoting site page
		GUIFunctions.clickElement(driver, logoutLink, "Sign out Button");
		// Click on Logout link in any Quoting site page
		GUIFunctions.clickElement(driver, logoutOkButton, "logout Ok Button");	
		//  Click on Enter button in keyboard (to skip the java script pop up)
		GUIFunctions.keyPressEnter(driver);					
		CustomFun.waitForPageLoaded(driver);	

		// verify success message					
		String actualLogoutMsg = driver.findElement(logoutSuccessMsg).getText();			
		Assert.assertEquals(actualLogoutMsg,logoutMsg,"Logout message verification fails");

		// return login page
		return new QuotingLoginPage(driver);		

	}

	/**
	 * Method Name: Reseller Logout Description:Method for all page logout
	 * 
	 * @param by
	 *            :Element locator
	 * @param driver
	 *            :WebDriver
	 * @return login page
	 */
	public static ResellerLoginPage ResellerLogout(WebDriver driver, String logoutMsg) throws Exception {

		// Click on Logout link in any reseller site page
		GUIFunctions.clickElement(driver, logoutLink, "Sign out Button");
		// Click on Logout link in any reseller site page
		GUIFunctions.clickElement(driver, logoutOkButton, "logout Ok Button");	
		//  Click on Enter button in keyboard (to skip the java script popup)
		GUIFunctions.keyPressEnter(driver);					
		CustomFun.waitForPageLoaded(driver);	

		return new ResellerLoginPage(driver);
	}
}
