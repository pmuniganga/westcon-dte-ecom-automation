package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;
import static in.valtech.custom.CustomFun.crmDSDetails;
import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC07_Quoting_CreateOpportunity extends BaseTest{
	
	@Test(description = "Navigate to quote detail page with end user selected by Calling TC06 with end user selected by Calling TC06", priority = 1)
	public void Step01_navigateToQuoteDetailPage() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");

		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");

		//Call TC 06
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");

		log.info("Quote detail page is displayed with End user selected");
		Reporter.log("<p>Quote detail page is displayed with End user selected");
	}      
	
	/**
	 * Now select Show in Pipeline checkbox in create Quote page`
	 * 
	 */

	@Test(description = "Step 2: Select Show in Pipeline checkbox", priority = 2)
	public void Step02_SelectingShowInPipelineCheckbox() throws Exception {

		QuotingQuoteDetailpage quoteDtl= new QuotingQuoteDetailpage(driver);
		//Selecting Show in Pipeline check box
		quoteDtl.PipeLineCheckBoxVerify(driver);
		quoteDtl.PipeLineCheckbox(driver);
		quoteDtl.CRMPencilIconVerify(driver);

		log.info("Show in Pipeline checkbox is selected\n");
		Reporter.log("<p>Show in Pipeline checkbox is selected");		

	}

	/**
	 * Clicking on Opportunity pencil icon, CRM Opportunity lookup overlay should be displayed
	 * Verify the Overlay title
	 * 
	 */
	@Test(description = "Step 3: Click on Opportunity pencil icon ", priority = 3)
	public void Step03_ClickingOnOpportunityPencilIcon() throws Exception {

		QuotingQuoteDetailpage quoteDtl= new QuotingQuoteDetailpage(driver);
		// clicking on Opportunity pencil icon
		quoteDtl.CRMPencilIcon(driver);

		log.info("CRM Opportunity lookup overlay is displayed\n");
		Reporter.log("<p>CRM Opportunity lookup overlay is displayed");		

	}

	/**
	 * Click on Create new tab,Tab should be opened
	 * 
	 */
	@Test(description = "Step 4: Click on Create new tab", priority = 4)
	public void Step04_ClickingOnCreateNewTab () throws Exception {
		QuotingQuoteDetailpage quoteDtl= new QuotingQuoteDetailpage(driver);
		// Clicking on Create new tab
		quoteDtl.CRMOppCreateNewTab(driver);

		log.info("Successfully clicked on Create new tab\n");
		Reporter.log("<p>Successfully clicked on Create new tab");		

	}
	/**
	 * Enter Opportunity Name, Select Stage Name , Select Oppurtunity Owner 
	 * Click on Submit button
	 * Create Quote page should be displayed with Opportunity selected 
	 * 
	 */
	@Test(description = "Step 5: Enter Opportunity Name, Select Stage Name , Select Oppurtunity Owner and Click on Submit button", priority = 5)
	public void Step05_FillOpportunityDetails() throws Exception {
		QuotingQuoteDetailpage quoteDtl= new QuotingQuoteDetailpage(driver);
		System.out.println("oppName: " + crmDSDetails.getOpportunityName());

		// Entering all data
		quoteDtl.OppNameTextBox(crmDSDetails.getOpportunityName());		
		quoteDtl.selectStageNameDDL(crmDSDetails.getStageName());  		
		quoteDtl.SelectOppOwnerDDL(crmDSDetails.getOpportunityOwner()); 
		
		
		//Clicking on Submit button
		quoteDtl.CreateNewOppSubmit(driver);
		
		//Verifying Linked opp in Create Quote page
		quoteDtl.verifyLinkedCRMOpportunity(crmDSDetails.getOpportunityName());
		

		log.info("Successfully CRM Opportunity is linked to Create Quote page\n");
		Reporter.log("<p>Successfully CRM Opportunity is linked to Create Quote page");		

	}
}
