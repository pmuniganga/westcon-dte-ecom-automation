package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC41_Quoting_ErrorCheckforQuoteCloseDateMandatory extends BaseTest{
	/**
	 * Call TC06 (without Opportunity)
	 * 
	 */	

	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 / Call TC07 (with Opportunity)", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06
		log.info("TC-06 Successfully Executed \n");
        Reporter.log("<p>TC-06 Successfully Eexecuted ");

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}

	/**
	 * Click on Requester info tab
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Click on Requester info tab", priority = 2)
	public void Step02_ClickonRequesterinfotab() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnRequesterinfotab();

		log.info("Successfully Click on Requester info tab\n");
		Reporter.log("<p>Successfully Click on Requester info tab");

	}

	/**
	 * Click Requester name pencil icon and enter requester name
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 3: Click Requester name pencil icon and enter requester name", priority = 3)
	public void Step03_EnterRequesterName() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterName(TextProp.getProperty("RequesterName"));

		log.info("Successfully Click Requester name pencil icon and enter requester name\n");
		Reporter.log("<p>Successfully Click Requester name pencil icon and enter requester name");

	}

	/**
	 * Click Requester email pencil icon and enter requester email
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Enter requester email", priority = 4)
	public void Step04_EnterRequesterEmail() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterEmail(TextProp.getProperty("RequesterEmail"));

		log.info("Successfully entered requester email\n");
		Reporter.log("<p>Successfully entered requester email");

	}

	@Test(description = "Step 5: Select Sales office from drop down", priority = 5)
	public void Step05_SelectSalesOffice() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		
		if(CustomFun.isElementVisible(By.xpath(ObjRepoProp.getProperty("SalesOfficeDDL_XPATH")), driver)== true)
		{
			if(locale.equalsIgnoreCase("US")) {
				amQuoteDetailsPage.selectSalesOffice(TextProp.getProperty("SalesOfficeValue"));
			}
			else if(locale.equalsIgnoreCase("CA")){
				amQuoteDetailsPage.selectSalesOffice(TextProp.getProperty("SalesOfficeValue_CA"));
			}}


		log.info("Successfully Selected Sales office\n");
		Reporter.log("<p>Successfully Selected Sales office");

	}	
	
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority = 6)
	public void Step06_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}

	/**
	 * Verify error message for Quote Close Date mandatory
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Verify error message for Quote Close Date mandatory", priority = 7)
	public void Step07_VerifyErrorMessage_QuoteCloseDate()throws Exception {

		System.out.println("Error Message*****"+driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("QuoteCloseDateErrorMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("QuoteCloseDateErrorMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("CommonErrorMsg"),
				"Error message not proper");				
		log.info("Successfully verified error message for Quote Close Date mandatory\n");
		Reporter.log("<p>Successfully verified error message for Quote Close Datemandatory");

	}
	
}
