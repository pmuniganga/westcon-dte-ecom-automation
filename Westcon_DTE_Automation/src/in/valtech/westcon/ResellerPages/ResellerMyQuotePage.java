package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class ResellerMyQuotePage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller self service page Constructor
	public ResellerMyQuotePage(WebDriver driver) throws InterruptedException {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right Reseller self service Page.

		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerMyQuotePage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not selfservice page");

			}

	}
	
	static By quote_Search_input_XPATH = By.xpath(ObjRepoProp.getProperty("quote_Search_input_XPATH"));
    static By no_Data_Available_Text_XPATH = By.xpath(ObjRepoProp.getProperty("no_Data_Available_Text_XPATH"));
	
	public ResellerMyQuotePage enterQuoteIDClickEnter(String quoteId) throws Exception
	{
		CustomFun.waitForPageLoaded(driver);

		GUIFunctions.typeTxtboxValue(driver, quote_Search_input_XPATH, quoteId);
		
		driver.findElement(By.xpath(ObjRepoProp.getProperty("quote_Search_input_XPATH"))).sendKeys(Keys.ENTER);
	
		CustomFun.waitObjectToLoad(driver, no_Data_Available_Text_XPATH, 60);
		Thread.sleep(2000);
		
		return new ResellerMyQuotePage(driver);
	}
}
