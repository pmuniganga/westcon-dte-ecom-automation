package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;

import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC14_Quoting_ValidateAndVerify_PricingValues extends BaseTest {

	ArrayList<String> tableValuesBeforeChange = new ArrayList<String>();
	ArrayList<String> tableValuesAfterChange = new ArrayList<String>();
	List<WebElement> tableValues;


	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step1 : Call TC08 which selects the End User and all mandatory fields filled", priority = 1)
	public void Step01_NavigateToTheCreateQuotePageWithAllMandatoryInfo() throws Exception {

		//Execute TC06 to navigate to Create Quote page with End User linked and all mandatory fields filled
		// Create Quote page is displayed with End user selected and all mandatory fields filled


		log.info("Create Quote page is displayed with End user selected and all mandatory fields filled");
		Reporter.log("<p>Create Quote page is displayed with End user selected and all mandatory fields filled-- "
				+ driver.getTitle());		
	}

	/**
	 * Click on Pricing Tab
	 */
	@Test(description = "Step2 : Click on Pricing Tab", priority = 2)
	public void Step02_ClickPricingTab() throws Exception {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");
	}

	/**
	 * Save all table values
	 */
	@Test(description = "Step3 : Save all table values", priority = 3)
	public void Step03_SaveAllTableValues() throws Exception {
		//Retrieve all the table values and Save in the list

		List<WebElement> tableRows = driver.findElements(By.xpath(ObjRepoProp.getProperty("tableRows_XPATH")));
		for(int i = 1 ; i <= tableRows.size() ; i++) {
			tableValues = driver.findElements(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody/tr["+i+"]/td[contains(@class,'htNumeric') or contains(@class,'listPrice') or contains(@class,'purchasePrice') or contains(@class,'undefined')]"));
			for(int j = 0 ; j < tableValues.size() ; j++) {	
				tableValuesBeforeChange.add(tableValues.get(j).getText());
				System.out.println(tableValuesBeforeChange.get(j));
			}
		}

		log.info("Successfully saved the table values");
		Reporter.log("<p>Successfully saved the table values");
	}

	/**
	 * Change Manufacturer List price and Vendor Discount
	 */
	@Test(description = "Step4 : Change Manufacturer List price and Vendor Discount", priority = 4)
	public void Step04_ChangePrice() throws Exception {
		//Change Manufacturer List price and Vendor Discount
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterManufacturerListPrice(TextProp.getProperty("alteredManufacturerListPrice"));
		//Thread.sleep(5000);
		quoteDetails.AlterVendorDiscount(TextProp.getProperty("alteredVendorDiscount"));

		log.info("Successfully altered Manufacturer List price and Vendor Discount");
		Reporter.log("<p>Successfully altered Manufacturer List price and Vendor Discount");
	}

	/**
	 * Click on Validate Pricing button
	 */
	@Test(description = "Step5 : Click on Validate Pricing button", priority = 5)
	public void Step05_ClickValidatePricingButton() throws Exception {
		//Click on Validate Pricing button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.ClickOnValidatePricingButton();

		log.info("Successfully clicked on Validate Pricing button");
		Reporter.log("<p>Successfully clicked on Validate Pricing button");
	}

	/**
	 * Verify SAP Price Validation Overlay
	 */
	@Test(description = "Step6 : Verify SAP Price Validation Overlay", priority =6)
	public void Step06_VerifyPriceValidationOverlay() throws Exception {
		//Verify SAP Price Validation Overlay
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("SAPPriceValidationOverlay_XPATH"))).getText(), 
				TextProp.getProperty("SAPValidationOverlay"));

		log.info("SAP price validation overlay is opened as expected");
		Reporter.log("<p>SAP price validation overlay is opened as expected");
	}

	/**
	 * Click on View Details link
	 */
	@Test(description = "Step7 : Click on View Details link", priority = 7)
	public void Step07_ClickViewDetails() throws Exception {
		//Click on View Details link
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickViewDetails();

		log.info("Successfully clicked on View Details link");
		Reporter.log("<p>Successfully clicked on View Details link");
	}

	/**
	 * Verify altered list price and vendor discount
	 */
	@Test(description = "Step8 : Verify altered list price and vendor discounty", priority = 8)
	public void Step08_VerifyAlteredPrice() throws Exception {
		//Verify Altered prices in SAP Validation Overlay
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("SAPPriceValidationOverlayAlteredListPrice_XPATH"))).getText(), 
				TextProp.getProperty("alteredManufacturerListPrice"));

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("SAPPriceValidationOverlayAlteredListPricVendorDiscount_XPATH"))).getText(), 
				TextProp.getProperty("alteredVendorDiscount"));

		log.info("Successfully verified altered values");
		Reporter.log("<p>Successfully verified altered values");
	}

	/**
	 * Click on Update list price and cost button
	 */
	@Test(description = "Step9 : Click on Update list price and cost button", priority = 9)
	public void Step09_ClickUpdateListPriceCostButton() throws Exception {
		//Click Update List Price and Cost button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickUpdateListPriceCostButton();

		log.info("Successfully clicked on on Update list price and cost button");
		Reporter.log("<p>Successfully clicked on on Update list price and cost button");
	}

	/**
	 * Save all table values after SAP validation and compare
	 */
	@Test(description = "Step10 : Save all table values after SAP validation and Compare", priority = 10)
	public void Step10_SaveAllTableValuesAndCompare() throws Exception {
		//Retrieve all the table values and Save in the list
		List<WebElement> tableRows = driver.findElements(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::div[contains(@class,'ht_master handsontable')]/descendant::tbody/tr"));
		for(int i = 1 ; i <= tableRows.size() ; i++) {
			tableValues = driver.findElements(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody/tr["+i+"]/td[contains(@class,'htNumeric') or contains(@class,'listPrice') or contains(@class,'purchasePrice') or contains(@class,'undefined')]"));
			for(int j = 0 ; j < tableValues.size() ; j++) {	
				tableValuesAfterChange.add(tableValues.get(j).getText());
				System.out.println(tableValuesAfterChange.get(j));
			}
		}

		log.info("Successfully saved the table values");
		Reporter.log("<p>Successfully saved the table values");

		for(int k = 0 ; k < tableValuesAfterChange.size() ; k++) {
			Assert.assertEquals(tableValuesAfterChange.get(k), tableValuesBeforeChange.get(k));
		}

		log.info("Successfully compared the table values");
		Reporter.log("<p>Successfully compared the table values");
	}
}
