package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC39_Quoting_CopyPasteBOM_AddVerifyErrorMessage_InvalidInputFormat extends BaseTest{
	static  QuotingAddProductsPage amAddProductsPage;
	String inputType;
	String[] skuNumber;
	
	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */

	@Test(description = "Step 1: Call TC01 TC02", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
	}
	
	/**
	 * Select input type from the dropdown
	 * 
	 */
	@Test(description = "Step 2: Select input type from the dropdown", priority = 2)
	public void Step02_DropdownSelect() throws Exception {
		//Select input type from the dropdown

		amAddProductsPage = new QuotingAddProductsPage(driver);
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			inputType = SKUdataSetList.get(i).getInputType();
			amAddProductsPage.selectInputFromDropdown(driver, inputType);
		}
		System.out.println("Value selected in dropdown****"+inputType);
		
		log.info("successfully selected the input format\n");
		Reporter.log("<p>successfully selected the input format");
	}

	/**
	 * Enter any input,but in invalid format
	 * 
	 */
	@Test(description = "Step 3: Enter input in invalid format", priority = 3)
	public void Step03_EnterInvalidFormatInput() throws Exception {
		
		//Enter input in valid format
		amAddProductsPage = new QuotingAddProductsPage(driver);

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {	
			skuNumber = SKUdataSetList.get(i).getSKU().split("\n");
		}
		
		for(int j = 0 ; j < skuNumber.length ; j++) {
				log.info("SKU*************" + skuNumber[j]);
				amAddProductsPage.CopyPasteBOM(driver , skuNumber[j]);
	  
		}

		log.info("successfully entered invalid input the part numbers\n");
		Reporter.log("<p>successfully entered invalid input the part numbers");

	}

	/**
	 * Click on Submit button 
	 * 
	 */
	@Test(description = "Step 4: Click on Submit button", priority = 4)
	public void Step04_ClickSubmit() throws Exception {
		
		//Click on Submit button
		amAddProductsPage = new QuotingAddProductsPage(driver);

		amAddProductsPage.CopyPasteBOMSubmit_ErrorValidation(driver);

		log.info("successfully clicked on Submit button and navigated to create quote page\n");
		Reporter.log("<p>successfully clicked on Submit button and navigated to create quote page");
	}
	

	/**
	 * Verify Error Message for Invalid Input
	 * 
	 * 
	 */
	@Test(description = "Step 5: Verify Error Message for Invalid Input", priority = 5)
	public void Step05_VerifyErrorMessage_InvalidInput() throws Exception {
		
		String errorMsg=driver.findElement(By.xpath(ObjRepoProp.getProperty("CopyPasteBOMInvalidInputErrorMsg_XPATH"))).getText().trim();
	          System.out.println("Error Message Displayed*****"+errorMsg);
	
      Assert.assertTrue(errorMsg.contains(TextProp
			.getProperty("InvalidInputFormatErrorMsg")),"");
	
		log.info("successfully verified error message for Invalid Input format\n");
		Reporter.log("<p>successfully verified error message for Invalid Input format");
	}
	
	/**
	 * Select input type from the dropdown
	 * 
	 */
	@Test(description = "Step 6: Select input type like P+Q+VD from the dropdown ", priority = 6)
	public void Step06_DropdownSelect() throws Exception {
		//Select input type from the dropdown

		amAddProductsPage = new QuotingAddProductsPage(driver);
		
		driver.navigate().refresh();
		CustomFun.waitForPageLoaded(driver);
		
		amAddProductsPage.selectInputFromDropdown(driver, TextProp.getProperty("InputFormat"));

		log.info("successfully selected the input format\n");
		Reporter.log("<p>successfully selected the input format");
	}

	/**
	 * Enter any input,but in invalid format
	 * 
	 */
	@Test(description = "Step 7: Enter input in invalid format", priority = 7)
	public void Step07_EnterInvalidFormatInput() throws Exception {
		
		//Enter input in valid format
		amAddProductsPage = new QuotingAddProductsPage(driver);
		driver.findElement(By.xpath(ObjRepoProp.getProperty("CopyPasteBOMTextBox_XPATH"))).clear();
		
		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {			
			skuNumber = SKUdataSetList.get(i).getSKU().split("\n");
		}
		
		for(int j = 0 ; j < skuNumber.length ; j++) {
				log.info("SKU*************" + skuNumber[j]);
				amAddProductsPage.CopyPasteBOM(driver , skuNumber[j]);
	  
		}

		log.info("successfully entered invalid input the part numbers\n");
		Reporter.log("<p>successfully entered invalid input the part numbers");

	}

	/**
	 * Click on Submit button 
	 * 
	 */
	@Test(description = "Step 8: Click on Submit button", priority = 8)
	public void Step08_ClickSubmit() throws Exception {
		
		//Click on Submit button
		amAddProductsPage = new QuotingAddProductsPage(driver);

		amAddProductsPage.CopyPasteBOMSubmit_ErrorValidation(driver);

		log.info("successfully clicked on Submit button and navigated to create quote page\n");
		Reporter.log("<p>successfully clicked on Submit button and navigated to create quote page");
	}
	

	/**
	 * Verify Error Message for Invalid Input
	 * 
	 * 
	 */
	@Test(description = "Step 09: Verify Error Message for Invalid Input", priority =9)
	public void Step09_VerifyErrorMessage_InvalidInput() throws Exception {
		
		String errorMsg=driver.findElement(By.xpath(ObjRepoProp.getProperty("CopyPasteBOMInvalidInputErrorMsg_XPATH"))).getText().trim();
	System.out.println("Error Message Displayed*****"+errorMsg);
	
     Assert.assertTrue(errorMsg.contains(TextProp
			.getProperty("InvalidInputFormatErrorMsg")),"");
	
		log.info("successfully verified error message for Invalid Input format\n");
		Reporter.log("<p>successfully verified error message for Invalid Input format");
	}
}
