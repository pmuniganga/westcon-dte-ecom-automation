package in.valtech.util;

public class ConfigDetails {
	private String Execute;
	private String Scenario;
	private String URL;
	private String Driver;
	private String Locale;
	private String Orientation;
	private String BreakPoint;

	public String getOrientation() {
		return Orientation;
	}

	public void setOrientation(String orientation) {
		Orientation = orientation;
	}

	public String getBreakPoint() {
		return BreakPoint;
	}

	public void setBreakPoint(String breakPoint) {
		BreakPoint = breakPoint;
	}

	
	public String getExecute() {
		return Execute;
	}

	public void setExecute(String execute) {
		Execute = execute;
	}

	public String getScenario() {
		return Scenario;
	}

	public void setScenario(String scenario) {
		Scenario = scenario;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getDriver() {
		return Driver;
	}

	public void setDriver(String driver) {
		Driver = driver;
	}

	public String getLocale() {
		return Locale;
	}

	public void setLocale(String locale) {
		Locale = locale;
	}

}
