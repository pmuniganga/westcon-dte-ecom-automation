package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC47_Quoting_CopyThisQuote_EditingCopiedData extends BaseTest {
	/**
	 * User to copy the quote in quoting by executing TC-46
	 * 
	 */
    String endUserName;

	@Test(description = "Step 1: User to copy the quote in quoting by executing TC46", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");
		// Execute TC-09
		log.info("TC-09 Successfully Executed \n");
		Reporter.log("<p>TC-09 Successfully Executed ");
		log.info("TC-46 Successfully Executed \n");
		Reporter.log("<p>TC-46 Successfully Executed ");

		log.info("Copied Quote details page is displayed with Requester Info, End user details, VRF fields  selected");
		Reporter.log("<p>Copied Quote details page is displayed with Requester Info, End user details, VRF fields  selected");	
	}

	
	/**
	 * Change Quote Close Date and Quote Expiration Date
	 *  @throws Exception
	 */
	@Test(description = "Step 2: Change Quote Expiration & Quote Close Date", priority =2)
	public void Step02_Changing_Quoteexpdate_Quoteclosedate()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
		
		System.out.println("Quote Expiration date-Before modification: "+
		driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim());
		
		System.out.println("Quote Close date-Before modification: "+
				driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim());

		
		amQuoteDetailsPage.change_Quoteexpdate_Quoteclosedate(
				        CustomFun.addDays(new Date(), (Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim())+4), locale),
						CustomFun.addDays(new Date(), (Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim())+2), locale));
										
		System.out.println("Quote Expiration date-After modification: "+
				driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim());
				
				System.out.println("Quote Close date-After modification: "+
						driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim());
	

		log.info("Successfully Changed Quote Expiration & Quote Close Date\n");
		Reporter.log("<p>Successfully Changed Quote Expiration & Quote Close Date\n");

	}
	
	/**
	 * Change End User
	 *  @throws Exception
	 */
	@Test(description = "Step 3: Change End User", priority =4)
	public void Step03_ChangingEndUser()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
	    GUIFunctions.pageScrollUP(driver, 150);
		amQuoteDetailsPage.AMClickEndUserSearchLink();
		
		endUserName=TextProp.getProperty("EndUserName");
		System.out.println("End user Name: " +endUserName);	
		
		// Search list should be displayed	
		driver.findElement(By.xpath(ObjRepoProp.getProperty("endUserModalNameTxtBox_XPATH"))).clear();
		amQuoteDetailsPage.AMendUserModal_enterEndUserName(endUserName);		
		amQuoteDetailsPage.AMendUserModal_ClickonSearch();
		amQuoteDetailsPage.AMVerifyEndUserModalHeader();		
		amQuoteDetailsPage.AMendUserModal_ClickonFirstRadio();
		amQuoteDetailsPage.AMendUserModal_ClickonSelectUser_FirstRadio();
        Thread.sleep(5000);
        
		System.out.println("Modified End User:"+
		driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserAccountDetails_XPATH"))).getText().trim());
		
		log.info("Successfully Changed End User\n");
		Reporter.log("<p>Successfully Changed End User\n");

	}
	

	/**
	 * Edit Requester Info
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Edit Requester Info", priority =4)
	public void Step05_EditingRequestorInfo() throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.clickOnRequesterinfotab();
		
		amQuoteDetailsPage.editRequesterinfo(TextProp.getProperty("RequesterName_V2"),TextProp.getProperty("RequesterEmail_V2"));
			
		System.out.println("Modified Requester Name: "+
		driver.findElement(By.id(ObjRepoProp.getProperty("requesterName_ID"))).getAttribute("value").trim());
			System.out.println("Modified Requester Email: "+driver.findElement(By.xpath(ObjRepoProp.getProperty("requesterEmail_XPATH"))).getAttribute("value").trim());
			
		log.info("Successfully Editted Requester Name and Email\n");
		Reporter.log("<p>Successfully Editted Requester Name and Email\n");

	}
	
	
}
