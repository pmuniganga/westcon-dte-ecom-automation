package in.valtech.westcon.test.am;


import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC40_Quoting_ErrorCheckforEndUserMandatory extends BaseTest{
	/**
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		

		log.info("Create Quote page is displayed without End user selected\n");
		Reporter.log("<p>Create Quote page is displayed without End user selected");
	}
	

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 3: Select Submit quote from the dropdown and click on GO button", priority = 3)
	public void Step03_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}

	/**
	 * Verify error message for End User Account mandatory
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Verify error message for End User Account mandatory", priority = 4)
	public void Step04_VerifyErrorMessage_EndUsrAcc()throws Exception {

		System.out.println("Error Message*****"+driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("EndUserAccErrorMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("EndUserAccErrorMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("EndUserAccErrorMsg"),
				"Error message not proper");				
		log.info("Successfully verified error message for End User Account mandatory\n");
		Reporter.log("<p>Successfully verified error message for End User Account mandatory");

	}
	
	
	/**
	 * Click on End User Company Name Instead link
	 * 
	 */
	
	@Test(description="Step 5: Click on End User Company Name Instead link", priority=5)
	public void Step05_ClickOnEndUsrCmpyInstead() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		
		amQuoteDetailsPage.ClickOnEndUsrCmpyInsteadLink(driver);
		log.info("Successfully Clicked on End User Company Name Instead link\n");
		Reporter.log("<p>Successfully Clicked on End User Company Name Instead link");
	}
	/**
	 * Save Quote details before submitting quote
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority = 6)
	public void Step06_RetriveQuoteDetails_BeforeSubmit()	throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		
		amQuoteDetailsPage.getValues();
		
		log.info("Successfully retrived Quote details before submitting Quote\n");
		Reporter.log("<p>Successfully retrived Quote details before submitting Quote");

	}
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Select Submit quote from the dropdown and click on GO button", priority = 7)
	public void Step07_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}
	
	/**
	 * Verify error message for End User company mandatory
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 8: Verify error message for End User company mandatory", priority = 8)
	public void Step08_VerifyErrorMessage_EndUsrCmpy()throws Exception {	

		System.out.println("Error Message*****"+driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("EndUserCmpyErrorMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("EndUserCmpyErrorMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("EndUserCmpyErrorMsg"),
				"Error message not proper");				
		log.info("Successfully verified error message for End User Account mandatory\n");
		Reporter.log("<p>Successfully verified error message for End User Account mandatory");

	}

}
