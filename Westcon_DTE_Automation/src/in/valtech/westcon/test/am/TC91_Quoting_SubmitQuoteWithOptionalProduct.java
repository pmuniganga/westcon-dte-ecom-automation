package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;

import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC91_Quoting_SubmitQuoteWithOptionalProduct extends BaseTest {

	String SKU;

	String[] skuNumber;
	String[] skuQty;
	int count;
	int countAfterDeletion;
	
	String[] actualResellerPriceBeforeOptional;
	String[] actualExResllerPriceBeforeOptional;
	String actualTotalPriceBeforeOptional;
	
	String[] actualResellerPriceAfterOptional;
	String[] actualExResllerPriceAfterOptional;
	String actualTotalPriceAfterOptional;

	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	/**
	 * Save the values like Reseller Unit Price, Reseller Extended Price and Total Price
	 */
	@Test(description = "Step2 : Save the values like Reseller Unit Price, Reseller Extended Price and Total Price", priority = 3)
	public void Step02_SaveTableValues() {
		//Save the table values like Reseller Unit Price, Reseller Extended Price and Total Price

		By pricingTab_XPATH = By.xpath(ObjRepoProp.getProperty("pricingTab_XPATH"));
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(pricingTab_XPATH));
		
		GUIFunctions.clickElement(driver, pricingTab_XPATH, "pricingTab");
		
		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualResellerPriceBeforeOptional = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][8]")).getText().split(" ");
				actualExResllerPriceBeforeOptional = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][10]")).getText().split(" ");
				actualTotalPriceBeforeOptional = driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().replaceAll("[a-zA-Z]", "").trim();
			}
		}

		System.out.println("actualResellerPriceBeforeOptional = "+actualResellerPriceBeforeOptional[1]);
		System.out.println("actualExResllerPriceBeforeOptional = "+actualExResllerPriceBeforeOptional[1]);
		System.out.println("actualTotalPriceBeforeOptional = "+actualTotalPriceBeforeOptional);

		log.info("Successfully saved table values\n");
		Reporter.log("<p>Successfully saved table values");
	}
	
	@Test(description = "Step 3 : Change the type of product to optional", priority = 3)
	public void Step03_ChangeProductType() throws Exception {
		//Change the type of the product
				
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.changeProductType();
		
		log.info("Successfully changed product type\n");
		Reporter.log("<p>Successfully changed product type");
	}
	
	@Test(description = "Step 4 : Verify optional / alternative unit and extended reseller prices", priority = 4)
	public void Step04_VerifyOptionalResellerPrices() throws Exception {
		//Verify optional / alternative unit and extended reseller prices
		
		By optionalExResellerPrice = By.xpath(ObjRepoProp.getProperty("optionalExResellerPrice_XPATH"));
		By optionalUnitResellerPrice = By.xpath(ObjRepoProp.getProperty("optionalUnitResellerPrice_XPATH"));
		
		GUIFunctions.clickElement(driver, optionalUnitResellerPrice, "optionalUnitResellerPrice");
		
		actualResellerPriceAfterOptional = driver.findElement(optionalUnitResellerPrice).getText().split(" ");
		actualExResllerPriceAfterOptional = driver.findElement(optionalExResellerPrice).getText().split(" ");
		
		Assert.assertEquals(actualResellerPriceAfterOptional[1], actualResellerPriceBeforeOptional[1]);
		Assert.assertEquals(actualExResllerPriceAfterOptional[1], actualExResllerPriceBeforeOptional[1]);
		
		log.info("Successfully verified alternative / optional unit and extended reseller prices\n");
		Reporter.log("<p>Successfully verified alternative / optional unit and extended reseller prices");
	}
	
	@Test(description = "Step 5 : Verify Total price after making product optional / alternative", priority = 5)
	public void Step05_VerifyTotalPrice() {
		//Verify Total price after making product optional / alternative
		actualTotalPriceAfterOptional = driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().replaceAll("[a-zA-Z]", "").trim();
		
		Assert.assertEquals(Float.parseFloat(actualTotalPriceAfterOptional), Float.parseFloat(actualTotalPriceBeforeOptional) - Float.parseFloat(actualExResllerPriceAfterOptional[1]));
		
		log.info("Total price is calculated properly\n");
		Reporter.log("<p>Total price is calculated properly");
	}
	
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority = 6)
	public void Step06_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Click On No, do not validate button", priority = 7)
	public void Step07_ClickOnDoNotValidateButton()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
	
		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");				
		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message");

	}
}