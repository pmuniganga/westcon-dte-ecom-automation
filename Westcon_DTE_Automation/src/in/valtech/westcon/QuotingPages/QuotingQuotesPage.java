package in.valtech.westcon.QuotingPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class QuotingQuotesPage {
	private final WebDriver driver;
	static String status=null;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// AM Quote Listing page Constructor
	public QuotingQuotesPage(WebDriver driver) {
		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		// Check that we're on the right AM Quote Listing Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("quotingQuotesPage_XPATH")),
						driver))) {
			throw new IllegalStateException("This is not Quoting Quotes page");
		}
	}

	By clickCreateNewQuoteButton_XPATH = By.xpath(ObjRepoProp.getProperty("createNewQuoteButton_XPATH"));
	By quote_Search_input_XPATH = By.xpath(ObjRepoProp.getProperty("quote_Search_input_XPATH"));
	By quote_Table_First_Row_XPATH = By.xpath(ObjRepoProp.getProperty("quote_Table_First_Row_XPATH"));
	By AmRequestedQuotesLink_XPATH = By.xpath(ObjRepoProp.getProperty("AmRequestedQuotesLink_XPATH"));
	By AMquotesPageSearch_XPATH = By.xpath(ObjRepoProp.getProperty("AMquotesPageSearch_XPATH"));
	By AmQuoteListingTableplusIcon_XPATH= By.xpath(ObjRepoProp.getProperty("AmQuoteListingTableplusIcon_XPATH"));
	
	By quoteStatus_XPATH =By.xpath(ObjRepoProp.getProperty("QuoteStatus_XPATH"));
	By quoteRev_XPATH =By.xpath(ObjRepoProp.getProperty("QuoteRev_XPATH"));
	By quoteIDLnk = By.xpath(ObjRepoProp.getProperty("quoteIdInTable_XPATH"));

	By clearFilterButton_XPATH = By.xpath(ObjRepoProp.getProperty("clearFilterButton_XPATH"));
	By searchTxtBox = By.xpath(ObjRepoProp.getProperty("searchResellerTextbox_XPATH"));
	
	/***
	 * This function is to create new quote
	 */
	public QuotingSelectResellerPage clickCreateNewQuoteButton()throws Exception {	
		// Click on Create New Quote Button
		GUIFunctions.clickElement(driver, clickCreateNewQuoteButton_XPATH, 
				"Navigate to Reseller listing page");
		return new QuotingSelectResellerPage(driver);	
	}

	/** This function will mouse hover on quotes and  click on requested quotes
	 * 
	 */
	public AMRequestQuotesListing mouseHoverOnQuotesAndClickOnRequestedQuotes() throws Exception{
		//mouse over on quotes 
		GUIFunctions.mouseOverElement(driver, driver.findElement(By.xpath(ObjRepoProp
				.getProperty("AmQuotesNavigationLink_XPATH"))));
		//click on requested quotes link
		GUIFunctions.clickElement(driver, AmRequestedQuotesLink_XPATH, "Requested quotes link");
		Thread.sleep(500);
		return new AMRequestQuotesListing(driver);
	}

	/**
	 * This function will search for quote id and hit enter
	 */
	public QuotingQuoteDetailpage searchForTheQuoteIdAndHitEnter(String QuoteID) throws Exception{
		GUIFunctions.typeTxtboxValue(driver,AMquotesPageSearch_XPATH, QuoteID);
		driver.findElement(By.xpath(ObjRepoProp
				.getProperty("AMquotesPageSearch_XPATH"))).sendKeys(Keys.ENTER);
		CustomFun.waitObjectToLoad(driver, AmQuoteListingTableplusIcon_XPATH, 60);
		return new QuotingQuoteDetailpage(driver);
	}

	/***
	 * This function will click on '+' in Quote listing table 
	 */
	public QuotingQuotesPage clickOnPlusIcon() throws Exception
	{
		GUIFunctions.clickElement(driver,AmQuoteListingTableplusIcon_XPATH, " + icon click");
		Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp
				.getProperty("AMQuotesTableCollapse_XPATH"))
				,driver),"Table not collapsed");
		return new QuotingQuotesPage(driver);
	}

	/***
	 * This function is to verify Quote Status
	 */
	public QuotingQuotesPage verifyQuoteStatus() throws Exception{
		//Verify the status
		status=driver.findElement(By.xpath(ObjRepoProp
				.getProperty("AMQuotesTableStatus_XPATH"))).getText();
		Assert.assertEquals(status, TextProp.getProperty("QuoteStatus"));
		return new QuotingQuotesPage(driver);
	}
	
	
	/***
	 * This function will verify Quote Status and Quote Revision version
	 */
	
	public QuotingQuotesPage verifyStatusAndRevision_SubmittedQuote(WebDriver driver) throws Exception
	{
		
  
            System.out.println("Quote Revision****"+driver.findElement(quoteRev_XPATH).getText().trim());	
        		Assert.assertEquals(
        				driver.findElement(quoteRev_XPATH).getText().trim(),"None",
        				"Revision mismatch");
       
        	System.out.println("Quote Status****"+driver.findElement(quoteStatus_XPATH).getText().trim());	
        		Assert.assertEquals(
        				driver.findElement(quoteStatus_XPATH).getText().trim(),"Submitted",
        				"Status mismatch");           
   

		return new QuotingQuotesPage(driver);
	}
	
	/**
	 * *
	 * This function will click on searched Quote link
	 */
	
	public QuotingQuoteDetailpage clickOnQuoteIdLink(WebDriver driver) throws Exception {
		//Click on Quote Id link
		CustomFun.waitObjectToLoad(driver, quoteIDLnk, 60);
		GUIFunctions.clickElement(driver, quoteIDLnk, "Click on QuoteID link");
		CustomFun.waitForPageLoaded(driver);
	
		return new QuotingQuoteDetailpage(driver);
	}
	
	By quoteExpansionIcon = By.xpath(ObjRepoProp.getProperty("quoteExpansionIcon_XPATH"));
	public QuotingQuotesPage expandQuote() {
		//Click on Expand icon for the quote
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(quoteExpansionIcon));
		
		GUIFunctions.clickElement(driver, quoteExpansionIcon, "+ Icon Click");
		
		return new QuotingQuotesPage(driver);
	}
	
	/***
	 * This function will enter quote id and search
	 */
	public QuotingQuotesPage EnterQuoteIDAndSearch(WebDriver driver) throws Exception {

		System.out.println(BaseTest.quoteID);
		CustomFun.waitForPageLoaded(driver);
		Thread.sleep(5000);
		GUIFunctions.clickElement(driver, clearFilterButton_XPATH, "Clear Filter Button Click");
		Thread.sleep(3000);
		GUIFunctions.typeTxtboxValue(driver, searchTxtBox, BaseTest.quoteID);	
		Thread.sleep(5000);
		GUIFunctions.keyPressEnter(driver);
		CustomFun.waitForPageLoaded(driver);

		return new QuotingQuotesPage(driver);
	}
}

