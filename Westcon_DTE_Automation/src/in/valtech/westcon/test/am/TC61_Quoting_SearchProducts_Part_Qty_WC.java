package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC61_Quoting_SearchProducts_Part_Qty_WC extends BaseTest {
	static  QuotingAddProductsPage amAddProductsPage;
	static QuotingQuoteDetailpage amQuoteDetailspage;
	String SKUCount = null;
	String inputType;
	String[] skuNumber;
	String[] skuQty;
	String[] skuListPrice;
	String[] skuVendorDiscount;
	String[] skuWestconCost;
	String[] skuResellerPrice;
	String[] skuResellerPriceOffList;
	String[] skuGlobalPoints;
	String skuData;

	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */


	@Test(description = "Step 1: Call TC01 TC02", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
	}

	@Test(description = "Step 2: Delete all the part numbers from the table", priority = 2)
	public void Step02_DeleteAllPartNumbers() throws Exception {
		//Click on Select All checkbox
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailspage.selectAllCheckbox(driver);
		log.info("successfully selected Select All checkbox\n");
		Reporter.log("<p>successfully selected Select All checkbox");

		//Delete all part numbers
		amQuoteDetailspage.deletePartNumbers(driver);
		log.info("successfully deleted all part numbers\n");
		Reporter.log("<p>successfully deleted all part numbers");

	}

	@Test(description = "Step 3: Open Search Products overlay", priority = 3)
	public void Step03_ClickSearchProductsIcon() throws Exception {
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		//Click on Search Products Icon
		amQuoteDetailspage.ClickOnProductIcon(driver);

		log.info("successfully clicked on Search Products Icon and overlay is displayed\n");
		Reporter.log("<p>successfully clicked on Search Products Icon and overlay is displayed");
	}

	@Test(description = "Step 4: Select input type from the dropdown", priority = 4)
	public void Step04_DropdownSelect() throws Exception {
		//Select input type from the dropdown

		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			inputType = SKUdataSetList.get(i).getInputType();
			amQuoteDetailspage.selectInputFromDropdown(driver, inputType);
		}

		log.info("successfully selected the input format\n");
		Reporter.log("<p>successfully selected the input format");
	}

	@Test(description = "Step 5: Enter input in valid format", priority = 5)
	public void Step05_EnterInputInValidFormat() throws Exception {
		//Enter input in valid format
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {
			skuNumber = SKUdataSetList.get(i).getSKU().split("\n");
			skuQty = SKUdataSetList.get(i).getQuantity().split("\n");
			skuListPrice = SKUdataSetList.get(i).getListPrice().split("\n");
			skuVendorDiscount = SKUdataSetList.get(i).getVendorDiscount().split("\n");
			skuWestconCost = SKUdataSetList.get(i).getWestconCost().split("\n");
			skuResellerPrice = SKUdataSetList.get(i).getResellerPrice().split("\n");
			skuResellerPriceOffList = SKUdataSetList.get(i).getResellerPriceOffList().split("\n");
			skuGlobalPoints = SKUdataSetList.get(i).getGlobalPoints().split("\n");
			for(int j = 0 ; j < skuNumber.length ; j++) {

				log.info("SKU*************" + SKUdataSetList.get(i).getSKU());
				log.info("SKU*************" + SKUdataSetList.get(i).getQuantity());
				skuData = skuNumber[j]+" "+skuQty[j]+" "+skuListPrice[j]+" "+skuVendorDiscount[j]+" "+
						skuWestconCost[j]+" "+skuResellerPrice[j]+" "+skuResellerPriceOffList[j]+" "+skuGlobalPoints[j];

				amQuoteDetailspage.CopyPasteBOM(driver , skuData.replace("NA", ""));
			} 	  
		}

		log.info("successfully submitted the part numbers and Quote detail page is displayed\n");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");

	}

	@Test(description = "Step 6: Click on Add button", priority = 6)
	public void Step06_ClickAddButton() throws Exception {
		//Click on Submit button
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailspage.PartNumberTab_ClickOnAdd(driver);

		log.info("successfully clicked on Submit button and navigated to create quote page\n");
		Reporter.log("<p>successfully clicked on Submit button and navigated to create quote page");
	}

	@Test(description = "Step 7: Verify part numbers and values in pricing grid", priority = 7)
	public void Step07_VerifyPricingGridValues() throws Exception {
		//Verify all pricing grid values
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailspage.verifyTableDetails(inputType);

		log.info("successfully verified all the values\n");
		Reporter.log("<p>successfully verified all the values");
	}
}