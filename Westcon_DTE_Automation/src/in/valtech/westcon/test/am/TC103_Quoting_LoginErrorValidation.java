package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import in.valtech.westcon.QuotingPages.QuotingLoginPage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

public class TC103_Quoting_LoginErrorValidation extends BaseTest {
	
	static QuotingLoginPage amLoginPage;
	static QuotingQuotesPage amQuotelistingPage;
	
	static String username = null;
	static String password = null;
	
	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */
	@Test(description = "Step 1: Open browser,Navigate to the AM URL", priority = 1)
	public void Step01_NavigateToWestconAM_URL() throws Exception {
		
		// Navigate/launch the AM URL
		amLoginPage = QuotingLoginPage.navigateTo_URL(driver, baseUrl, driverName);
		
		log.info("Navigated to the AM URL");
		Reporter.log("<p>Navigated to AM Login Page URL -- "
				+ driver.getTitle());			
	}
	
	/**
	 * Verify the error message for invalid login credentials
	 * 
	 */
	@Test(description = "Step 2: Verify the error message for invalid login credentials", priority = 2)
	public void Step02_AMLoginErrorValidation() throws Exception {

		username = userInfoDSDetails.getAmUserName();		
		password = userInfoDSDetails.getAmPassword();
	 
		amLoginPage.errorMsgVerification(username, password);
		
		log.info("Successfully validated all the error messages for invalid login\n");
		Reporter.log("<p>Successfully validated all the error messages for invalid login");
			
	}	
}

