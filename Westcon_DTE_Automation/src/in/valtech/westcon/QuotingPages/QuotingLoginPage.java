package in.valtech.westcon.QuotingPages;

import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static in.valtech.custom.CustomFun.userInfoDSDetails;

import static in.valtech.util.PropertyFileReader.TextProp;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;

public class QuotingLoginPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// AM login page Constructor
	public QuotingLoginPage(WebDriver driver) {
		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		// Check that we're on the right AM login Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("quotingLoginPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Quoting Login page");
		}
	}

	By userName_XPATH = By.xpath(ObjRepoProp.getProperty("usernameField_XPATH"));
	By password_XPATH = By.xpath(ObjRepoProp.getProperty("passwordField_XPATH"));
	By submit_XPATH = By.xpath(ObjRepoProp.getProperty("loginButton_XPATH"));
	static By browserOverride = By.xpath(ObjRepoProp.getProperty("browserOverride_XPATH"));
	/**
	 * This function navigates to Westcon DTE application URL
	 * 
	 * @param driver
	 *            :WebDriver
	 * @param baseUrl
	 *            : Application URL
	 * @param driverName
	 *            : Name of the driver.
	 * @return AMLoginPage object
	 * @throws Exception
	 */
	public static QuotingLoginPage navigateTo_URL(WebDriver driver, String baseUrl,
			String driverName) throws Exception {

		// navigate to Westcon DTE URL
		if (driverName.equalsIgnoreCase("IE")
				|| driverName.equalsIgnoreCase("SAFARI")) {
			driver.get(baseUrl);
			Thread.sleep(2000);	
			if(CustomFun.isElementVisible(browserOverride, driver))
			{
				driver.navigate().to("javascript:document.getElementById('overridelink').click()");
			}
		}else{
			driver.get(baseUrl);				
		}
		return new QuotingLoginPage(driver);		
	}	

	/**
	 * This function Enter User name, Password & Click Submit 
	 * @return: AMQuoteListing object
	 * @throws Exception
	 */
	public QuotingLoginPage enterLoginCreds(String username,String pwd)
			throws Exception {	

		System.out.println("test");
		// Entering email
		GUIFunctions.typeTxtboxValue(driver, userName_XPATH, username);

		// Entering password
		GUIFunctions.typeTxtboxValue(driver, password_XPATH, pwd);

		return new QuotingLoginPage(driver);		

	}

	/**
	 * This function Enter User name, Password & Click Submit 
	 * @return: AMQuoteListing object
	 * @throws Exception
	 */
	public QuotingQuotesPage clickSubmit() {
		// Click on Login button
		GUIFunctions.clickElement(driver, submit_XPATH, "Sign in Button");

		return new QuotingQuotesPage(driver);
	}

	public QuotingLoginPage errorMsgVerification(String username,String pwd) {
		//click on Submit button without entering User name and password
		GUIFunctions.clickElement(driver, submit_XPATH, "Sign in Button");

		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("usernameError_XPATH"))).getText(), TextProp.getProperty("CommonErrorMsg"));
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("passwordError_XPATH"))).getText(), TextProp.getProperty("CommonErrorMsg"));

		//Click on Submit button by entering only User Name
		driver.findElement(userName_XPATH).clear();
		driver.findElement(password_XPATH).clear();
		GUIFunctions.typeTxtboxValue(driver, userName_XPATH, username);
		GUIFunctions.clickElement(driver, submit_XPATH, "Sign in Button");

		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("passwordError_XPATH"))).getText(), TextProp.getProperty("CommonErrorMsg"));

		//Click on Submit button by entering only Password
		driver.findElement(userName_XPATH).clear();
		driver.findElement(password_XPATH).clear();
		GUIFunctions.typeTxtboxValue(driver, password_XPATH, pwd);
		GUIFunctions.clickElement(driver, submit_XPATH, "Sign in Button");

		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("usernameError_XPATH"))).getText(), TextProp.getProperty("CommonErrorMsg"));

		//Click on Submit button by entering invalid user name or password
		driver.findElement(userName_XPATH).clear();
		driver.findElement(password_XPATH).clear();
		GUIFunctions.typeTxtboxValue(driver, userName_XPATH, userInfoDSDetails.getReseller());
		GUIFunctions.typeTxtboxValue(driver, password_XPATH, userInfoDSDetails.getReseller());

		GUIFunctions.clickElement(driver, submit_XPATH, "Sign in Button");
		
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("invalidUsernamePasswordError_XPATH"))).getText(), TextProp.getProperty("InvalidLoginErrorMsg"));
		
		return new QuotingLoginPage(driver);
	}

}

