package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;

import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC83_Quoting_SBAErrorForGreaterThanAllowedQTY extends BaseTest {

	String manufacturerListPriceBeforeSBA;
	String purchasePriceBeforeSBA;
	String vendorDiscountBeforeSBA;
	String SKU;
	String sbaRefNumber;
	String manufacturerListPriceAfterSBA;
	String purchasePriceAfterSBA;
	String vendorDiscountAfterSBA;
	String sbaDiscount;
	String[] sbaDiscountvalue;

	String[] skuNumber;
	String[] actualMListBeforeSBA;
	String[] actualVDiscountBeforeSBA;
	String[] actualWestCostBeforeSBA;

	String[] actualMListAfterSBA;
	String[] actualVDiscountAfterSBA;
	String[] actualWestCostAfterSBA;
	
	String[] actualMListAfterRemovingSBA;
	String[] actualVDiscountAfterRemovingSBA;
	String[] actualWestCostAfterRemovingSBA;
	
	String euserCompany;
	String sbaAvailableQty;
	String[] sbaQty;
	String sbaAgreement;
	String[] agreement;

	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step1 : Call TC06 which selects the End User and all mandatory fields filled", priority = 1)
	public void Step01_NavigateToTheCreateQuotePageWithAllMandatoryInfo() throws Exception {

		//Execute TC06 to navigate to Create Quote page with End User linked and all mandatory fields filled
		// Create Quote page is displayed with End user selected and all mandatory fields filled

		log.info("Create Quote page is displayed with End user selected and all mandatory fields filled");
		Reporter.log("<p>Create Quote page is displayed with End user selected and all mandatory fields filled-- "
				+ driver.getTitle());			
	}

	/**
	 * Save the values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step3 : Save the values like vendor discount, list price and purchase price", priority = 3)
	public void Step03_SaveTableValues() {
		//Save the table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceBeforeSBA = "+actualMListAfterSBA[1]);
		System.out.println("vendorDiscountBeforeSBA = "+actualVDiscountAfterSBA[0]);
		System.out.println("purchasePriceBeforeSBA = "+actualWestCostAfterSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");
	}

	/**
	 * Click on SBA tab
	 */
	@Test(description = "Step4 : Click on SBA tab", priority = 4)
	public void Step04_ClickSBATab() {
		//Click on SBA tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBATab();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaTabSearchLink_XPATH"))).getText().trim(),
				TextProp.getProperty("SBATabData"));

		log.info("Successfully clicked on SBA tab and SBA tab is opened");
		Reporter.log("<p>Successfully clicked on SBA tab and SBA tab is opened");
	}
	
	/**
	 * Click on applied SBA and verify the overlay displayed
	 * @throws InterruptedException 
	 */
	@Test(description = "Step5 : Click on applied SBA and verify the overlay displayed", priority = 5)
	public void Step05_ClickAppliedSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on applied SBA

		quoteDetails.clickAppliedManualSBA();
		log.info("Successfully clicked on applied SBA");
		Reporter.log("<p>Successfully clicked on applied SBA");

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlay_XPATH"))).getText()
				, TextProp.getProperty("SBADetailsOverlay"));

		log.info("Successfully verified the SBA details ovarlay");
		Reporter.log("<p>Successfully verified the SBA details ovarlay");
	}
	
	/**
	 * Save the discount
	 */
	@Test(description = "Step6 : Save the SBA discount", priority = 6)
	public void Step06_SaveSBADiscount() {
		//Save SBA Discount
		sbaDiscount = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsoverlayDiscount_XPATH")))
				.getText();

		sbaDiscountvalue = sbaDiscount.split("-");
		//sbaDiscount = sbaDiscount.substring(1,sbaDiscount.length()-1);
		System.out.println("sbaDiscountvalue = "+sbaDiscountvalue[1]);

		log.info("Successfully saved SBA discount");
		Reporter.log("<p>Successfully saved SBA discount");
	}
	
	/**
	 * Save the discount
	 */
	@Test(description = "Step7 : Save the SBA available quantity", priority = 7)
	public void Step07_SaveSBAAvailableQty() {
		//Save SBA Discount
		sbaAvailableQty = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlayAvailableQty_XPATH")))
				.getText();
		
		System.out.println("sbaAvailableQty = "+sbaAvailableQty);

		sbaQty = sbaAvailableQty.split(":");
		System.out.println("sbaQty = "+sbaQty[1]);
		
		sbaAgreement = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlayAgreement_XPATH")))
				.getText();
		
		System.out.println("sbaAgreement = "+sbaAgreement);
		
		agreement = sbaAgreement.split(":");
		System.out.println("agreement = "+agreement[1]);

		log.info("Successfully saved SBA Available Qty");
		Reporter.log("<p>Successfully saved SBA Available Qty");
	}
	
	/**
	 * Click on Cancel button
	 * @throws InterruptedException 
	 */
	@Test(description = "Step8 : Click Cancel button", priority = 8)
	public void Step08_ClickCancelButton() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Cancel button
		quoteDetails.clickCancelButton();

		log.info("Successfully closed the overlay");
		Reporter.log("<p>Successfully closed the overlay");
	}
	
	/**
	 * Click on Pricing Tab
	 */
	@Test(description = "Step9 : Click on Pricing Tab", priority = 9)
	public void Step09_ClickPricingTab() throws Exception {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");
	}
	
	@Test(description = "Step10 : Update the quantity of the product", priority = 10)
	public void Step10_UpdateQuantity() throws InterruptedException {
		//Update the quantity
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		int qty = Integer.parseInt(sbaQty[1].trim()) + 10 ;
		quoteDetails.updateQuantity(Integer.toString(qty));
		
		log.info("Successfully updated quantity");
		Reporter.log("<p>Successfully updated quantity");
	}
	
	/**
	 * Click on Validate Pricing button
	 */
	@Test(description = "Step11 : Click on Validate Pricing button", priority = 11)
	public void Step11_ClickValidatePricingButton() throws Exception {
		//Click on Validate Pricing button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		By SAPPriceValidationOverlayViewDetailsLink = By.xpath(ObjRepoProp.getProperty("SAPPriceValidationOverlayViewDetailsLink_XPATH"));
		
		quoteDetails.ClickOnValidatePricingButton();
		CustomFun.waitObjectToLoad(driver, SAPPriceValidationOverlayViewDetailsLink, 60);

		log.info("Successfully clicked on Validate Pricing button");
		Reporter.log("<p>Successfully clicked on Validate Pricing button");
	}
	
	@Test(description = "Step12 : Verify the error message", priority = 12)
	public void Step12_verifyErrorMessage() {
		//Verify the error message
		String errorMessage1 = driver.findElement(By.xpath(ObjRepoProp.getProperty("validatePricingSAPError1_XPATH"))).getText();
		String errorMessage2 = driver.findElement(By.xpath(ObjRepoProp.getProperty("validatePricingSAPError2_XPATH"))).getText();
		
		Assert.assertEquals(errorMessage1+"  "+errorMessage2, TextProp.getProperty("errorMessageForSBA")+agreement[1]);
		
		log.info("Successfully Verified Error Message");
		Reporter.log("<p>Successfully Verified Error Message");
	}
	
	/**
	 * Click on Update list price and cost button
	 */
	@Test(description = "Step13 : Click on Update list price and cost button", priority = 13)
	public void Step13_ClickUpdateListPriceCostButton() throws Exception {
		//Click Update List Price and Cost button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickUpdateListPriceCostButton();

		log.info("Successfully clicked on on Update list price and cost button");
		Reporter.log("<p>Successfully clicked on on Update list price and cost button");
	}
		
	/**
	 * Click on Pricing tab and verify updated pricing conditions
	 * @throws InterruptedException 
	 */
	@Test(description = "Step14 : Click on Pricing tab and verify updated pricing conditions", priority = 14)
	public void Step14_ClickPricingTabAndVerifyPricingConditions() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		
		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("sbaTab_XPATH"))));
		
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");

		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingConditionColumnNone_XPATH"))));
		
		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingTab_XPATH"))));

		//Verify updated pricing conditions
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL") && SKUdataSetList.get(i).getSbaType().equals("ZSD0")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto");
			}
			
			else if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText()
						, "None");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto","Improper pricing condition");
			}

		}
		log.info("Create Quote page with pricing condition as "+driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText());
		Reporter.log("<p>Create Quote page with pricing condition as -- "
				+ driver.findElement(By.xpath(ObjRepoProp
						.getProperty("pricingConditionColumnNone_XPATH"))).getText());
	}
}
