package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Reseller_OrderDetailPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller order detail page
	public  Reseller_OrderDetailPage (WebDriver driver) throws InterruptedException 
	{

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right reseller order detail page
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerOrderDetailPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Reseller order detail  page");

		}
	}

	public static String actualAuthorizationNo;
	//This function is to fetch Authorization no 
	public  String FetchAutorizationNo() throws Exception
	{
		actualAuthorizationNo=driver.findElement(By.xpath(ObjRepoProp.getProperty("ActualAuthorizationNo_XPATH"))).getText(); 
		System.out.println("actualAuthorizationNo="+actualAuthorizationNo);
		return actualAuthorizationNo;
	}

	public static String actualSalesOrder;
	//This function is to fetch Authorization no 
	public  String FetchSalesOrderNo() throws Exception
	{
		actualSalesOrder=driver.findElement(By.xpath(ObjRepoProp.getProperty("SalesOrderNoConfirmation_XPATH"))).getText(); 
		System.out.println("Sales Order Number ="+actualSalesOrder);
		return actualSalesOrder;
	}
	
	public static String endUserNameDetails;
	//This function is to fetch Authorization no 
	public  String FetchEndUserName() throws Exception
	{
		endUserNameDetails=driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserNameConfirmation_XPATH"))).getText();
		String[] getEndUserName = endUserNameDetails.split("\n");
		String endUserName =getEndUserName[0];
		System.out.println("End User Name ="+ endUserName);
		return endUserName;
	}
	
	public static String extendedPrice;
	//This function is to fetch Authorization no 
	public  String FetchExtendedPrice() throws Exception
	{
		extendedPrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("ExtendedPriceConfirmation_XPATH"))).getText(); 
		System.out.println("Extended Price ="+extendedPrice);
		return extendedPrice;
	}
	

	By firstLineItemPlusIcon=By.xpath(ObjRepoProp.getProperty("firstLineItemPlusIcon_XPATH"));
	//To click on + icon of first line item
	public Reseller_OrderDetailPage ExpandFirstLineItem() throws Exception
	{
		GUIFunctions.pageScrollDown(driver, 500);
		Thread.sleep(20000);
		//To click on + icon first line item
		GUIFunctions.clickElement(driver, firstLineItemPlusIcon, "Clicked on + icon->First line item");
		GUIFunctions.pageScrollDown(driver,100);
		Thread.sleep(2000);
		System.out.println("first line item expanded");
		return new Reseller_OrderDetailPage(driver);
	}

	//To fetch line level vrf value for first part number 10925
	public String ActualfirstEndUserPoNumber,ActualfirstRSPoNumber;
	public String FetchedActualfirstEndUserPoNumber;
	public String FetchedActualfirstResellerPoNo;

	By firstResellerPoNo =By.xpath(ObjRepoProp.getProperty("firstResellerPoNo_XPATH"));
	By firstEndUserPoNumber =By.xpath(ObjRepoProp.getProperty("firstEndUserPoNumber_XPATH"));

	public  String FetchEndUserPONumber() throws Exception
	{
		//To fetch first item end user PO number
		ActualfirstEndUserPoNumber=driver.findElement(firstEndUserPoNumber).getText();		
		FetchedActualfirstEndUserPoNumber=ActualfirstEndUserPoNumber.replaceAll("[^0-9]", "");
		System.out.println("FetchedActualfirstEndUser PoNumber ---- "+FetchedActualfirstEndUserPoNumber);
		return FetchedActualfirstEndUserPoNumber;
	}

	public String FetchResellerPONumber() throws Exception
	{
		//To fetch first item reseller PO number
		ActualfirstRSPoNumber=driver.findElement(firstResellerPoNo).getText();		
		FetchedActualfirstResellerPoNo=ActualfirstRSPoNumber.replaceAll("[^0-9]", "");
		System.out.println("FetchedActualfirst ResellerPoNo---- "+FetchedActualfirstResellerPoNo);
		return FetchedActualfirstResellerPoNo;
	}
}
