package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC44_Quoting_ValidatePricing  extends BaseTest {

	String[] skuNumber;
	/**
	 * Navigate to quote detail page by Calling TC03
	 * 
	 */
	
	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		

		log.info("Create Quote page is displayed with products added in pricing grid \n");
		Reporter.log("<p>Create Quote page is displayed with products added in pricing grid ");
	}

	/**
	 * Verify part numbers added on Pricing Grid
	 */
	@Test(description = "Step 2 : Verify added part number in Pricing grid", priority = 2)
	public void Step02_VerifyPartNumbersAdded() throws Exception {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		
		//Click on Pricing tab
		quoteDetails.clickPricingTab();
		
		//Verify that same part numbers added or not      
		Thread.sleep(5000);
        List<WebElement> actualSKU = driver.findElements(By.xpath(ObjRepoProp.getProperty("partNumberTableValue_XPATH")));		
        
       // for (int i = 0; i < SKUdataSetList.size(); i++) { }

		
		for (int i=0;i<SKUdataSetList.size();i++){
    		skuNumber = SKUdataSetList.get(i).getSKU().split("\n");	
			Assert.assertEquals(actualSKU.get(i).getText().trim(), skuNumber[i],
					"Partnumber mismatch");
			System.out.println(SKUdataSetList.get(i).getSKU());
			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());
		}
		   
		log.info("Successfully Verified part numbers added on Pricing Grid");
		Reporter.log("<p>Successfully Verified part numbers added on Pricing Grid");
	}

	
	/**
	 * Click on Validate Pricing button
	 */
	@Test(description = "Step 3 : Click on Validate Pricing button and Verify success message", priority = 3)
	public void Step03_ClickValidatePricingButton() throws Exception {
		//Click on Validate Pricing button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.ClickOnValidatePricingButton();
		quoteDetails.verifyValidatePricingSuccessMessage();
		
		log.info("Successfully clicked on Validate Pricing button and verified success message");
		Reporter.log("<p>Successfully clicked on Validate Pricing button and verified success message");
	}

	
	
	/**
	 * Click on Close button
	 */
	@Test(description = "Step 4 : Click on Close button", priority =4)
	public void Step04_ClickOnCloseButton() throws Exception {
		
		//Click on Cancel button and verify Validate Pricing Overlay is closed
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
        quoteDetails.clickOnCancelButtonInValidatePricingOverlay();
		log.info("Validation overlay is closed");
		Reporter.log("<p>Validation overlay is closed");
	}
	
	
}
