package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.custom.CustomFun.SKUdataSetList;

public class TC04_Quoting_VendorFileUpload extends BaseTest{

	//static QuotingAddProductsPage AMAddProductsPage;

	/**
	 * Execute TC02,Navigate to Add Product page
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02", priority = 1)
	public void Step01_NavigateToAddProductPage() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");

		//Execute TC02 to navigate to Add product page with reseller selected		
		log.info("Successfully navigated to Add product page\n");
		Reporter.log("<p>--Successfully navigated to Add product page ");	
	}

	/**
	 * Select Vendor
	 * 
	 */
	@Test(description = "Step 2: Select Vendor", priority = 2)
	public void Step02_Select_Vendor() throws Exception {

		//Execute TC02 to navigate to Add product page with reseller selected
		QuotingAddProductsPage AMAddProductsPage =new QuotingAddProductsPage(driver);
		//Select upload a file
		AMAddProductsPage.ClickOnUploadFile();

		log.info("Successfully clciked on upload a file\n");
		Reporter.log("<p>--Successfully clciked on upload a file ");	
		//Select a vendor from vendor drop down
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getVendorFilePath());
			log.info("SKU*************" + SKUdataSetList.get(i).getVendorName());
			AMAddProductsPage.SelectVendor(SKUdataSetList.get(i).getVendorName());
		}
	}
	String dir;
	/**
	 * Browse file and Click on Submit button 
	 */
	@Test(description = "Step 3: Browse file and Click on Submit button ", priority = 3)
	public void Step03_BrowseFileAndSubmit() throws Exception {
		QuotingAddProductsPage AMAddProductsPage =new QuotingAddProductsPage(driver);

		//Click on Browse button
		AMAddProductsPage.EnterUploadFilePath();
		
	/*	//upload file 
		dir = CustomFun.getRootDir();	
		String path = dir+"/resources/autoit/Chrome_UploadingVendorFile.exe";
		System.out.println(path);

		for (int i = 0; i < SKUdataSetList.size(); i++) {

			Runtime.getRuntime().exec(dir+SKUdataSetList.get(i).getVendorFilePath());
		}*/
		Thread.sleep(5000);
		AMAddProductsPage.UploadAFileSubmit(driver);  
		log.info("Successfully uploaded a file\n");
		Reporter.log("<p>--Successfully uploaded a file");
	}	

	@Test(description = "Step 4: Verifying the vendor Ref.No displayed", priority = 4)
	public void Step04_VendorRefnoVerify() throws Exception {

		QuotingQuoteDetailpage am= new QuotingQuoteDetailpage(driver);
		am.VendorFieldVerifyAssertion(driver);
		am.vendorValueVerify(driver);
		log.info("Selected the vendor");
		Reporter.log("<p>Verified the vendor's Ref.No -- "+ driver.getTitle());	
	}

	@Test(description = "Step 5: Click on Requester info tab", priority = 5)
	public void Step05_ClickonRequesterinfotab() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 200);
		amQuoteDetailsPage.clickOnRequesterinfotab();

		log.info("Successfully Click on Requester info tab\n");
		Reporter.log("<p>Successfully Click on Requester info tab");
	}

	@Test(description = "Step 6: Verificaiton of Quote file link", priority = 6)
	public void Step06_VerifyQuoteFileLnk() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.QuoteFileLnkVerify(driver);
		GUIFunctions.pageScrollUP(driver, 200);
		CustomFun.waitForPageLoaded(driver);
		log.info("Quote file link verified\n");
		Reporter.log("<p>Quote file link verified");

	}
}
