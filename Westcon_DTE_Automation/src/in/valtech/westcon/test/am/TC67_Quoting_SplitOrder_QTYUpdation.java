package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.common.CommonFun;
import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuoteToOrderConfirmationPage;
import in.valtech.westcon.QuotingPages.QuoteToOrderPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC67_Quoting_SplitOrder_QTYUpdation extends BaseTest {

	/**
	 * Call TC09
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08 TC09", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");
		// Execute TC-08 (without Opportunity)
		log.info("TC-09 Quote Submission Successful \n");
		Reporter.log("<p>TC-09 Quote Submission Successful ");
		

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}
	
	@Test(description = "Step 2: Select Convert to Order option from dropdown and click on GO", priority = 2)
	public void Step02_SelectConvertToOrderOption() throws Exception {
		//Select Convert to Order option and click on GO
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.selectConvertToOrderAndSubmit();
		
		log.info("Convert to Order option selected successfully and clicked on GO button\n");
		Reporter.log("<p>Convert to Order option selected successfully and clicked on GO button");
	}
	
	@Test(description = "Step 3: Verify Split Order Overlay confirmation popup", priority = 3)
	public void Step03_VerifySplitOrderOverlayConfPopup() throws Exception {
		//Split Order Overlay Confirmation Popup
		String splitOrderText = driver.findElement(By.xpath(ObjRepoProp.getProperty("splitOrderMessage_XPATH"))).getText();
		Assert.assertEquals(splitOrderText, TextProp.getProperty("splitOrderMessage"), "Split Order Overlay Text doesn't match");
			
		log.info("Split Overlay Text verified successfully\n");
		Reporter.log("<p>Split Overlay Text verified successfully");
	}
	
	@Test(description = "Step 4: Click on Yes button", priority = 4)
	public void Step04_ClickYesButton() throws Exception {
		//Click on Yes button in Split Order Overlay
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver); 
		amQuoteDetailsPage.clickYesButton(driver);
		
		log.info("Clicked on Yes button successfully\n");
		Reporter.log("<p>Clicked on Yes button successfully");
	}
	
	@Test(description = "Step 5: Enter Qty value greater than and less than the existing quantity", priority = 5)
	public void Step05_EditQuantity() throws Exception {
		//Edit the quantity of part numbers more than and less than the added quantity
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.editQuantity(driver);
		
		log.info("Quantity edited successfully\n");
		Reporter.log("<p>Quantity edited successfully");
	}
	
	@Test(description = "Step 6: Click on Submit button", priority = 6)
	public void Step06_SubmitSplitOrder() throws Exception {
		//Click on Submit button from Split Order Overlay
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.submitSplitOrder(driver);
		
		amQuoteDetailsPage.emailYesButton(driver);
	}
	
	/**
	 * Function to enter PO number
	 */

	@Test(description = "Step 7:Enter PO number ", priority = 7)
	public void Step07_EnterPoNumber() throws Exception 
	{
		QuoteToOrderPage amConvertToOrder = new QuoteToOrderPage(driver);
		amConvertToOrder.EnterPoNumber(userInfoDSDetails.getPoNumber());
		log.info("PO number entered\n");
		Reporter.log("<p>PO number entered");

	}
	
	/**
	 *Click on submit
	 */
	@Test(description="step 8: Click on submit button in quote to order page",priority = 8)
	public void Step08_ClickSubmitButton()throws Exception
	{
		QuoteToOrderPage amConvertToOrder=new QuoteToOrderPage(driver);
		amConvertToOrder.ClickOnSubmit();
		log.info("SUbmit button clicked");
		Reporter.log("<p>--SUbmit button clicked");
	}

	/**
	 * To fetch sales order number
	 */
	String SalesOrdNumber;
	@Test(description="step 9: To fetch sales order number",priority = 9)
	public void Step09_salesOrderNumber()throws Exception
	{
		QuoteToOrderConfirmationPage amOrderConfirmation=new QuoteToOrderConfirmationPage(driver);
		//amOrderConfirmation.FetchOrderNumber();
		amOrderConfirmation.FetchOrderNumber();
		SalesOrdNumber = amOrderConfirmation.salesorderNumber;
		System.out.println("Sales order number : "+ SalesOrdNumber);
		Thread.sleep(5000);
		CommonFun.QuotingLogout(driver, "You have signed out of your account.");
		log.info("Sales order number displayed");
		Reporter.log("<p>--Sales order number displayed");
	}
}
