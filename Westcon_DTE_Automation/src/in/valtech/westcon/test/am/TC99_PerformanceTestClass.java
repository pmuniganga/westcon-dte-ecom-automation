package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import in.valtech.westcon.QuotingPages.QuotingLoginPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;
import in.valtech.westcon.QuotingPages.QuotingSelectResellerPage;

public class TC99_PerformanceTestClass extends BaseTest{

	static QuotingLoginPage amLoginPage;
	static QuotingQuotesPage amQuotelistingPage;
	static QuotingSelectResellerPage amResellerlistingpage;
	static  QuotingAddProductsPage amAddProductsPage;
	static QuotingQuoteDetailpage amQuoteDetailspage;
	static String username = null;
	static String password = null;
	static String Attn = null;
	static String Company = null;
	static String Add1 = null;
	static String Add2 = null;
	static String City = null;
	static String postal = null;
	static String state = null;
	static String Phone = null;
	static String carrier=null;
	String inputType;
	String[] skuNumber;
	String[] skuQty;
	String[] skuListPrice;
	String[] skuVendorDiscount;
	String[] skuWestconCost;
	String[] skuResellerPrice;
	String[] skuResellerPriceOffList;
	String[] skuGlobalPoints;
	String skuData;
	static String ename = null;
	ArrayList<String> tableValuesBeforeChange = new ArrayList<String>();
	ArrayList<String> tableValuesAfterChange = new ArrayList<String>();
	List<WebElement> tableValues;



	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */
	@Test(description = "Step 1: Open browser,Navigate to the AM URL", priority = 1)
	public void Step01_NavigateToWestconAM_URL() throws Exception {

		// Navigate/launch the AM URL
		amLoginPage = QuotingLoginPage.navigateTo_URL(driver, baseUrl, driverName);

		log.info("Navigated to the AM URL");
		Reporter.log("<p>Navigated to AM Login Page URL -- "
				+ driver.getTitle());			
	}

	/**
	 * Enter username , Pwd
	 * 
	 */
	@Test(description = "Step 2: Enter username , Pwd and click on Login link", priority = 2)
	public void Step02_EnterCridential() throws Exception {

		amLoginPage = new QuotingLoginPage(driver);
		System.out.println("user=" +userInfoDSDetails.getAmUserName());
		System.out.println("pwd=" +userInfoDSDetails.getAmPassword());
		username = userInfoDSDetails.getAmUserName();		
		password = userInfoDSDetails.getAmPassword();

		//amLoginPage = new AMLogin(driver);

		amLoginPage.enterLoginCreds(username, password);
		//amLoginPage= AMLogin.enterLoginCreds(username, password);

		log.info("Successfully Entered Cridential\n");
		Reporter.log("<p>Successfully Entered Cridential");

	}

	/**
	 * click on Login link
	 * 
	 */
	@Test(description = "Step 3: Click on Login link", priority = 3)
	public void Step03_AMLogin() throws Exception {

		amLoginPage = new QuotingLoginPage(driver);

		amLoginPage.clickSubmit();

		log.info("The AM successfully logged-in and Quote listing page is displayed\n");
		Reporter.log("<p>The AM successfully logged-in and Quote listing page is displayed");

	}

	@Test(description = "Step 4: Click on Create new Quote", priority = 4)
	public void Step04_clickCreateNewQuoteButton() throws Exception {

		amQuotelistingPage = new QuotingQuotesPage(driver);

		amQuotelistingPage.clickCreateNewQuoteButton();

		log.info("Reseller listing page is displayed\n");
		Reporter.log("<p>Reseller listing page is displayed");
	}

	@Test(description = "Step 5: Enter Reseller and Search", priority = 5)
	public void Step05_enteResellerandSearch() throws Exception {
		System.out.println("user=" +userInfoDSDetails.getReseller());

		amResellerlistingpage= new QuotingSelectResellerPage(driver);

		amResellerlistingpage.enterResellerAndSearch(userInfoDSDetails.getReseller());



		log.info("Respective reseller is displayed in the listing page\n");
		Reporter.log("<p>Respective reseller is displayed in the listing page");
	}

	@Test(description = "Step 6: Select Reseller", priority = 6)
	public void Step06_selectReseller() throws Exception {

		// Select First searched Reseller in list
		amResellerlistingpage.selectFirstReseller();

		//Verifying selected tab
		amAddProductsPage = new QuotingAddProductsPage(driver);
		amAddProductsPage.verifyCopyPasteBOMTabOne();


		log.info("Successfully displayed Add product page and default Copy & Paste BOM is selected \n");
		Reporter.log("<p>Successfully displayed Add product page and default Copy & Paste BOM is selected ");
	}

	@Test(description = "Step 7: Enter PartNumbers and Click Submit", priority = 7)
	public void Step07_EnterPartNumbersAndClickSubmit() throws Exception {

		QuotingAddProductsPage amAddProductsPage = new QuotingAddProductsPage(driver);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			inputType = SKUdataSetList.get(i).getInputType();
			System.out.println("inputType = "+inputType);
			amAddProductsPage.selectInputFromDropdown(driver, inputType);

			skuNumber = SKUdataSetList.get(i).getSKU().split("\n");
			skuQty = SKUdataSetList.get(i).getQuantity().split("\n");
			skuListPrice = SKUdataSetList.get(i).getListPrice().split("\n");
			skuVendorDiscount = SKUdataSetList.get(i).getVendorDiscount().split("\n");
			skuWestconCost = SKUdataSetList.get(i).getWestconCost().split("\n");
			skuResellerPrice = SKUdataSetList.get(i).getResellerPrice().split("\n");
			skuResellerPriceOffList = SKUdataSetList.get(i).getResellerPriceOffList().split("\n");
			skuGlobalPoints = SKUdataSetList.get(i).getGlobalPoints().split("\n");
			System.out.println("Number***"+skuNumber.length);
			for(int j = 0 ; j < skuNumber.length ; j++) {

				skuData = skuNumber[j]+" "+skuQty[j]+" "+skuListPrice[j]+" "+skuVendorDiscount[j]+" "+
						skuWestconCost[j]+" "+skuResellerPrice[j]+" "+skuResellerPriceOffList[j]+" "+skuGlobalPoints[j];

				System.out.println("skuNumber["+j+"] = "+skuNumber[j]);

				amAddProductsPage.CopyPasteBOM(driver , skuData.replace("NA", ""));
			} 	  
		}

		log.info("successfully entered the part numbers \n");
		Reporter.log("<p>successfully entered the part numbers ");


	}  

	//adding product to Quote
	@Test(description = "Step 8: Click on Copy Paste BOM Submit", priority = 8)
	public void Step08_clickCopyPasteBOMSubmitButton() throws Exception {

		QuotingAddProductsPage amAddProductsPage = new QuotingAddProductsPage(driver);

		amAddProductsPage.CopyPasteBOMSubmit(driver);

		log.info("Reseller listing page is displayed\n");
		Reporter.log("<p>Reseller listing page is displayed");
	}

	//Select End USer 
	@Test(description = "Step 9: Selecting end user ", priority = 9)
	public void Step09_ClickOnPencilIcon() throws Exception {
		// Selecting end user from end user modal 

		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 100);
		amQuotePage.AMClickEndUserSearchLink();

		log.info("Successfully end user search modal is displayed");
		Reporter.log("<p>Successfully end user search modal is displayed");
	}

	@Test(description = "Step 10: Enter End Username & Click on Search button", priority = 10)
	public void Step10_EnterUN_Search() throws Exception {
		// Search list should be displayed	
		System.out.println("End user=" +userInfoDSDetails.getEndUser());
		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		ename = userInfoDSDetails.getEndUser();	

		amQuotePage.AMendUserModal_enterEndUserName(ename);
		amQuotePage.AMendUserModal_ClickonSearch();
		amQuotePage.AMVerifyEndUserModalHeader();

		log.info("Successfully end user list is displayed");
		Reporter.log("<p>Successfully end user list is displayed");
	}

	//Add End User to Quote
	@Test(description = "Step 11: Select End user from the list shown", priority =11)
	public void Step11_SelectEndUser() throws Exception {
		// Create Quote page should be displayed with End user selected 
		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		amQuotePage.AMendUserModal_ClickonRadio();
		amQuotePage.AMendUserModal_ClickonSelectUser();
		log.info("Successfully end user is selected from modal");
		Reporter.log("<p>Successfully end user is selected from modal");

	}	

	//Enter mandatory fields in Quote like Quote Close date,Requester Info
	/**
	 * Select Quote exp date , Quote close date, Enter probability
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 12: Select Quote exp date , Quote close date, Enter probability", priority = 12)
	public void Step12_Enter_Quoteexpdate_Quoteclosedate_probability()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		System.out.println("Date ="+new Date());

		amQuoteDetailsPage.enterQuoteexpdate_Quoteclosedate_probability(CustomFun
				.addDays(new Date(), Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim()), locale),CustomFun
						.addDays(new Date(), Integer.parseInt(userInfoDSDetails
								.getQuoteCloseDate().trim()), locale), userInfoDSDetails.getProbability());


		log.info("Successfully Enter Quote exp date,Quote close date and probability\n");
		Reporter.log("<p>Successfully Enter Quote exp date,Quote close date and probability");

	}

	/**
	 * Click on Requester info tab
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 13: Click on Requester info tab", priority = 13)
	public void Step13_ClickonRequesterinfotab() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnRequesterinfotab();

		log.info("Successfully Click on Requester info tab\n");
		Reporter.log("<p>Successfully Click on Requester info tab");

	}

	/**
	 * Click Requester name pencil icon and enter requester name
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 14: Click Requester name pencil icon and enter requester name", priority = 14)
	public void Step14_EnterRequesterName() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterName(TextProp.getProperty("RequesterName"));

		log.info("Successfully Click Requester name pencil icon and enter requester name\n");
		Reporter.log("<p>Successfully Click Requester name pencil icon and enter requester name");

	}

	/**
	 * Click Requester email pencil icon and enter requester email
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 15: Enter requester email", priority = 15)
	public void Step15_EnterRequesterEmail() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterEmail(TextProp.getProperty("RequesterEmail"));

		log.info("Successfully entered requester email\n");
		Reporter.log("<p>Successfully entered requester email");

	}

	@Test(description = "Step 16: Select Sales office from drop down", priority = 16)
	public void Step16_SelectSalesOffice() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);

		if(CustomFun.isElementVisible(By.xpath(ObjRepoProp.getProperty("SalesOfficeDDL_XPATH")), driver)== true)
		{
			if(locale.equalsIgnoreCase("US")) {
				amQuoteDetailsPage.selectSalesOffice(TextProp.getProperty("SalesOfficeValue"));
			}
			else if(locale.equalsIgnoreCase("CA")){
				amQuoteDetailsPage.selectSalesOffice(TextProp.getProperty("SalesOfficeValue_CA"));
			}}

		amQuoteDetailsPage.clickPricingTab();
		log.info("Successfully Selected Sales office\n");
		Reporter.log("<p>Successfully Selected Sales office");

	}

	/**
	 * Click on Validate Pricing button
	 */
	@Test(description = "Step 17 : Click on Validate Pricing button and Verify success message", priority = 17)
	public void Step17_ClickValidatePricingButton() throws Exception {
		//Click on Validate Pricing button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.ClickOnValidatePricingButton();

		log.info("Successfully clicked on Validate Pricing button and verified success message");
		Reporter.log("<p>Successfully clicked on Validate Pricing button and verified success message");
	}



	/**
	 * Click on Close button
	 */
	@Test(description = "Step 18 : Click on Close button", priority =18)
	public void Step18_ClickOnCloseButton() throws Exception {


		//Click on Cancel button and verify Validate Pricing Overlay is closed
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.verifyValidatePricingSuccessMessage();
		quoteDetails.clickOnCancelButtonInValidatePricingOverlay();
		log.info("Validation overlay is closed");
		Reporter.log("<p>Validation overlay is closed");
	}


	//delete 10 part numbers
	@Test(description = "Step 19: Delete all the part numbers from the table", priority = 19)
	public void Step19_DeleteAllPartNumbers() throws Exception {
		//Click on Select All checkbox
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		/*	amQuoteDetailspage.selectAllCheckbox(driver);
		log.info("successfully selected Select All checkbox\n");
		Reporter.log("<p>successfully selected Select All checkbox");

		//Delete all part numbers
		amQuoteDetailspage.deletePartNumbers(driver);
		 */

		//delected part number based on count
		amQuoteDetailspage.selectSomeCheckbox(skuNumber.length/2);
		amQuoteDetailspage.deletePartNumbers(driver);

		log.info("successfully deleted all part numbers\n");
		Reporter.log("<p>successfully deleted all part numbers");

	}

	@Test(description = "Step 20: Open Search Products overlay", priority = 20)
	public void Step20_ClickSearchProductsIcon() throws Exception {
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		Thread.sleep(10000);
		//Click on Search Products Icon
		amQuoteDetailspage.ClickOnProductIcon(driver);

		log.info("successfully clicked on Search Products Icon and overlay is displayed\n");
		Reporter.log("<p>successfully clicked on Search Products Icon and overlay is displayed");
	}

	@Test(description = "Step 21: Select input type from the dropdown", priority = 21)
	public void Step21_DropdownSelect() throws Exception {
		//Select input type from the dropdown

		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			inputType = SKUdataSetList.get(i).getInputType();
			amQuoteDetailspage.selectInputFromDropdown(driver, inputType);
		}

		log.info("successfully selected the input format\n");
		Reporter.log("<p>successfully selected the input format");
	}

	@Test(description = "Step 22: Enter input in valid format", priority = 22)
	public void Step22_EnterInputInValidFormat() throws Exception {
		//Enter input in valid format
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {
			skuNumber = SKUdataSetList.get(i).getSKU().split("\n");
			skuQty = SKUdataSetList.get(i).getQuantity().split("\n");
			skuListPrice = SKUdataSetList.get(i).getListPrice().split("\n");
			skuVendorDiscount = SKUdataSetList.get(i).getVendorDiscount().split("\n");
			skuWestconCost = SKUdataSetList.get(i).getWestconCost().split("\n");
			skuResellerPrice = SKUdataSetList.get(i).getResellerPrice().split("\n");
			skuResellerPriceOffList = SKUdataSetList.get(i).getResellerPriceOffList().split("\n");
			skuGlobalPoints = SKUdataSetList.get(i).getGlobalPoints().split("\n");
			for(int j = 0 ; j < skuNumber.length/2 ; j++) {
				skuData = skuNumber[j]+" "+skuQty[j]+" "+skuListPrice[j]+" "+skuVendorDiscount[j]+" "+
						skuWestconCost[j]+" "+skuResellerPrice[j]+" "+skuResellerPriceOffList[j]+" "+skuGlobalPoints[j];

				amQuoteDetailspage.CopyPasteBOM(driver , skuData.replace("NA", ""));
			} 	  
		}

		log.info("successfully submitted the part numbers and Quote detail page is displayed\n");
		Reporter.log("<p>successfully submitted the part numbers and Quote detail page is displayed");

	}

	@Test(description = "Step 23: Click on Add button", priority = 23)
	public void Step23_ClickAddButton() throws Exception {
		//Click on Submit button
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailspage.PartNumberTab_ClickOnAdd(driver);

		log.info("successfully clicked on Submit button and navigated to create quote page\n");
		Reporter.log("<p>successfully clicked on Submit button and navigated to create quote page");
	}

	//Adding shipping info for freight calculation
	/**
	 * Click on Shipping tab
	 * 
	 */
	@Test(description="Step 24:Click on Shipping tab", priority=24)
	public void Step24_ClickOnShippingTab() throws Exception{
		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 400);
		//click on shipping tab
		amQuoteDetailspage.clickOnShippingtab();

		//Verify shipping tab opened 
		Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("ShippingTabOpen_XPATH")), driver),
				"Shipping tab is not open");

		log.info("Successfully verified shipping tab is open");
		Reporter.log("<p>--Successfully verified shipping tab is open");	

	}

	/**
	 * Click on Shipping tab
	 * 
	 */
	@Test(description="Step 25:Click on Edit shipping information button", priority=25)
	public void Step25_ClickOnEditShippingInformationBtn() throws Exception{
		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);

		//Click on Edit shipping information button
		amQuoteDetailspage.clickOnEditShippingInfoButton();

		log.info("Successfully clicked on edit shipping info button");
		Reporter.log("<p>--Successfully clicked on edit shipping info button");	


	}

	/**
	 * Select 1View Address Book radio button
	 */
	@Test(description = "Step 26:Select Search SAP Ship-to account radio button", priority = 26)
	public void Step26_SelectSearchSAPShiptoAccountRadioButton() throws Exception {

		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);

		Boolean flag = false;
		flag = CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("InputSAPAccountNo_XPATH")),driver);
		if(!flag) {
			//click on new address radio button
			amQuoteDetailspage.clickOnSearchSAPAccountRadioBtn();

			log.info("Successfully clicked on Search SAP Ship-to account radio button");
			Reporter.log("<p>--Successfully clicked on Search SAP Ship-to account radio button");
		}
		else {

			//Verify fields enabled
			Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("InputSAPAccountNo_XPATH")), driver),
					"Fields not enabled");

			log.info("Successfully verified Search SAP Ship-to account fields enabled");
			Reporter.log("<p>--Successfully verified Search SAP Ship-to account fields enabled");	
		}
	}

	/**
	 * Select saved address from dropdown
	 * 
	 */
	@Test(description="Step 27:Enter SAP Ship to Account Number", priority=27)
	public void Step27_SelectSavedAddressfromDropdown() throws Exception{
		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);

		//Enter Ship to account
		String shipToAccount=userInfoDSDetails.getReseller();
		amQuoteDetailspage.EnterSAPShipToAccount(shipToAccount);

		log.info("Successfully Entered SAP Ship to Account Number");
		Reporter.log("<p>--Successfully Entered SAP Ship to Account Number");
	}


	/***
	 * Click on Search button to add an ship to address
	 * 
	 */
	@Test(description = "Step 28:Click on Search button", priority = 28)
	public void Step28_ClickOnSearchButton() throws Exception {

		amQuoteDetailspage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailspage.clickOnSAPAccountSearchBtn();

		log.info("Successfully Clicked on Search button");
		Reporter.log("<p>--Successfully Clicked on Search button");	
	}

	/**
	 * Select shipping method , enter carrier account number, shipdate , select request blind shipment,
	 *  select request ship complete, enter shipping instructions and Click Save button
	 */

	@Test(description = "Step 30:Select shipping method , enter carrier account number, shipdate , select request blind shipment, select request ship complete,"
			+ " enter shipping instructions and Click Save button", priority =30)
	public void Step30_SelectShippingMethodOtherDetailsAndSave() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		//enter carrier account number
		carrier=userInfoDSDetails.getCarrierNumber();
		amQuoteDetailsPage.EnterCarrierNumber(carrier);

		//select request blind shipment
		amQuoteDetailsPage.clickOnBlindshipmentCheckbox();

		//select request ship complete

		//enter shipping instructions 
		amQuoteDetailsPage.enterShippingInstructions(TextProp.getProperty("shippingInstructions"));

		//click on save shipping instruction
		amQuoteDetailsPage.clickOnSaveShippingInstructionCheckBox();

		//select ship date 
		amQuoteDetailsPage.enterEarliestRequiredShipDate(CustomFun.getCurrentDate());


		//Select shipping method
		if(locale.equalsIgnoreCase("US")) {
			System.out.println("Shipping method***"+userInfoDSDetails.getShippingMtd());
			amQuoteDetailsPage.SelectShippingMethod(userInfoDSDetails.getShippingMtd());
		}
		else if(locale.equalsIgnoreCase("CA")){
			amQuoteDetailsPage.SelectShippingMethod(userInfoDSDetails.getShippingMtd());
		}

		log.info("Successfully selected the shipping method");
		Reporter.log("<p>--Successfully selected the shipping method");	

		//Click Save button
		amQuoteDetailsPage.clickOnSaveButton();

		log.info("Successfully saved the shipping details");
		Reporter.log("<p>--Successfully saved the shipping details");	
	}

	//FreighT Calculator
	@Test(description = "Step 31: Click on Freight Calculator button", priority = 31)
	public void Step31_FreightCalculator() throws Exception {
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailspage.clickPricingTab();
		amQuoteDetailspage.clickOnFreightCalculatorButton();
		amQuoteDetailspage.verifyEstimatedfreightValue();
	}

	/**
	 * Click on Pricing Tab
	 */
	@Test(description = "Step32 : Click on Pricing Tab", priority = 32)
	public void Step32_ClickPricingTab() throws Exception {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");
	}

	/**
	 * Save all table values
	 */
	@Test(description = "Step33 : Save all table values", priority = 33)
	public void Step33_SaveAllTableValues() throws Exception {
		//Retrieve all the table values and Save in the list

		//List<WebElement> tableRows = driver.findElements(By.xpath(ObjRepoProp.getProperty("tableRows_XPATH")));
		for(int i = 1 ; i <= 1 ; i++) {
			tableValues = driver.findElements(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody/tr["+i+"]/td[contains(@class,'htNumeric') or contains(@class,'listPrice') or contains(@class,'purchasePrice') or contains(@class,'undefined')]"));
			for(int j = 0 ; j < tableValues.size() ; j++) {	
				tableValuesBeforeChange.add(tableValues.get(j).getText());
				System.out.println(tableValuesBeforeChange.get(j));
			}
		}

		log.info("Successfully saved the table values");
		Reporter.log("<p>Successfully saved the table values");
	}

	/**
	 * Change Manufacturer List price and Vendor Discount
	 */
	@Test(description = "Step34 : Change Manufacturer List price and Vendor Discount", priority = 34)
	public void Step34_ChangePrice() throws Exception {
		//Change Manufacturer List price and Vendor Discount
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterManufacturerListPrice(TextProp.getProperty("alteredManufacturerListPrice"));
		//Thread.sleep(5000);
		//quoteDetails.AlterVendorDiscount();

		log.info("Successfully altered Manufacturer List price and Vendor Discount");
		Reporter.log("<p>Successfully altered Manufacturer List price and Vendor Discount");
	}

	/**
	 * Click on Validate Pricing button
	 */
	@Test(description = "Step35 : Click on Validate Pricing button", priority = 35)
	public void Step35_ClickValidatePricingButton() throws Exception {
		//Click on Validate Pricing button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.ClickOnValidatePricingButton();

		log.info("Successfully clicked on Validate Pricing button");
		Reporter.log("<p>Successfully clicked on Validate Pricing button");
	}

	/**
	 * Verify SAP Price Validation Overlay
	 */
	@Test(description = "Step36 : Verify SAP Price Validation Overlay", priority =36)
	public void Step36_VerifyPriceValidationOverlay() throws Exception {
		//Verify SAP Price Validation Overlay
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("SAPPriceValidationOverlay_XPATH"))).getText(), 
				TextProp.getProperty("SAPValidationOverlay"));

		log.info("SAP price validation overlay is opened as expected");
		Reporter.log("<p>SAP price validation overlay is opened as expected");
	}

	/**
	 * Click on View Details link
	 */
	@Test(description = "Step37 : Click on View Details link", priority = 37)
	public void Step37_ClickViewDetails() throws Exception {
		//Click on View Details link
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickViewDetails();

		log.info("Successfully clicked on View Details link");
		Reporter.log("<p>Successfully clicked on View Details link");
	}

	/**
	 * Verify altered list price and vendor discount
	 */
	/*	@Test(description = "Step38 : Verify altered list price and vendor discounty", priority = 38)
	public void Step38_VerifyAlteredPrice() throws Exception {
		//Verify Altered prices in SAP Validation Overlay
		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("SAPPriceValidationOverlayAlteredListPrice_XPATH"))).getText(), 
				TextProp.getProperty("alteredManufacturerListPrice"));

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("SAPPriceValidationOverlayAlteredListPricVendorDiscount_XPATH"))).getText(), 
				TextProp.getProperty("alteredVendorDiscount"));

		log.info("Successfully verified altered values");
		Reporter.log("<p>Successfully verified altered values");
	}
	 */
	/**
	 * Click on Update list price and cost button
	 */
	@Test(description = "Step39 : Click on Update list price and cost button", priority = 39)
	public void Step39_ClickUpdateListPriceCostButton() throws Exception {
		//Click Update List Price and Cost button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickUpdateListPriceCostButton();

		log.info("Successfully clicked on on Update list price and cost button");
		Reporter.log("<p>Successfully clicked on on Update list price and cost button");
	}

	/**
	 * Save all table values after SAP validation and compare
	 */
	@Test(description = "Step40 : Save all table values after SAP validation and Compare", priority = 40)
	public void Step40_SaveAllTableValuesAndCompare() throws Exception {
		//Retrieve all the table values and Save in the list
		//List<WebElement> tableRows = driver.findElements(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::div[contains(@class,'ht_master handsontable')]/descendant::tbody/tr"));
		for(int i = 1 ; i <= 1 ; i++) {
			tableValues = driver.findElements(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody/tr["+i+"]/td[contains(@class,'htNumeric') or contains(@class,'listPrice') or contains(@class,'purchasePrice') or contains(@class,'undefined')]"));
			for(int j = 0 ; j < tableValues.size() ; j++) {	
				tableValuesAfterChange.add(tableValues.get(j).getText());
				System.out.println(tableValuesAfterChange.get(j));
			}
		}

		log.info("Successfully saved the table values");
		Reporter.log("<p>Successfully saved the table values");

		for(int k = 0 ; k < tableValuesAfterChange.size() ; k++) {
			Assert.assertEquals(tableValuesAfterChange.get(k), tableValuesBeforeChange.get(k));
		}

		log.info("Successfully compared the table values");
		Reporter.log("<p>Successfully compared the table values");
	}


	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	/*@Test(description = "Step 41: Select Submit quote from the dropdown and click on GO button", priority = 41)
	public void Step41_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		Thread.sleep(5000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button");

	}
	 */
	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	/*@Test(description = "Step 42: Click On No, do not validate button", priority = 42)
	public void Step42_ClickOnDoNotValidateButton()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");	

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message");

	}*/


}
