package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

public class TC11_Quoting_SubmitQuote_VRF extends BaseTest {
	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08 -- ", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}

	/**
	 * Click on VRF tab
	 * @throws Exception 
	 */
	@Test(description = "Step2 : Click on VRF tab", priority = 2)
	public void Step02_ClickVRFTab() throws Exception {
		//Click on VRF tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//GUIFunctions.pageScrollUP(driver, 10);
		quoteDetails.clickVRFTab();

		log.info("Successfully Clicked on VRF tab");
		Reporter.log("<p>Successfully Clicked on VRF tab");
	}

	/**
	 * Enter VRF Auth Number
	 */
	@Test(description = "Step3 : Enter VRF Auth Number", priority = 3)
	public void Step03_EnterVRFAuthNumber() {
		//Enter VRF Auth Number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.EnterVRFAuthNumber();

		log.info("Successfully Entered VRF Auth Number");
		Reporter.log("<p>Successfully Entered VRF Auth Number");
	}

	/**
	 * Click on Apply Button
	 */
	@Test(description = "Step4 : Click on Apply Button", priority = 4)
	public void Step04_ClickApplyButton() {
		//Click on Apply Button
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.ClickApplybutton();

		log.info("Successfully Clicked on Apply Button");
		Reporter.log("<p>Successfully Clicked on Apply Button");
	}

	/**
	 * Enter VRF Reseller PO number and EndUser PO number
	 * @throws InterruptedException 
	 */
	@Test(description = "Step4 : Enter VRF field values", priority = 4)
	public void Step04_EnterVRFFieldValues() throws InterruptedException {
		//Enter VRF Reseller PO number and EndUser PO number
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.EnterVRFFieldValues();

		log.info("Successfully Entered VRF field values");
		Reporter.log("<p>Successfully Entered VRF field values");
	}

	/**
	 * This is to check VRF field is not editable
	 * @throws InterruptedException 
	 */
	//Step 5 - As discussed, will be scripted later

	/*@Test(description = "Step5 : Check VRF field is not editable", priority = 5)
	public void Step05_CheckFieldNotEditable() throws InterruptedException {
		//Check VRF field not editable
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);


		log.info("Successfully Verified the VRF field is not editable");
		Reporter.log("<p>Successfully Verified the VRF field is not editable");
	}*/

	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Select Submit quote from the dropdown and click on GO button", priority = 6)
	public void Step06_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {
		GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Select Submit quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Click On No, do not validate button", priority = 7)
	public void Step07_ClickOnDoNotValidateButton1()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}

	/**
	 * Search created/submitted Quote
	 * 
	 * @throws Exception
	 */
	
	@Test(description = "Step 8: Click on Quotes tab", priority = 8)
	public void Step08_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);
		
		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");
	
	}
	
	@Test(description = "Step 9 : Search for the submitted quote", priority = 9)
	public void Step09_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);
		
		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}
	
	@Test(description = "Step 10 : Verify the total price", priority = 10)
	public void Step10_VerifyTotalPrice() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.verifyTotalQuotePrice(driver);
		
		log.info("Successfully verified the Quote total price\n");
		Reporter.log("<p>Successfully verified the Quote total price");
	}
}
