package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC86_Quoting_VendorDiscountError extends BaseTest {

	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	/**
	 * Change Vendor Discount to Negative value
	 */
	@Test(description = "Step2 : Change Vendor Discount to negative value", priority = 2)
	public void Step02_ChangeDiscount() throws Exception {
		//Change Vendor Discount to negative value
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterVendorDiscount(TextProp.getProperty("negativeVendorDiscount"));
		
		log.info("Successfully altered Vendor Discount");
		Reporter.log("<p>Successfully altered Vendor Discount");
	}
	
	@Test(description = "Step03 : Verify Vendor Discount Error for Negative value", priority = 3)
	public void Step03_VerifyVendorDiscountError() {
		//Verify Vendor Discount Error
		By vendorDiscountError = By.xpath(ObjRepoProp.getProperty("vendorDiscountError_XPATH"));
		
		Assert.assertTrue(driver.findElement(vendorDiscountError).isDisplayed());
		
		log.info("Successfully verified Vendor Discount Error");
		Reporter.log("<p>Successfully verified Vendor Discount Error");
	}
	
	/**
	 * Change Vendor Discount to more than 100
	 */
	@Test(description = "Step4 : Change Vendor Discount to more than 100", priority = 4)
	public void Step04_ChangeDiscount() throws Exception {
		//Change Vendor Discount to more than 100
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterVendorDiscount(TextProp.getProperty("vendorDiscountMoreThan100"));
		
		log.info("Successfully altered Vendor Discount");
		Reporter.log("<p>Successfully altered Vendor Discount");
	}
	
	@Test(description = "Step05 : Verify Vendor Discount Error for Negative value", priority = 5)
	public void Step05_VerifyVendorDiscountError() {
		//Verify Vendor Discount Error
		By vendorDiscountError = By.xpath(ObjRepoProp.getProperty("vendorDiscountError_XPATH"));
		
		Assert.assertTrue(driver.findElement(vendorDiscountError).isDisplayed());
		
		log.info("Successfully verified Vendor Discount Error");
		Reporter.log("<p>Successfully verified Vendor Discount Error");
	}
}

