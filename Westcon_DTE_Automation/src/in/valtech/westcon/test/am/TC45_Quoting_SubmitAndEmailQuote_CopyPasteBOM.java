package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC45_Quoting_SubmitAndEmailQuote_CopyPasteBOM extends BaseTest {

	//static QuotingQuoteDetailpage amQuoteDetailsPage;

	/**
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");		

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}
	/**
	 * Select Submit quote from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Select 'Submit & Email' quote from the dropdown and click on GO button", priority = 2)
	public void Step02_SelectDropdownAndSubmitAndEmailQuoteFrom()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitAndEmailQuoteFrom();

		log.info("Successfully Select Submit and Email quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit and Email quote from the dropdown and click on GO button");

	}

	/**
	 * Select Submit and Email quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 3: Click On No, do not validate button", priority = 3)
	public void Step03_ClickOnDoNotValidateButton()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");				
		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message");

	}

	/**
	 * Verify Send Quote Overlay details 
	 * 
	 * @throws Exception
	 */

	@Test(description = "Step 4: Verify Send Quote Overlay details ", priority = 4)
	public void Step04_VerifySendQuoteOverlayDetails() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		amQuoteDetailsPage.VerifySendQuoteOverlay(TextProp.getProperty("RequesterEmail"));

		log.info("Successfully verified Send Quote Overlay details \n");
		Reporter.log("<p>Successfully verified Send Quote Overlay details");

	}

	/**
	 * Provide message for reseller
	 * 
	 * @throws Exception
	 */

	@Test(description = "Step 5: Enter Message for Reseller", priority = 4)
	public void Step05_EnterMessageForReseller() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterMessage("Test message");

		log.info("Successfully entered message for Reseller \n");
		Reporter.log("<p>Successfully entered message for Reseller ");

	}

	/**
	 * Click on Send button
	 * 
	 * @throws Exception
	 */

	@Test(description = "Step 6: Click on Send button ", priority = 4)
	public void Step06_ClickOnSendButton() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnSendButton();

		log.info("Successfully Clicked on Send button \n");
		Reporter.log("<p>Successfully Clicked on Send button ");

	}


	/**
	 * Get Quote ID and Total Quote Price after Submitting the Quote
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 7: Get Quote ID and Total Quote Price", priority = 7)
	public void Step07_GetQuoteIDAndTotalPrice() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.getValues();

		log.info("Successfully fetched Quote ID and Quote total price\n");
		Reporter.log("<p>Successfully fetched Quote ID and Quote total price");

	}

	@Test(description = "Step 8: Click on Quotes tab", priority = 8)
	public void Step08_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);

		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");

	}

	@Test(description = "Step 9 : Search for the submitted quote", priority = 9)
	public void Step09_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);

		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}


	@Test(description = "Step 10 : Verify the total price", priority = 10)
	public void Step10_VerifyTotalPrice() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.verifyTotalQuotePrice(driver);

		log.info("Successfully verified the Quote total price\n");
		Reporter.log("<p>Successfully verified the Quote total price");
	}

}
