package in.valtech.westcon.test.am;


import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.ArrayList;
import java.util.List;

import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC46_Quoting_CopyThisQuote extends BaseTest {
	
    List<String> partNumbers= new ArrayList<String>();
	String totalQuotePrice;
	String endUserAccNo;
	String requesterEmail;
	String requesterName;
	String quoteCloseDate;
	String quoteExpDate;
	String quoteId_Before;
	
	/**
	 * User to Submit quote in quoting by executing TC-11
	 * 
	 */

	@Test(description = "Step 1: User to submit quote  by executing TC11", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");		
		log.info("TC-11 Successfully Executed \n");
		Reporter.log("<p>TC-11 Successfully Executed ");

		log.info("Quote details page is displayed with Requester Info, End user details, VRF fields  selected");
		Reporter.log("<p>Quote details page is displayed with Requester Info, End user details, VRF fields  selected");	
	}
	
	/**
	 * Save values like: Part Number, Total Quote price, End User, Requester Info, Quote Close Date values and VRF data
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Retrive values from Quote details page", priority =2)
	public void Step02_SaveQuoteValues()
			throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
		amQuoteDetailsPage.getValues();
		quoteId_Before=quoteID;		
				
		totalQuotePrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().trim();
        
        endUserAccNo=driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserAccountDetails_XPATH"))).getText().trim();	
        
        quoteExpDate=driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim();
        quoteCloseDate=driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim();
        
        List<WebElement> SKUs = driver.findElements(By.xpath(ObjRepoProp.getProperty("partNumberTableValue_XPATH")));
        for (int i=0;i<SKUs.size();i++){
        	
        	partNumbers.add(SKUs.get(i).getText().trim());
	            
		}        
        
        amQuoteDetailsPage.clickOnRequesterinfotab();
        requesterName=driver.findElement(By.id(ObjRepoProp.getProperty("requesterName_ID"))).getAttribute("value").trim();
        requesterEmail=driver.findElement(By.xpath(ObjRepoProp.getProperty("requesterEmail_XPATH"))).getAttribute("value").trim();
     
		log.info("Successfully saved Quote details:Part Number, Total Quote price, End User, Requester Info, Quote Close Date values and VRF\n");
		Reporter.log("<p>Successfully saved Quote details:Part Number, Total Quote price, End User, Requester Info, Quote Close Date values and VRF\n");

	}
	

	/**
	 * Select 'Copy this Quote' quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 3: Select 'Copy this Quote' quote from the dropdown and click on GO button", priority =3)
	public void Step03_SelectDropdownAndCopyThisQuoteFrom()
			throws Exception {
		GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndCopyThisQuoteFrom();
	
		Assert.assertTrue(GUIFunctions.isElementPresent(By.xpath(ObjRepoProp.getProperty("CopyQuoteOverlay_XPATH")), driver), "Copy Quote Overlay not displayed");
		
		log.info("Successfully Selected Copy this quote from the dropdown and clicked on GO button\n");
		Reporter.log("<p>Successfully Selected Copy this quote from the dropdown and click on GO button\n");

	}
	
	/**
	 * Select 'Copy this Quote' quote from the drop down and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Verify Copy Quote Overlay", priority =4)
	public void Step04_VerifyCopyQuoteOverlay() throws Exception {
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("CopyQuoteOverlay_XPATH"))));
		
		System.out.println("Copy Quote Overlay message:"+driver.findElement(By.xpath(ObjRepoProp
						.getProperty("CopyQuoteOverlayMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(By.xpath(ObjRepoProp.getProperty("CopyQuoteOverlayMsg_XPATH"))).getText().trim(), 
						TextProp.getProperty("CopyQuoteOverlayMsg"),"Copy Quote Overlay is not proper");			
		Assert.assertTrue(GUIFunctions.isElementPresent(By.xpath(ObjRepoProp.getProperty("CreateQuoteCopyBtn_XPATH")), driver), "Create Quote Copy Btn not displayed");
		Assert.assertTrue(GUIFunctions.isElementPresent(By.xpath(ObjRepoProp.getProperty("CopyQuoteCancelBtn_XPATH")), driver), "Copy Quote Cancel Btn not displayed");
		
		log.info("Successfully Verified Copy Quote Overlay details\n");
		Reporter.log("<p>Successfully Verified Copy Quote Overlay details\n");

	}
	
	/**
	 * Click on 'Create Quote copy' button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Click on 'Create Quote copy' button", priority = 5)
	public void Step05_ClickOnCreateQuoteCopyButton()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.createQuoteCopyButton();

		System.out.println("Quote copied success message:"+driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("CopiedQuoteSuccessMsg"),
				"Copy of the Quote is not proper");

		log.info("Successfully clicked on 'Create Quote copy' button and verified Success message\n");
		Reporter.log("<p>Successfully clicked on 'Create Quote copy' button and verified Success message\n");

	}
	
	/**
	 * Verify Quote details after copying the Quote
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6: Verify Quote details after copying the Quote", priority = 6)
	public void Step06_VerifyQuoteDetails_CopiedQuote() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		
		//Verify new Quote created with new Quote ID
		amQuoteDetailsPage.getValues();
	    System.out.println("Quote Id of copied Quote:"+BaseTest.quoteID);
		Assert.assertFalse(quoteId_Before.equals(BaseTest.quoteID), "Quote Id verification fails");
		
		//Verify End User 
		String actualEndUserAcc=driver.findElement(By.xpath(ObjRepoProp.getProperty("EndUserAccountDetails_XPATH"))).getText().trim();
		System.out.println("End User******"+actualEndUserAcc);
		Assert.assertTrue(actualEndUserAcc.contains(endUserAccNo), "End User Account mismatch");
		
		//Verify Quote Expiration Date
		System.out.println("Quote Expiration Date******"+driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim());
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim(),
				             quoteExpDate,"Quote Expiration Date mismatch");
		
		//Verify Quote Close Date
		System.out.println("Quote Close Date******"+driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteExpDate_XPATH"))).getAttribute("value").trim());
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCloseDate_XPATH"))).getAttribute("value").trim(),
				             quoteCloseDate,"Quote Close Date mismatch");
		
		//Verify Part Numbers added to Pricing grid copied from Quote
	     List<WebElement> actualSKU = driver.findElements(By.xpath(ObjRepoProp.getProperty("partNumberTableValue_XPATH")));
	        	        
	    	for (int i=0;i<actualSKU.size();i++){
				System.out.println(actualSKU.get(i).getText().trim());
		Assert.assertEquals(actualSKU.get(i).getText().trim(), partNumbers.get(i).trim(),
					    	"Partnumber mismatch");
			}
	    		   
		//Verify Total price	
       GUIFunctions.pageScrollDown(driver, 500);		
	   String actTotalPrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValueInForm_XPATH"))).getText().trim();
		System.out.println("Total Price******"+actTotalPrice);
		Assert.assertEquals(actTotalPrice,totalQuotePrice,
				"Quote Price mismatch");
		
       //Verify Requester Info
		amQuoteDetailsPage.clickOnRequesterinfotab();
		
        System.out.println("Requester Name******"+driver.findElement(By.id(ObjRepoProp.getProperty("requesterName_ID"))).getAttribute("value").trim());
        Assert.assertEquals(driver.findElement(By.id(ObjRepoProp.getProperty("requesterName_ID"))).getAttribute("value").trim(),
        		          requesterName,"Requester name mismatch");
        amQuoteDetailsPage.clickOnRequesterinfotab();
        System.out.println("Requester Email Id******"+driver.findElement(By.xpath(ObjRepoProp.getProperty("requesterEmail_XPATH"))).getAttribute("value").trim());
        Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("requesterEmail_XPATH"))).getAttribute("value").trim(),
		    	          requesterEmail,"Requester Email mismatch");
        
		log.info("Successfully Quote details of Copied Quote\n");
		Reporter.log("<p>Successfully Quote details of Copied Quote");

	}
	
}
