package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

public class ResellerReviewAndSubmitPage {
	private final WebDriver driver;
	GUIFunctions gui = new GUIFunctions();
	//For logging
	public Logger log = Logger.getLogger(this.getClass().getName());
	//RS end user and shipping page Constructor
	public ResellerReviewAndSubmitPage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Check that we're on the right RS end user and shipping  Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerReviewAndSubmitPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not reseller Checkout Address Page");
		}

	}
	By acceptTermsAndConditionsCheckBox_XPATH=By.xpath(ObjRepoProp.getProperty("acceptTermsAndConditionsCheckBox_XPATH"));
	By submitButtoncheckout_XPATH=By.xpath(ObjRepoProp.getProperty("submitButtoncheckout_XPATH"));
	public ResellerOrderConfirmationPage clickOnTermsAndSubmit() throws Exception {
		
		/*	int pixel=4000;
			//To scroll down 
			GUIFunctions.pageScrollDown(driver,pixel);*/
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("scroll(0, 250);");
			//Click on terms n cond checkbox
			GUIFunctions.clickElement(driver,acceptTermsAndConditionsCheckBox_XPATH, 	"acceptTermsAndConditionsCheckBox click");
			
			GUIFunctions.clickElement(driver,submitButtoncheckout_XPATH, 	"submitButtoncheckout click");

			Thread.sleep(3000);

			return new ResellerOrderConfirmationPage(driver);
		}

}
