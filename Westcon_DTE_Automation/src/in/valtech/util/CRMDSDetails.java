package in.valtech.util;

public class CRMDSDetails {

	private String dataVersion;
	private String opportunityName;
	private String stageName;
	private String opportunityOwner;

	/**
	 * @return the dataVersion
	 */
	public String getDataVersion() {
		return dataVersion;
	}

	/**
	 * @param dataVersion
	 *            the dataVersion to set
	 */
	public void setDataVersion(String dataVersion) {
		this.dataVersion = dataVersion;
	}

	/**
	 * @return the stageName
	 */
	public String getOpportunityName() {
		return opportunityName;
	}

	/**
	 * @param opportunityName
	 *            the opportunityName to set
	 */
	public void setOpportunityName(String opportunityName) {
		this.opportunityName = opportunityName;
	}

	/**
	 * @return the stageName
	 */
	public String getStageName() {
		return stageName;
	}

	/**
	 * @param stageName
	 *            the stageName to set
	 */
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	/**
	 * @return the opportunityOwner
	 */
	public String getOpportunityOwner() {
		return opportunityOwner;
	}

	/**
	 * @param opportunityOwner
	 *            the opportunityOwner to set
	 */
	public void setOpportunityOwner(String opportunityOwner) {
		this.opportunityOwner = opportunityOwner;
	}

}
