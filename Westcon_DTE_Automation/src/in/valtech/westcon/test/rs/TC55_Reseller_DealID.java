package in.valtech.westcon.test.rs;

import static in.valtech.custom.CustomFun.SKUdataSetList;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.util.SKU_DSDetails;
import in.valtech.westcon.ResellerPages.ResellerRequestAQuotePage;

public class TC55_Reseller_DealID extends BaseTest{

	//SKU_DSDetails SKU = new SKU_DSDetails();
	SKU_DSDetails SKU = new SKU_DSDetails();
	String SKUCount = null;

	@Test(description = "Step 1: Call TC51 TC52 ", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-51
		log.info("TC-51 Successfully Executed \n");
		Reporter.log("<p>TC-51 Successfully Eexecuted ");
		// Execute TC-52
		log.info("TC-52 Successfully Executed \n");
		Reporter.log("<p>TC-52 Successfully Eexecuted ");		
	}

	@Test(description = "Step 2: To click on vendor ref no tab", priority = 2)
	public void Step02_ClickOnVendorRefNoTab() throws Exception {
		ResellerRequestAQuotePage rsRequestAQuotePage=new ResellerRequestAQuotePage(driver);
		//To click on vendor ref no tab
		rsRequestAQuotePage.ClickVendorRefNoTab(driver);
		
		log.info("Successfully clicked on Vendor ref tab \n");
		Reporter.log("<p>Successfully clicked on Vendor ref tab");
	}

	@Test(description = "Step 3: To select vendor from drop down", priority = 3)
	public void Step03_SelectVendor() throws Exception 
	{
		ResellerRequestAQuotePage rsRequestAQuotePage=new ResellerRequestAQuotePage(driver);
		int SKUCountInt = SKUdataSetList.size();
		SKUCount = Integer.toString(SKUCountInt);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());
			String vendor=SKUdataSetList.get(i).getVendorName();
			rsRequestAQuotePage.SelectVendorFromDropDown(vendor);

			log.info("Successfully selected the Vendor \n");
			Reporter.log("<p>Successfully selected the Vendor");
		} 
	}

	@Test(description = "Step 4: To enter deal id", priority =4)
	public void Step04_EnterDealId() throws Exception 
	{
		int SKUCountInt = SKUdataSetList.size();

		SKUCount = Integer.toString(SKUCountInt);

		log.info("Total number of products" + SKUdataSetList.size());
		Reporter.log("Total number of products" + SKUdataSetList.size());

		// Loop to add multiple products
		for (int i = 0; i < SKUdataSetList.size(); i++) {

			log.info("SKU*************" + SKUdataSetList.get(i).getSKU());
			String vendorRefNo=SKUdataSetList.get(i).getVendorRefNo();
			//To enter deal id in to text box
			ResellerRequestAQuotePage rsRequestAQuotePage=new ResellerRequestAQuotePage(driver);
			rsRequestAQuotePage.EnterDealID(vendorRefNo);

			log.info("Successfully entered DealID \n");
			Reporter.log("<p>Successfully entered DealID");
		}
	}

	@Test(description = "Step 5: To click on submit button", priority =5)
	public void Step05_clickOnSubmit() throws Exception 

	{
		ResellerRequestAQuotePage rsRequestAQuotePage=new ResellerRequestAQuotePage(driver);
		rsRequestAQuotePage.VendorSubmitButton(driver);

		log.info("Successfully submitted the DealID and navigated to reseller quote details page \n");
		Reporter.log("<p>Successfully submitted the DealID and navigated to reseller quote details page");
	}
}