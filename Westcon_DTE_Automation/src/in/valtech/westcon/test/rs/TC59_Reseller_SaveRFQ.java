package in.valtech.westcon.test.rs;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.ResellerPages.ResellerEndUserandShippingPage;
import in.valtech.westcon.ResellerPages.ResellerRFQListingPage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;
import static in.valtech.util.PropertyFileReader.TextProp;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.custom.CustomFun.userInfoDSDetails;

public class TC59_Reseller_SaveRFQ extends BaseTest{

	static ResellerEndUserandShippingPage resellerEnduserAndShippingPage;
	static ResellerSelfServicePage resellerSelfServicePage;
	static ResellerRFQListingPage resellerRFQListingPage;
	static String RetriveID= null;
	static String RFQID= null;
	static String SearchRFQID=null;
	static String status=null;

	/**
	 * Call TC57
	 */

	@Test(description = "Step 1: Call TC51 ,TC52 ,TC53,TC56,TC57", priority = 1)
	public void Step01_NavigateToRFQShippingPage() throws Exception {

		// Execute TC51
		log.info("TC51 Successfully Executed \n");
		Reporter.log("<p>TC51 Successfully Eexecuted ");
		// Execute TC52
		log.info("TC52 Successfully Executed \n");
		Reporter.log("<p>TC52 Successfully Eexecuted ");
		// Execute TC53
		log.info("TC53 Successfully Executed \n");
		Reporter.log("<p>TC53 Successfully Eexecuted ");

		// Execute TC56
		log.info("TC56 Successfully Executed \n");
		Reporter.log("<p>TC56 Successfully Eexecuted ");
		// Execute TC57
		log.info("TC57 Successfully Executed \n");
		Reporter.log("<p>TC57 Successfully Eexecuted ");

		log.info(" Successfully navigated to RFQ shipping page \n");
		Reporter.log("<p>Successfully navigated to RFQ shipping pag ");

	}

	/**
	 * 
	 * Select shipping method and click on Save
	 */
	@Test(description = "Step 2: Select shipping method and click on Save", priority = 2)
	public void Step02_SelectShippingMethodAndSave() throws Exception {

		ResellerEndUserandShippingPage resellerEnduserAndShippingPage = new ResellerEndUserandShippingPage(driver);

		//select shipping method
		//resellerEnduserAndShippingPage.selectShippingMehtod(TextProp.getProperty("shippingMethodvalue"));

		resellerEnduserAndShippingPage.selectShippingMehtod(userInfoDSDetails.getShippingMtd());

		log.info(" Successfully selected shipping method \n");
		Reporter.log("<p>Successfully selected shipping method ");

		//click on save button
		resellerEnduserAndShippingPage.clickOnRFQSaveButton();

		log.info(" Successfully clicked on save button \n");
		Reporter.log("<p>Successfully clicked on save button ");

	}

	/**
	 * Retrive RFQ number and save
	 */
	@Test(description = "Step 3: Retrive RFQ number and save", priority = 3)
	public void Step03_RetriveRFQNumberAndSave() throws Exception {

		//Retrive RFQ saved
		RetriveID=driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerSaveConfirmationRFQIdText_XPATH"))).getText();

		System.out.println("The Retrived Id is-----------------------" +RetriveID);

		//Fetch only RFQ id from retrived text
		RFQID=RetriveID.replaceAll("[^0-9]", "");

		System.out.println("The RFQ ID is ------------------------"  +RFQID);

		log.info(" Successfully saved the retrived ID \n");
		Reporter.log("<p>Successfully saved the retrived ID ");

	}

	/**
	 * Click on OK in overlay
	 */
	@Test(description = "Step 4: Click on OK in overlay", priority = 4)
	public void Step04_ClickOKInOverlay() throws Exception {

		ResellerEndUserandShippingPage resellerEnduserAndShippingPage = new ResellerEndUserandShippingPage(driver);
		//click on ok button in the overlay
		resellerEnduserAndShippingPage.clickOnOKButtonInOverlay();

		log.info(" Successfully clicked on ok button \n");
		Reporter.log("<p>Successfully clicked on ok button ");
	}

	/***
	 * Click on My dashboard and click on enter button
	 */
	@Test(description = "Step 5: Click on My dashboard and click on enter button", priority = 5)
	public void Step05_NavigateToMyDashboard() throws Exception {

		//Click on My dashboard
		ResellerEndUserandShippingPage resellerEnduserAndShippingPage = new ResellerEndUserandShippingPage(driver);

		resellerSelfServicePage=resellerEnduserAndShippingPage.clickOnMydashboard();
		

		log.info(" Successfully navigated to resellerSelfServicePage\n");
		Reporter.log("<p>Successfully navigated to resellerSelfServicePage ");
	}

	/***
	 * Verify the RFQ saved in the Unsubmitted Quote requests section
	 */
	@Test(description = "Step 6: Verify the RFQ saved in the Unsubmitted Quote requests section", priority = 6)
	public void Step06_VerifyTheRFQSubmitted() throws Exception {

		ResellerSelfServicePage resellerSelfServicePage= new ResellerSelfServicePage(driver);
		Thread.sleep(5000);
		//click on un processed quotes link
		resellerRFQListingPage=resellerSelfServicePage.mouseHoverOnQuotesAndClickOnUnProcessedQuotesLink();

		log.info(" Successfully navigated to resellerRFQListingPage\n");
		Reporter.log("<p>Successfully navigated to resellerRFQListingPage ");

		//Enter RFQ in search 

		resellerRFQListingPage.searchForTheRFQSavedAndHitEnter(RFQID);

		//Verify the RFQ in the search result

		SearchRFQID=driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerRFQListingPageRFQIDInSearchTable_XPATH"))).getText();

		Assert.assertEquals(SearchRFQID,RFQID);

		log.info(" Successfully verified the RFQ in the search result\n");
		Reporter.log("<p>Successfully verified the RFQ in the search result ");

	}

	/***
	 * Verify the status of the RFQ
	 */

	@Test(description = "Step 7: Verify the status of the RFQ", priority = 7)
	public void Step07_VerifyTheRFQStatus() throws Exception {

		ResellerRFQListingPage resellerRFQListingPage= new ResellerRFQListingPage(driver);

		//click on "+" icon
		resellerRFQListingPage.clickOnPlusIcon();

		Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("resellerRFQListingTablecollapse_XPATH")), driver),
				"Table not collapsed");

		//Verify the status
		status=driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerRFQListingInProgressStatus_XPATH"))).getText();

		Assert.assertEquals(status, TextProp.getProperty("RFQStatus"));

		log.info(" Successfully verified the RFQ status\n");
		Reporter.log("<p>Successfully verified the RFQ status ");

	}

}
