package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import in.valtech.common.CommonFun;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuoteToOrderConfirmationPage;
import in.valtech.westcon.QuotingPages.QuoteToOrderPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.ResellerPages.ResellerLoginPage;
import in.valtech.westcon.ResellerPages.ResellerOrderListingPage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;
import in.valtech.westcon.ResellerPages.Reseller_OrderDetailPage;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC24_Quoting_ConvertQuoteToOrderByAM extends BaseTest {

	String PoNumber;
	static String username = null;
	static String password = null;
	/**
	 * User to Submit quote in quoting by executing TC-11
	 * 
	 */

	@Test(description = "Step 1: User to submit quote  by executing TC09", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		
		// Execute TC-08 (without Opportunity)
		log.info("TC-09 Successfully Executed \n");
		Reporter.log("<p>TC-09 Successfully Eexecuted ");	

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");	
	}

	/**
	 * Select Convert quote to Order from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Select Convert quote to Order from the dropdown and click on GO button", priority = 2)
	public void Step02_SelectConvertQuoteFromDropdown()
			throws Exception {
		//GUIFunctions.pageScrollUP(driver, 1000);
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectConvertQuoteOption();

		log.info("Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Selected Convert quote to Order option from the dropdown and click on GO button\n");
	}


	/*@Test(description = "Step 3:Click on Yes in Split order dialog ", priority = 3)
	public void Step03_ActionInSplitOrderDialog() throws Exception 
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		//amQuoteDetailsPage.yesButtonSplitOrderDialog();
		amQuoteDetailsPage.submitSplitOrderConfirmation();		
		log.info("Split Order dialog accepted successfully\n");
		Reporter.log("<p>Split Order dialog accepted successfully");
	}*/

	/**
	 * Function to enter PO number
	 */

	@Test(description = "Step 4:Enter PO number ", priority = 4)
	public void Step04_EnterPoNumber() throws Exception 
	{
		QuoteToOrderPage amConvertToOrder = new QuoteToOrderPage(driver);
		PoNumber=userInfoDSDetails.getPoNumber();
		amConvertToOrder.EnterPoNumber(PoNumber);
		log.info("PO number entered\n");
		Reporter.log("<p>PO number entered");

	}

	/**
	 * Browse file and Click on Submit button 
	 */
	@Test(description = "Step 5: Browse file ", priority = 5)
	public void Step05_BrowseFile() throws Exception {
		QuoteToOrderPage amConvertToOrder=new QuoteToOrderPage(driver);
		//Click on Browse button
		amConvertToOrder.ClickOnBrowseButton();
		//upload file 
		Thread.sleep(10000);

		log.info("Successfully uploaded a PO file");
		Reporter.log("<p>--Successfully uploaded a PO file");
	}	

	/**
	 *Click on submit
	 */
	@Test(description="step 6: Click on submit button",priority = 6)
	public void Step06_ClickSubmitButton()throws Exception
	{
		QuoteToOrderPage amConvertToOrder=new QuoteToOrderPage(driver);
		amConvertToOrder.ClickOnSubmit();
		log.info("SUbmit button clicked");
		Reporter.log("<p>--SUbmit button clicked");
	}

	/**
	 * To fetch sales order number
	 */
	String SalesOrdNumber;
	@Test(description="step 7: To fetch sales order number",priority = 7)
	public void Step07_salesOrderNumber()throws Exception
	{
		QuoteToOrderConfirmationPage amOrderConfirmation=new QuoteToOrderConfirmationPage(driver);
		//amOrderConfirmation.FetchOrderNumber();
		amOrderConfirmation.FetchOrderNumber();
		SalesOrdNumber = amOrderConfirmation.salesorderNumber;
		System.out.println("Sales order number : "+ SalesOrdNumber);
		Thread.sleep(5000);
		CommonFun.QuotingLogout(driver, "You have signed out of your account.");
		log.info("Sales order number displayed");
		Reporter.log("<p>--Sales order number displayed");
	}

	/**
	 * Access Reseller Portal
	 */
	@Test(description = "Step 8: Open browser,Navigate to the reseeler URL", priority = 8)
	public void Step08_NavigateToWestconReseller_URL() throws Exception {

		// Navigate/launch the Reseller URL	
		ResellerLoginPage.navigateTo_URL(driver, userInfoDSDetails.getRsUrl(), driverName);
		log.info("Navigated to the Reseller URL");
		Reporter.log("<p>Navigated to Reseller Login Page URL -- "
				+ driver.getTitle());
	}

	/**
	 * Enter user name , Password and click on Login link
	 * User will be landed in Self 
	 */
	@Test(description = "Step 9: Enter username , Pwd and click on Login link", priority = 9)
	public void Step09_ResellerLogin() throws Exception {

		username = userInfoDSDetails.getRsUserName();		
		password = userInfoDSDetails.getRsPassword();

		ResellerLoginPage rsLoginPage = new  ResellerLoginPage(driver);
		rsLoginPage.enterLoginCreds(username, password);
		log.info("Reseller successfully logged-in and navigated to SelfSerivice page\n");
		Reporter.log("<p>Reseller successfully logged-in and navigated to SelfSerivice page");
	}

	/**
	 * mouse hover on order history
	 */
	@Test(description="step 10: Mouse hover order history",priority = 10)

	public void Step10_MouseHoverOnOrderHistory()throws Exception
	{
		ResellerSelfServicePage rsSelfService=new ResellerSelfServicePage(driver);
		rsSelfService.mouseHoverOnOrderHistory();
		log.info("Mouse hovered on order history link");
		Reporter.log("<p>--Mouse hovered on order history link");
	}

	/**
	 * To select sales order number from drop down
	 */

	@Test(description="step 12: Select slaes order number from drop down  Enter sales order id and perform search",priority = 12)

	public void Step12_SelectSalesOrderNumberInDropdown()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);
		rsOrderlistingpage.searchCreatedOrder(SalesOrdNumber);

		log.info("Selected slaes order number from drop down");
		Reporter.log("<p>--Selected slaes order number from drop down");
	}

	/**
	 * To click on order id
	 */
	@Test(description="step 13: click on order id",priority = 13)


	public void Step13_ClickOnOrderNo()throws Exception
	{
		ResellerOrderListingPage rsOrderlistingpage=new ResellerOrderListingPage(driver);

		rsOrderlistingpage.ClickOrderId();
		log.info("Clicked on order no");
		Reporter.log("<p>--Clicked on order no");
	}

	//To Verify order details
	@Test(description="step 14: Verify Order details",priority = 14)

	public void Step14_VerifyOrderDetails()throws Exception
	{

		Reseller_OrderDetailPage rsOrderDetailPage=new Reseller_OrderDetailPage(driver);

		//Verifying Sales order number
		String salesOrderNoInConfirmation = rsOrderDetailPage.FetchSalesOrderNo();		
		Assert.assertEquals(salesOrderNoInConfirmation,SalesOrdNumber, "Sales order number not matching");

		log.info("Sales order number matching");
		Reporter.log("<p>--Sales order number matching");
		Thread.sleep(5000);

		//Verifying EndUser Name	
		String endUserName = rsOrderDetailPage.FetchEndUserName();		
		Assert.assertEquals(endUserName,userInfoDSDetails.getEndUser(), "End User name does not match");		
		log.info("End User name matching");
		Reporter.log("<p>--End User name matching");
		Thread.sleep(5000);

		GUIFunctions.pageScrollDown(driver, 300);

		//Verifying ExtendedPrice
		String extendedPrice = rsOrderDetailPage.FetchExtendedPrice();		
		Assert.assertEquals(extendedPrice,QuotingQuoteDetailpage.totalQuotePrice, "Extended Price does not match");	
		log.info("Extended Price matching");
		Reporter.log("<p>--Extended Price matching");
		Thread.sleep(5000);				
	}
}
