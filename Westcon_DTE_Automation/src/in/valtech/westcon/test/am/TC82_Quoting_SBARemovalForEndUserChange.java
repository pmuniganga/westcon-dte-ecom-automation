package in.valtech.westcon.test.am;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;

import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.custom.CustomFun.SKUdataSetList;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

public class TC82_Quoting_SBARemovalForEndUserChange extends BaseTest {

	String manufacturerListPriceBeforeSBA;
	String purchasePriceBeforeSBA;
	String vendorDiscountBeforeSBA;
	String SKU;
	String sbaRefNumber;
	String manufacturerListPriceAfterSBA;
	String purchasePriceAfterSBA;
	String vendorDiscountAfterSBA;
	String sbaDiscount;
	String[] sbaDiscountvalue;

	String[] skuNumber;
	String[] actualMListBeforeSBA;
	String[] actualVDiscountBeforeSBA;
	String[] actualWestCostBeforeSBA;

	String[] actualMListAfterSBA;
	String[] actualVDiscountAfterSBA;
	String[] actualWestCostAfterSBA;
	
	String[] actualMListAfterRemovingSBA;
	String[] actualVDiscountAfterRemovingSBA;
	String[] actualWestCostAfterRemovingSBA;
	
	String euserCompany;
	String euserAccNo;

	/**
	 * Call TC08 which selects the End User and and all mandatory fields filled
	 */
	@Test(description = "Step1 : Call TC06 which selects the End User and all mandatory fields filled", priority = 1)
	public void Step01_NavigateToTheCreateQuotePageWithAllMandatoryInfo() throws Exception {

		//Execute TC06 to navigate to Create Quote page with End User linked and all mandatory fields filled
		// Create Quote page is displayed with End user selected and all mandatory fields filled

		log.info("Create Quote page is displayed with End user selected and all mandatory fields filled");
		Reporter.log("<p>Create Quote page is displayed with End user selected and all mandatory fields filled-- "
				+ driver.getTitle());			
	}

	/**
	 * Click on Pricing tab and verify updated pricing conditions
	 * @throws InterruptedException 
	 */
	@Test(description = "Step2 : Click on Pricing tab and verify updated pricing conditions", priority = 2)
	public void Step02_ClickPricingTabAndVerifyPricingConditions() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");

		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingConditionColumn_XPATH"))));

		//Verify updated pricing conditions
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Manual");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto + Manual");
			}
		}
		log.info("Create Quote page with pricing condition as "+driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText());
		Reporter.log("<p>Create Quote page with pricing condition as -- "
				+ driver.findElement(By.xpath(ObjRepoProp
						.getProperty("pricingConditionColumn_XPATH"))).getText());
	}

	/**
	 * Save the values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step3 : Save the values like vendor discount, list price and purchase price", priority = 3)
	public void Step03_SaveTableValues() {
		//Save the table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostAfterSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("manufacturerListPriceBeforeSBA = "+actualMListAfterSBA[1]);
		System.out.println("vendorDiscountBeforeSBA = "+actualVDiscountAfterSBA[0]);
		System.out.println("purchasePriceBeforeSBA = "+actualWestCostAfterSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");
	}

	/**
	 * Click on SBA tab
	 */
	@Test(description = "Step4 : Click on SBA tab", priority = 4)
	public void Step04_ClickSBATab() {
		//Click on SBA tab
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		quoteDetails.clickSBATab();

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaTabSearchLink_XPATH"))).getText().trim(),
				TextProp.getProperty("SBATabData"));

		log.info("Successfully clicked on SBA tab and SBA tab is opened");
		Reporter.log("<p>Successfully clicked on SBA tab and SBA tab is opened");
	}
	
	/**
	 * Click on applied SBA and verify the overlay displayed
	 * @throws InterruptedException 
	 */
	@Test(description = "Step5 : Click on applied SBA and verify the overlay displayed", priority = 5)
	public void Step05_ClickAppliedSBA() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on applied SBA

		quoteDetails.clickAppliedManualSBA();
		log.info("Successfully clicked on applied SBA");
		Reporter.log("<p>Successfully clicked on applied SBA");

		Assert.assertEquals(driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsOverlay_XPATH"))).getText()
				, TextProp.getProperty("SBADetailsOverlay"));

		log.info("Successfully verified the SBA details ovarlay");
		Reporter.log("<p>Successfully verified the SBA details ovarlay");
	}
	
	/**
	 * Save the discount
	 */
	@Test(description = "Step6 : Save the SBA discount", priority = 6)
	public void Step06_SaveSBADiscount() {
		//Save SBA Discount
		sbaDiscount = driver.findElement
				(By.xpath(ObjRepoProp.getProperty("sbaDetailsoverlayDiscount_XPATH")))
				.getText();

		sbaDiscountvalue = sbaDiscount.split("-");
		//sbaDiscount = sbaDiscount.substring(1,sbaDiscount.length()-1);
		System.out.println("sbaDiscountvalue = "+sbaDiscountvalue[1]);

		log.info("Successfully saved SBA discount");
		Reporter.log("<p>Successfully saved SBA discount");
	}
	
	/**
	 * Click on Cancel button
	 */
	@Test(description = "Step7 : Click Cancel button", priority = 7)
	public void Step07_ClickCancelButton() {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		//Click on Cancel button
		quoteDetails.clickCancelButton();

		log.info("Successfully closed the overlay");
		Reporter.log("<p>Successfully closed the overlay");
	}
	
	@Test(description = "Step 8: Enter End User Account No. ", priority = 8)
	 public void Step08_EnterEndUserAccNo() throws Exception {
	  
	  // Direct Entering End User Account No. 
	  QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
	  euserAccNo=userInfoDSDetails.getReseller();
	        System.out.println("End User Account Number*****"+euserAccNo);

	  GUIFunctions.pageScrollDown(driver, 100);
	        amQuotePage.enterEndUserAcc_DirectInput(euserAccNo);
	  
	  log.info("Successfully entered End User Account No.");
	  Reporter.log("<p>Successfully entered End User Account No.");
	 }
	 
	 /**
	  * Click on Retrieve Account button
	     * 
	    */
	 @Test(description = "Step 9: Click on Retrieve Account button", priority = 9)
	 public void Step09_ClickOnRetrieveAccount() throws Exception {
	  
	  //Clicking on Retrieve Account button
	  QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
	  amQuotePage.ClickOnEndUsrRetrivAccBtn(driver);

	  log.info("Successfully Clicking on Retrive Account button");
	  Reporter.log("<p>Successfully Clicking on Retrive Account button");
	 }
	
	/**
	 * Click on Pricing tab and verify updated pricing conditions
	 * @throws InterruptedException 
	 */
	@Test(description = "Step10 : Click on Pricing tab and verify updated pricing conditions", priority = 10)
	public void Step10_ClickPricingTabAndVerifyPricingConditions() throws InterruptedException {
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);
		
		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("sbaTab_XPATH"))));
		
		//Click on Pricing tab
		quoteDetails.clickPricingTab();

		log.info("Successfully Clicked on Pricing tab");
		Reporter.log("<p>Successfully Clicked on Pricing tab");

		((JavascriptExecutor)
				driver).executeScript("arguments[0].scrollIntoView(true);", 
						driver.findElement(By.xpath(ObjRepoProp
								.getProperty("pricingConditionColumnNone_XPATH"))));

		//Verify updated pricing conditions
		for(int i = 0 ; i < SKUdataSetList.size() ; i++) {
			System.out.println("SKUdataSetList.get(i).getSbaFlag() = "+SKUdataSetList.get(i).getSbaFlag());

			if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL") && SKUdataSetList.get(i).getSbaType().equals("ZSD0")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto");
			}
			
			else if(SKUdataSetList.get(i).getSbaFlag().equals("MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText()
						, "None");
			}

			else if(SKUdataSetList.get(i).getSbaFlag().equals("AUTO+MANUAL")) {
				Assert.assertEquals(driver.findElement
						(By.xpath(ObjRepoProp.getProperty("pricingConditionColumn_XPATH"))).getText()
						, "Auto","Improper pricing condition");
			}

		}
		log.info("Create Quote page with pricing condition as "+driver.findElement(By.xpath(ObjRepoProp.getProperty("pricingConditionColumnNone_XPATH"))).getText());
		Reporter.log("<p>Create Quote page with pricing condition as -- "
				+ driver.findElement(By.xpath(ObjRepoProp
						.getProperty("pricingConditionColumnNone_XPATH"))).getText());
	}
	
	/**
	 * Save the values like vendor discount, list price and purchase price
	 */
	@Test(description = "Step11 : Save the values like vendor discount, list price and purchase price", priority = 11)
	public void Step11_SaveTableValues() {
		//Save the table values like vendor discount, list price and purchase price

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualMListAfterRemovingSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][1]")).getText().split(" ");
				actualVDiscountAfterRemovingSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][2]")).getText().split("%");
				actualWestCostAfterRemovingSBA = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
			}
		}

		System.out.println("actualMListAfterRemovingSBA = "+actualMListAfterRemovingSBA[1]);
		System.out.println("actualVDiscountAfterRemovingSBA = "+actualVDiscountAfterRemovingSBA[0]);
		System.out.println("actualWestCostAfterRemovingSBA = "+actualWestCostAfterRemovingSBA[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");
	}
	
	/**
	 * Compare vendor discount, list price and purchase price values before and after applying SBA
	 */
	@Test(description = "Step12 : Compare vendor discount, list price and purchase price values before and after applying SBA", priority = 12)
	public void Step12_CompareTableValues() {	

		//Compare the values before and after applying SBA
		Assert.assertEquals(Float.parseFloat(actualMListAfterRemovingSBA[1].replace(",", "")), Float.parseFloat(actualMListAfterSBA[1].replace(",", "")));
		Assert.assertEquals(Float.parseFloat(actualVDiscountAfterRemovingSBA[0]), Float.parseFloat(actualVDiscountAfterSBA[0]) - Float.parseFloat(sbaDiscountvalue[1]));
		Assert.assertEquals(Float.parseFloat(actualWestCostAfterRemovingSBA[1].replace(",", "")), Float.parseFloat(actualMListAfterSBA[1].replace(",", ""))-((Float.parseFloat(actualMListAfterSBA[1].replace(",", "")) * Float.parseFloat(actualVDiscountAfterRemovingSBA[0]))/100));

		log.info("Successfully compared the table values before and after applying SBA");
		Reporter.log("<p>Successfully compared the table values before and after applying SBA");
	}
	
}
