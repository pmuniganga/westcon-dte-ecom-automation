
package in.valtech.uiFunctions;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import static in.valtech.config.BaseTest.driverName;

public class GUIFunctions {

	public static Logger log = Logger.getLogger(GUIFunctions.class.getName());

	/**
	 * Method Name: isElementPresent Description:Method to verify the Element
	 * 
	 * @param by
	 *            :Element locator
	 * @param driver
	 *            :WebDriver
	 * @return true: if element is present, false: if element is not present
	 */
	public static boolean isElementPresent(By by, WebDriver driver) {
		try {
			// Verify element is present
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			log.error("No such element present--> \n" + e);
			Reporter.log("<p>No such element present");

			return false;
		}
	}

	/**
	 * Method Name: navigateBack Description: Method to used to go back to home
	 * page by clicking on browser back button
	 * 
	 * @param driver
	 *            :WebDriver
	 * @return driver
	 */
	public static WebDriver navigateBack(WebDriver driver) {
		try {

			if (driverName.equalsIgnoreCase("SAFARI")) {
				// Navigate back to Previous page
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("history.go(-1)");

			} else {
				// Navigate back to Previous page
				driver.navigate().back();
			}

		} catch (Exception e) {
			log.error("Navigation to previous page failed--> \n" + e);
			Reporter.log("<p>Navigation to previous page failed");
		}

		return (driver);
	}

	/**
	 * Method Name: clickElement Description: This method clicks on WebElement
	 * specified
	 * 
	 * @param driver
	 *            : WebDriver
	 * @param ele
	 *            : WebElement locator
	 * @param eleName
	 *            : Name of the element to be clicked
	 */
	public static void clickElement(WebDriver driver, By by, String eleName) {
		try {

			/*WebElement element = driver.findElement(by);
			// Click on element
			element.click();*/

			WebElement element = driver.findElement(by);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);

			log.info("Successfully clicked on element: " + eleName);
			Reporter.log("<p>Successfully clicked on element: " + eleName);

		} catch (NoSuchElementException e) {
			log.error("Element to click is not present " + e);
			Reporter.log("<p>Element to click is not present");
			throw new NoSuchElementException("Element to click is not present");
		}
	}

	/**
	 * Method Name: mouseOverElement Description: This method used to scroll to
	 * an element which not visible in the screen
	 * 
	 * @param driver
	 *            : WebDriver
	 * @param ele
	 *            : WebElement locator
	 * */
	public static void mouseOverElement(WebDriver driver, WebElement ele)
			throws Exception {
		try {

			if (!driverName.equalsIgnoreCase("SAFARI")) {
				// Mouse hover/roll over on element
				Thread.sleep(5000);
				Actions builder = new Actions(driver);
				builder.moveToElement(ele).build().perform();
			}
		} catch (NoSuchElementException e) {
			log.error("Element to mouse over is not present " + e);
			Reporter.log("<p>Element to mouse over is not present");
			throw new NoSuchElementException("Element to click is not present");
		}

	}

	/**
	 * This Function selects the radio button iFrame.
	 * 
	 * @param driver
	 *            : WebDriver instance.
	 */

	public static void selectRaidoButton(WebDriver driver, By by, String eleName) {
		// Initialize WebElement
		WebElement ele = driver.findElement(by);

		try {
			if (!ele.isSelected()) {
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", ele);
				//ele.click();
			}
			log.info("Successfully selected the raido button '" + eleName + "'");
		}

		catch (NoSuchElementException e) {

			log.error("Exception element not present : " + eleName);
			throw new NoSuchElementException("Exception element not present : " + eleName);
		}

	}

	/**
	 * Method Name: mouseOverElementAndClick Description: This method used to
	 * scroll to an element which not visible in the screen/ or Not Clickable by
	 * GUIFunctions.clickOnElement method and clicks on it
	 * 
	 * @param driver
	 * @param ele
	 * @param eleName
	 * @throws Exception
	 */
	public static void mouseOverElementAndClick(WebDriver driver,
			WebElement ele, String eleName) throws Exception {
		try {

			if (!driverName.equalsIgnoreCase("SAFARI")) {
				// Mouse hover/roll over on element
				Thread.sleep(5000);
				Actions builder = new Actions(driver);
				builder.moveToElement(ele).build().perform();
				builder.click().perform();

				log.info("Successfully mouse over and clicked on: " + eleName);
				Reporter.log("<p>Successfully mouse over and clicked on: "
						+ eleName);

			}
		} catch (NoSuchElementException e) {
			log.error("Element to mouse over and click is not present " + e);
			Reporter.log("<p>Element to mouse over and click is not present");
			throw new NoSuchElementException("Element to mouse over and click is not present");
		}

	}

	/**
	 * Method Name: clickElement Description: This method clicks on WebElement
	 * specified
	 * 
	 * @param driver
	 *            : WebDriver
	 * @param ele
	 *            : WebElement locator
	 * @param eleName
	 *            : Name of the element to be clicked
	 */
	public static void typeTxtboxValue(WebDriver driver, By by, String value) {
		try {

			// Click on element
			//driver.findElement(by).clear();
			driver.findElement(by).sendKeys(value);
		} catch (NoSuchElementException e) {
			log.error("Element to type data is not present " + e);
			Reporter.log("<p>Element to type data is not present");
			throw new NoSuchElementException("Element Not Found to Enter the data");
		}
	}

	/**
	 * 
	 * Method Name: keyPressPageUp Description: This method Press on Page up Key
	 * Once
	 * 
	 * @param driver
	 *            : WebDriver
	 * @throws Exception
	 * 
	 */
	public static void keyPressPageUp(WebDriver driver) throws Exception {

		if (!driverName.equalsIgnoreCase("SAFARI")) {
			Thread.sleep(2500);
			Actions action = new Actions(driver);
			action.sendKeys(Keys.PAGE_UP).build().perform();
		}
	}

	/**
	 * Method Name: selectDropDownValue_new Description: This method selects
	 * dropdown value
	 * 
	 * @param ele
	 *            : DropDown webelement
	 * @param dropDownValue
	 *            : dropDown Value to be selected
	 * */
	public static void selectDropDownValue(WebElement ele,
			String dropDownValue, String optType) throws Exception {
		try {
			// Dropdown in which value to be selected
			Select dropDown = new Select(ele);

			// Selecting dropdown value
			if (optType.equalsIgnoreCase("text")) {
				dropDown.selectByVisibleText(dropDownValue.trim());
			} else if (optType.equalsIgnoreCase("value")) {
				System.out.println("dropDownValue = "+dropDownValue);
				dropDown.selectByValue(dropDownValue);
			} else if (optType.equalsIgnoreCase("index")) {
				int dropDownValueInt = Integer.parseInt(dropDownValue);
				dropDown.selectByIndex(dropDownValueInt);
			}
		} catch (NoSuchElementException e) {
			log.error("Element to select drop down is not present " + e);
			Reporter.log("<p>Element to drop down is not present");
			throw new NoSuchElementException("Element Not Found to select the value");
		}
	}	

	/**
	 * Method Name: checkOrUncheckCheckBox Description: This method
	 * checks/Unchecks the checkbox
	 * 
	 * @param driver
	 *            : WebDriver
	 * @param By
	 *            : WebElement locator
	 * @param eleName
	 *            : Name of the element to be checked
	 * @param toCheck
	 *            : True: To check the checkbox false: To uncheck the checkbox
	 * 
	 * */
	public static void checkOrUncheckCheckBox(WebDriver driver, By by,
			String eleName, Boolean toCheck) throws Exception {
		try {

			if (toCheck) {
				// check checkbox if not checked
				if (!driver.findElement(by).isSelected()) {
					//driver.findElement(by).click();

					WebElement ele =driver.findElement(by);
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", ele);

					log.info("Successfully checked the checkbox: " + eleName);
					Reporter.log("<p>Successfully checked the checkbox: "
							+ eleName);
				}
			} else if (!toCheck) {
				// Uncheck checkbox if it is checked
				if (driver.findElement(by).isSelected()) {
					//driver.findElement(by).click();
					WebElement ele =driver.findElement(by);
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", ele);
					
					log.info("Successfully unchecked the checkbox: " + eleName);
					Reporter.log("<p>Successfully unchecked the checkbox: "
							+ eleName);
				}
			}

		} catch (NoSuchElementException e) {
			log.error("Checkbox element is not present " + e);
			Reporter.log("<p>Checkbox element is not present ");
			throw new NoSuchElementException("Checkbox Element Not Found");
		}

	}

	/**
	 * 
	 * Method Name: pageScrollDown .Description: This method scrolls down till required pixels
	 * 
	 * 
	 * @param driver
	 *            : WebDriver
	 * @throws Exception
	 * 
	 */
	public static void pageScrollDown(WebDriver driver,int reqYaxisPixelsScroll) throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,"+reqYaxisPixelsScroll+")", "");
	}

	/**
	 * 
	 * Method Name: pageScrollUP .Description: This method scrolls up till required pixels
	 * 
	 * @param driver
	 *            : WebDriver
	 * @throws Exception
	 * 
	 */
	public static void pageScrollUP(WebDriver driver,int reqXaxisPixelsScroll) throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scroll("+reqXaxisPixelsScroll+",0)", "");
	}

	/**
	 * 
	 * Method Name: keyPressEnter Description: This method Press on 'Enter' Key Once
	 * 
	 * 
	 * @param driver
	 *            : WebDriver
	 * @throws Exception
	 * 
	 */
	public static void keyPressEnter(WebDriver driver) throws Exception {

		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();	
	}

}

