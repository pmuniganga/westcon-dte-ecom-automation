package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC111_Quoting_SIGMAError extends BaseTest {

	/**
	 * Call TC03
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		
		log.info("Create Quote page is displayed\n");
		Reporter.log("<p>Create Quote page is displayed");
	}
	
	@Test(description = "Step 2 : Unselect Pipeline checkbox", priority = 2)
	public void Step02_UncheckPipelineCheckbox() throws Exception {
		//Unselect Pipeline checkbox
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		if(locale.equalsIgnoreCase("UK")) {
			amQuoteDetailsPage.UncheckPipeLineCheckbox(driver);
		}

		log.info("Successfully unselected pipeline checkbox\n");
		Reporter.log("<p>Successfully unselected pipeline checkbox");
	}
	
	@Test(description = "Step 3: Click on Save button", priority = 3)
	public void Step03_SaveQuote() {
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.saveQuote();
		
		log.info("Successfully saved the Quote\n");
		Reporter.log("<p>Successfully saved the Quote");
	}
	
	@Test(description = "Step 4: Verify Success Msg in the overlay", priority = 4)
	public void Step04_VerifySuccessMsgAndCloseOverlay() {
		By saveQuoteOverlayMsg = By.xpath(ObjRepoProp.getProperty("saveQuoteOverlayMsg_XPATH"));
		CustomFun.waitObjectToLoad(driver, saveQuoteOverlayMsg, 60);
		Assert.assertEquals(driver.findElement(saveQuoteOverlayMsg).getText().replaceAll("\n", " "), TextProp.getProperty("saveQuoteMsg"));
		
		QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.closeSaveOverlay();
		
		log.info("Successfully verified the message and closed the overlay\n");
		Reporter.log("<p>Successfully verified the message and closed the overlay");
	}
	
	/**
	 * Select Submit to SIGMA from the dropdown and click on GO button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Select Submit quote from the dropdown and click on GO button", priority = 5)
	public void Step05_SelectDropdownAndSubmitToSIGMA() throws Exception {	
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitSIGMA();

		log.info("Successfully Select Submit to SIGMA from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit to SIGMA from the dropdown and click on GO button");

	}
	
	@Test(description = "Step 6 : Verify SIGMA failure message", priority = 6)
	public void Step06_VerifySIGMAFailureMsg() {
		By sigmaErrorMsg = By.xpath(ObjRepoProp.getProperty("sigmaErrorMsg_XPATH"));
		CustomFun.waitObjectToLoad(driver, sigmaErrorMsg, 60);
		Assert.assertTrue(driver.findElement(sigmaErrorMsg).getText().contains(TextProp.getProperty("SIGMAFailureMsg")));
		
		log.info("Successfully verified SIGMA Failure Message\n");
		Reporter.log("<p>Successfully verified SIGMA Failure Message");
	}
}
