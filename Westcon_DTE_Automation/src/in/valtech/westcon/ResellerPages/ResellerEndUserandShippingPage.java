package in.valtech.westcon.ResellerPages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Set;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.Key;

public class ResellerEndUserandShippingPage 
{
	private final WebDriver driver;
	GUIFunctions gui = new GUIFunctions();		
	public String rootDir = CustomFun.getRootDir();
	//For logging
	public Logger log = Logger.getLogger(this.getClass().getName());
	//RS end user and shipping page Constructor
	public ResellerEndUserandShippingPage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Check that we're on the right RS end user and shipping  Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerEndUserandShipping_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not end user and shipping  page");
		}
	}


	static By createNewButton=By.xpath(ObjRepoProp.getProperty("createNewButton_XPATH"));
	static By createNewEnduserTextbox=By.xpath(ObjRepoProp.getProperty("createNewEnduserTextbox_XPATH"));
	static By enterNewshippingAddressRadioButton=By.xpath(ObjRepoProp.getProperty("enterNewshippingAddressRadioButton_XPATH"));
	static By attnTextBox=By.xpath(ObjRepoProp.getProperty("attnTextBox_XPATH"));
	static By companyTextBox=By.xpath(ObjRepoProp.getProperty("companyTextBox_XPATH"));
	static By countryDropDown=By.xpath(ObjRepoProp.getProperty("countryDropDown_XPATH"));
	static By addressTextBox=By.xpath(ObjRepoProp.getProperty("addressTextBox_XPATH"));
	static By postalcodeTextBox=By.xpath(ObjRepoProp.getProperty("postalcodeTextBox_XPATH"));
	static By PhonenumberTextBox=By.xpath(ObjRepoProp.getProperty("Phonenumber_XPATH"));
	static By postalCodeLabel=By.xpath(ObjRepoProp.getProperty("postalCodeLabel_XPATH"));
	By resellerRFQSaveButton_XPATH=By.xpath(ObjRepoProp.getProperty("resellerRFQSaveButton_XPATH"));
	By resellerSaveConfirmationOKButton_XPATH=By.xpath(ObjRepoProp.getProperty("resellerSaveConfirmationOKButton_XPATH"));
	By resellerMyDashboard_XPATH=By.xpath(ObjRepoProp.getProperty("resellerMyDashboard_XPATH"));
	By resellerRFQSubmitButton_XPATH=By.xpath(ObjRepoProp.getProperty("resellerRFQSubmitButton_XPATH"));
	By resellerRFQSubmitConfirmationOKButton_XPATH=By.xpath(ObjRepoProp.getProperty("resellerRFQSubmitConfirmationOKButton_XPATH"));


	/**
	 * //function to Click on End User - Create New radio button
	 * 
	 * @throws Exception
	 */
	public ResellerEndUserandShippingPage selectcreateNewEnduserRadioButton() throws Exception
	{

		//To select create new end user radio button
		GUIFunctions.selectRaidoButton(driver, createNewButton, "Create new end user  radio button selected");
		System.out.println("Create new end user  radio button selected");
		return new ResellerEndUserandShippingPage(driver);
	}

	/**
	 * //function to enter new end user n to text box
	 * 
	 * @throws Exception
	 */
	public ResellerEndUserandShippingPage enterNewEndUser(String enduser) throws Exception
	{
		// to enter new end user in to text box
		GUIFunctions.typeTxtboxValue(driver, createNewEnduserTextbox,enduser);
		System.out.println("End user entered");
		return new ResellerEndUserandShippingPage(driver);
	}

	/**
	 * This function is to select enter new address radio button
	 */
	public ResellerEndUserandShippingPage EnterNewAddressRadioButton(String Attn,String Company,String country,String Address,String Postalcode,String Phonenumber) throws Exception
	{
		CustomFun.waitObjectToLoad(driver, enterNewshippingAddressRadioButton, 90);
		int pixel=2000;
		// To select enter new address radio button
		GUIFunctions.selectRaidoButton(driver, enterNewshippingAddressRadioButton, "Enter New shipping Address RadioButton selected");
		//To scroll down 
		GUIFunctions.pageScrollDown(driver,pixel);
		//To enter input for Attn
		GUIFunctions.typeTxtboxValue(driver, attnTextBox,Attn);
		//To enter value for company field
		GUIFunctions.typeTxtboxValue(driver, companyTextBox,Company);


		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(countryDropDown));
		//To select country
		GUIFunctions.selectDropDownValue(driver.findElement(countryDropDown), country, "value");
		//To enter address
		GUIFunctions.typeTxtboxValue(driver,addressTextBox,Address);
		//To enter postal code
		GUIFunctions.typeTxtboxValue(driver,postalcodeTextBox,Postalcode);
		//Click outside postal code text box to auto generate state and city
		GUIFunctions.clickElement(driver, postalCodeLabel,"postal code label clicked");
		//To enter phone number
		GUIFunctions.typeTxtboxValue(driver,PhonenumberTextBox,Phonenumber);
		return new ResellerEndUserandShippingPage(driver);

	}
	/**
	 * This function will select shipping method
	 */

	public ResellerEndUserandShippingPage selectShippingMehtod(String shippinMethod) throws Exception
	{
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.id(ObjRepoProp.getProperty("resellerShippinMethod_ID"))));

		GUIFunctions.selectDropDownValue(driver.findElement(By.id(ObjRepoProp
				.getProperty("resellerShippinMethod_ID"))), shippinMethod, "value");
		return new ResellerEndUserandShippingPage(driver);
	}

	/**
	 * This method will click on save RFQ button
	 */


	public ResellerEndUserandShippingPage clickOnRFQSaveButton() throws Exception
	{

		GUIFunctions.clickElement(driver, resellerRFQSaveButton_XPATH, "RFQ save button");
		return new ResellerEndUserandShippingPage(driver);
	}

	/**
	 * This function will click on OK button in the confirmation overlay after Save
	 */

	public ResellerEndUserandShippingPage clickOnOKButtonInOverlay() throws Exception
	{

		GUIFunctions.clickElement(driver,resellerSaveConfirmationOKButton_XPATH , "OK button");
		//		
		//		Robot robot = new Robot();
		//		robot.keyPress(KeyEvent.VK_ESCAPE);
		//		robot.keyRelease(KeyEvent.VK_ESCAPE);

		return new ResellerEndUserandShippingPage(driver);
	}

	/***
	 * This function will click on my dash board
	 */

	public ResellerSelfServicePage clickOnMydashboard() throws Exception
	{

		GUIFunctions.clickElement(driver, resellerMyDashboard_XPATH , "OK My dashborad");
		Thread.sleep(3000);
				
		if (BaseTest.driverName.equalsIgnoreCase("CHROME"))
		{
			System.out.println("checking driver name - chrome");
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			System.out.println("Alert data: " + alertText);
			alert.accept();
			CustomFun.waitForPageLoaded(driver);
		}else if (BaseTest.driverName.equalsIgnoreCase("IE"))
		{
			System.out.println("Checking driver name - IE");
			Thread.sleep(5000);
			System.setProperty("webdriver.ie.driver", rootDir + "/resources/autoit/IE_LeavePage.exe");
			System.out.println(rootDir);
			try{
			Runtime.getRuntime()
			.exec("D:\\Westcon_Auto_Workspace\\Westcon DTE Automation\\resources\\autoit\\IE_LeavePage.exe");
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return new ResellerSelfServicePage(driver);

	}

	/**
	 * This method will click on submit RFQ button
	 */

	public ResellerEndUserandShippingPage clickOnRFQSubmitButton() throws Exception
	{

		GUIFunctions.clickElement(driver,resellerRFQSubmitButton_XPATH, "RFQ save button");
		return new ResellerEndUserandShippingPage(driver);
	}

	/**
	 * This function will clcik on OK button after submiting the RFQ
	 */
	public ResellerSelfServicePage clickOnOKButtonInSubmitConfirmationOverlay() throws Exception
	{
		GUIFunctions.clickElement(driver,resellerRFQSubmitConfirmationOKButton_XPATH, "OK button");
		return new ResellerSelfServicePage(driver);
	}


	/**
	 * This function will retrive the RFQ Id
	 */
	public ResellerEndUserandShippingPage getRFQId()
	{
		//Retrive RFQ saved
		BaseTest.RetriveID=driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerRFQSubmitConfirmationOverlayRFQIDText_XPATH"))).getText();
		System.out.println("The Retrived Id is-----------------------" +BaseTest.RetriveID);
		//Fetch only RFQ id from retrived text
		BaseTest.RFQID=BaseTest.RetriveID.replaceAll("[^0-9]","");
		System.out.println("The RFQ ID is ------------------------"  +BaseTest.RFQID);
		return new ResellerEndUserandShippingPage(driver);
	}

}


