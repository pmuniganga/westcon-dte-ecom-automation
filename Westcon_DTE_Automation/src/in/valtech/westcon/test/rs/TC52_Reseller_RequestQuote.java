package in.valtech.westcon.test.rs;

import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.westcon.ResellerPages.ResellerRequestAQuotePage;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;

public class TC52_Reseller_RequestQuote extends BaseTest {
		
	/**
	 * Login to Reseller web site by calling TC51
	 * 
	 */
	@Test(description = "Step 1: Login to Reseller website by calling TC51", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-51
		log.info("TC-51 Successfully Executed \n");
		Reporter.log("<p>TC-51 Successfully Eexecuted ");
	}
	
	@Test(description = "Step 2: Mouse hover on Quotes from Menu and Click on Request a new Quote", priority = 2)
	public void Step02_clickRequestNewQuoteLnk() throws Exception 
	{
		ResellerSelfServicePage rsSelfServicePage=new ResellerSelfServicePage(driver);
		Thread.sleep(3000);
		rsSelfServicePage.mouseHoverOnQuotesAndClickOnRequestNewQuoteLink();
		log.info("Reseller Create Quote page is displayed \n");
		Reporter.log("<p>Reseller Create Quote page is displayed");
	}
	
	@Test(description = "Step3: To verify Copy and paste bom is selected by default", priority = 3)
	public void Step03_verifyDefaultTabSelected() throws Exception 
	{
		ResellerRequestAQuotePage rsRequestAQuotePage=new ResellerRequestAQuotePage(driver);
		rsRequestAQuotePage.ToVerifyCopyAndPasteSelecteByDefault();
		Thread.sleep(5000);
		System.out.println("Copy & Paste BOM tab is selected by default");
		log.info("Copy & Paste BOM tab is selected by default \n");
		Reporter.log("<p> Copy & Paste BOM tab is selected by default ");
	}	
	}

