package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import org.openqa.selenium.support.ui.Select;

public class ResellerCheckoutAddressPage {
	
	private final WebDriver driver;
	GUIFunctions gui = new GUIFunctions();
	//For logging
	public Logger log = Logger.getLogger(this.getClass().getName());
	//RS end user and shipping page Constructor
	public ResellerCheckoutAddressPage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Check that we're on the right RS end user and shipping  Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerCheckoutAddressPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not reseller Checkout Address Page");
		}

	}

	By enterNewAddrssRadioButton_XPATH=By.xpath(ObjRepoProp.getProperty("enterNewAddrssRadioButton_XPATH"));
	By firstNameTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("firstNameTextBox_XPATH"));
	By companyNameTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("companyNameTextBox_XPATH"));
	By CountryDropDown_XPATH=By.xpath(ObjRepoProp.getProperty("CountryDropDown_XPATH"));
	By shippingAdress1TextBox_XPATH = By.xpath(ObjRepoProp.getProperty("shippingAdress1TextBox_XPATH"));
	By cityTextBox_XPATH = By.xpath(ObjRepoProp.getProperty("cityTextBox_XPATH"));
	By postalCodeTextBox_XPATH = By.xpath(ObjRepoProp.getProperty("postalCodeTextBox_XPATH"));
	By postalCodeLabl_XPATH = By.xpath(ObjRepoProp.getProperty("postalCodeLabl_XPATH"));
	By phNumTextBox_XPATH = By.xpath(ObjRepoProp.getProperty("phNumTextBox_XPATH"));
	By poNumberTextBox_XPATH = By.xpath(ObjRepoProp.getProperty("poNumberTextBox_XPATH"));
	By shippingMethodRSCheckout_XPATH = By.xpath(ObjRepoProp.getProperty("shippingMethodRSCheckout_XPATH"));
	By carrierAccount_ID=By.id(ObjRepoProp.getProperty("carrierAccuontNumber_ID"));
	By blindshipment_XPATH=By.xpath(ObjRepoProp.getProperty("blindShipmentRSCheckout_XPATH"));
	By shippingInstruction_ID=By.id(ObjRepoProp.getProperty("shippingInstruction_ID"));
	By saveShippingInstruction_XPATH=By.xpath(ObjRepoProp.getProperty("saveShippingInstruction_XPATH"));
	By RequiredShipDate_ID=By.id(ObjRepoProp.getProperty("RequiredShipDate_ID"));
	By ShippingFormSaveButton_XPATH=By.xpath(ObjRepoProp.getProperty("shippingFormSaveButtonRS_XPATH"));
	By nextButtonaddressPage_XPATH=By.xpath(ObjRepoProp.getProperty("nextButtonaddressPage_XPATH"));
	By shippinginstructionTextArea_XPATH=By.xpath(ObjRepoProp.getProperty("shippinginstructionTextArea_XPATH"));

	public ResellerCheckoutAddressPage clickEnterNewAddressVerifyFields() throws Exception
	{
		//Wait for enter New Address Radio Button to display after navigating to ResellerCheckoutAddressPage
		CustomFun.waitObjectToLoad(driver, enterNewAddrssRadioButton_XPATH, 60);
		//click on enter New Address Radio Button
		GUIFunctions.selectRaidoButton(driver, enterNewAddrssRadioButton_XPATH
				, "Click on enter New Addrss Radio Button");
		// verify attn company name shipping dropdown etc are enabled/disable
		Boolean FirstNameTextBox = CustomFun.isElementEnable(driver
				, firstNameTextBox_XPATH, "FirstNAme Text box ");
		System.out.print("\nAfter : First Name Text box enabled status is : "+FirstNameTextBox);
		Boolean companyNameTextBox = CustomFun.isElementEnable(driver
				, companyNameTextBox_XPATH, "FirstNAme Text box ");
		System.out.print("\nAfter : First Name Text box enabled status is : "+companyNameTextBox);
		Boolean countryDropDown = CustomFun.isElementEnable(driver
				, CountryDropDown_XPATH, "FirstNAme Text box ");
		System.out.print("\nAfter : First Name Text box enabled status is : "+countryDropDown);
		Boolean shippingAdressTextBox = CustomFun.isElementEnable(driver
				, shippingAdress1TextBox_XPATH, "FirstNAme Text box ");
		System.out.print("\nAfter : First Name Text box enabled status is : "+shippingAdressTextBox);
		Boolean cityTextBox = CustomFun.isElementEnable(driver
				, cityTextBox_XPATH, "FirstNAme Text box ");
		System.out.print("\nAfter : First Name Text box enabled status is : "+cityTextBox);

		return new ResellerCheckoutAddressPage(driver);
	}


	public ResellerCheckoutAddressPage enterDataInFields(String Attn,String Company,String country,String Address,String Postalcode,String Phonenumber)throws Exception
	{
		int pixel=2000;
		//To scroll down 
		GUIFunctions.pageScrollDown(driver,pixel);
		//To enter input for Attn
		GUIFunctions.typeTxtboxValue(driver, firstNameTextBox_XPATH,Attn);
		//To enter value for company field
		GUIFunctions.typeTxtboxValue(driver, companyNameTextBox_XPATH,Company);
		//To select country
		GUIFunctions.selectDropDownValue(driver.findElement(CountryDropDown_XPATH), country, "value");
		//To enter address
		GUIFunctions.typeTxtboxValue(driver,shippingAdress1TextBox_XPATH,Address);
		//To enter postal code
		GUIFunctions.typeTxtboxValue(driver,postalCodeTextBox_XPATH,Postalcode);
		Thread.sleep(2000);
		//Click outside postal code text box to auto generate state and city
		GUIFunctions.clickElement(driver, postalCodeLabl_XPATH,"postal code label clicked");
		//To enter phone number
		GUIFunctions.typeTxtboxValue(driver,phNumTextBox_XPATH,Phonenumber);

		return new ResellerCheckoutAddressPage(driver);
	}

	public ResellerCheckoutAddressPage enterPONum(String PONum)throws Exception
	{
		//To enter value for PO num field
		GUIFunctions.typeTxtboxValue(driver, poNumberTextBox_XPATH,PONum);
		return new ResellerCheckoutAddressPage(driver);
	}

	public ResellerCheckoutAddressPage SelectShippingMethod(String shippingmethodValue) throws Exception{


		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("scroll(0, -150);");
		Select dropdown = new Select(driver.findElement(By.id("shippingMethod")));
		dropdown.selectByValue(shippingmethodValue);
		//	dropdown.selectByVisibleText(shippingmethodName);
		//	GUIFunctions.selectDropDownValue(driver.findElement(shippingMethodRSCheckout_XPATH), shippingmethodName, "1");

		return new ResellerCheckoutAddressPage(driver);

	}
	public ResellerCheckoutAddressPage EnterCarrierNumber(String carrierNumber) throws Exception {

		GUIFunctions.typeTxtboxValue(driver,carrierAccount_ID, carrierNumber);
		return new ResellerCheckoutAddressPage(driver);
	}

	public ResellerCheckoutAddressPage clickOnBlindshipmentCheckbox() throws Exception {

		//click on blind shipment
		GUIFunctions.clickElement(driver, blindshipment_XPATH , "blind shipment click");
		return new ResellerCheckoutAddressPage(driver);

	}

	public ResellerCheckoutAddressPage enterShippingInstructions(String instructions) throws Exception {

		GUIFunctions.typeTxtboxValue(driver,shippinginstructionTextArea_XPATH, instructions);
		return new ResellerCheckoutAddressPage(driver);
	}

	public ResellerCheckoutAddressPage clickOnSaveShippingInstructionCheckBox() throws Exception {

		//click on save shipping instructions
		GUIFunctions.clickElement(driver, saveShippingInstruction_XPATH , "blind shipment click");
		return new ResellerCheckoutAddressPage(driver);
	}	
	
	public ResellerCheckoutAddressPage enterEarliestRequiredShipDate(String date) throws Exception {

		GUIFunctions.clickElement(driver, RequiredShipDate_ID, "click on date textbox");
		GUIFunctions.typeTxtboxValue(driver,RequiredShipDate_ID, date);
		return new ResellerCheckoutAddressPage(driver);	

	}	
	
	public ResellerReviewAndSubmitPage clickOnSaveButton() throws Exception {
		
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);

		//Click on save
		GUIFunctions.clickElement(driver,nextButtonaddressPage_XPATH, 	"next button click");
		Thread.sleep(3000);
		return new ResellerReviewAndSubmitPage(driver);
	}

}



