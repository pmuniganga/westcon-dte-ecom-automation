package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.SKUdataSetList;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;

import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC88_Quoting_EditExWestCostAndVerifyWestCost extends BaseTest {

	String SKU;

	String[] skuNumber;
	String[] skuQty;
	String[] actualMList;
	String[] actualVDiscount;
	String[] actualWestCost;
	String[] actualExWestCost;
	
	String[] actualWestconCostAfterUpdation;
	String[] actualExWestconCostAfterUpdation;
	
	
	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}
	
	/**
	 * Save the values like Westcon Cost and Extended Westcon Cost
	 */
	@Test(description = "Step2 : Save the values like vendor discount, list price, purchase price, Service Period and Sales Unit", priority = 2)
	public void Step02_SaveTableValues() {
		//Save the table values like vendor discount, list price, purchase price, Service Period and Sales Unit

		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");
			skuQty = SKUdataSetList.get(j).getQuantity().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualWestCost = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][3]")).getText().split(" ");
				actualExWestCost = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[@class='htNumeric'][4]")).getText().split(" ");
			}
		}
		
		System.out.println("purchasePrice = "+actualWestCost[1]);
		System.out.println("Extended purchasePrice = "+actualExWestCost[1]);

		log.info("Successfully saved table values");
		Reporter.log("<p>Successfully saved table values");
	}

	/**
	 * Change Extended Westcon CostService Period
	 */
	@Test(description = "Step3 : Change Extended Westcon Cost", priority = 3)
	public void Step03_ChangeExtendedWestCost() throws Exception {
		//Change Extended Westcon Cost
		QuotingQuoteDetailpage quoteDetails = new QuotingQuoteDetailpage(driver);

		quoteDetails.AlterExWestCost(TextProp.getProperty("alteredExWestCost"));
		
		log.info("Successfully altered Extended Westcon Cost");
		Reporter.log("<p>Successfully altered Extended Westcon Cost");
	}
	
	@Test(description = "Step4 : Verify Westcon Cost Calculation based on Extended Westcon Cost updation", priority = 4)
	public void Step04_VerifyPrices() {
		//Verify Manufacturer List Price updation
		
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("manufacturerListPrice_XPATH"))));
		
		
		for (int j = 0; j < SKUdataSetList.size(); j++) {
			skuNumber = SKUdataSetList.get(j).getSKU().split("\n");
			skuQty = SKUdataSetList.get(j).getQuantity().split("\n");

			for(int i = 0 ; i < skuNumber.length ; i++) {
				actualWestconCostAfterUpdation = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[contains(@class,'htNumeric')][3]")).getText().split(" ");
				actualExWestconCostAfterUpdation = driver.findElement(By.xpath("//div[contains(@class,'quote-detail-table')]/descendant::tbody[1]/tr["+(i+1)+"]/descendant::td[contains(@class,'htNumeric')][4]")).getText().split(" ");
				
				float expectedWestconCost = Float.parseFloat(actualExWestconCostAfterUpdation[1].replace(",","")) / Float.parseFloat(skuQty[i]);
				
				Assert.assertEquals(Double.parseDouble(actualWestconCostAfterUpdation[1].replace(",","")) , (double)Math.round(expectedWestconCost* 100) / 100);
			}
		}
		
		log.info("Successfully verified Westcon Cost Calculation");
		Reporter.log("<p>Successfully verified Westcon Cost Calculation");
	}
}