package in.valtech.config;

import in.valtech.custom.CustomFun;
import in.valtech.util.PropertyFileReader;

import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.google.common.base.Stopwatch;

public class BaseTest {

	public static WebDriver driver;
	public static Logger log;
	public String rootDir = CustomFun.getRootDir();
	public static String baseUrl;
	public static String locale;
	public static String orientation;
	public static ITestResult result;
	public static String breakPoint;
	public static String driverName;
	public static boolean isE_Commerce = false;
	public static Process process;
	public static String dataSet_Config;
	public static String cardExpMonthAll;
	public static int counter;
	public static Stopwatch stopwatch;
	public static String quoteID;
	public static String revesionID;
	
	public static String RetriveID;
	public static String RFQID;
	public static String SearchRFQID;
	
	
	@BeforeSuite
	public void suiteSetUp() throws Exception {
		// For logging
		log = Logger.getLogger(this.getClass().getName());
	}

	@Parameters({ "ORIENTATION", "LOCALE", "DS_CONFIG", "APPLICATION",
			"BREAKPOINT", "URL", "DRIVER", "ACTUAL_TC" })
	@BeforeTest
	public void setUp(String deviceOrientation, String appLocale,
			String dataSet, String application, String deviceResolution,
			String url, String nativeDriver, String actualTestCase)
			throws Exception {
		
		Thread.sleep(5000);
		
		// Initializing the value of Counter
		counter=0;

		stopwatch = Stopwatch.createUnstarted();
		// Starting the StopWatch to calculate the time
		stopwatch.start();
		
		// Verifying StopWatch is Running or not
		log.info("is StopWatch Running :" + stopwatch.isRunning());

		// Assigning default value to ITestResult variable
		result = null;

		log.info("*****************************");
		log.info("Test case->" + actualTestCase);

		// Assigning locale value
		locale = appLocale;
		log.info("Locale->" + locale);

		// Assigning driver value
		driverName = nativeDriver;
		log.info("Driver->" + driverName);

		// Assigning driver value
		dataSet_Config = dataSet;
		log.info("Test dataSet_Config->" + dataSet_Config);

		// Get Test Data set Details
		CustomFun.getTestDataSetDetails(dataSet_Config);

		// Assigning baseUrl value
		baseUrl = url;
		log.info("BaseURL->" + baseUrl);

		// Assigning orientation value
		orientation = deviceOrientation;
		log.info("DeviceOrientation->" + orientation);

		// Assigning breakPoint value
		breakPoint = deviceResolution;
		log.info("Breakpoint->" + breakPoint);
		log.info("*****************************");

		// ********e-Commerce or Non e-commerce value assignment based on
		// locale******

		if (locale.equals("FR") || locale.equals("DE") || locale.equals("ES")
				|| locale.equals("IT") || locale.equals("UK")
				|| locale.equals("US") || locale.equals("BR")
				|| locale.equals("CA") || locale.equals("CA_FR")
				|| locale.equals("JP") || locale.equals("AU"))
			isE_Commerce = true;
		else if (locale.equals("RU") || locale.equals("CN")
				|| locale.equals("HK_TC") || locale.equals("HK_ENG")
				|| locale.equals("KR") || locale.equals("TW")
				|| locale.equals("INT_ENG"))
			isE_Commerce = false;
		else {
			log.error("Not a valid locale: Please specify valid locale");
			Reporter.log("Not a valid locale: Please specify valid locale");

		}

		// ************* Load Property File********************
		PropertyFileReader.loadProprtyFile(locale, application);

		/*
		 * Select the relative drive as configured at BasicConfig.csv file using
		 * switch-case.
		 */
		driver = CustomFun.initializeDriver(nativeDriver, driver, breakPoint,
				rootDir, process);
		if (driverName.equalsIgnoreCase("FF")
				|| driverName.equalsIgnoreCase("CHROME")
				|| driverName.equalsIgnoreCase("SAFARI")
				|| driverName.equalsIgnoreCase("IE")) {
			if (breakPoint.equalsIgnoreCase("L")) {
		driver.manage().window().setSize(new Dimension(1350, 730));
				driver.manage().window().maximize();
			} else if (breakPoint.equalsIgnoreCase("M")) {
				driver.manage().window().setSize(new Dimension(897, 568));
			} else if (breakPoint.equalsIgnoreCase("AS")) {
				driver.manage().window().setSize(new Dimension(400, 600));
			}
			log.info("Window size ---" + driver.manage().window().getSize());
		}
		
	}

	@AfterTest
	public static void tearDown() throws Exception {
		if (!driverName.equals("IOS")) {
			// Closing the driver once the suite execution is finished.
			driver.close();
		}
		// Quitting the driver once the suite execution is finished.
		driver.quit();
		
		// Initializing the value of Counter
		counter = 1501;
		// Stopping the StopWatch
		stopwatch.stop();
		
		// Calculating the time Taken
		log.info("TimeTaken :" + stopwatch);
		
		// Verifying StopWatch is Running or not
		log.info("is StopWatch Running :" + stopwatch.isRunning());
		
		// Kill the Appium/nodejs server process -if exists.

		// result=null;
		if (CustomFun.isProcessRunging("node.exe", driverName)) {
			Runtime.getRuntime().exec("taskkill /F /IM node.exe");
		}
		// Kill the chromedriver.exe server process -if exists.
		// if (driverName.equalsIgnoreCase("CHROME")) {
		// Runtime.getRuntime().exec("taskkill /im chromedriver.exe /f");

		// }

	}

	@BeforeMethod
	public void methodLevelSetup() throws Exception {

		/*
		 * Checks for exceptions like IllegalStateException or SkipException or
		 * SessionNotFoundException or UnreachableBrowserException, If found
		 * skips the test method
		 */
		if (result != null) {
			if ((result.getThrowable().toString()
					.contains("IllegalStateException")
					|| result.getThrowable().toString()
							.contains("SkipException")
					|| result.getThrowable().toString()
							.contains("SessionNotFoundException") || result
					.getThrowable().toString()
					.contains("UnreachableBrowserException"))) {

				throw new SkipException("Skip Methods");
			}
		}

	}

	@AfterMethod
	public void methodLevelTearDown() throws Exception {

		/*
		 * Checks for exceptions like IllegalStateException or SkipException or
		 * SessionNotFoundException or UnreachableBrowserException, If found
		 * skips the test method
		 */
		if (result != null) {
			if ((result.getThrowable().toString()
					.contains("IllegalStateException")
					|| result.getThrowable().toString()
							.contains("SkipException")
					|| result.getThrowable().toString()
							.contains("SessionNotFoundException") || result
					.getThrowable().toString()
					.contains("UnreachableBrowserException"))) {

				throw new SkipException("Skip Methods");
			}
		}

	}

	@AfterClass
	public void classLevelTearDown() throws Exception {
		/*
		 * Checks for exceptions like IllegalStateException or SkipException or
		 * UnreachableBrowserException, If found skips the test cases
		 */
		if (result != null) {
			if ((result.getThrowable().toString()
					.contains("IllegalStateException")
					|| result.getThrowable().toString()
							.contains("SkipException") || result.getThrowable()
					.toString().contains("UnreachableBrowserException")|| result.getThrowable().toString()
					.contains("SessionNotFoundException"))) {

				throw new SkipException("Skip Testcases");
			}
		}
	}

}
