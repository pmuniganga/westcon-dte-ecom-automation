package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.common.CommonFun;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.ResellerPages.ResellerLoginPage;

public class TC16_Quoting_HideQuote_ToReseller extends BaseTest{

	static ResellerLoginPage rsLoginPage;

	static String username = null;
	static String password = null;
	static String quoteNumber="";

	/**
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}

	@Test(description = "Step 2: Select hide from Reseller view checkbox", priority = 2)
	public void Step02_SelectHideFromResellerViewCheckbox()throws Exception {
		log.info(" Select hide from Reseller view checkbox");
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.clickOnHideFromResellerViewCheckBox();

		log.info("Successfully Select hide from Reseller view checkbox\n");
		Reporter.log("<p>Successfully Select hide from Reseller view checkbox \n");

	}
	
	@Test(description = "Step 3: Select Submit quote from the dropdown and click on GO button", priority = 3)
	public void Step03_SelectDropdownAndSubmitQuoteFrom()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Click On No, do not validate button
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Click On No, do not validate button", priority = 4)
	public void Step04_ClickOnDoNotValidateButton1()throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");

		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}
	/**
	 * Retrieve & save the quote number
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5 :Retrieve & save the quote number", priority = 5)
	public void Step05_RetrieveQuoteNumber()throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);
		amQuoteDetailsPage.getValues();
		quoteNumber =quoteID;

		log.info("Retrieve & save the quote number\n"+quoteNumber);
		Reporter.log("<p>Retrieve & save the quote number\n");

	}
	/**
	 * Click on Logout
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 6 :Click on Logout", priority = 6)
	public void Step06_ClickonLogout() throws Exception {
		CommonFun.QuotingLogout(driver, "You have signed out of your account.");

		log.info("Retrieve & save the quote number\n");
		Reporter.log("<p>Retrieve & save the quote number\n");

	}

	/**
	 * Open browser,Navigate to the reseller URL
	 * 
	 *//*
	@Test(description = "Step 7: Open browser,Navigate to the reseeler URL", priority = 7)
	public void Step01_NavigateToWestconReseller_URL() throws Exception {

		// Navigate/launch the Reseller URL
		String resellerUrl = userInfoDSDetails.getRsUrl();
		System.out.println(resellerUrl);
		rsLoginPage = ResellerLoginPage.navigateTo_URL(driver, resellerUrl, driverName);
		
		log.info("Navigated to the Reseller URL");
		Reporter.log("<p>Navigated to Reseller Login Page URL -- "
				+ driver.getTitle());

	}

	*//**
	 * Enter username , Pwd and click on Login link
	 * 
	 *//*
	@Test(description = "Step 8: Enter username , Pwd and click on Login link", priority = 8)
	public void Step8_ResellerLogin() throws Exception {

		System.out.println("user=" +userInfoDSDetails.getRsUserName());
		System.out.println("pwd=" +userInfoDSDetails.getRsPassword());
		username = userInfoDSDetails.getRsUserName();		
		password = userInfoDSDetails.getRsPassword();	
		ResellerLoginPage rsLoginPage = new  ResellerLoginPage(driver);	
		rsLoginPage.enterLoginCreds(username, password);
		log.info("The Reseller successfully logged-in and SelfSerivice page is displayed\n");
		Reporter.log("<p>The Reseller successfully logged-in and SelfSerivice  page is displayed");

	}

	*//**
	 * Click on user select Form icon
	 * 
	 *//*
	@Test(description = "Step 9: Click on user select Form icon", priority = 9)
	public void Step9_userSelectFormIcon() throws Exception {

		ResellerSelfServicePage resellerSelfServicePage = new ResellerSelfServicePage(driver);
		resellerSelfServicePage.clickOnUserSelectFormIcon();
		
		log.info("Successfully click on user select Form icon\n");
		Reporter.log("<p>Successfully click on user select Form icon");

	}

	*//**
	 * select reseller and click select
	 * 
	 *//*
	@Test(description = "Step 10: select reseller and cleck select", priority = 10)
	public void Step10_SelectReseller() throws Exception {

		ResellerSelfServicePage resellerSelfServicePage = new ResellerSelfServicePage(driver);
		resellerSelfServicePage.selectReseller("000"+userInfoDSDetails.getReseller().trim());
		
		log.info("Successfully select reseller and click select\n");
		Reporter.log("<p>Successfully select reseller and click select");

	}

	*//**
	 *  click on yes button
	 * 
	 *//*
	@Test(description = "Step 11: click on yes button", priority = 11)
	public void Step11_ClickOnYesButton() throws Exception {

		ResellerSelfServicePage resellerSelfServicePage = new ResellerSelfServicePage(driver);
		resellerSelfServicePage.clickOnYesButton();
		
		log.info("Successfully sclick on yes button\n");
		Reporter.log("<p>Successfully select click on yes button");

	}
	*//**
	 *  Click on View processed Quote requests
	 * 
	 *//*
	@Test(description = "Step 12: Click on View processed Quote requests", priority = 12)
	public void Step12_ClickOnViewProcessedQuoteRequests() throws Exception {

		ResellerSelfServicePage resellerSelfServicePage = new ResellerSelfServicePage(driver);
		resellerSelfServicePage.clickOnViewProcessedQuoteRequests();
		
		log.info("Successfully click on yes button\n");
		Reporter.log("<p>Successfully select click on yes button");

	}
	*//**
	 *  Enter Quote Id (saved on create quote) in search input field and click enter
	 * 
	 *//*
	@Test(description = "Step 13:Enter Quote Id (saved on create quote) in search input field and click enter", priority = 13)
	public void Step13_EnterQuoteIDClickEnter() throws Exception {

		ResellerMyQuotePage resellerMyQuotePage = new ResellerMyQuotePage(driver);

		resellerMyQuotePage.enterQuoteIDClickEnter(quoteNumber);
		
		log.info("Successfully Enter Quote Id (saved on create quote) in search input field and click enter\n");
		Reporter.log("<p>Successfully Enter Quote Id (saved on create quote) in search input field and click enter");

	}*/

}
