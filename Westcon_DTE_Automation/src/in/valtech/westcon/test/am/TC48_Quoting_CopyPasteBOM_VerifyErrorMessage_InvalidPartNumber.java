package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC48_Quoting_CopyPasteBOM_VerifyErrorMessage_InvalidPartNumber extends BaseTest{
	static  QuotingAddProductsPage amAddProductsPage;
	String SKUCount = null;
	String inputType;
	String[] skuNumber;
	
	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */

	@Test(description = "Step 1: Call TC01 TC02", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
	}	

	/**
	 * Enter any input,but in invalid format
	 * 
	 */
	@Test(description = "Step 2: Enter invalid part number as input", priority = 2)
	public void Step02_EnterInvalidPartNum() throws Exception {
		
		//Enter input in valid format
		amAddProductsPage = new QuotingAddProductsPage(driver);
		
				amAddProductsPage.CopyPasteBOM(driver , TextProp.getProperty("InvalidPartNumber"));

		log.info("successfully entered invalid input the part numbers\n");
		Reporter.log("<p>successfully entered invalid input the part numbers");

	}

	/**
	 * Click on Submit button 
	 * 
	 */
	@Test(description = "Step 3: Click on Submit button", priority = 3)
	public void Step03_ClickSubmit() throws Exception {
		
		//Click on Submit button
		amAddProductsPage = new QuotingAddProductsPage(driver);

		amAddProductsPage.CopyPasteBOMSubmit_ErrorValidation(driver);

		log.info("successfully clicked on Submit button and navigated to create quote page\n");
		Reporter.log("<p>successfully clicked on Submit button and navigated to create quote page");
	}
	

	/**
	 * Verify Error Message for Invalid Input
	 * 
	 * 
	 */
	@Test(description = "Step 4: Verify Error Message for Invalid part numbers", priority = 4)
	public void Step04_VerifyErrorMessage_InvalidInput() throws Exception {
		
	String errorMsg=driver.findElement(By.xpath(ObjRepoProp.getProperty("CopyPasteBOMInvalidInputErrorMsg_XPATH"))).getText().trim();
	System.out.println("Error Message Displayed*****"+errorMsg);
	
    Assert.assertTrue(errorMsg.contains(TextProp
    		.getProperty("InvalidPartNumberErrorMsg")),"Error message mismatch");
    
		log.info("successfully verified error message for Invalid part numbers\n");
		Reporter.log("<p>successfully verified error message for Invalid part numbers");
	}
}


