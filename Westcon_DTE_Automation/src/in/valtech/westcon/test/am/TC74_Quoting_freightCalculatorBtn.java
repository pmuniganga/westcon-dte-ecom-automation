
package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC74_Quoting_freightCalculatorBtn extends BaseTest{
	static QuotingQuoteDetailpage amQuoteDetailspage;
	/**
	 *
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_NavigateToCreateQuotePage() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");		

		log.info("Successfully navigated to Create quote page");
		Reporter.log("<p>--Successfully navigated to Create quote page ");	


	}

	/*
	 *  Click on Pricing tab and retreived Values
	 * 
	 */
	@Test(description = "Step 2: Click on Pricing tab and retreive Values", priority = 2)
	public void Step02_ClickOnPricingTabAndRetreivePrices() throws Exception {
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailspage.clickPricingTab();
		CustomFun.waitObjectToLoad(driver, By.xpath(ObjRepoProp.getProperty("FreightCalculatorBtn_XPATH")), 60);
		
		log.info("Successfully Clicked on Pricing tab and retreived Values");
		Reporter.log("<p>--Successfully Clicked on Pricing tab and retreived Values");	
	}

	/**
	 * Click on Freight Calculator button
	 * 
	 */
	@Test(description="Step 3:Click on Freight Calculator button", priority=3)
	public void Step3_ClickOnFreightCalculatorBtn() throws Exception{
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);

		amQuoteDetailspage.clickOnFreightCalculatorButton();
		
		CustomFun.waitObjectToLoad(driver, By.xpath("//div[@class='qut-totals-spinner fadeOut']"), 60);
		
		log.info("Successfully Clicked on Freight Calculator button");
		Reporter.log("<p>--Successfully Clicked on Freight Calculator button");		
	}


	/**
	 * Verify Total Quote price
	 */
	@Test(description = "Step 4:Verify Total Quote price", priority = 4)
	public void Step04_VerifyTotalQuotePrice() throws Exception {

		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);
		By EstimatedfreightValue=By.xpath(ObjRepoProp.getProperty("EstimatedfreightValue_XPATH"));
		
		((JavascriptExecutor) driver).executeScript(
			    "arguments[0].scrollIntoView();",
			    driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalResellerPriceValueInForm_XPATH"))));
	
		
		float totalResellerPrice_act;
		float totalQuotePrice_act;
		float freightCharge_act;
		
		System.out.println("Freight Charge applied****"+driver.findElement(EstimatedfreightValue).getText().trim());
		
		    if(locale.equals("US")){
	        
			totalQuotePrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValue_XPATH"))).getText().trim().replaceAll("[$,]", ""));
	        System.out.println("Total Quote Price_After****"+totalQuotePrice_act);
	        
	        totalResellerPrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalResellerPriceValueInForm_XPATH"))).getText().trim().replaceAll("[$,]", ""));
	    	System.out.println("Total Reseller Price_After****"+totalResellerPrice_act);
	    	
	    	freightCharge_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("EstimatedfreightValue_XPATH"))).getText().trim().replaceAll("[$,]", ""));
	    	System.out.println("freight Charge_After"+freightCharge_act);
		    }
		
	        else{
	        totalQuotePrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValue_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", ""));
	        System.out.println("Total Quote Price_After****"+totalQuotePrice_act);
	        
	        totalResellerPrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalResellerPriceValueInForm_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", ""));
	        System.out.println("Total Reseller Price_After****"+totalResellerPrice_act);
	        
	        freightCharge_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("EstimatedfreightValue_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", ""));
	    	System.out.println("freight Charge_After"+freightCharge_act);
	        }
		    
		    //verify Total Quote Price
		    Assert.assertEquals(totalQuotePrice_act, (float)(totalResellerPrice_act+freightCharge_act), "Total Quote Price mismatch");

		    
		    
		log.info("Successfully Verified Total Quote price");
		Reporter.log("<p>--Successfully Verified Total Quote price");	
	}

}
