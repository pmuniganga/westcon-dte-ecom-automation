package in.valtech.westcon.test.am;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import static in.valtech.util.PropertyFileReader.TextProp;
import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;

public class TC13_Quoting_AddNewShippingAddress extends BaseTest{

	static QuotingQuoteDetailpage amQuoteDetailsPage;

	static String Attn = null;
	static String Company = null;
	static String Add1 = null;
	static String Add2 = null;
	static String City = null;
	static String postal = null;
	static String state = null;
	static String Phone = null;
	static String carrier=null;

	/**
	 *
	 * Call TC08 
	 * 
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08", priority = 1)
	public void Step01_NavigateToCreateQuotePage() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		

		log.info("Successfully navigated to Create quote page");
		Reporter.log("<p>--Successfully navigated to Create quote page ");	

		//click on shipping tab
		amQuoteDetailsPage.clickOnShippingtab();

		log.info("Successfully clicked on shipping tab");
		Reporter.log("<p>--Successfully clicked on shipping tab ");	

		//Verify shipping tab opened 
		Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("ShippingTabOpen_XPATH")), driver),
				"Shipping tab is not open");

		log.info("Successfully verified shipping tab is open");
		Reporter.log("<p>--Successfully verified shipping tab is open");	

		//Click on Edit shipping information button
		amQuoteDetailsPage.clickOnEditShippingInfoButton();

		log.info("Successfully clicked on edit shipping info button");
		Reporter.log("<p>--Successfully clicked on edit shipping info button");

	}

	/**
	 * Select enter new address radio button
	 */
	@Test(description = "Step 2:Select enter new address radio button", priority = 2)
	public void Step02_SelectNewAddressRadioButton() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		
		Boolean flag = false;
		flag = CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("AddressFieldsEnabled_XPATH")),driver);
		if(!flag) {
			//click on new address radio button
			amQuoteDetailsPage.clcikOnNewAddressRadioButton();

			log.info("Successfully clicked on new address radio button");
			Reporter.log("<p>--Successfully clicked on new address radio button");
		}
		else {

			//Verify fields enabled
			Assert.assertTrue(CustomFun.isElementPresent(By.xpath(ObjRepoProp.getProperty("AddressFieldsEnabled_XPATH")), driver),
					"Fields not enabled");

			log.info("Successfully verified fields enabled");
			Reporter.log("<p>--Successfully verified fields enabled");	
		}
	}

	/***
	 * Fill Attn , Company Name, address , postal code , phone number 
	 */
	@Test(description = "Step 3:Fill Attn , Company Name, address , postal code , phone numbe", priority = 3)
	public void Step03_FillTheAddressForm() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		Attn=userInfoDSDetails.getAttn();
		Company=userInfoDSDetails.getCompanyName();
		Add1=userInfoDSDetails.getAdddress1();
		Add2=userInfoDSDetails.getAdddress2();
		City=userInfoDSDetails.getCity();
		postal=userInfoDSDetails.getPostalCode();
		//state=userInfoDSDetails.getPostalCode();
		Phone=userInfoDSDetails.getPhoneNumber();
		
		System.out.println("attn= " +Attn);
		System.out.println("Comapny_name= " +Company);
		System.out.println("Address1= "+Add1);
		System.out.println("Address2= "+Add2);
		System.out.println("Postal Code= "+postal);
		System.out.println("City="+City);
		System.out.println("Phone No.= "+Phone);


		//enter form details 
		amQuoteDetailsPage.EnterDetailsForNewAddressForm(driver,Attn,Company,Add1,Add2,postal,Phone);

		log.info("Successfully entered the form details");
		Reporter.log("<p>--Successfully entered the form details");	
	}

	/**
	 * Select shipping method , enter carrier account number, shipdate , select request blind shipment,
	 *  select request ship complete, enter shipping instructions and Click Save button
	 */

	@Test(description = "Step 4:Select shipping method , enter carrier account number, shipdate , select request blind shipment, select request ship complete,"
			+ " enter shipping instructions and Click Save button", priority = 4)
	public void Step04_SelectShippingMethodOtherDetailsAndSave() throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		//Select shipping method
		if(locale.equalsIgnoreCase("US")) {
			amQuoteDetailsPage.SelectShippingMethod(TextProp.getProperty("shippingMethodvalue"));
		}
		else if(locale.equalsIgnoreCase("CA")){
			amQuoteDetailsPage.SelectShippingMethod(TextProp.getProperty("shippingMethodvalue_CA"));
		}

		log.info("Successfully selected the shipping method");
		Reporter.log("<p>--Successfully selected the shipping method");	


		//enter carrier account number
		carrier=userInfoDSDetails.getCarrierNumber();
		amQuoteDetailsPage.EnterCarrierNumber(carrier);


		//select request blind shipment
		amQuoteDetailsPage.clickOnBlindshipmentCheckbox();

		//select request ship complete

		//enter shipping instructions 
		amQuoteDetailsPage.enterShippingInstructions(TextProp.getProperty("shippingInstructions"));

		//click on save shipping instruction
		amQuoteDetailsPage.clickOnSaveShippingInstructionCheckBox();

		//select ship date 
		amQuoteDetailsPage.enterEarliestRequiredShipDate(CustomFun.addDays(new Date(), Integer.parseInt(userInfoDSDetails
				.getQuoteCloseDate().trim()), locale));
         //Save entered New Shipping address
		amQuoteDetailsPage.clickOnSaveShippingAddressCheckBox();
		
		log.info("Successfully saved the shipping details");
		Reporter.log("<p>--Successfully saved the shipping details");	
	}
	
	/**
	 * Click Save button
	 * 
	 * @throws Exception
	 */
	@Test(description="Step5:Click on Save Button",priority=5)
	public void Step05_ClickOnSavebtn() throws Exception{
		amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		//Click Save button
		amQuoteDetailsPage.clickOnSaveButton();
		
		log.info("Successfully Clicked on Save Button");
		Reporter.log("<p>--Successfully Clicked on Save Button");
	}
	
	/**
	 * Verify Shipping method , carrier account number, 
	 * shipdate , request blind shipment,  request ship complete, shipping instructions 
	 * 
	 */

	@Test(description = "Step 6:Verify Shipping method , carrier account number, shipdate , request blind shipment,  "
			+ "request ship complete, shipping instructions ", priority = 6)
	public void Step06_VerifyShippingDetails() throws Exception {

		//QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(By.xpath(ObjRepoProp.getProperty("shippingMethodEntered_XPATH"))));
		
		Thread.sleep(4000);

		//Verify shipping method
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp.getProperty("shippingMethodEntered_XPATH"))).getText().trim(),TextProp.getProperty("shippingMethod"), " The shipping method selected is not as expected");

		log.info("Successfully verified the shipping method");
		Reporter.log("<p>--Successfully verified the shipping method");


		//verify carrier account number
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("carrierAccountNumberEntered_XPATH"))).getText(), carrier , " The carrier account number is not as expected ");

		log.info("Successfully verified carrier account number");
		Reporter.log("<p>--Successfully verified carrier account number");

		//Verify required ship date
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("BlindShipmentEntered_XPATH"))).getText(), "Yes", "The required shipdate is not as expected");

		log.info("Successfully verified the required ship date");
		Reporter.log("<p>--Successfully verified the required ship date");

		/*//Verify ship complete
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("shipCompleteEntered_XPATH"))).getText(), "Yes", " The ship complete is not as expected");*/

	/*	log.info("Successfully verified the ship complete");
		Reporter.log("<p>--Successfully verified the ship complete");*/

		//Verify instructions
		Assert.assertEquals(driver.findElement(By.xpath(ObjRepoProp
				.getProperty("shippingInstructionEntered_XPATH"))).getText(),TextProp
				.getProperty("shippingInstructions"), " The instructions are not as expected ");

		log.info("Successfully verified instructions");
		Reporter.log("<p>--Successfully verified instructions");



	}


}