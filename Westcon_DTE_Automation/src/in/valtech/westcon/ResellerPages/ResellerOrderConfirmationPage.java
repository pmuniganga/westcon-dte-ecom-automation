package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

public class ResellerOrderConfirmationPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller login page Constructor
	public ResellerOrderConfirmationPage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		// Check that we're on the right AM login Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerOrderConfirmationPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Reseller Order Confirmation page");
		}

	}
	
	By orderNumber_XPATH=By.xpath(ObjRepoProp.getProperty("orderNumber_XPATH"));
	By myOrdersButton_XPATH=By.xpath(ObjRepoProp.getProperty("myOrdersButton_XPATH"));
	
	public String saveOrderNum() throws Exception
	{

		String OrderNum= driver.findElement(orderNumber_XPATH).getText();
		System.out.println("#####################################################################################");
		
		System.out.println(OrderNum);
	
		return  OrderNum;
	}
	
	public ResellerOrderListingPage clickMYOrderButton() throws Exception
	{

		GUIFunctions.clickElement(driver, myOrdersButton_XPATH, "click on My Orders button");
		

	
		return new ResellerOrderListingPage(driver);
	}
}
