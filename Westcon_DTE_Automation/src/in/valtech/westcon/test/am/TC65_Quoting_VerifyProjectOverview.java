package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.westcon.QuotingPages.QuotingAddProductsPage;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC65_Quoting_VerifyProjectOverview extends BaseTest {
	static  QuotingAddProductsPage amAddProductsPage;
	static QuotingQuoteDetailpage amQuoteDetailspage;

	/**
	 * Open browser,Navigate to the AM URL
	 * 
	 */

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
	}

	@Test(description = "Step 2: Fetch all values from pricing grid", priority = 2)
	public void Step02_FetchAllValues() {
		//Fetch all pricing grid values
		amQuoteDetailspage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailspage.fetchAndVerifyValues(driver);
		
		log.info("Verified all values successfully\n");
		Reporter.log("Verified all values successfully");
		
	}
}