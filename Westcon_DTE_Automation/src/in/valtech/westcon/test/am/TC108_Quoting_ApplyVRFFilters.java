package in.valtech.westcon.test.am;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;

public class TC108_Quoting_ApplyVRFFilters extends BaseTest {

	/**
	 * Call TC03 which adds products
	 */
	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08 -- ", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");	

		log.info("Successfully added part numbers and land on quote details page\n");
		Reporter.log("<p>Successfully added part numbers and land on quote details page");
	}

	
}