package in.valtech.util;

import in.valtech.custom.CustomFun;

import java.io.*;
import java.util.*;

public class PropertyFileReader {

	/**
	 * Properties.
	 */
	public static Properties ObjRepoProp;
	public static Properties TextProp;
	public static String rootDir = CustomFun.getRootDir();

	/**
	 * Load Property File.
	 * 
	 **/
	public static void loadProprtyFile(String locale, String application) {

		ObjRepoProp = new Properties();
		TextProp = new Properties();
		

		try {

			if (application.equalsIgnoreCase("FRONT")) {
				// Reading/loading the ObjectRepository property file.
				ObjRepoProp.load(new FileInputStream(rootDir
						+ "/resources/front/ObjRepository.properties"));
				switch (locale) {

				case "US":
					// Reading/loading the Text property file-US Locale.
					TextProp.load(new FileInputStream(rootDir
							+ "/resources/front/textData/Text_US.properties"));
					break;
	
				case "UK":
					// Reading/loading the Text property file-UK Locale.
					TextProp.load(new FileInputStream(rootDir
							+ "/resources/front/textData/Text_US.properties"));
					break;
									
				case "CA":
					// Reading/loading the Text property file-CA Locale.
					TextProp.load(new FileInputStream(rootDir
							+ "/resources/front/textData/Text_US.properties"));
					break;	
					
				default:
					TextProp.load(new FileInputStream(rootDir
							+ "/resources/front/textData/Text_US.properties"));

				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
