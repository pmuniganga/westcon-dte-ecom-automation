package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResellerQuoteListingPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller self service page Constructor
	public ResellerQuoteListingPage(WebDriver driver) throws InterruptedException {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right Reseller quote listing page.

		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerQuoteListingpage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Quote listing page");

			}
	}
		//This function is to enter quote id
		
		By enterQuoteIdTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("enterPartNoTextBox_XPATH"));
		public ResellerQuoteListingPage EnterValueTextbox(String QuoteId)throws Exception
		{
			
			System.out.println("QuoteId" +QuoteId);
			GUIFunctions.typeTxtboxValue(driver, enterQuoteIdTextBox_XPATH, QuoteId);
			
			return new ResellerQuoteListingPage(driver);
		}
		
		
		By rsEnterPartNoTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("rsEnterPartNoTextBox_XPATH"));
		public ResellerQuoteListingPage MyQuotes_EnterValueTextbox(String QuoteId)throws Exception
		{
			
			System.out.println("QuoteId" +QuoteId);
			GUIFunctions.typeTxtboxValue(driver, rsEnterPartNoTextBox_XPATH, QuoteId);
			
			return new ResellerQuoteListingPage(driver);
		}
		
		//This function is to hit on enter key
		
		public ResellerQuoteListingPage HitEnterKey()throws Exception
		{
			GUIFunctions.keyPressEnter(driver);
			return new ResellerQuoteListingPage(driver);
		}
		
		
		//This function is to hit on quote id
		public Reseller_QuoteDeatilPage ClickonQuoteID()throws Exception
		{
			WebElement CreatedQuoteiId=driver.findElement(By.xpath("//table[@id='rnq-table']/descendant::a[contains(@href,'"+BaseTest.quoteID+"')]"));
			System.out.println("CreatedQuoteiId="+CreatedQuoteiId);
			CreatedQuoteiId.click();
			Thread.sleep(5000);
			return new Reseller_QuoteDeatilPage(driver);
		}
		
		
		
}
