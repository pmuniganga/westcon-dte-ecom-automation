package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;

import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC50_Quoting_EnterEndUserCompany_DirectInput extends BaseTest{
	//static QuotingQuoteDetailpage amQuotePage;
		static String euserCompany = null;
		String endUserDetails;
		
	     /**
	       * Call TC03
	      */
		@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
		
		public void Step01_Call() throws Exception {

			// Execute TC-01
			log.info("FTC-01 Successfully Executed \n");
			Reporter.log("<p>TC-01 Successfully Executed ");
			// Execute TC-02
			log.info("TC-02 Successfully Executed \n");
			Reporter.log("<p>TC-02 Successfully Executed ");
			// Execute TC-03
			log.info("TC-03 Successfully Executed \n");
			Reporter.log("<p>TC-03 Successfully Executed ");
		}
		
		/**
		 * Click on End User Company Name Instead link
		 * 
		 */
		
		@Test(description="Step 2: Click on End User Company Name Instead link", priority=2)
		public void Step02_ClickOnEndUsrCmpyInstead() throws Exception {
			QuotingQuoteDetailpage amQuoteDetailsPage=new QuotingQuoteDetailpage(driver);
			
			amQuoteDetailsPage.ClickOnEndUsrCmpyInsteadLink(driver);
			log.info("Successfully Clicked on End User Company Name Instead link\n");
			Reporter.log("<p>Successfully Clicked on End User Company Name Instead link");
		}

		/**
		 * Enter End User Account Number
	     * 
	    */
		@Test(description = "Step 3: Enter End User Company ", priority = 3)
		public void Step03_EnterEndUserCompany() throws Exception {
			
			// Direct Entering End User Account No. 
			QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
			euserCompany=userInfoDSDetails.getEndUser();
	        System.out.println("End User Account Number*****"+euserCompany);

			GUIFunctions.pageScrollDown(driver, 100);
	        amQuotePage.enterEndUserCmpy_DirectInput(euserCompany);
	        
			log.info("Successfully entered End User Company Name");
			Reporter.log("<p>Successfully entered End User Company Name");
		}
		
		
}
