package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.ResellerPages.ResellerRequestAQuotePage;

public class ResellerSelfServicePage {

	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller self service page Constructor
	public ResellerSelfServicePage(WebDriver driver) throws InterruptedException {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right Reseller self service Page.

		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerSelfservicepage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not selfservice page");

		}

	}
	By resellerSeconadryNaviagtionLink = By.xpath(ObjRepoProp.getProperty("resellerSeconadryNaviagtionLink_XPATH"));
	By resellerPrimaryNavigationQuoteLink=By.xpath(ObjRepoProp.getProperty("resellerPrimaryNavigationQuoteLink_XPATH"));
	By resellerunProcessedQuoteLink_XPATH=By.xpath(ObjRepoProp.getProperty("resellerunProcessedQuoteLink_XPATH"));

	By userSelectFormIcon_XPATH =By.xpath(ObjRepoProp.getProperty("userSelectFormIcon_XPATH"));
	By resellerDropdown_XPATH= By.xpath(ObjRepoProp.getProperty("resellerDropdown_XPATH"));
	By userSelectButton_XPATH = By.xpath(ObjRepoProp.getProperty("userSelectButton_XPATH"));
	By selectUserYesButton_XPATH =By.xpath(ObjRepoProp.getProperty("selectUserYesButton_XPATH"));
	By quoteMenu_XPATH =By.xpath(ObjRepoProp.getProperty("quoteMenu_XPATH"));
	By viewProcessedQuoteRequests_XPATH=By.xpath(ObjRepoProp.getProperty("viewProcessedQuoteRequests_XPATH"));
	By quickOrderEntry_XPATH=By.xpath(ObjRepoProp.getProperty("quickOrderEntry_XPATH"));
	By productCatalogMenu_XPATH=By.xpath(ObjRepoProp.getProperty("productCatalogMenu_XPATH"));

	By productCatalogMenuCa_XPATH=By.xpath(ObjRepoProp.getProperty("productCatalogMenuCanada_XPATH"));

	/**
	 * This function is to  Mouse hover on Quotes from Menu and Click on Request a new Quote
	 * 
	 * @throws Exception
	 */
	public ResellerRequestAQuotePage mouseHoverOnQuotesAndClickOnRequestNewQuoteLink() throws Exception
	{

		//WebElement resellerPrimaryNavigationQuoteLink = driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerPrimaryNavigationQuoteLink_XPATH")));
		//Mouse hover on Quotes from Menu  Click on secondary navigation Request a new Quote

		// For US
		//GUIFunctions.clickElement(driver, resellerPrimaryNavigationQuoteLink, "Clicked on Quotes link");

		//GUIFunctions.mouseOverElement(driver, resellerPrimaryNavigationQuoteLink);
		//Thread.sleep(10000);
		//Click on secondary navigation Request a new Quote
		//GUIFunctions.clickElement(driver, resellerSeconadryNaviagtionLink, "Clicked on Request New quote link");

		// For CA
		GUIFunctions.mouseOverElement(driver, driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerPrimaryNavigationQuoteLink_XPATH"))));
		Thread.sleep(10000);
		//Click on secondary navigation Request a new Quote
		GUIFunctions.clickElement(driver, resellerSeconadryNaviagtionLink, "Clicked on Request New quote link");


		return new ResellerRequestAQuotePage (driver);
	}

	/**
	 * This function is to  Mouse hover on Quotes from Menu and Click on Request a new Quote
	 * 
	 * @throws Exception
	 */
	public ResellerSelfServicePage clickOnUserSelectFormIcon() throws Exception
	{

		GUIFunctions.clickElement(driver, userSelectFormIcon_XPATH,"UserSelectFormIcon" );
		return new ResellerSelfServicePage(driver);
	}
	public ResellerSelfServicePage selectReseller(String resellerId) throws Exception
	{
		System.out.println("------------------------------------------------>"+resellerId);
		GUIFunctions.selectDropDownValue(driver.findElement(resellerDropdown_XPATH),resellerId, "value");
		GUIFunctions.clickElement(driver, userSelectButton_XPATH,"Select button" );
		return new ResellerSelfServicePage(driver);
	}

	/**
	 * This function Click On "Yes" button
	 * 
	 * @throws Exception
	 */		
	public ResellerSelfServicePage clickOnYesButton() throws Exception{
		//Thread.sleep(5000);	
		GUIFunctions.clickElement(driver, selectUserYesButton_XPATH," Yes button" );		

		return new ResellerSelfServicePage(driver);
	}

	/**
	 *  Click on View processed Quote request
	 * 
	 * @throws Exception
	 */		
	public ResellerMyQuotePage clickOnViewProcessedQuoteRequests() throws Exception{


		System.out.println("Click on View processed Quote request mouse Over");
		Thread.sleep(1000);
		GUIFunctions.mouseOverElement(driver, driver.findElement(quoteMenu_XPATH));
		System.out.println("Click on View processed Quote request mouse Over finished");
		//Click on secondary navigation Request a new Quote	
		GUIFunctions.clickElement(driver, viewProcessedQuoteRequests_XPATH, "Click on View processed Quote request");
		return new ResellerMyQuotePage(driver);

	}
	public ResellerQuoteRequestCategoryPage mouseHoverOnProductCatalogNavigateQuickEntryPage() throws Exception
	{
		System.out.println(BaseTest.locale);
		if(BaseTest.locale.equalsIgnoreCase("US"))
		{
			System.out.println("test1");
			WebElement productCatalogMenu = driver.findElement(productCatalogMenu_XPATH);
			//Mouse hover on product Catalog Menu from Menu  
			GUIFunctions.mouseOverElement(driver, productCatalogMenu);
			Thread.sleep(15000);
			CustomFun.waitObjectToLoad(driver, quickOrderEntry_XPATH, 60);
			//Click on quick Order Entry sub menu link

			boolean ele = CustomFun.isElementPresent(quickOrderEntry_XPATH, driver);

			if(ele==true)
			{
				GUIFunctions.clickElement(driver, quickOrderEntry_XPATH, "Clicked on quick Order Entry sub menu link");
			}
			else
			{

				
				GUIFunctions.mouseOverElement(driver, productCatalogMenu);
				Thread.sleep(15000);
				CustomFun.waitObjectToLoad(driver, quickOrderEntry_XPATH,60);
				GUIFunctions.clickElement(driver, quickOrderEntry_XPATH, "Clicked on quick Order Entry sub menu link");

			}
		}else{

			System.out.println("test2");
			CustomFun.waitForPageLoaded(driver);
			WebElement productCatalogMenuca = driver.findElement(productCatalogMenuCa_XPATH);
			//Mouse hover on product Catalog Menu from Menu  
			GUIFunctions.mouseOverElement(driver, productCatalogMenuca);
			Thread.sleep(15000);
			CustomFun.waitObjectToLoad(driver, quickOrderEntry_XPATH, 60);
			//Click on quick Order Entry sub menu link

			boolean ele = CustomFun.isElementPresent(quickOrderEntry_XPATH, driver);

			if(ele==true)
			{
				GUIFunctions.clickElement(driver, quickOrderEntry_XPATH, "Clicked on quick Order Entry sub menu link");
			}
			else
			{


				GUIFunctions.mouseOverElement(driver, productCatalogMenuca);
				Thread.sleep(15000);
				CustomFun.waitObjectToLoad(driver, quickOrderEntry_XPATH,60);
				GUIFunctions.clickElement(driver, quickOrderEntry_XPATH, "Clicked on quick Order Entry sub menu link");

			}
		}


		return new ResellerQuoteRequestCategoryPage(driver);
	}


	/** This function will mouse hover on quotes and  click on unsubmitted quotes link 
	 * 
	 */
	By resellerRFQListingPageSearchInput_XPATH=By.xpath(ObjRepoProp.getProperty("resellerRFQListingPageSearchInput_XPATH"));
	public ResellerRFQListingPage mouseHoverOnQuotesAndClickOnUnProcessedQuotesLink() throws Exception
	{
		//mouse over on quotes 
		GUIFunctions.mouseOverElement(driver, driver.findElement(By.xpath(ObjRepoProp.getProperty("resellerPrimaryNavigationQuoteLink_XPATH"))));

		//click on unsubmitted quotes resellerunProcessedQuoteLink_XPATH
		GUIFunctions.clickElement(driver, resellerunProcessedQuoteLink_XPATH, "unprocessed quotes link");
		CustomFun.waitObjectToLoad(driver, resellerRFQListingPageSearchInput_XPATH, 60);
		return new ResellerRFQListingPage(driver);
	}
	
	By orderHistoryNavigationLink=By.xpath(ObjRepoProp.getProperty("orderHistoryNavigationLink_XPATH"));
	public ResellerOrderListingPage mouseHoverOnOrderHistory() throws Exception {
		
		GUIFunctions.mouseOverElement(driver, driver.findElement(orderHistoryNavigationLink));
		GUIFunctions.clickElement(driver, viewOpenAndClosedOrderNavigationLink,
				"view open and closed order link ");
		System.out.println("clicked on view open and closed order link");
		
		return new ResellerOrderListingPage(driver);
	}

	By viewOpenAndClosedOrderNavigationLink=By.xpath(ObjRepoProp.getProperty("viewOpenAndClosedOrderNavigationLink_XPATH"));
	public ResellerOrderListingPage clickOnViewOpenAndClosedOrderLink() {
		
		GUIFunctions.clickElement(driver, viewOpenAndClosedOrderNavigationLink,
				"view open and closed order link ");
		System.out.println("clicked on view open and closed order link");
		
		return new ResellerOrderListingPage(driver);
	}

}
