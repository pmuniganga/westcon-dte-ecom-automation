package in.valtech.westcon.ResellerPages;

import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.ResellerPages.ResellerSelfServicePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;


public class ResellerLoginPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller login page Constructor
	public ResellerLoginPage(WebDriver driver) {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		// Check that we're on the right AM login Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerLoginPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Reseller Login page");
		}
	}

	By userName_XPATH = By.xpath(ObjRepoProp.getProperty("usernameField_XPATH"));
	By password_XPATH = By.xpath(ObjRepoProp.getProperty("passwordField_XPATH"));
	By submit_XPATH = By.xpath(ObjRepoProp.getProperty("loginButton_XPATH"));
	static By browserOverride = By.xpath(ObjRepoProp.getProperty("browserOverride_XPATH"));
	/**
	 * This function navigates to Reseller application URL
	 * 
	 * @param driver
	 *            :WebDriver
	 * @param baseUrl
	 *            : Application URL
	 * @param driverName
	 *            : Name of the driver.
	 * @return ResellerLoginPage object
	 * @throws Exception
	 */
	public static ResellerLoginPage navigateTo_URL(WebDriver driver, String baseUrl,
			String driverName) throws Exception {

		// navigate to RS URL
		if (driverName.equalsIgnoreCase("CHROME")){
			driver.get(baseUrl);	
		}else if (driverName.equalsIgnoreCase("IE")
				|| driverName.equalsIgnoreCase("SAFARI")) {
			driver.get(baseUrl);
			Thread.sleep(10000);
			if(CustomFun.isElementVisible(browserOverride, driver))
			{
				driver.navigate().to("javascript:document.getElementById('overridelink').click()");
			}		
		}

		return new ResellerLoginPage(driver);		
	}	

	/**
	 * This function Enter Username, Password & Click Submit
	 * 
	 * @return: AMQuoteListing object
	 * @throws Exception
	 */
	public ResellerSelfServicePage enterLoginCreds(String username,String pwd)throws Exception {	

		// Entering email
		GUIFunctions.typeTxtboxValue(driver, userName_XPATH, username);

		// Entering password
		GUIFunctions.typeTxtboxValue(driver, password_XPATH, pwd);

		// Click on Login button
		GUIFunctions.clickElement(driver, submit_XPATH, "Sign in Button");
		//		Thread.sleep(2000);

		CustomFun.waitForPageLoaded(driver);
		return new ResellerSelfServicePage(driver);	
	}
}
