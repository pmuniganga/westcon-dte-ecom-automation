package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;
import in.valtech.westcon.QuotingPages.QuotingQuotesPage;

public class TC20_Quoting_Revision_Request extends BaseTest{

	static String revisionNumber = "";
	static String quoteNumber = "";
	static QuotingQuotesPage quotingQuotesPage;

	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 TC08 TCO9", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Eexecuted ");	
		// Execute TC-08 (without Opportunity)
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Eexecuted ");		
		log.info("TC-09 Successfully Executed \n");
		Reporter.log("<p>TC-09 Successfully Eexecuted ");	

		log.info("Create Quote page is displayed with End user selected");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}

	@Test(description = "Step 2 Select Submit quote from the dropdown and click on GO button", priority = 2)
	public void Step02_SelectDropdownAndSubmitQuoteFrom()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		
		amQuoteDetailsPage.selectDropdownAndSubmitQuoteFrom();

		log.info("Successfully Select Submit quote from the dropdown and click on GO button\n");
		Reporter.log("<p>Successfully Select Submit quote from the dropdown and click on GO button\n");

	}

	/**
	 * Click On No, do not validate button
	 * 
	 * @throws Exception
	 */
	String initialRevesionID=null;
	@Test(description = "Step 3: Click On No, do not validate button", priority = 3)
	public void Step03_ClickOnDoNotValidateButton1()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	

		amQuoteDetailsPage.ClickOnDoNotValidateButton();

		System.out.println(driver.findElement(
				By.xpath(ObjRepoProp
						.getProperty("successfullMsg_XPATH")))
						.getText().trim());

		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("successfullMsg_XPATH")))
								.getText().trim(), TextProp
								.getProperty("successfullMsg"),
				"Quote submission is not proper");
		
		initialRevesionID=revesionID;		
		System.out.println("initial revision  --- "+initialRevesionID);
		
		log.info("Successfully clicked on No,do not validate and verified Success message\n");
		Reporter.log("<p>Successfully clicked on No,do not validate and verified Success message\n");

	}

	/**
	 * Retrieve revision number.. 
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4:Retrieve & verify Revision Number", priority = 4)
	public void Step04_RetrieveVerifyRevisionNumber()throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
		GUIFunctions.pageScrollDown(driver, 1500);
		amQuoteDetailsPage.getValues();		

		log.info("Successfully Retrieved Revision Number\n");
		Reporter.log("<p>Successfully Retrieved Revision Number\n");

	}
	
	@Test(description = "Step 5: Click on Quotes tab", priority = 5)
	public void Step05_ClickQuotesTab() throws Exception
	{
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 1500);

		amQuoteDetailsPage.ClickOnQuotesTab(driver);
		
		log.info("Successfully Clicked on Quotes Tab\n");
		Reporter.log("<p>Successfully Clicked on Quotes Tab");
	
	}
	
	@Test(description = "Step 6 : Search for the submitted quote", priority = 6)
	public void Step06_SearchForQuote() throws Exception {
		QuotingQuotesPage amQuotesPage = new QuotingQuotesPage(driver);
		amQuotesPage.EnterQuoteIDAndSearch(driver);
		
		log.info("Successfully searched created Quote\n");
		Reporter.log("<p>Successfully searched created Quote");
	}
	
	@Test(description = "Step 7 : Verify the total price and Revision", priority = 7)
	public void Step06_VerifyTotalPriceAndRevision() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.verifyTotalQuotePrice(driver);
		
		String updatedRevisionNumber =revesionID;
		Assert.assertFalse(updatedRevisionNumber.equals(initialRevesionID));
		
		log.info("Successfully verified the Quote total price\n");
		Reporter.log("<p>Successfully verified the Quote total price");
	}

}
