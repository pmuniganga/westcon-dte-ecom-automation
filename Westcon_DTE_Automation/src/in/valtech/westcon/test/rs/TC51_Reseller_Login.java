package in.valtech.westcon.test.rs;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.westcon.ResellerPages.ResellerLoginPage;

public class TC51_Reseller_Login extends BaseTest
{
	static ResellerLoginPage rsLoginPage;
	
	static String username = null;
	static String password = null;
	/**
	 * Open browser,Navigate to the reseller URL
	 * 
	 */
	@Test(description = "Step 1: Open browser,Navigate to the reseeler URL", priority = 1)
	public void Step01_NavigateToWestconReseller_URL() throws Exception {

		// Navigate/launch the Reseller URL
		rsLoginPage = ResellerLoginPage.navigateTo_URL(driver, baseUrl, driverName);
		log.info("Navigated to the Reseller URL");
		Reporter.log("<p>Navigated to Reseller Login Page URL -- "
				+ driver.getTitle());
	}
	
	/**
	 * Enter user name , Password and click on Login link
	 * User will be landed in Self 
	 */
	@Test(description = "Step 2: Enter username , Pwd and click on Login link", priority = 2)
	public void Step02_ResellerLogin() throws Exception {

		username = userInfoDSDetails.getRsUserName();		
		password = userInfoDSDetails.getRsPassword();

		ResellerLoginPage rsLoginPage = new  ResellerLoginPage(driver);
		rsLoginPage.enterLoginCreds(username, password);
		log.info("Reseller successfully logged-in and navigated to SelfSerivice page\n");
		Reporter.log("<p>Reseller successfully logged-in and navigated to SelfSerivice page");
	}

}
