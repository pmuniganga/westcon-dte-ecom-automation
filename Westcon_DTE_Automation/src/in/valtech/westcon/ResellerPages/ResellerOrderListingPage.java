package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

public class ResellerOrderListingPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller login page Constructor
	public ResellerOrderListingPage(WebDriver driver) {


		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		// Check that we're on the right AM login Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerOrderListingPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Reseller Order Listing page");
		}

	}
	By OrderNumberTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("orderNumberTextBox_XPATH"));
	By firstWestconSaleOrderNumRow_XPATH=By.xpath(ObjRepoProp.getProperty("firstWestconSaleOrderNumRow_XPATH"));
	By searchOrderDropDown_XPATH=By.xpath(ObjRepoProp.getProperty("searchOrderDropDown_XPATH"));
	By searchOrderTextBox_XPATH=By.xpath(ObjRepoProp.getProperty("searchOrderTextBox_XPATH"));

	public ResellerOrderListingPage searchCreatedOrder(String OrderNum) throws Exception
	{
		Thread.sleep(2000);
		CustomFun.waitObjectToLoad(driver, searchOrderDropDown_XPATH, 60);

		if (BaseTest.driverName.equalsIgnoreCase("FF")
				|| BaseTest.driverName.equalsIgnoreCase("CHROME"))
		{
			driver.findElement(searchOrderDropDown_XPATH).click();
		}else{
			
			WebElement ContainerOH = driver.findElement(searchOrderDropDown_XPATH);
			ContainerOH.click();
		}
		
		CustomFun.waitObjectToLoad(driver, searchOrderTextBox_XPATH, 60);
		GUIFunctions.typeTxtboxValue(driver, searchOrderTextBox_XPATH, "Sale Order Number");
		GUIFunctions.keyPressEnter(driver);

		GUIFunctions.typeTxtboxValue(driver, OrderNumberTextBox_XPATH, OrderNum);
		GUIFunctions.keyPressEnter(driver);
		CustomFun.waitObjectToLoad(driver, firstWestconSaleOrderNumRow_XPATH, 120);
		String FirstOrderNum = driver.findElement(firstWestconSaleOrderNumRow_XPATH).getText();

		Assert.assertEquals(FirstOrderNum, OrderNum);

		return new ResellerOrderListingPage(driver);
	}

	//Function to click on order id
	By orderId=By.xpath(ObjRepoProp.getProperty("orderId_XPATH"));
	public Reseller_OrderDetailPage ClickOrderId() throws Exception
	{
		GUIFunctions.clickElement(driver, orderId, "Order Id is clicked");
		return new Reseller_OrderDetailPage(driver);
	}
}
