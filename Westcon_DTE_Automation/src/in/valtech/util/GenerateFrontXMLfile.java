package in.valtech.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import Fillo.Recordset;

public class GenerateFrontXMLfile {

	public static void generateXMLfile() throws Exception {

		List<String> featureNameList = ExcelReader
				.getGlobalFeatureSheets("resources/front/Automation_DS.xls");

		for (int i = 0; i < featureNameList.size(); i++) {

			generateModuleXMLFile(featureNameList.get(i).toLowerCase());



		}

	}

	public static void generateModuleXMLFile(String module) {

		int i = 0;
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements :Suite
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("suite");
			doc.appendChild(rootElement);
			Element listeners = doc.createElement("listeners");
			rootElement.appendChild(listeners);
			// Element listener = doc.createElement("listener");
			listeners.setAttribute("class-name",
					"org.uncommons.reportng.HTMLReporter");
			Element listener1 = doc.createElement("listener");
			listener1.setAttribute("class-name",
					"org.uncommons.reportng.JUnitXMLReporter");

			// Set the name attribute
			Attr attr = doc.createAttribute("name");
			attr.setValue("Westcon Automation :" + module.toUpperCase());
			rootElement.setAttributeNode(attr);
			Recordset recordset = ExcelReader.getFilloConnection(
					"resources/front/Automation_DS.xls", module.toUpperCase());


			while (recordset.next()) {

				if (recordset.getField("EXECUTE").equalsIgnoreCase("YES")) {
					i++;

					// tests elements
					Element tests = doc.createElement("test");
					rootElement.appendChild(tests);
					// Set the name attribute
					Attr attr1 = doc.createAttribute("name");
					attr1.setValue(recordset.getField("ACTUAL_TC") + "_"
							+ recordset.getField("LOCALE") + "_"
							+ recordset.getField("DRIVER") + "_"
							+ recordset.getField("BREAKPOINT") + "_"
							+ recordset.getField("ORIENTATION"));

					tests.setAttributeNode(attr1);

					// parameter Relative_test case
					Element par = doc.createElement("parameter");
					tests.appendChild(par);
					// Set the name attribute
					Attr attr2 = doc.createAttribute("name");
					attr2.setValue("RELATIVE_TC");
					par.setAttributeNode(attr2);
					Attr attr21 = doc.createAttribute("value");

					attr21.setValue(recordset.getField("RELATIVE_TC"));
					par.setAttributeNode(attr21);

					// parameter orientation
					Element par2 = doc.createElement("parameter");
					tests.appendChild(par2);
					// Set the name attribute
					Attr attr3 = doc.createAttribute("name");
					attr3.setValue("ORIENTATION");
					par2.setAttributeNode(attr3);
					Attr attr31 = doc.createAttribute("value");
					attr31.setValue(recordset.getField("ORIENTATION"));
					par2.setAttributeNode(attr31);

					// parameter
					Element par3 = doc.createElement("parameter");
					tests.appendChild(par3);
					// Set the name attribute
					Attr attr4 = doc.createAttribute("name");
					attr4.setValue("LOCALE");
					par3.setAttributeNode(attr4);
					Attr attr41 = doc.createAttribute("value");
					attr41.setValue(recordset.getField("LOCALE"));
					par3.setAttributeNode(attr41);

					Element par5_1 = doc.createElement("parameter");
					tests.appendChild(par5_1);
					// Set the name attribute
					Attr attr12 = doc.createAttribute("name");
					attr12.setValue("DS_CONFIG");
					par5_1.setAttributeNode(attr12);
					Attr attr612 = doc.createAttribute("value");
					attr612.setValue(recordset.getField("DS_CONFIG"));
					par5_1.setAttributeNode(attr612);

					// parameter
					Element par5 = doc.createElement("parameter");
					tests.appendChild(par5);
					// Set the name attribute
					Attr attr6 = doc.createAttribute("name");
					attr6.setValue("APPLICATION");
					par5.setAttributeNode(attr6);
					Attr attr61 = doc.createAttribute("value");
					attr61.setValue("FRONT");
					par5.setAttributeNode(attr61);

					// parameter
					Element par6 = doc.createElement("parameter");
					tests.appendChild(par6);
					// Set the name attribute
					Attr attr7 = doc.createAttribute("name");
					attr7.setValue("ACTUAL_TC");
					par6.setAttributeNode(attr7);
					Attr attr71 = doc.createAttribute("value");
					attr71.setValue(recordset.getField("ACTUAL_TC"));
					par6.setAttributeNode(attr71);

					// parameter
					Element par7 = doc.createElement("parameter");
					tests.appendChild(par7);
					// Set the name attribute
					Attr attr8 = doc.createAttribute("name");
					attr8.setValue("BREAKPOINT");
					par7.setAttributeNode(attr8);
					Attr attr81 = doc.createAttribute("value");
					attr81.setValue(recordset.getField("BREAKPOINT"));
					par7.setAttributeNode(attr81);

					// parameter
					Element par8 = doc.createElement("parameter");
					tests.appendChild(par8);
					// Set the name attribute
					Attr attr9 = doc.createAttribute("name");
					attr9.setValue("URL");
					par8.setAttributeNode(attr9);
					Attr attr91 = doc.createAttribute("value");
					attr91.setValue(recordset.getField("URL"));
					par8.setAttributeNode(attr91);

					// parameter
					Element par9 = doc.createElement("parameter");
					tests.appendChild(par9);
					// Set the name attribute
					Attr attr10 = doc.createAttribute("name");
					attr10.setValue("DRIVER");
					par9.setAttributeNode(attr10);
					Attr attr101 = doc.createAttribute("value");
					attr101.setValue(recordset.getField("DRIVER"));
					par9.setAttributeNode(attr101);

					// classes
					Element classes = doc.createElement("classes");
					tests.appendChild(classes);

					String[] array = null;
					ArrayList<String> depclasses = new ArrayList<String>();
					if (!recordset.getField("DEPENDENCIES").equalsIgnoreCase(
							"NA")) {
						array = recordset.getField("DEPENDENCIES").split(",");

						Recordset resultset = ExcelReader.getFilloConnection(
								"resources/front/Automation_DS.xls",
								module.toUpperCase());
						while (resultset.next()) {
							for (String e : array) {

								if (e.equalsIgnoreCase(resultset
										.getField("RELATIVE_TC"))) {
									String class_name = resultset
											.getField("ACTUAL_TC");
									String feature_name=resultset.getField("FEATURE");
									Element class_names = doc
											.createElement("class");
									classes.appendChild(class_names);
									Attr depclass = doc.createAttribute("name");
									depclass.setValue("in.valtech.westcon.test."
											+ feature_name.toLowerCase()+ "." + class_name);
									class_names.setAttributeNode(depclass);

								}
							}
						}
					}

					Element class_names = doc.createElement("class");
					classes.appendChild(class_names);
					Attr attr11 = doc.createAttribute("name");
					attr11.setValue("in.valtech.westcon.test." +recordset.getField("FEATURE").toLowerCase() + "."
							+ recordset.getField("ACTUAL_TC"));
					class_names.setAttributeNode(attr11);

					// write xml to file
					TransformerFactory transformerFactory = TransformerFactory
							.newInstance();

					Transformer transformer = transformerFactory
							.newTransformer();
					transformer.setOutputProperty(OutputKeys.INDENT, "yes");
					transformer.setOutputProperty(
							"{http://xml.apache.org/xslt}indent-amount", "2");
					DOMSource source = new DOMSource(doc);

					StreamResult result = new StreamResult(new File(
							System.getProperty("user.dir")
							+ "/testSuites/front/TestSuite_" + module
							+ ".xml").getPath());

					transformer.transform(source, result);			
				}

			}
			System.out.println("List Size : " + i);
			System.out.println("Feature name: " + module);
		} catch (Exception e) {

		}

	}

	public static void main(String args[]) throws Exception {

		generateXMLfile();
	}
}
