package in.valtech.westcon.QuotingPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static in.valtech.custom.CustomFun.userInfoDSDetails;

public class QuotingSelectResellerPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// AM Reseller Listing page Constructor
	public QuotingSelectResellerPage(WebDriver driver) {
		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		// Check that we're on the right AM Reseller Listing Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("quotingSelectResellerPage_XPATH")),
						driver))) {
			throw new IllegalStateException("This is not Quoting Select Reseller page");		
		}
	}

	By searchResellerTextbox_XPATH = By.xpath(ObjRepoProp.getProperty("searchResellerTextbox_XPATH"));
	By sellerAccountNum1stRow_XPATH = By.xpath(ObjRepoProp.getProperty("sellerAccountNum1stRow_XPATH"));

	/***
	 * This function will enter reseller ID and search
	 */
	public QuotingSelectResellerPage enterResellerAndSearch(String resellerId)throws Exception {	
		CustomFun.waitForPageLoaded(driver);
		// Enter the reseller id 
		GUIFunctions.typeTxtboxValue(driver, searchResellerTextbox_XPATH, resellerId);
		// Enter the reseller id 
		GUIFunctions.keyPressEnter(driver);
		/*driver.findElement(By.xpath(ObjRepoProp
				.getProperty("searchResellerTextbox_XPATH"))).sendKeys(Keys.ENTER);*/
		CustomFun.waitObjectToLoad(driver, sellerAccountNum1stRow_XPATH, 60);
		return new QuotingSelectResellerPage(driver);	
	}

	/***
	 * This function is to select the first reseller in the list displayed in table
	 */
	public QuotingAddProductsPage selectFirstReseller()throws Exception {
		// Select the reseller id 
		CustomFun.waitObjectToLoad(driver, sellerAccountNum1stRow_XPATH, 100);
		
		By selectResellerAccount = By.xpath("//table[@id='reseller-table']/tbody/tr/td[contains(text(),'"+userInfoDSDetails.getSalesOrg()+"')]/preceding-sibling::td[1]/a[contains(text(),'"+userInfoDSDetails.getReseller()+"')]");
		
		GUIFunctions.clickElement(driver, selectResellerAccount, 
				"click on account number");
		CustomFun.waitForPageLoaded(driver);
		return new QuotingAddProductsPage(driver);
	}
}
