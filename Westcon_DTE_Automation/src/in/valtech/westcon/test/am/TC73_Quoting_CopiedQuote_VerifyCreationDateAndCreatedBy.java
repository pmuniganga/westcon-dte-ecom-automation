package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;

import java.util.Date;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC73_Quoting_CopiedQuote_VerifyCreationDateAndCreatedBy extends BaseTest{
	
	String quoteCreationDate=null;
	String quoteCreatedBy=null;
	/**
	 * User to Submit quote in quoting by executing TC-11
	 * 
	 */

	@Test(description = "Step 1: User to submit quote  by executing TC11", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");	
		// Execute TC-09 
		log.info("TC-09 Successfully Executed \n");
		Reporter.log("<p>TC-09 Successfully Executed ");
		// Execute TC-46 
		log.info("TC-46 Successfully Executed \n");
		Reporter.log("<p>TC-46 Successfully Executed ");

		log.info("Copied Quote details page is displayed with Requester Info, End user details, VRF fields  selected");
		Reporter.log("<p>Copied Quote details page is displayed with Requester Info, End user details, VRF fields  selected");	
	}
	
	/**
	 * Click on Comments & History tab
	 *  @throws Exception
	 */
	@Test(description = "Step 2: Click on Comments & History tab", priority =2)
	public void Step02_ClickonCommentsAndHistoryTab() throws Exception {
		
		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
	 	
	    //Click on Comments & History tab		
        amQuoteDetailsPage.clickOnQuoteCommentandHistoryTab(driver);
        
		log.info("Successfully Clicked on Comments & History tab\n");
		Reporter.log("<p>Successfully Clicked on Comments & History tab\n");

	}
	
	/**
	 * Verify Quote Creation Date & Created By
	 *  @throws Exception
	 */
	@Test(description = "Step 3: Verify Quote Creation Date & Created By", priority =3)
	public void Step03_VerifyQuoteCreationDateAndCreatedBy() throws Exception {
		
	//	QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		 	
		
	    
		String expCreationDate=CustomFun.addDays(new Date(),0, locale);
    	String expCreatedBy=driver.findElement(By.xpath(ObjRepoProp.getProperty("LoginUserName_XPATH"))).getText().split("\\|")[2];
		quoteCreationDate=driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCreationDateValue_XPATH"))).getText().trim();
		quoteCreatedBy=driver.findElement(By.xpath(ObjRepoProp.getProperty("QuoteCreatedByValue_XPATH"))).getText().trim();
		
		//Verify Quote Creation Date
		System.out.println("Quote Creation Date*****"+quoteCreationDate);
		System.out.println("Quote Created By*****"+quoteCreatedBy);
		System.out.println("expCreatedBy****"+expCreatedBy.trim());
    	System.out.println("expCreationDate****"+expCreationDate);

 		Assert.assertEquals(quoteCreationDate,expCreationDate,"Quote Creation date mismatch");
 		Assert.assertTrue(driver.findElement(By.xpath(ObjRepoProp.getProperty("LoginUserName_XPATH"))).getText().contains(quoteCreatedBy), "Quote Created By mismatch");
       
        
		log.info("Successfully Verified Quote Creation Date & Created By\n");
		Reporter.log("<p>Successfully Verified Quote Creation Date & Created By\n");

	}
	
}
