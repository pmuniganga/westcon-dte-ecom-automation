package in.valtech.westcon.QuotingPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class QuoteToOrderConfirmationPage {
	
	private final WebDriver driver;
	GUIFunctions gui = new GUIFunctions();
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// AM login page Constructor
	public QuoteToOrderConfirmationPage(WebDriver driver) {


		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("quotingOrderConfirmationPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Quoting Order Confirmation page");
		}
	}

	/** This function will fetch sales order number from order confirmation page
	 * 
	 */
	public String salesorderNumber;
	By OrderConfirmationHdr = By.xpath(ObjRepoProp.getProperty("orderConfirmationHdr_XPATH"));
	public String FetchOrderNumber() throws Exception
	{

		String orderNumber=driver.findElement(OrderConfirmationHdr).getText();
		salesorderNumber = orderNumber.substring(27, orderNumber.length());
		System.out.println("orderNumber1="+salesorderNumber);
		return orderNumber;
		
	}
}
