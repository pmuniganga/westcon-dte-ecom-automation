package in.valtech.westcon.ResellerPages;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;

import org.apache.log4j.Logger;
import org.apache.poi.hslf.model.textproperties.TextProp;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Reseller_QuoteDeatilPage {
	private final WebDriver driver;
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// Reseller quote detail page Constructor
	public  Reseller_QuoteDeatilPage (WebDriver driver) throws InterruptedException {

		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		//Thread.sleep(10000);
		// Check that we're on the right quote detail page
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("resellerQuoteDeatailpage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Quote Deatil  page");

		}
	}
	//This function is to select convert to order option

	By selectActionDropDown=By.xpath(ObjRepoProp.getProperty("selectActionDropDown_XPATH"));

	public Reseller_QuoteDeatilPage SelectValueFromActionDropdown(String dropdown)throws Exception
	{
		driver.navigate().refresh();		
		Thread.sleep(5000);
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();",
				driver.findElement(selectActionDropDown));
		GUIFunctions.selectDropDownValue(driver.findElement(selectActionDropDown),"2","index");
		return new Reseller_QuoteDeatilPage(driver);
	}

	//This function is to click on submit button
	By QuotedetailPageSubmit=By.xpath(ObjRepoProp.getProperty("QuotedetailPageSubmit_XPATH"));
	public  Reseller_ConvertToOrderPage SubmitClick()throws Exception
	{
		//To click on submit button
		GUIFunctions.clickElement(driver, QuotedetailPageSubmit, "Submit clicked");
		return new Reseller_ConvertToOrderPage(driver);
	}

}