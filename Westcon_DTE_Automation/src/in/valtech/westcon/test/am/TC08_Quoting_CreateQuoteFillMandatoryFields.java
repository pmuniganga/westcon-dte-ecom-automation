package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TC08_Quoting_CreateQuoteFillMandatoryFields extends BaseTest {

	//static QuotingQuoteDetailpage amQuoteDetailsPage;

	/**
	 * Call TC06 (without Opportunity)
	 * 
	 */	

	@Test(description = "Step 1: Call TC01 TC02 TC03 TC06 (without Opportunity) / Call TC07 (with Opportunity)", priority = 1)
	public void Step01_Call() throws Exception {
		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");

		log.info("Create Quote page is displayed with End user selected\n");
		Reporter.log("<p>Create Quote page is displayed with End user selected");
	}

	/**
	 * Select Quote exp date , Quote close date, Enter probability
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 2: Select Quote exp date , Quote close date, Enter probability", priority = 2)
	public void Step02_Enter_Quoteexpdate_Quoteclosedate_probability()
			throws Exception {

		QuotingQuoteDetailpage amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);

		System.out.println("Date ="+new Date());

		amQuoteDetailsPage.enterQuoteexpdate_Quoteclosedate_probability(CustomFun
				.addDays(new Date(), Integer.parseInt(userInfoDSDetails
						.getQuoteCloseDate().trim()), locale),CustomFun
						.addDays(new Date(), Integer.parseInt(userInfoDSDetails
								.getQuoteCloseDate().trim()), locale), userInfoDSDetails.getProbability());	

		log.info("Successfully Enter Quote exp date,Quote close date and probability\n");
		Reporter.log("<p>Successfully Enter Quote exp date,Quote close date and probability");

	}

	/**
	 * Click on Requester info tab
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 3: Click on Requester info tab", priority = 3)
	public void Step03_ClickonRequesterinfotab() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.clickOnRequesterinfotab();

		log.info("Successfully Click on Requester info tab\n");
		Reporter.log("<p>Successfully Click on Requester info tab");

	}

	/**
	 * Click Requester name pencil icon and enter requester name
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 4: Click Requester name pencil icon and enter requester name", priority = 4)
	public void Step04_EnterRequesterName() throws Exception {
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterName(TextProp.getProperty("RequesterName"));

		log.info("Successfully Click Requester name pencil icon and enter requester name\n");
		Reporter.log("<p>Successfully Click Requester name pencil icon and enter requester name");

	}

	/**
	 * Click Requester email pencil icon and enter requester email
	 * 
	 * @throws Exception
	 */
	@Test(description = "Step 5: Enter requester email", priority = 5)
	public void Step05_EnterRequesterEmail() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		amQuoteDetailsPage.enterRequesterEmail(TextProp.getProperty("RequesterEmail"));

		log.info("Successfully entered requester email\n");
		Reporter.log("<p>Successfully entered requester email");

	}

	@Test(description = "Step 6: Select Sales office from drop down", priority = 6)
	public void Step06_SelectSalesOffice() throws Exception {

		Thread.sleep(2000);
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);

		if(!locale.equalsIgnoreCase("US") && !locale.equalsIgnoreCase("CA")) {
			amQuoteDetailsPage.selectSalesOffice(TextProp.getProperty("SalesOfficeValue_UK"));
		}


		log.info("Successfully Selected Sales office\n");
		Reporter.log("<p>Successfully Selected Sales office");

	}

	@Test(description = "Step 7 : Unselect Pipeline checkbox", priority = 7)
	public void Step07_UncheckPipelineCheckbox() throws Exception {
		//Unselect Pipeline checkbox
		QuotingQuoteDetailpage amQuoteDetailsPage = new QuotingQuoteDetailpage(driver);
		if(locale.equalsIgnoreCase("UK")) {
			amQuoteDetailsPage.UncheckPipeLineCheckbox(driver);
		}

		log.info("Successfully unselected pipeline checkbox\n");
		Reporter.log("<p>Successfully unselected pipeline checkbox");
	}


}
