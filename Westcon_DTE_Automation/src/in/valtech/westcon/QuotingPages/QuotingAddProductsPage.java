package in.valtech.westcon.QuotingPages;

import static in.valtech.util.PropertyFileReader.TextProp;
import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import static in.valtech.custom.CustomFun.SKUdataSetList;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;


public class QuotingAddProductsPage {
	private final WebDriver driver;

	GUIFunctions gui = new GUIFunctions();
	// For logging
	public Logger log = Logger.getLogger(this.getClass().getName());

	// AM Add Products page Constructor
	public QuotingAddProductsPage(WebDriver driver) {
		this.driver = driver;
		CustomFun.waitForPageLoaded(driver);
		// Check that we're on the right AM Add Products Page.
		if (!(CustomFun
				.isElementPresent(
						By.xpath(ObjRepoProp.getProperty("quotingAddProductPage_XPATH")),
						driver))) {

			throw new IllegalStateException("This is not Quoting Add Product page");
		} 
	}

	//**********************BY - path**********************//

	static By BOMDefaultTextVeify=By.xpath(ObjRepoProp.getProperty("BOMDefaultTextVeify_XPATH"));
	static By BOMDefaultText=By.xpath(TextProp.getProperty("BOMDefaultText"));
	static By CopyPasteBOMTextBox=By.xpath(ObjRepoProp.getProperty("CopyPasteBOMTextBox_XPATH"));
	static By CopyPasteBOMSubmit=By.xpath(ObjRepoProp.getProperty("CopyPasteBOMSubmit_XPATH"));
	static By VendorTabVerify=By.xpath(ObjRepoProp.getProperty("VendorTabVerify_XPATH"));
	static By VendorTabTextVerify=By.xpath(ObjRepoProp.getProperty("VendorTabTextVerify_XPATH"));
	static By VendorTabTextVerify1=By.xpath(TextProp.getProperty("VendorTabTextVerify"));
	By VendorSelectTable=By.xpath(ObjRepoProp.getProperty("VendorSelectTable_XPATH"));
	static By VendorRefNoTxtBox=By.xpath(ObjRepoProp.getProperty("VendorRefNoTxtBox_XPATH"));
	static By VendorSubmitButton=By.xpath(ObjRepoProp.getProperty("VendorSubmitButton_XPATH"));
	By copyPasteBOMTabOne_XPATH = By.xpath(ObjRepoProp.getProperty("copyPasteBOMTabOne_XPATH"));
	By pencilIcon = By.xpath(ObjRepoProp.getProperty("editIcon_XPATH"));
	By uploadFile_xpath =By.xpath(ObjRepoProp.getProperty("uploadFileButton_XPATH"));
	By browseButton_xpath=By.xpath(ObjRepoProp.getProperty("browseButton_XPATH"));
	By selectVendor_XPATH=By.xpath(ObjRepoProp.getProperty("selectVenforDropdown_XPATH"));
	By UploadAFileSubmit=By.xpath(ObjRepoProp.getProperty("uploadFileSubmit_XPATH"));
	By quotingQuoteDetailpage = By.xpath(ObjRepoProp.getProperty("quotingQuoteDetailpage_XPATH"));


	/**  
	 * This function is to verify the CopyPaste BOM Text 	 
	 **/
	public void CopyPasteBOMVerify(WebDriver driver){
		Assert.assertEquals(driver.findElement(BOMDefaultTextVeify), 
				driver.findElement(BOMDefaultText));
		//If not throw IllegalStateException
		throw new IllegalStateException("Message displayed is incorrect");
	}

	/**  
	 * This function is to enter product in CopyPaste BOM 	 
	 **/
	public QuotingAddProductsPage CopyPasteBOM(WebDriver driver, String skuData) {
		
		GUIFunctions.typeTxtboxValue(driver, CopyPasteBOMTextBox, skuData+Keys.ENTER);
		System.out.println("Entered the PartNumbers");
		return new QuotingAddProductsPage(driver);
	}
	
	
	/**  
	 * This function is to click on submit in CopyPaste BOM 	 
	 **/
	public QuotingAddProductsPage CopyPasteBOMSubmit_ErrorValidation(WebDriver driver) {
		GUIFunctions.clickElement(driver,CopyPasteBOMSubmit , "Submit");
		//System.out.println("----------- Waiting");
		//CustomFun.waitObjectToLoad(driver,pencilIcon,30);
		System.out.println("submitted successfully");
		//CustomFun.waitObjectToLoad(driver, pencilIcon, 30);
		return new QuotingAddProductsPage(driver);
	}
	
	/**  
	 * This function is to click on submit in CopyPaste BOM 	 
	 **/
	public QuotingQuoteDetailpage CopyPasteBOMSubmit(WebDriver driver) {
		GUIFunctions.clickElement(driver,CopyPasteBOMSubmit , "Submit");
		//System.out.println("----------- Waiting");
		//CustomFun.waitObjectToLoad(driver,pencilIcon,30);
		System.out.println("submitted successfully");
		CustomFun.waitObjectToLoad(driver, quotingQuoteDetailpage, 30);
		return new QuotingQuoteDetailpage(driver);
	}

	/**  
	 * This function is to verify Vendor RefNo tab	 
	 **/
	public QuotingAddProductsPage VendorRefNoTabAssertion(WebDriver driver){
		Boolean flag=driver.findElement(VendorTabVerify).isDisplayed();
		Assert.assertTrue(flag, "Vendor tab is displayed");	
		return new QuotingAddProductsPage(driver);
	}

	/**  
	 * This function is to click on Vendor RefNo tab	 
	 **/
	public QuotingAddProductsPage VendorRefNo(WebDriver driver){
		GUIFunctions.clickElement(driver, VendorTabVerify, "Vendor Tab");
		System.out.println("In Vendor Ref no tab");
		return new QuotingAddProductsPage(driver);
	}

	/**  
	 * This function is to verify text of Vendor RefNo tab	 
	 **/
	public QuotingAddProductsPage VendorText(WebDriver driver){
		Assert.assertEquals(
				driver.findElement(
						By.xpath(ObjRepoProp
								.getProperty("VendorTabTextVerify_XPATH")))
								.getText().trim(), TextProp
								.getProperty("VendorTabTextVerify"),
				"Vendor tab description not displayed");		
		return new QuotingAddProductsPage(driver);
	}

	/**  
	 * This function is to select vendor from drop down 
	 **/
	public QuotingAddProductsPage VendorDDL(WebDriver driver,String vendor) throws Exception{
		GUIFunctions.selectDropDownValue(driver.findElement(VendorSelectTable),vendor,"value");
		System.out.println("Selected the vendor ");
		return new QuotingAddProductsPage(driver);
	}

	/**  
	 * This function is to enter vendor RefNo 
	 **/
	public QuotingAddProductsPage VendorRefNoTextBox(WebDriver driver, String data) {
		GUIFunctions.typeTxtboxValue(driver,VendorRefNoTxtBox , data);
		System.out.println("Entered the PartNumbers");
		return new QuotingAddProductsPage(driver);
	}

	/**  
	 * This function is to submit details under Vendor RefNo Tab
	 **/
	public QuotingQuoteDetailpage VendorSubmitButton(WebDriver driver) {
		GUIFunctions.clickElement(driver,VendorSubmitButton , "Submit");
		CustomFun.waitObjectToLoad(driver, pencilIcon,60);
		System.out.println("submitted successfully");
		return new QuotingQuoteDetailpage(driver);
	}

	/** THis Function is to click on upload file in Add product page 
	 * 
	 * **/
	public QuotingAddProductsPage ClickOnUploadFile() throws Exception{
		//Click on upload file
		GUIFunctions.clickElement(driver, uploadFile_xpath," uploadFile ");
		return new QuotingAddProductsPage(driver);
	}

	/**  
	 * This function is to select vendor under UploadAFile tab
	 **/
	public QuotingAddProductsPage SelectVendor(String vendorname) throws Exception{
		//Click on upload file
		GUIFunctions.selectDropDownValue(driver.findElement(By.xpath(ObjRepoProp.
				getProperty("selectVenforDropdown_XPATH"))), vendorname, "value");

		return new QuotingAddProductsPage(driver);		 
	}

	/**  
	 * This function is to upload a file 
	 **/
	public QuotingAddProductsPage EnterUploadFilePath() throws Exception{
		//		String dir = CustomFun.getRootDir();	
		//		
		//		for (int i = 0; i < SKUdataSetList.size(); i++) {
		//			String path = dir+SKUdataSetList.get(i).getVendorFilePath();
		//			driver.findElement(browseButton_xpath).sendKeys(path);
		//		}

		String path = null;		
		String dir = CustomFun.getRootDir();	
		System.out.println(dir);				
		for (int i = 0; i < SKUdataSetList.size(); i++) {
			path = dir+SKUdataSetList.get(i).getVendorFilePath();
			System.out.println(path);			
		}
		if (BaseTest.driverName.equalsIgnoreCase("FF")
				|| BaseTest.driverName.equalsIgnoreCase("CHROME"))
		{			
			driver.findElement(browseButton_xpath).sendKeys(path);
		}else {
			// For IE
			WebElement browseBtn =driver.findElement(browseButton_xpath);			
			//browseBtn.sendKeys(path);
			if (BaseTest.locale.equalsIgnoreCase("US"))
			{
				browseBtn.sendKeys("D:\\Westcon_Auto_Workspace\\Westcon DTE Automation\\resources\\Bartercard Group Series Options.xlsx");
			}else
			{
				browseBtn.sendKeys("D:\\Westcon_Auto_Workspace\\Westcon DTE Automation\\resources\\AudioCodes Support - SPANSION 10.14.15REV.XLSX");

			}
			//GUIFunctions.clickElement(driver, browseButton_xpath," Browse button ");	
		}	

		//driver.findElement(browseButton_xpath).sendKeys("D://Westcon-DTE-Ecom//Westcon_Auto_Workspace//Westcon DTE Automation//resources//Bartercard Group Series Options.xlsx");
		//GUIFunctions.clickElement(driver, browseButton_xpath," Browse button ");		
		return new QuotingAddProductsPage(driver);
	}

	/**  
	 * This function is to verify CopyPasteBOM is selected or not
	 * Will return Boolean true or false value
	 **/
	public QuotingAddProductsPage verifyCopyPasteBOMTabOne()throws Exception {	
		// click the reseller id 
		WebElement FirstTab = driver.findElement(copyPasteBOMTabOne_XPATH);
		CustomFun.isTabSelected(FirstTab, "copyPasteBOMTabOne");

		return new QuotingAddProductsPage(driver);		
	}

	/**  
	 * This function is to click on submit under UploadAFile tab
	 * @throws Exception 
	 **/
	public QuotingQuoteDetailpage UploadAFileSubmit(WebDriver driver) throws Exception {
		GUIFunctions.clickElement(driver,UploadAFileSubmit , "Submit");
		//CustomFun.waitForPageLoaded(driver);
		CustomFun.waitObjectToLoad(driver,pencilIcon,60);
		Thread.sleep(5000);
		GUIFunctions.pageScrollDown(driver, 200);
		System.out.println("submitted successfully");
		return new QuotingQuoteDetailpage(driver);
	}	
	
	By productInputDropdown_XPATH = By.xpath(ObjRepoProp.getProperty("productInputDropdown_XPATH"));
	
	/**  
	 * This function is to select type of input from dropdown
	 * @throws Exception 
	 **/
	public QuotingAddProductsPage selectInputFromDropdown(WebDriver driver, String inputType) throws Exception {
		//Select dropdownValue = new Select(driver.findElement(productInputDropdown_XPATH));
		GUIFunctions.pageScrollDown(driver, 400);
		GUIFunctions.selectDropDownValue(driver.findElement(productInputDropdown_XPATH), inputType, "value");
		return new QuotingAddProductsPage(driver);
	}
}
