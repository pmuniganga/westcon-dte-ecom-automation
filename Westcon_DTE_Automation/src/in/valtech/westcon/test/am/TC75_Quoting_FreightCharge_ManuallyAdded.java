package in.valtech.westcon.test.am;

import static in.valtech.util.PropertyFileReader.ObjRepoProp;
import static in.valtech.util.PropertyFileReader.TextProp;

import java.util.Date;

import org.apache.poi.hslf.model.textproperties.TextProp;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.custom.CustomFun;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

public class TC75_Quoting_FreightCharge_ManuallyAdded extends BaseTest{
	QuotingQuoteDetailpage amQuoteDetailsPage;
	float totalResellerPrice;
	float totalQuotePrice;
	float freightCharge;
	/**
	 * User to Submit quote in quoting by executing TC-11
	 * 
	 */

	@Test(description = "Step 1: User to submit quote  by executing TC11", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Executed ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Executed ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Executed ");
		// Execute TC-06 (without Opportunity)
		log.info("TC-06 Successfully Executed \n");
		Reporter.log("<p>TC-06 Successfully Executed ");	
		// Execute TC-08 
		log.info("TC-08 Successfully Executed \n");
		Reporter.log("<p>TC-08 Successfully Executed ");	
		
		log.info("Create Quote page should displayed");
		Reporter.log("<p>Create Quote page should displayed");	
	}
	
	/**
	 * Click on Pricing Tab tab
	 *  @throws Exception
	 */
	@Test(description = "Step 2: Click on Pricing tab and retrieve price values", priority =2)
	public void Step02_ClickonPricingTabAndRetrievePriceValues() throws Exception {
		
		amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);	
		String tResellerPrice;
	    String tQuotePrice;
	
	    //Click on Pricing Tab		
        amQuoteDetailsPage.clickPricingTab();
        CustomFun.waitObjectToLoad(driver, By.xpath(ObjRepoProp.getProperty("FreightCalculatorBtn_XPATH")), 60);
        
        if(locale.equals("US")){
        
        tQuotePrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValue_XPATH"))).getText().trim().replaceAll("[$,]", "");
        System.out.println("Total Quote Price_Before****"+tQuotePrice);
        
        tResellerPrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalResellerPriceValueInForm_XPATH"))).getText().trim().replaceAll("[$,]", "");
    	System.out.println("Total Reseller Price_Before****"+tResellerPrice);
    	}
        else{
        tQuotePrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValue_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", "");
        System.out.println("Total Quote Price_Before****"+tQuotePrice);
        
        tResellerPrice=driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalResellerPriceValueInForm_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", "");
        System.out.println("Total Reseller Price_Before****"+tResellerPrice);
        }
 
        totalResellerPrice=Float.parseFloat(tResellerPrice);
        totalQuotePrice=Float.parseFloat(tQuotePrice);
        freightCharge=(float)Float.parseFloat(TextProp.getProperty("FreightCharge"));
        System.out.println("Freight Charge applied****"+freightCharge);
        
        Assert.assertEquals(totalQuotePrice, totalResellerPrice,"TotalQuotePrice and TotalResellerPrice are not same, if freight charge not applied");
        
		log.info("Successfully Clicked on Pricing tab and retreived prices\n");
		Reporter.log("<p>Successfully Clicked on Pricing tab and retreived prices\n");

	}
	
	/**
	 * Click on Pencil icon for freight charge editor
	 *  @throws Exception
	 */
	@Test(description = "Step 03: Click on Pencil icon for freight charge editor", priority =3)
	public void Step03_ClickOnPencilIcon_FreightChargeEditor() throws Exception {
		
	   amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		 	
		
	   amQuoteDetailsPage.clickOnEstimatedFreightEditIcon(driver);
        
		log.info("Successfully Clicked on Pencil icon for freight charge editor\n");
		Reporter.log("<p>Successfully Clicked on Pencil icon for freight charge editor\n");

	}
	
	/**
	 * Enter Freight charge value and Click On Freight Calculator Btn
	 * 
	 *  @throws Exception
	 */
	@Test(description = "Step 04: Enter Freight charge value and Click On Freight Calculator Btn", priority =4)
	public void Step04_EnterFreightChargeValueAndClickOnFreightCalculatorBtn() throws Exception {
		
	    amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		 	
	    amQuoteDetailsPage.enterFreightChargeInput(""+freightCharge);
	    
	    GUIFunctions.keyPressEnter(driver);
	    //amQuoteDetailsPage.clickOnFreightCalculatorButton();
	    CustomFun.waitObjectToLoad(driver, By.xpath(ObjRepoProp.getProperty("EstimatedFreightEditIcon_XPATH")), 60);
        
		log.info("Successfully Entered Freight charge value and Clicked on Freight Calculator Btn\n");
		Reporter.log("<p>Successfully Entered Freight charge value and Clicked on Freight Calculator Btn\n");

	}
	
	/**
	 * Verify  Total Quote price
	 * 
	 *  @throws Exception
	 */
	@Test(description = "Step 05: Verify  Total Quote price", priority =5)
	public void Step05_VerifyTotalQuotePrice() throws Exception {
		
	     amQuoteDetailsPage =new QuotingQuoteDetailpage(driver);		 	
	 	float totalResellerPrice_act;
		float totalQuotePrice_act;
		float freightCharge_act;
		
		Thread.sleep(3000);
		    if(locale.equals("US")){
	        
			totalQuotePrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValue_XPATH"))).getText().trim().replaceAll("[$,]", ""));
	        System.out.println("Total Quote Price_After****"+totalQuotePrice_act);
	        
	        totalResellerPrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalResellerPriceValueInForm_XPATH"))).getText().trim().replaceAll("[$,]", ""));
	    	System.out.println("Total Reseller Price_After****"+totalResellerPrice_act);
	    	
	    	freightCharge_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("EstimatedfreightValue_XPATH"))).getText().trim().replaceAll("[$,]", ""));
	    	System.out.println("freight Charge_After"+freightCharge_act);
		    }
		
	        else{
	        totalQuotePrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalQuotePriceValue_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", ""));
	        System.out.println("Total Quote Price_After****"+totalQuotePrice_act);
	        
	        totalResellerPrice_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("bytotalResellerPriceValueInForm_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", ""));
	        System.out.println("Total Reseller Price_After****"+totalResellerPrice_act);
	        
	        freightCharge_act=Float.parseFloat(driver.findElement(By.xpath(ObjRepoProp.getProperty("EstimatedfreightValue_XPATH"))).getText().trim().replaceAll("[a-zA-Z,]", ""));
	    	System.out.println("freight Charge_After"+freightCharge_act);
	        }
		    
		    //verify Total Quote Price
		    Assert.assertEquals(totalQuotePrice_act, (float)(totalResellerPrice+freightCharge), "Total Quote Price mismatch");
		   //verify Total Reseller Price
		    Assert.assertEquals(totalResellerPrice_act, totalResellerPrice, "Total Reseller Price mismatch");
		    ////verify Freight Charge 
		    Assert.assertEquals(freightCharge_act, freightCharge, "Freight Charge mismatch");
        
		   log.info("Successfully Verified  Total Quote price\n");
		   Reporter.log("<p>Successfully Verified  Total Quote price\n");

	}
}
