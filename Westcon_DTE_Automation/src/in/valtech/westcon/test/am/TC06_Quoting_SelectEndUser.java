package in.valtech.westcon.test.am;

import static in.valtech.custom.CustomFun.userInfoDSDetails;

import org.testng.Reporter;
import org.testng.annotations.Test;

import in.valtech.config.BaseTest;
import in.valtech.uiFunctions.GUIFunctions;
import in.valtech.westcon.QuotingPages.QuotingQuoteDetailpage;

/**
 * Clicking on Pencil icon,Searching the end user & Selecting the user 
 */
public class TC06_Quoting_SelectEndUser extends BaseTest {

	//static QuotingQuoteDetailpage amQuotePage;
	static String ename = null;

	@Test(description = "Step 1: Call TC01 TC02 TC03", priority = 1)
	public void Step01_Call() throws Exception {

		// Execute TC-01
		log.info("FTC-01 Successfully Executed \n");
		Reporter.log("<p>TC-01 Successfully Eexecuted ");
		// Execute TC-02
		log.info("TC-02 Successfully Executed \n");
		Reporter.log("<p>TC-02 Successfully Eexecuted ");
		// Execute TC-03
		log.info("TC-03 Successfully Executed \n");
		Reporter.log("<p>TC-03 Successfully Eexecuted ");
	}

	/*@Test(description = "Step 2: Selecting end user ", priority = 2)
	public void Step02_ClickOnPencilIcon() throws Exception {
		// Selecting end user from end user modal 

		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 100);
		amQuotePage.AMClickOnPencilIcon();
		log.info("Successfully end user search modal is displayed");
		Reporter.log("<p>Successfully end user search modal is displayed");

	}*/

	@Test(description = "Step 2: Selecting end user ", priority = 2)
	public void Step02_ClickOnPencilIcon() throws Exception {
		// Selecting end user from end user modal 

		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		GUIFunctions.pageScrollDown(driver, 100);
		amQuotePage.AMClickEndUserSearchLink();
		
		log.info("Successfully end user search modal is displayed");
		Reporter.log("<p>Successfully end user search modal is displayed");
	}

	@Test(description = "Step 3: Enter End Username & Click on Search button", priority = 3)
	public void Step03_EnterUN_Search() throws Exception {
		// Search list should be displayed	
		System.out.println("End user=" +userInfoDSDetails.getEndUser());
		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		ename = userInfoDSDetails.getEndUser();	

		amQuotePage.AMendUserModal_enterEndUserName(ename);
		amQuotePage.AMendUserModal_ClickonSearch();
		amQuotePage.AMVerifyEndUserModalHeader();

		log.info("Successfully end user list is displayed");
		Reporter.log("<p>Successfully end user list is displayed");
	}


	@Test(description = "Step 4: Select End user from the list shown", priority = 4)
	public void Step04_SelectEndUser() throws Exception {
		// Create Quote page should be displayed with End user selected 
		QuotingQuoteDetailpage amQuotePage = new QuotingQuoteDetailpage(driver);
		amQuotePage.AMendUserModal_ClickonRadio();
		amQuotePage.AMendUserModal_ClickonSelectUser();
		log.info("Successfully end user is selected from modal");
		Reporter.log("<p>Successfully end user is selected from modal");

	}	

}

